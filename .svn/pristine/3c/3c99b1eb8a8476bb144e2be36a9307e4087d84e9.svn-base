---------------------------------------------- 
-- FUNCTION TO UPDATE JOB POSITION INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetJobPositionData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @jobPositionID long varchar,  
        IN @jobTitle long varchar,  IN @jobDescription long varchar default null,  IN @jobCode long varchar default null,  
        IN @jobGrade long varchar default null, IN @deleted char(1) default 'N')
returns numeric(10)
BEGIN
    declare @newJobPositionID numeric(10);
    set @newJobPositionID = -1;


    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return -1;
    end if;

    -- Can't have NULL description field
    if (@jobDescription is null) then
        set @jobDescription = @jobTitle
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this Job Position?
    if (@jobPositionID='-1') then
        //
        // Create Job Position
        //
        if not exists(select * from P_JOBPOSITION where trim(job_name)=trim(@jobTitle)) then
            begin
                select  coalesce(max(jobPosition_ID)+1,1) into @newJobPositionID from p_jobposition;
                set @jobPositionID = @newJobPositionID;
                insert into p_jobposition (company_id, jobposition_id, job_name, description, JOB_CODE, GRADE) 
                    values (@companyID, @jobPositionID, @jobTitle, '','','');
            end;
        end if;
    end if;

        
    -- Should we delete this entity?
    if (@deleted='Y') then
       delete from p_jobposition where company_id=@companyID and jobposition_id=@jobPositionID
    else
        //
        // Change VALUES in P_JOBPOSITION
        //
        begin
            if (@jobTitle is not null) then
                if not exists(select * from P_JOBPOSITION where trim(job_name)=trim(@jobTitle) and jobposition_id<>@jobPositionID) then
                    update p_jobposition set job_name=@jobTitle where company_id=@companyID and jobposition_id=@jobPositionID;
                end if;
            end if;
            if (@jobDescription is not null) then
              update p_jobposition set "DESCRIPTION"=@jobDescription where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
            if (@jobCode is not null) then
              update p_jobposition set JOB_CODE=@jobCode where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
            if (@jobGrade is not null) then
              update p_jobposition set GRADE=@jobGrade where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
        end;
    end if;

    return @jobPositionID; 
END;

commit;