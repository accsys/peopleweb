-------------------------------------------------
-- PROCEDURE TO ADVANCE PAYROLL TO THE NEXT PERIOD
-------------------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_AdvancePayrollPeriod ( IN @sessionid long VARCHAR, IN @companyID long varchar, IN @payrollID long varchar )
returns varchar(10)
BEGIN
    DECLARE @Command LONG VARCHAR;
	DECLARE @openPeriod_ID numeric(10);

    -- If this session doesn't exist, return empty-handed
    if not exists(select * from WEB_SESSION where session_id=@sessionid) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);
	
	-- Set the open period ID
	set @openPeriod_ID = fn_P_GetOpenPeriod(@companyID, @payrollID);

	-- If this is the last period do a tax year roll over
	if (@openPeriod_ID  = (select max(PAYROLLDETAIL_ID) from P_PAYROLLDETAIL where PAYROLL_ID = @payrollID)) then
		-- Close run
		call sp_P_SetCloseRun(@companyID, @payrollID, @openPeriod_ID , 1);
		-- Close period
		call sp_P_SetClosePeriod(@companyID, @payrollID, @openPeriod_ID);
		-- Tax Year Roll Over
		call fn_P_RollOverTaxYear(@payrollID);
		-- Open the first period
		call sp_P_SetOpenPeriod(@companyID, @payrollID, 1);
		-- Open basic run
		call sp_P_SetOpenRunType(@companyID, @payrollID, 1, 1, null, null );
		-- Mark all employees as To-Be-Processed
		update P_CALC_EMPLOYEELIST set RUN_STATUS=2 where company_id=@companyID and payroll_id=@payrollID and payrolldetail_id=1;
	else
	-- Normal roll over
		-- Close run
		call sp_P_SetCloseRun(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID), 1);
		-- Close period
		call sp_P_SetClosePeriod(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID));
		-- Open basic run
		call sp_P_SetOpenRunType(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID), 1, null, null );
		-- Mark all employees as To-Be-Processed
		update P_CALC_EMPLOYEELIST set RUN_STATUS=2 where company_id=@companyID and payroll_id=@payrollID and payrolldetail_id=fn_P_GetOpenPeriod(@companyID, @payrollID);
	end if;
    return 'OK';
END;
comment on procedure dba.fn_HTTP_AdvancePayrollPeriod is 'Advances the payroll to the next week/month/tax year';

commit;
