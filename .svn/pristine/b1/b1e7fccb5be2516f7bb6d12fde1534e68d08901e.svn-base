create or replace function "DBA"."fn_http_SaveEmployeeSaving"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @SAVING_ID numeric(10),in @START_DATE date,in @PREMIUM numeric(16,4),in @EMSAVING_ID numeric(10),in @DEPOSIT numeric(16,4),in @PREMIUM_TEMP numeric(16,4),in @PAYROLL_ID numeric(10),in @PAYROLLDETAIL_ID numeric(10),in @REFERENCE char(50))
returns numeric(10)
begin
  /*   
  <purpose>  
  fn_http_SaveEmployeeSaving saves the Employee''s Saving information details for PeopleWeb
  </purpose>  


  <history>
  Date          CR#     Programmer        Changes
  ----          ---     ----------        -------
  2014/09/18    14233   Liam Terblanche   Initial script
  2015/09/16	14942	Francois		  Interest did not calculate correctly
  </history>
  */
  -- No EMsaving_ID?  Create a new record  
  if (@EMsaving_ID is null) then
    if not exists(select * from pl_emaster_saving where company_id=@company_id and employee_id=@employee_id and saving_id=@saving_id) then
      insert into PL_EMASTER_saving (company_id, employee_id, saving_id, start_date, premium, deposit) values (@COMPANY_ID, @EMPLOYEE_ID, @saving_ID, now(), 0, 0);
    end if;
    select emsaving_id into @EMsaving_ID from pl_emaster_saving where company_id=@company_ID and employee_id=@employee_id and saving_id=@saving_id;
  end if;

  -- Now let's update it
        -- START_DATE
        if @START_DATE is not null then
            update PL_EMASTER_saving set
                START_DATE = @START_DATE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PREMIUM
        if @PREMIUM is not null then
            update PL_EMASTER_saving set
                PREMIUM = @PREMIUM
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PREMIUM_TEMP
        if @PREMIUM_TEMP is not null then
            update PL_EMASTER_saving set
                PREMIUM_TEMP = @PREMIUM_TEMP
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PAYROLL_ID
        if @PAYROLL_ID is not null then
            update PL_EMASTER_saving set
                PAYROLL_ID = @PAYROLL_ID
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
		-- PAYROLLDETAIL_ID
        if @PAYROLLDETAIL_ID is not null then
            update PL_EMASTER_saving set
                PAYROLLDETAIL_ID = @PAYROLLDETAIL_ID
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- DEPOSIT
        if @DEPOSIT is not null then
            update PL_EMASTER_saving set
                DEPOSIT = @DEPOSIT
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID;

        end if;
        -- "REFERENCE"
        if @REFERENCE is not null then
            update PL_EMASTER_saving set
                "REFERENCE" = @REFERENCE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
  
    commit;
  return(@EMsaving_ID);
end;