/**
 * Performs date validation for Discharge Dates
 *
 */
package za.co.accsys.web.helperclasses;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import za.co.accsys.web.beans.PageManager;

@FacesValidator("dateValidator_DischargeDate")
public class DateValidator_DischargeDate implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }

        //Leave the null handling of startDate to required="true"
        Object startDateValue = component.getAttributes().get("startDate");
        if (startDateValue == null) {
            return;
        }

        // Discharge date cannot be earlier than the Engagement date
        Date startDate = (Date) startDateValue;
        Date endDate = (Date) value;
        if (endDate.before(startDate)) {
            FacesContext.getCurrentInstance().addMessage(
                    "", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            PageManager.translate("hintDischargeDateBeforeEngagementDate"), PageManager.translate("hintDischargeDateBeforeEngagementDate")));
        }
    }
}
