// requires: "P_C_TAXCERTIFICATE_EMPLOYER_REJECTION.txt","P_C_TAXCERTIFICATE_EMPLOYER_WARNING.txt","P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION.txt","P_E_TAXCERTIFICATE_EMPLOYEE_WARNING.txt"
CREATE OR REPLACE FUNCTION "DBA"."fn_HTTP_GetTaxCertificateWarningsAndErrors"(in @RUN_ID numeric(10)) 
returns long varchar
/*  
<purpose>  
fn_HTTP_GetTaxCertificateWarningsAndErrors is called to create the electronic 
file output record for the errors and warnings when generating tax certificates
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/05/18          Francois        Original
</history>
*/
begin
  declare @ErrorsAndWarnings long varchar;
  declare @FileCode numeric(5);
  declare @FileId numeric(10);
  declare @EmployeeName char(100);
  declare @Description varchar(2500);
  declare @CompanyErrorsCount integer;
  declare @CompanyWarningsCount integer;
  declare @EmployeeErrorsCount integer;
  declare @EmployeeWarningsCount integer;
  declare @Debug_On bit;
  
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop 
  declare CompanyErrorsCursor dynamic scroll cursor for select FILE_CODE,ERROR_DESCRIPTION 
	from P_C_TAXCERTIFICATE_EMPLOYER_REJECTION where RUN_ID = @RUN_ID
    order by FILE_CODE asc; 
  declare CompanyWarningsCursor dynamic scroll cursor for select FILE_CODE,WARNING_DESCRIPTION 
	from P_C_TAXCERTIFICATE_EMPLOYER_WARNING where RUN_ID = @RUN_ID
    order by FILE_CODE asc; 
  declare EmployeeErrorsCursor dynamic scroll cursor for select FILE_ID,FILE_CODE,ERROR_DESCRIPTION 
    from P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION 
	where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID)
    order by FILE_CODE asc; 
  declare EmployeeWarningsCursor dynamic scroll cursor for select FILE_ID,FILE_CODE,WARNING_DESCRIPTION 
    from P_E_TAXCERTIFICATE_EMPLOYEE_WARNING 
	where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID)
    order by FILE_CODE asc;

  set @Debug_On = 0;	
  
  -- Count the Errors and Warnings
  set @CompanyErrorsCount = (select count(*) from P_C_TAXCERTIFICATE_EMPLOYER_REJECTION where RUN_ID = @RUN_ID);
  set @CompanyWarningsCount = (select count(*) from P_C_TAXCERTIFICATE_EMPLOYER_WARNING where RUN_ID = @RUN_ID);
  set @EmployeeErrorsCount = (select count(*) from P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION 
							where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID));
  set @EmployeeWarningsCount = (select count(*) from P_E_TAXCERTIFICATE_EMPLOYEE_WARNING 
							where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID));
							
  -- Add Run_ID for future use  -------------------------------------------------------------------------
  
  set @ErrorsAndWarnings = @RUN_ID;
  
  -- Company Errors  ------------------------------------------------------------------------------------
  if (@CompanyErrorsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Company Errors</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@CompanyErrorsCount||'</b> company errors found</br>';
	-- Loop through all the errors
	open CompanyErrorsCursor with hold;
      CompanyErrors_LOOP: loop
        -- Fetch next record
		fetch next CompanyErrorsCursor into @FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave CompanyErrors_LOOP
        end if;
		-- Add the Errors to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description;
	  end loop CompanyErrors_LOOP; -- Close the cursor
    close CompanyErrorsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No company errors found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Company Errors  
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Company Warnings  ------------------------------------------------------------------------------------
  if (@CompanyWarningsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings ||'<b>Company Warnings</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@CompanyWarningsCount||'</b> company warnings found</br>';
	-- Loop through all the warnings
	open CompanyWarningsCursor with hold;
      CompanyWarnings_LOOP: loop
        -- Fetch next record
		fetch next CompanyWarningsCursor into @FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave CompanyWarnings_LOOP
        end if;
		-- Add the Warnings to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description;
	  end loop CompanyWarnings_LOOP; -- Close the cursor
    close CompanyWarningsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No company warnings found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Company Warnings 
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Employee Errors  ------------------------------------------------------------------------------------
  if (@EmployeeErrorsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings ||'<b>Employee Errors</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@EmployeeErrorsCount||'</b> employee errors found</br>';
	-- Loop through all the errors
	open EmployeeErrorsCursor with hold;
      EmployeeErrors_LOOP: loop
        -- Fetch next record
		fetch next EmployeeErrorsCursor into @FileID,@FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave EmployeeErrors_LOOP
        end if;
		-- Set the employee name
		set @EmployeeName = (select SURNAME||', '||FIRSTNAME||' ['||COMPANY_EMPLOYEE_NUMBER||']' from E_MASTER where EMPLOYEE_ID = (select EMPLOYEE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where FILE_ID = @FileId));
		-- Add the Errors to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description || '    (  ' || @EmployeeName || '  )';
	  end loop EmployeeErrors_LOOP; -- Close the cursor
    close EmployeeErrorsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No employee errors found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Employee Errors  
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Employee Warnings  ------------------------------------------------------------------------------------
  if (@EmployeeWarningsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Employee Warnings</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@EmployeeWarningsCount||'</b> employee warnings found</br>';
	-- Loop through all the warnings
	open EmployeeWarningsCursor with hold;
      EmployeeWarnings_LOOP: loop
        -- Fetch next record
		fetch next EmployeeWarningsCursor into @FileId,@FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave EmployeeWarnings_LOOP
        end if;
		-- Set the employee name
		set @EmployeeName = (select SURNAME||', '||FIRSTNAME||' ['||COMPANY_EMPLOYEE_NUMBER||']' from E_MASTER where EMPLOYEE_ID = (select EMPLOYEE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where FILE_ID = @FileId));
		-- Add the Warnings to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description || '    (  ' || @EmployeeName || '  )';
	  end loop EmployeeWarnings_LOOP; -- Close the cursor
    close EmployeeWarningsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No employee warnings found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Employee Warnings  
  
  if @Debug_On = 1 then
    message(' ErrorsAndWarnings = '||@ErrorsAndWarnings) to console
  end if;
  return(@ErrorsAndWarnings)
end;

commit;

comment on procedure dba.fn_HTTP_GetTaxCertificateWarningsAndErrors is "<h2>Purpose</h2><br/>Prepares the Employee Warning Messages for the preview of the Tax Certificate electronic file creation.<br/><h2>Usage</h2>Payroll Tax Certificate Extract, South African electronic file.";
