----------------------------------------------
-- FUNCTION TO UPDATE COMPANY ACCOUNT INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCompanyAccountData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, 
            IN @accountType long VARCHAR default null, IN @accountName long varchar default null, 
            IN @accountDescription long varchar default null, IN @accountBank long varchar default null, IN @accountBranch long VARCHAR default null, 
            IN @accountBankCode long varchar default null, IN @accountBranchCode long varchar default null, IN @accountNumber long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- With the web interface, we ALWAYS only work with one account.  If more than one exist, only retain the first one.
    delete from c_bankdet where company_id=@companyID and account_id > 
        (select min(account_id) from c_bankdet where company_id=@companyID);

    -- Create an account if it doesn't already exist
    if not exists(select * from c_bankdet where company_id=@companyID ) then
        insert into c_bankdet (company_id, account_id) values (@companyID, 1);
    end if;

    -- Account Type
    if (@accountType is not null) then
        update c_bankdet set ACCOUNT_TYPE=@accountType
            where company_id=@companyID;
    end if;
    -- Account Name
    if (@accountName is not null) then
        update c_bankdet set ACCOUNT_NAME=@accountName
            where company_id=@companyID;
    end if;
    -- Account Description
    if (@accountDescription is not null) then
        update c_bankdet set ACCOUNT_DESCRIPTION=@accountDescription
            where company_id=@companyID;
    end if;
    -- Account Bank
    if (@accountBank is not null) then
        update c_bankdet set BANK=@accountBank
            where company_id=@companyID;
    end if;
    -- Account Branch
    if (@accountBranch is not null) then
        update c_bankdet set BRANCH=@accountBranch
            where company_id=@companyID;
    end if;
    -- Account Bank Code
    if (@accountBankCode is not null) then
        update c_bankdet set BANK_CODE=@accountBankCode
            where company_id=@companyID;
    end if;
    -- Account Branch Code
    if (@accountBranchCode is not null) then
        update c_bankdet set BRANCH_CODE=@accountBranchCode
            where company_id=@companyID;
    end if;
    -- Account Number
    if (@accountNumber is not null) then
        update c_bankdet set ACCOUNT_NUMBER=@accountNumber
            where company_id=@companyID;
    end if;
    commit;
    return('OK'); 
END;

commit;