----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE PACKAGE INFO
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeePackage" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @entityID long VARCHAR default null, IN @entityType long varchar default null, 
            IN @value long varchar default null, IN @balance long varchar default null,
            IN @payrollID long VARCHAR default null, IN @referenceNo long varchar default null,
            IN @rfi long varchar default 'N',
            IN @unlink long VARCHAR default 'no', IN @removeTempVal long VARCHAR default 'no' )
returns varchar(10)
BEGIN
  declare @RSLT numeric;
    truncate table P_CALC_LASTCALCED_EMPLOYEE;
    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Make sure the record exists
    if not exists(select * from p_e_basic where company_id=@companyID and employee_id=@employeeID) then
        insert into P_E_BASIC  (company_id, employee_id, pay_per1) values (@companyID, @employeeID, 0);
    end if;

    -- Is this a VARIABLE?
    if ((@entityType='entityType_GenericVariable') or
        (@entityType='entityType_Deduction') or
        (@entityType='entityType_Interim') or
        (@entityType='entityType_PackageComponent') or
        (@entityType='entityType_Earning') or
        (@entityType='entityType_Tax')) then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_pvariable where  company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
        else
            begin
                if (@value is null) then
                    call sp_P_SaveDefaultVariableValues(@companyID, @employeeID, null,null,@entityID,null,'',null,null,null);
                    --insert into PL_EMASTER_PVARIABLE (company_id, employee_id, variable_id, default_value)
                    --values (@companyID, @employeeID,@entityID,'')
                else
                    call sp_P_SaveDefaultVariableValues(@companyID, @employeeID, null,null,@entityID,@value,@value,null,null,null);
                end if;
                -- Reference Number
                if (@referenceNo is not null) then
                    update PL_EMASTER_PVARIABLE set "REFERENCE"=@referenceNo 
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
                -- Remove TEMP value
                if (@removeTempVal='yes') then
                    update PL_EMASTER_PVARIABLE set temp_value=null
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
                -- Set RFI
                update PL_EMASTER_PVARIABLE set RFI_YN='N'
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                if (@rfi='Y') then
                    update PL_EMASTER_PVARIABLE set RFI_YN='Y'
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
            end;
        end if;

    end if;
    -- Is this a LOAN?
    if (@entityType='entityType_Loan') then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_loan where  company_id=@companyID and employee_id=@employeeID and loan_id=@entityID
        else
            select fn_http_SaveEmployeeLoan(@companyID, @employeeID, @entityID, @balance, 
                        fn_P_GetPeriodStartDate(@payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID)),@value,null,null,@payrollID,fn_P_GetOpenPeriod(@companyID, @payrollID),@referenceNo ) into @RSLT;
            -- Remove TEMP value
            if (@removeTempVal='yes') then
                update pl_emaster_loan set premium_temp=null
                where company_id=@companyID and employee_id=@employeeID and loan_id=@entityID;
            end if;
        end if;
    end if;
    -- Is this a SAVING?
    if (@entityType='entityType_Saving') then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_saving where  company_id=@companyID and employee_id=@employeeID and saving_id=@entityID
        else
            select fn_http_SaveEmployeeSaving(@companyID, @employeeID, @entityID, 
                        fn_P_GetPeriodStartDate(@payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID)),@value,null,null,null,@payrollID,fn_P_GetOpenPeriod(@companyID, @payrollID),@referenceNo ) into @RSLT;
            -- Remove TEMP value
            if (@removeTempVal='yes') then
                update pl_emaster_saving set premium_temp=null
                where company_id=@companyID and employee_id=@employeeID and saving_id=@entityID;
            end if;
        end if;
    end if;

    -- We need to make sure the Payroll calculates this person as many times as it needs, without thinking it's the same calculation that simply got stuck
    truncate table P_CALC_LASTCALCED_EMPLOYEE;
    commit;
    return('OK'); 
END;

commit;