/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.event.SelectEvent;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.helperclasses.JSONRecord;
import za.co.accsys.web.helperclasses.ServerPrefs;

/**
 *
 * @author liam
 */
public final class JobPositionManager {

    private JobPosition selectedJobPosition;
    private JobPosition newJobPosition;
    private String newStringJobPosition;
    private final SessionUser sessionUser;
    private ArrayList<JobPosition> jobPositions;

    public JobPositionManager(SessionUser sessionUser) {
        this.sessionUser = sessionUser;

        loadJobPositions();

        newJobPosition = new JobPosition(sessionUser);
    }

    /**
     * Runs a web service call to retrieve the list of Job Positions
     *
     * @return
     */
    public boolean loadJobPositions() {
        jobPositions = new ArrayList<>();
        boolean rslt = true;

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetJobTitleList, listToSend);

        // What did we get back?
        // The result is a JSON Array listing job titles
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the cost centre
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            JobPosition jobPosition = new JobPosition(sessionUser);
            jobPosition.setDeleted(false);

            // Step through each record, and construct an EMPLOYEE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("jobPositionID")) {
                    jobPosition.setJobPositionID(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("jobTitle")) {
                    jobPosition.setJobTitle(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("jobDescription")) {
                    jobPosition.setJobDescription(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("jobCode")) {
                    jobPosition.setJobCode(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("jobGrade")) {
                    jobPosition.setJobGrade(nvp.getValue());
                }
            }

            jobPositions.add(jobPosition);
        }

        return rslt;
    }

    public JobPosition getSelectedJobPosition() {
        return selectedJobPosition;
    }

    public void setSelectedJobPosition(JobPosition selectedJobPosition) {
        this.selectedJobPosition = selectedJobPosition;
    }

    /**
     * All the attributes for this payEntity already exists in 'newPayEntity'
     *
     * @param actionEvent
     */
    public void createNewJobPosition(ActionEvent actionEvent) {
        // Add current 'newEntity' into the ArrayList
        saveJobPosition(newJobPosition);
        jobPositions.add(newJobPosition);
        // Mark it as the current selected entity
        selectedJobPosition = newJobPosition;

        // Create a new entity for future use
        newJobPosition = new JobPosition(sessionUser);
    }

    public void onRowSelect(SelectEvent event) {
        this.selectedJobPosition = ((JobPosition) event.getObject());
    }

    public JobPosition getNewJobPosition() {
        return newJobPosition;
    }

    public boolean titleExists(String currentJobTitle) {
        for (JobPosition pos : jobPositions) {
            if (pos.getJobTitle().trim().equalsIgnoreCase(currentJobTitle)) {
                return true;
            }
        }
        return false;
    }

    public void setNewJobPosition(JobPosition newJobPosition) {
        this.newJobPosition = newJobPosition;
    }

    public String getNewStringJobPosition() {
        return newStringJobPosition;
    }

    public void setNewStringJobPosition(String newStringJobPosition) {
        this.newStringJobPosition = newStringJobPosition;
        this.newJobPosition.setJobTitle(newStringJobPosition);
    }

    public ArrayList<JobPosition> getJobPositions() {
        return jobPositions;
    }

    public void setJobPositions(ArrayList<JobPosition> jobPositions) {
        this.jobPositions = jobPositions;
    }

    /**
     * Returns a String[] of Job Positions
     *
     * @return
     */
    public String[] getJobTitles() {
        String[] rslt = new String[jobPositions.size()];
        int i = 0;
        for (JobPosition jobPosition : jobPositions) {
            rslt[i++] = jobPosition.getJobTitle();
        }
        return rslt;
    }

    public void deleteSelectedJobPosition(ActionEvent actionEvent) {
        selectedJobPosition.setDeleted(true);
        saveJobPosition(selectedJobPosition);
        jobPositions.remove(selectedJobPosition);
        selectedJobPosition = null;
    }

    /**
     * Runs a web service call to update all the information for all the
     * loans/savings/earnings/deductions/etc
     *
     * @param jobPosition
     * @return
     */
    public boolean saveJobPosition(JobPosition jobPosition) {

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair("jobPositionID", jobPosition.getJobPositionID()));
        listToSend.add(new BasicNameValuePair("jobTitle", jobPosition.getJobTitle()));
        listToSend.add(new BasicNameValuePair("jobDescription", jobPosition.getJobDescription()));
        listToSend.add(new BasicNameValuePair("jobCode", jobPosition.getJobCode()));
        listToSend.add(new BasicNameValuePair("jobGrade", jobPosition.getJobGrade()));

        if (jobPosition.isDeleted()) {
            listToSend.add(new BasicNameValuePair("deleted", "Y"));
        } else {
            listToSend.add(new BasicNameValuePair("deleted", "N"));
        }

        // Call the SetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetJobPositionData, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {
            if (nvp.getName().equalsIgnoreCase("jobPositionID")) {
                jobPosition.setJobPositionID(nvp.getValue());
            }
        }

        return true;
    }

}
