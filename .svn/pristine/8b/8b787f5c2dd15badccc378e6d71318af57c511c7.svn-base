CREATE OR REPLACE FUNCTION "DBA"."fn_P_TC_SouthAfrica_HeaderOutputRecord"(in @PAYROLL_ID numeric(10), in @RUN_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_TC_SouthAfrica_HeaderOutputRecord is called from the South African tax 
DLL to create the electronic file output record for the company Heading 
according to the formatting requirements specified by SARS.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/07/06  14705   Francois        Move Tax Certificate extract creation to a function so that PeopleWeb can use it.
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @FieldType char(10);
  declare @TaxNumber char(21);
  declare @Counter integer;
  declare @IsValid bit;
  declare @IsStart bit;
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  declare DetailCursor dynamic scroll cursor for select R.FILE_CODE, R.VALUE, C.FIELD_TYPE 
    from P_C_TAXCERTIFICATE_EMPLOYER_HEADER as R join P_SARS_CODE as C on R.FILE_CODE = C.SARS_CODE 
    where R.RUN_ID = @RUN_ID and COUNTRY_ID = 2 and C.RECORD_TYPE = 'Header'
    order by C.FILE_SEQUENCE,R.FILE_CODE;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_HeaderOutputRecord ... ') to console
  end if;
  
  set @IsStart = 1;
  -- 1 Loop over the rows of the query
  open DetailCursor;
    DetailCode_LOOP: loop
      --
      --
      fetch next DetailCursor into @FileCode,@CodeValue,@FieldType;
      --
      if sqlstate = err_notfound then
        leave DetailCode_LOOP
      end if;
      -- 
      if length(@CodeValue) > 0 then
        if @FieldType like 'A%' or @FieldType like 'F%' then
          set @CodeValue = '"'||@CodeValue||'"'
        end if;
        if @Debug_On = 1 then
          message('FileRecord = '||@FileRecord ||' FileCode = '||@FileCode||' CodeValue = '||@CodeValue) to console
        end if;
        -- 
        if @FileCode <> 9999 then
			if @IsStart = 1 then 
				set @FileRecord = @FileCode ||','|| @CodeValue;
				set @IsStart = 0;
			else
				set @FileRecord = @FileRecord ||','|| @FileCode ||','|| @CodeValue;
			end if;
        end if;
      end if;
	  -- Add the Tax, SDL and UIF numbers
	  if @FileCode = 2015 then
		set @TaxNumber = (select TAX_NUMBER from P_PAYROLL where PAYROLL_ID = @PAYROLL_ID);
		set @FileRecord = @FileRecord || ',2020,' || @TaxNumber || ',2022,"L' || substring(@TaxNumber,2,9) || '",2024,"U' || substring(@TaxNumber,2,9) || '"';
      end if;
      set @FileCode = null;
      set @CodeValue = null;
    end loop DetailCode_LOOP;
  close DetailCursor;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_HeaderOutputRecord: DetailCursor done ... ') to console
  end if;
  -- 2 Add the end code
  set @FileRecord = @FileRecord || ',9999';
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;