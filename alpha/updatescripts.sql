-------------------------------------------------------
-- Automatically calculate employees
-------------------------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_autocalc')) then
      drop event "DBA"."evt_http_autocalc";

end if;
	  
    CREATE EVENT "DBA"."evt_http_autocalc"
                  SCHEDULE "sch_http_autocalc" BETWEEN '00:01' AND '23:59' EVERY 1 SECONDS
                  HANDLER
      BEGIN
        -- Check, at very short intervals, if anything changed that warrants a re-calculation and then triggers that calculation
        if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
          select sp_P_ResetEmployeeCalcStatus(company_id, employee_id, payroll_id, payrolldetail_id, runtype_id, 0),
            sp_P_CALC_Initiate_PerEmployee(company_id, employee_id, payroll_id, payrolldetail_id, runtype_id, 0) into #B
          from p_calc_employeelist
            where payrolldetail_id=fn_P_GetOpenPeriod(company_id, payroll_id)
            and run_status=2 and runtype_id=1; commit;
        end if;
      END;

COMMENT ON EVENT "DBA"."evt_http_autocalc" IS 'Ensures staff are always calculated (Basic Run only) by automatically adding them into the calculation queue if they are flagged to be calculated.';
-----------------------------------------------------------
-- Automatically make the Administrator the Contact person
-----------------------------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_autoCompanyContact')) then
      drop event "DBA"."evt_http_autoCompanyContact";
end if;

 CREATE EVENT "DBA"."evt_http_autoCompanyContact"
          SCHEDULE "sch_http_autoCompanyContact" BETWEEN '00:01' AND '23:59' EVERY 10 MINUTES
 HANDLER
 BEGIN 
   -- Check, on a semi-regular basis, if the C_CONTACT information is the same as that of the system administrator in PeopleWeb
   if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
     if exists(select * from dba.e_master e join dba.al_emaster_agroup al join e_contact c
        where fn_E_EmployeeActiveOnDate_YN(e.company_id, e.employee_id, now(*))='Y') then    
        -- Make sure we're using the Admin as Contact person                    
        delete from c_contact;
        insert into c_contact (company_id, contact_id, name, position, tel_number, e_mail) (
             select e.company_id, 1, e.FIRSTNAME||' '||e.SURNAME, j.job_name, c.tel_work, c.e_mail from dba.e_master e join dba.al_emaster_agroup al join dba.PL_EMASTER_PJOBPOSITION pl join dba.P_JOBPOSITION j join e_contact c)

     end if;
    end if;
 END;
 COMMENT ON EVENT "DBA"."evt_http_autoCompanyContact" IS 'Makes the PeopleWeb administrator the contact person for the company.';

-------------------------------------------------------
-- Automatically update report tables for calculated employees
-------------------------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_autoupdatereports')) then
      drop event "DBA"."evt_http_autoupdatereports";
end if;	  
	
	CREATE EVENT "DBA"."evt_http_autoupdatereports"
	SCHEDULE "sch_http_autoupdatereports" BETWEEN '00:01' AND '23:59' EVERY 2 SECONDS
	HANDLER
	BEGIN
	  if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
            select sp_P_REPORT_PrepareTables_Preview(company_id, employee_id, payroll_id, payrolldetail_id, 0) into #A
		from p_calc_employeelist pce 
		where payrolldetail_id = fn_P_GetOpenPeriod(company_id, payroll_id) 
			and runtype_id=1 
			and run_status=3 
			and employee_id not in (
				select employee_id from p_report_data 
					where company_id=pce.company_id 
					and payroll_id=pce.payroll_id 
					and payrolldetail_id=pce.payrolldetail_id 
					and runtype_id=pce.runtype_id);
         end if;


	END;
	COMMENT ON EVENT "DBA"."evt_http_autoupdatereports" IS 'Ensures that calculated staff get their reporting tables updated.';

-------------------------------------
-- EVENT TO REMOVE EXPIRED SESSIONS
-------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_sessionexpiry')) then
    drop event "DBA"."evt_http_sessionexpiry";
end if;	
    
CREATE EVENT "DBA"."evt_http_sessionexpiry"
	SCHEDULE "sch_session_expiry" BETWEEN '00:00' AND '23:59' EVERY 2 MINUTES
HANDLER
BEGIN
		if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
	delete from WEB_SESSION where datediff(minute, update_time, now())>timeout_minutes;
		end if;
END;

COMMENT ON EVENT "DBA"."evt_http_sessionexpiry" IS 'Remove unused http sessions';
-------------------------------------------------
-- PROCEDURE TO ADVANCE PAYROLL TO THE NEXT PERIOD
-------------------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_AdvancePayrollPeriod ( IN @sessionid long VARCHAR, IN @companyID long varchar, IN @payrollID long varchar )
returns varchar(10)
BEGIN
    DECLARE @Command LONG VARCHAR;
	DECLARE @openPeriod_ID numeric(10);

    -- If this session doesn't exist, return empty-handed
    if not exists(select * from WEB_SESSION where session_id=@sessionid) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);
	
	-- Set the open period ID
	set @openPeriod_ID = fn_P_GetOpenPeriod(@companyID, @payrollID);

	-- If this is the last period do a tax year roll over
	if (@openPeriod_ID  = (select max(PAYROLLDETAIL_ID) from P_PAYROLLDETAIL where PAYROLL_ID = @payrollID)) then
		-- Close run
		call sp_P_SetCloseRun(@companyID, @payrollID, @openPeriod_ID , 1);
		-- Close period
		call sp_P_SetClosePeriod(@companyID, @payrollID, @openPeriod_ID);
		-- Tax Year Roll Over
		call fn_P_RollOverTaxYear(@payrollID);
		-- Open the first period
		call sp_P_SetOpenPeriod(@companyID, @payrollID, 1);
		-- Open basic run
		call sp_P_SetOpenRunType(@companyID, @payrollID, 1, 1, null, null );
		-- Mark all employees as To-Be-Processed
		update P_CALC_EMPLOYEELIST set RUN_STATUS=2 where company_id=@companyID and payroll_id=@payrollID and payrolldetail_id=1;
	else
	-- Normal roll over
		-- Close run
		call sp_P_SetCloseRun(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID), 1);
		-- Close period
		call sp_P_SetClosePeriod(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID));
		-- Open basic run
		call sp_P_SetOpenRunType(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID), 1, null, null );
		-- Mark all employees as To-Be-Processed
		update P_CALC_EMPLOYEELIST set RUN_STATUS=2 where company_id=@companyID and payroll_id=@payrollID and payrolldetail_id=fn_P_GetOpenPeriod(@companyID, @payrollID);
	end if;
    return 'OK';
END;
comment on procedure dba.fn_HTTP_AdvancePayrollPeriod is 'Advances the payroll to the next week/month/tax year';

commit;
------------------------------------------------------
-- FUNCTION TO FILTER WHO THIS USER HAS ACCESS TO
------------------------------------------------------
CREATE OR REPLACE FUNCTION "DBA"."fn_http_AllowEmployeeView"(in CompanyID numeric(10),in EmployeeID numeric(10),in sessionID long varchar)
returns char(1)
begin
  /*  
  <purpose>  
  Similar to fn_E_AllowEmployeeView, this function is used to filter the employees accessible through the web services, given a certain session id
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2014/07/31            Liam Terblanche Initial
  </history>
  */
  declare GroupID numeric(10);
  declare CostID numeric(10);
  declare PayrollID numeric(10);
  declare PayrollSecurityDisabled char(3);

  -- IMPORTANT: For now, allow access to EVERYONE.
  -- This is a temporary override to make testing a bit easier :-)  [Liam]
  return ('Y');
  --
  --  Get the group ID for the current http session.  This could slow down the 
  --  vw_E_MASTER view because we call this procedure per employee!
  --
    select min(GROUP_id) into GroupID
    from AL_EMASTER_AGROUP NATURAL JOIN WEB_SESSION 
    where WEB_SESSION.session_id=sessionID;

  if GroupID = -1 then
    return('N')
  end if;
  --
  -- Get the cost ID of the cost centre this employee belongs to
  --
  select COST_ID into CostID
    from E_MASTER where
    COMPANY_ID = CompanyID and
    EMPLOYEE_ID = EmployeeID;
  if CostID is null then
    return('Y')
  end if;
  --
  -- Check if the payroll security is disabled.
  --
  select first(DATA) into PayrollSecurityDisabled from A_SETUP where
    ITEM = 'DisablePayrollSecurity';
  if PayrollSecurityDisabled = 'Yes' then
    --
    -- Only check the Cost Centre and ignore payroll security 
    --
    return(fn_A_CheckCostCentre(GroupID,CompanyID,CostID))
  end if;
  --
  -- Get the payroll ID of the payroll this employee belongs to.
  --
  select PAYROLL_ID into PayrollID
    from PL_PAYDEF_EMASTER where
    COMPANY_ID = CompanyID and
    EMPLOYEE_ID = EmployeeID;
  --
  --
  --
  if PayrollID is null then
    return('Y')
  end if;
  --
  -- Check the cost centre, together with payroll.
  --
  if CostID >= 0 and PayrollID >= 0 then
    if fn_A_CheckCostCentreAndPayroll(GroupID,CompanyID,CostID,PayrollID) = 'N' then
      return('N')
    end if
  end if;
  return('Y')
end;

commit;
if exists (select * from sysprocedure where upper(proc_name) = upper('fn_HTTP_CanCloseTaxYear')) then
  drop procedure DBA.fn_HTTP_CanCloseTaxYear;
end if;

create function
"DBA"."fn_HTTP_CanCloseTaxYear" ( IN @sessionid long VARCHAR, IN @companyID long varchar, IN @payrollID long varchar )
returns varchar(250)
/*
<purpose>
fn_HTTP_CanCloseTaxYear is called by the Payroll Manager to check that all 
requirements have been met before performing a tax-year-end process, that is:
1. No other payroll may be busy closing the tax year
2. All companies linked to the payroll must be closed in the last period
3. No payroll calculation may be in progress
</purpose>
<history>
Date          CR#     Programmer      Changes
----          ---     ----------      -------
2015/03/09	 14693    Francois        Convert to PeopleWeb Script from fn_P_CanCloseTaxYear
</history>
*/
BEGIN
  declare @ResultMessage varchar(250);
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop
  declare @Counter integer; 
  declare @Company_Id numeric(10); 
  declare @ClosingPayroll_Id numeric(10); 
  declare @LastPeriod_ID numeric(10);
  declare @CurrentPeriod_ID numeric(10);
  declare @Company_Name char(100);
  declare @Payroll_Name char(50);
  declare CompanyCursor no scroll cursor for 
    select distinct COMPANY_ID from PL_PAYDEF_EMASTER with (readuncommitted) where PAYROLL_ID = @payrollID;
  declare PayrollCursor no scroll cursor for 
    select distinct PAYROLL_ID from P_CLOSEYEAR_STATUS with (readuncommitted) where PAYROLL_ID <> @payrollID and (PAYROLL_ID <> 0);
  set @Counter = 0;
  
  -- If this session doesn't exist, return empty-handed
  if not exists(select * from WEB_SESSION where session_id=@sessionid) then
      return 'NO SESSION';
  end if;

  -- Touch this session
  call fn_HTTP_TouchSession(@sessionid);
	
  --
  -- 1. Payrolls in the process of closing the tax year.
  --
  set @ResultMessage = 'The following payrolls are currently busy closing their tax years:\x0a';
  open PayrollCursor; 
    -- Loop over the rows of the query
    Payroll_LOOP: loop
      --        
      fetch next PayrollCursor into @ClosingPayroll_Id;
      if sqlstate = err_notfound then
        leave Payroll_LOOP
      end if;
      set @Counter = @Counter + 1; 
      select Name into @Payroll_Name from P_PAYROLL with (readuncommitted) where Payroll_Id = @ClosingPayroll_Id;
      set @ResultMessage = @ResultMessage || '\x0a' ||@Payroll_Name;
    end loop Payroll_LOOP;
  -- Close the cursor
  close PayrollCursor;
  if @Counter > 0 then
    set @ResultMessage = @ResultMessage || '\x0a\x0aPlease wait for them to complete before closing this payroll.';
    return @ResultMessage
  end if;
  --
  -- 2. Outstanding Companies
  --
  select first Payrolldetail_ID into @LastPeriod_ID from P_Payrolldetail with (readuncommitted) where Payroll_Id = @payrollID
    order by EndDate desc; 
  set @ResultMessage = 'Outstanding Companies Warning: The following companies are not in the last period yet:';
  open CompanyCursor; 
    -- Loop over the rows of the query
    Company_LOOP: loop
      --        
      fetch next CompanyCursor into @Company_Id;
      if sqlstate = err_notfound then
        leave Company_LOOP
      end if;
      select max(Payrolldetail_Id) into @CurrentPeriod_ID from P_Calc_Master with (readuncommitted) where Company_Id = @Company_Id and 
        Payroll_Id = @payrollID;
      if @CurrentPeriod_ID < @LastPeriod_ID then
        -- Check that the company has employees who were active during the current tax year
        if (select count(*) from PL_Paydef_EMaster where company_id = @Company_Id and payroll_id = @payrollID and
          fn_P_EmployeeStillEngaged_YN(@Company_Id,PL_Paydef_EMaster.EMPLOYEE_ID,@payrollID,@CurrentPeriod_ID,
            fn_P_GetOpenRunType(@Company_Id,@payrollID,@CurrentPeriod_ID)) = 'Y') > 0 then
          set @Counter = @Counter + 1; 
          select Name into @Company_Name from C_Master with (readuncommitted) where Company_Id = @Company_Id;
          set @ResultMessage = @ResultMessage || '\x0a' ||@Company_Name;
        end if;
      end if;
    end loop Company_LOOP;
  -- Close the cursor
  close CompanyCursor;
  if @Counter > 0 then
    return @ResultMessage
  end if;
  --
  -- 3. Open Companies
  --
  set @ResultMessage = 'Open Companies Warning: The following companies are still open in the last period:';
  open CompanyCursor; 
    -- Loop over the rows of the query
    Company_LOOP: loop
      --        
      fetch next CompanyCursor into @Company_Id;
      if sqlstate = err_notfound then
        leave Company_LOOP
      end if;
      if (Select Run_Status from P_Calc_Master with (readuncommitted) where Company_Id = @Company_Id and 
        Payroll_Id = @payrollID and Payrolldetail_Id = @LastPeriod_ID) <> 3  then
        -- Check that the company has employees who were active during the current tax year
        if (select count(*) from PL_Paydef_EMaster where company_id = @Company_Id and payroll_id = @payrollID and
          fn_E_EmployeeActiveInRange_YN(@Company_Id,PL_Paydef_EMaster.EMPLOYEE_ID,
            (select Tax_Year_Start from P_Payroll where payroll_id = @payrollID),
            (select Tax_Year_End from P_Payroll where payroll_id = @payrollID)) = 'Y') > 0 then
          set @Counter = @Counter + 1; 
          select Name into @Company_Name from C_Master with (readuncommitted) where Company_Id = @Company_Id;
          set @ResultMessage = @ResultMessage || '\x0a' ||@Company_Name;
        end if;
      end if;
    end loop Company_LOOP;
  -- Close the cursor
  close CompanyCursor;
  
  if @Counter > 0 then
    return @ResultMessage
  end if;
  
  if exists (select * from p_calc_queue with (readuncommitted) ) then
    set @ResultMessage = 'Payrolls Calculating Warning: \x0a The tax year cannot be closed while a calculation is in progress';
    return @ResultMessage
  end if;
  
  -- CR 12388 - don't start closing in the can-close call... do it in fn_P_RollOverTaxYear call...
  -- insert into P_CLOSEYEAR_STATUS (Payroll_Id, Status) values (@payrollID,'CLOSING');
  
  set @ResultMessage = 'OK';
    
  return (@ResultMessage)  
END;

commit;
// requires: "P_C_TAXCERTIFICATE_EMPLOYER_REJECTION.txt","P_C_TAXCERTIFICATE_EMPLOYER_WARNING.txt","P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION.txt","P_E_TAXCERTIFICATE_EMPLOYEE_WARNING.txt"
CREATE OR REPLACE FUNCTION "DBA"."fn_HTTP_GetTaxCertificateWarningsAndErrors"(in @RUN_ID numeric(10)) 
returns long varchar
/*  
<purpose>  
fn_HTTP_GetTaxCertificateWarningsAndErrors is called to create the electronic 
file output record for the errors and warnings when generating tax certificates
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/05/18          Francois        Original
</history>
*/
begin
  declare @ErrorsAndWarnings long varchar;
  declare @FileCode numeric(5);
  declare @FileId numeric(10);
  declare @EmployeeName char(100);
  declare @Description varchar(2500);
  declare @CompanyErrorsCount integer;
  declare @CompanyWarningsCount integer;
  declare @EmployeeErrorsCount integer;
  declare @EmployeeWarningsCount integer;
  declare @Debug_On bit;
  
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop 
  declare CompanyErrorsCursor dynamic scroll cursor for select FILE_CODE,ERROR_DESCRIPTION 
	from P_C_TAXCERTIFICATE_EMPLOYER_REJECTION where RUN_ID = @RUN_ID
    order by FILE_CODE asc; 
  declare CompanyWarningsCursor dynamic scroll cursor for select FILE_CODE,WARNING_DESCRIPTION 
	from P_C_TAXCERTIFICATE_EMPLOYER_WARNING where RUN_ID = @RUN_ID
    order by FILE_CODE asc; 
  declare EmployeeErrorsCursor dynamic scroll cursor for select FILE_ID,FILE_CODE,ERROR_DESCRIPTION 
    from P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION 
	where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID)
    order by FILE_CODE asc; 
  declare EmployeeWarningsCursor dynamic scroll cursor for select FILE_ID,FILE_CODE,WARNING_DESCRIPTION 
    from P_E_TAXCERTIFICATE_EMPLOYEE_WARNING 
	where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID)
    order by FILE_CODE asc;

  set @Debug_On = 0;	
  
  -- Count the Errors and Warnings
  set @CompanyErrorsCount = (select count(*) from P_C_TAXCERTIFICATE_EMPLOYER_REJECTION where RUN_ID = @RUN_ID);
  set @CompanyWarningsCount = (select count(*) from P_C_TAXCERTIFICATE_EMPLOYER_WARNING where RUN_ID = @RUN_ID);
  set @EmployeeErrorsCount = (select count(*) from P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION 
							where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID));
  set @EmployeeWarningsCount = (select count(*) from P_E_TAXCERTIFICATE_EMPLOYEE_WARNING 
							where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID));
							
  -- Add Run_ID for future use  -------------------------------------------------------------------------
  
  set @ErrorsAndWarnings = @RUN_ID;
  
  -- Company Errors  ------------------------------------------------------------------------------------
  if (@CompanyErrorsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Company Errors</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@CompanyErrorsCount||'</b> company errors found</br>';
	-- Loop through all the errors
	open CompanyErrorsCursor with hold;
      CompanyErrors_LOOP: loop
        -- Fetch next record
		fetch next CompanyErrorsCursor into @FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave CompanyErrors_LOOP
        end if;
		-- Add the Errors to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description;
	  end loop CompanyErrors_LOOP; -- Close the cursor
    close CompanyErrorsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No company errors found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Company Errors  
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Company Warnings  ------------------------------------------------------------------------------------
  if (@CompanyWarningsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings ||'<b>Company Warnings</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@CompanyWarningsCount||'</b> company warnings found</br>';
	-- Loop through all the warnings
	open CompanyWarningsCursor with hold;
      CompanyWarnings_LOOP: loop
        -- Fetch next record
		fetch next CompanyWarningsCursor into @FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave CompanyWarnings_LOOP
        end if;
		-- Add the Warnings to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description;
	  end loop CompanyWarnings_LOOP; -- Close the cursor
    close CompanyWarningsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No company warnings found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Company Warnings 
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Employee Errors  ------------------------------------------------------------------------------------
  if (@EmployeeErrorsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings ||'<b>Employee Errors</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@EmployeeErrorsCount||'</b> employee errors found</br>';
	-- Loop through all the errors
	open EmployeeErrorsCursor with hold;
      EmployeeErrors_LOOP: loop
        -- Fetch next record
		fetch next EmployeeErrorsCursor into @FileID,@FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave EmployeeErrors_LOOP
        end if;
		-- Set the employee name
		set @EmployeeName = (select SURNAME||', '||FIRSTNAME||' ['||COMPANY_EMPLOYEE_NUMBER||']' from E_MASTER where EMPLOYEE_ID = (select EMPLOYEE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where FILE_ID = @FileId));
		-- Add the Errors to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description || '    (  ' || @EmployeeName || '  )';
	  end loop EmployeeErrors_LOOP; -- Close the cursor
    close EmployeeErrorsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No employee errors found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Employee Errors  
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Employee Warnings  ------------------------------------------------------------------------------------
  if (@EmployeeWarningsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Employee Warnings</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@EmployeeWarningsCount||'</b> employee warnings found</br>';
	-- Loop through all the warnings
	open EmployeeWarningsCursor with hold;
      EmployeeWarnings_LOOP: loop
        -- Fetch next record
		fetch next EmployeeWarningsCursor into @FileId,@FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave EmployeeWarnings_LOOP
        end if;
		-- Set the employee name
		set @EmployeeName = (select SURNAME||', '||FIRSTNAME||' ['||COMPANY_EMPLOYEE_NUMBER||']' from E_MASTER where EMPLOYEE_ID = (select EMPLOYEE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where FILE_ID = @FileId));
		-- Add the Warnings to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description || '    (  ' || @EmployeeName || '  )';
	  end loop EmployeeWarnings_LOOP; -- Close the cursor
    close EmployeeWarningsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No employee warnings found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Employee Warnings  
  
  if @Debug_On = 1 then
    message(' ErrorsAndWarnings = '||@ErrorsAndWarnings) to console
  end if;
  return(@ErrorsAndWarnings)
end;

commit;

comment on procedure dba.fn_HTTP_GetTaxCertificateWarningsAndErrors is "<h2>Purpose</h2><br/>Prepares the Employee Warning Messages for the preview of the Tax Certificate electronic file creation.<br/><h2>Usage</h2>Payroll Tax Certificate Extract, South African electronic file.";
---------------------------------------
-- PROCEDURE TO RETURN SELECTED COLUMNS
---------------------------------------
CREATE OR REPLACE procedure dba.fn_HTTP_GETVALUE ( IN @sessionid  long VARCHAR, IN @tablename long varchar, IN @colname long varchar, IN @filter long varchar default null, IN @order long varchar default null ) 
BEGIN
    DECLARE Command LONG VARCHAR;


    -- If this session doesn't exist, return empty-handed
    if not exists(select * from WEB_SESSION where session_id=@sessionid) then
        return '';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    SET Command = 'SELECT ' || @colname || ' FROM ' || @tablename;
    message(command) type info to console;
    IF ISNULL( @filter,'') <> '' THEN
         SET Command = Command || ' WHERE ' || @filter;
    END IF;
    IF ISNULL( @order,'') <> '' THEN
         SET Command = Command || ' ORDER BY ' || @order;
    END IF;

    EXECUTE IMMEDIATE WITH RESULT SET ON Command;
END;
commit;---------------------------------------------
-- FUNCTION TO CHECK IF A PERSON IS LOGGED IN
---------------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_ISSESSIONLIVE ( IN @sessionid  long VARCHAR ) 
returns varchar(10)
BEGIN
      
     -- If this session already exists, clear it
    if exists(select * from WEB_SESSION where session_id=@sessionid) then
      return 'OK';
    else
      return 'NO SESSION';
    end if;

END; -------------------------------------
-- FUNCTION TO LOG A PERSON IN
-------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_LOGIN ( IN @web_login long varchar, IN @web_pwd long varchar, IN @old_sessionid  long VARCHAR ) 
returns long varchar
BEGIN
    DECLARE @session_id long VARCHAR;
    DECLARE @company_id numeric(10);
    DECLARE @employee_id numeric(10);
    
    // If this session already exists, clear it
    delete from WEB_SESSION where session_id=@old_sessionid;

    // Who is trying to log in?
    select company_id, employee_id into @company_id, @employee_id from 
        DBA.al_emaster_agroup 
        where 
            upper(trim(LOGIN_NAME_DECRYPTED))=upper(trim(@web_login)) and 
            upper(trim(fn_S_DecryptString(PASSWORD)))=upper(trim(@web_pwd)); 

    // If no matching login/pwd, return empty-handed
    if @@rowcount=0 then return ''; end if;    
 
    // If this person is logged in through another session, clear it
    delete from WEB_SESSION where company_id=@company_id and employee_id=@employee_id;

    // create a new session id
    set @session_id = newid();
    message('LOGIN:New session:'||@session_id);
 
    -- Update WEB_SESSION
    insert into WEB_SESSION (session_id, creation_time, update_time, timeout_minutes, company_id, employee_id)
        values (@session_id, now(), now(), 5, @company_id, @employee_id);

    return @session_id;

END; 
commit;

-------------------------------------
-- FUNCTION TO LOG A PERSON OUT
-------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_LOGOUT ( IN @sessionid  long VARCHAR ) 
returns varchar(10)
BEGIN
      
    // If this session already exists, clear it
    if exists(select * from WEB_SESSION where session_id=@sessionid) then
      delete from WEB_SESSION where session_id=@sessionid;
      message('LOGOUT:Removed session:'||@sessionid);
      return 'OK';
    else
      return 'NO SESSION';
      message('LOGOUT:Session:'||@sessionid||' no longer exists');
    end if;

END; 
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_PrepareTaxCertificateData" ( 
        IN @sessionID long VARCHAR, IN @payrollID long varchar, IN @companyID long varchar,
        IN @reconMonth long varchar, IN @reconYear long varchar, IN @tradeCode long varchar)
returns varchar(5000)
BEGIN
    declare @Run_ID numeric(10);
	declare @RSLT varchar(5000);
	
    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Prepare the Data?
	set @Run_ID = fn_P_TC_SouthAfrica_DoTaxCertificate(@companyID,@payrollID,'LIVE',1,@reconYear,@reconMonth,1,null,null,null,null,null,@tradeCode,null);
	set @RSLT = fn_HTTP_GetTaxCertificateWarningsAndErrors(@Run_ID); 
	
    return(@RSLT); 
END;


commit;

------------------------------------------------
-- Function to mark a person to be recalculated
------------------------------------------------
create or replace function dba.fn_HTTP_RequestCalc(IN @companyID long varchar, IN @employeeID long varchar, IN @payrollID long varchar, IN @payrollDetailID long varchar, IN @runtypeID long varchar)
returns bit
begin
  call sp_P_ResetEmployeeCalcStatus(@companyID, @employeeID, @payrollID, @payrollDetailID, @runtypeID,0);
    commit;
  call sp_P_CALC_Initiate_PerEmployee(@companyID, @employeeID, @payrollID, @payrollDetailID, @runtypeID,0);
    commit;
  return 1; 
end;------------------------------------------------------------------------
-- FUNCTION TO RESET THE LOGIN_NAME and PASSWORD for an ADMINISTRATOR
-- Note: The person must already be a PeopleWeb admin 
--        (have a record in AL_EMASTER_AGROUP)
-- The function returns the first name || '*' || login name || '*' || new_password
------------------------------------------------------------------------
create or replace function dba.fn_HTTP_RESET_ADMINPASSWORD(IN @adminEmail long varchar)
returns long varchar
begin
  declare @newPwd char(10);
  declare @firstName long varchar;
  declare @preferredName long varchar;
  declare @loginName long varchar;
  declare @returnString long varchar;
  declare @adminCompanyID numeric(10);
  declare @adminEmployeeID numeric(10);

    -- Randomise a new password (10 characters)
    set @newPwd = char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);

    -- Get Login Name
    select first PREFERRED_NAME, firstname, 
            fn_S_DecryptString(login_name), al.company_id, al.employee_id 
        into 
           @preferredName, @firstName, @loginName, @adminCompanyID, @adminEmployeeID
        from 
            al_emaster_agroup al join e_master e join e_contact ec
            on al.company_id=e.company_id and al.employee_id=e.employee_id
            and e.company_id=ec.company_id and e.employee_id=ec.employee_id
        where upper(trim(E_MAIL))=upper(trim(@adminEmail))
        order by al.employee_id;
    
    if (@loginName is null) then
        return 'msgNoAdminWithThisEmail'
    end if;

    -- Do we have a preferred name?
    if ((@preferredName is not null) and (trim(@preferredName)>'')) then
        set @firstName = @preferredName
    end if;
    -- OK, now let's update the password.
    update al_emaster_agroup set password = fn_S_EncryptString(@newPwd) where
        company_id=@adminCompanyID and employee_id=@adminEmployeeID;

    -- Return login_name + '*' + password
    return @firstName||'*'||@loginName||'*'||@newPwd;
end;-------------------------------------------------------------
-- FUNCTION TO SAVE CREDIT PURCHASE HISTORY
-------------------------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SaveCreditPurchase" ( 
        in @CREDIT_DATE timestamp, in @PROCESS_NAME char(50),in @NBCREDITS int, in @TRANS_ID char(50), in @TRANS_REF char(50), in @TRANS_STATUS char(50),
        in @RESULT_CODE char(50), in @RESULT_DESCR char(200), in @AUTH_CODE char(50),  in @AMT_IN_CENTS int, in @RISK_INDICATOR char(10), in @CHKSUM char(50), in @INV_TO_CC char(200), in @INV_TO_PERSON char(200))
returns int
BEGIN
   insert into dba.WEB_CREDITTRANSACTION (CREDIT_DATE ,PROCESS_NAME ,NBCREDITS ,TRANS_ID ,TRANS_REF ,TRANS_STATUS ,RESULT_CODE , RESULT_DESCR ,AUTH_CODE ,AMT_IN_CENTS ,RISK_INDICATOR, CHKSUM, INV_TO_CC,INV_TO_PERSON ) on existing update
        values (@CREDIT_DATE ,@PROCESS_NAME ,@NBCREDITS ,@TRANS_ID ,@TRANS_REF ,@TRANS_STATUS ,@RESULT_CODE ,@RESULT_DESCR ,@AUTH_CODE ,@AMT_IN_CENTS ,@RISK_INDICATOR, @CHKSUM, @INV_TO_CC, @INV_TO_PERSON);
    return @@rowcount;
END;create or replace FUNCTION "DBA"."fn_http_SaveEmployeeLoan"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @LOAN_ID numeric(10),in @LOAN_AMMOUNT numeric(16,4),in @LOAN_DATE timestamp,in @PREMIUM numeric(16,4),in @EMLOAN_ID numeric(10),in @PREMIUM_TEMP numeric(16,4),in @PAYROLL_ID numeric(10),in @PAYROLLDETAIL_ID numeric(10),in @REFERENCE char(50))
returns numeric(10)
begin
  /*   
  <purpose>  
  fn_http_SaveEmployeeLoan saves the Employee''s loan information details for PeopleWeb
  </purpose>  

  <history>
  Date          CR#     Programmer        Changes
  ----          ---     ----------        -------
  2014/09/18    14233   Liam Terblanche   Initial script
  
  </history>
  */

  -- No EMLOAN_ID?  Create a new record  
  if (@EMLOAN_ID is null) then
    if not exists(select * from pl_emaster_loan where company_id=@company_id and employee_id=@employee_id and loan_id=@loan_id) then
      insert into PL_EMASTER_LOAN (company_id, employee_id, loan_id, loan_ammount, loan_date, premium) values (@COMPANY_ID, @EMPLOYEE_ID, @LOAN_ID, 0, now(), 0);
    end if;
    select emloan_id into @EMLOAN_ID from pl_emaster_loan where company_id=@company_ID and employee_id=@employee_id and loan_id=@loan_id;
  end if;

  -- Now let's update it
        -- Loan Date
        if @LOAN_DATE is not null then
            update PL_EMASTER_LOAN set
                LOAN_DATE = @LOAN_DATE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- PREMIUM
        if @PREMIUM is not null then
            update PL_EMASTER_LOAN set
                PREMIUM = @PREMIUM
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- PREMIUM_TEMP
        if @PREMIUM_TEMP is not null then
            update PL_EMASTER_LOAN set
                PREMIUM_TEMP = @PREMIUM_TEMP
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- PAYROLL_ID
        if @PAYROLL_ID is not null then
            update PL_EMASTER_LOAN set
                PAYROLL_ID = @PAYROLL_ID
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- LOAN_AMMOUNT
        if @LOAN_AMMOUNT is not null then
            update PL_EMASTER_LOAN set
                LOAN_AMMOUNT = @LOAN_AMMOUNT
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- "REFERENCE"
        if @REFERENCE is not null then
            update PL_EMASTER_LOAN set
                "REFERENCE" = @REFERENCE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
  
    commit;
  return(@EMLOAN_ID);
end;create or replace function "DBA"."fn_http_SaveEmployeeSaving"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @SAVING_ID numeric(10),in @START_DATE date,in @PREMIUM numeric(16,4),in @EMSAVING_ID numeric(10),in @DEPOSIT numeric(16,4),in @PREMIUM_TEMP numeric(16,4),in @PAYROLL_ID numeric(10),in @PAYROLLDETAIL_ID numeric(10),in @REFERENCE char(50))
returns numeric(10)
begin
  /*   
  <purpose>  
  fn_http_SaveEmployeeSaving saves the Employee''s Saving information details for PeopleWeb
  </purpose>  


  <history>
  Date          CR#     Programmer        Changes
  ----          ---     ----------        -------
  2014/09/18    14233   Liam Terblanche   Initial script
  </history>
  */
  -- No EMsaving_ID?  Create a new record  
  if (@EMsaving_ID is null) then
    if not exists(select * from pl_emaster_saving where company_id=@company_id and employee_id=@employee_id and saving_id=@saving_id) then
      insert into PL_EMASTER_saving (company_id, employee_id, saving_id, start_date, premium) values (@COMPANY_ID, @EMPLOYEE_ID, @saving_ID, now(), 0);
    end if;
    select emsaving_id into @EMsaving_ID from pl_emaster_saving where company_id=@company_ID and employee_id=@employee_id and saving_id=@saving_id;
  end if;

  -- Now let's update it
        -- START_DATE
        if @START_DATE is not null then
            update PL_EMASTER_saving set
                START_DATE = @START_DATE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PREMIUM
        if @PREMIUM is not null then
            update PL_EMASTER_saving set
                PREMIUM = @PREMIUM
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PREMIUM_TEMP
        if @PREMIUM_TEMP is not null then
            update PL_EMASTER_saving set
                PREMIUM_TEMP = @PREMIUM_TEMP
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PAYROLL_ID
        if @PAYROLL_ID is not null then
            update PL_EMASTER_saving set
                PAYROLL_ID = @PAYROLL_ID
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- DEPOSIT
        if @DEPOSIT is not null then
            update PL_EMASTER_saving set
                DEPOSIT = @DEPOSIT
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID;

        end if;
        -- "REFERENCE"
        if @REFERENCE is not null then
            update PL_EMASTER_saving set
                "REFERENCE" = @REFERENCE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
  
    commit;
  return(@EMsaving_ID);
end;----------------------------------------------
-- FUNCTION TO UPDATE COMPANY ACCOUNT INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCompanyAccountData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, 
            IN @accountType long VARCHAR default null, IN @accountName long varchar default null, 
            IN @accountDescription long varchar default null, IN @accountBank long varchar default null, IN @accountBranch long VARCHAR default null, 
            IN @accountBankCode long varchar default null, IN @accountBranchCode long varchar default null, IN @accountNumber long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- With the web interface, we ALWAYS only work with one account.  If more than one exist, only retain the first one.
    delete from c_bankdet where company_id=@companyID and account_id > 
        (select min(account_id) from c_bankdet where company_id=@companyID);

    -- Create an account if it doesn't already exist
    if not exists(select * from c_bankdet where company_id=@companyID ) then
        insert into c_bankdet (company_id, account_id) values (@companyID, 1);
    end if;

    -- Account Type
    if (@accountType is not null) then
        update c_bankdet set ACCOUNT_TYPE=@accountType
            where company_id=@companyID;
    end if;
    -- Account Name
    if (@accountName is not null) then
        update c_bankdet set ACCOUNT_NAME=@accountName
            where company_id=@companyID;
    end if;
    -- Account Description
    if (@accountDescription is not null) then
        update c_bankdet set ACCOUNT_DESCRIPTION=@accountDescription
            where company_id=@companyID;
    end if;
    -- Account Bank
    if (@accountBank is not null) then
        update c_bankdet set BANK=@accountBank
            where company_id=@companyID;
    end if;
    -- Account Branch
    if (@accountBranch is not null) then
        update c_bankdet set BRANCH=@accountBranch
            where company_id=@companyID;
    end if;
    -- Account Bank Code
    if (@accountBankCode is not null) then
        update c_bankdet set BANK_CODE=@accountBankCode
            where company_id=@companyID;
    end if;
    -- Account Branch Code
    if (@accountBranchCode is not null) then
        update c_bankdet set BRANCH_CODE=@accountBranchCode
            where company_id=@companyID;
    end if;
    -- Account Number
    if (@accountNumber is not null) then
        update c_bankdet set ACCOUNT_NUMBER=@accountNumber
            where company_id=@companyID;
    end if;
    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE COMPANY INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCompanyData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @name long varchar default null,IN @registrationNo long varchar default null,
        IN @taxNo long varchar default null,IN @phone long varchar default null,  IN @fax long varchar default null,IN @email long varchar default null,
        IN @diplomaticIndemnity char(1) default null,IN @pensionNo long varchar default null,IN @postalCountry long varchar default null,
        IN @postalProvince long varchar default null,IN @postalAddress long varchar default null,IN @postalSuburb long varchar default null,
        IN @postalCity long varchar default null, IN @postalCode long varchar default null, IN @physUnitNo long varchar default null,
        IN @physComplex long varchar default null,IN @physStreetNo long varchar default null, IN @physStreet long varchar default null,IN @physSuburb long varchar default null,
        IN @physCity long varchar default null,IN @physCode long varchar default null, IN @physProvince long varchar default null,IN @physCountry long varchar default null,
        IN @postalSameAsPhysical char(1) default null,IN @mainBusinessActivity long varchar default null,IN @tradeCode long varchar default null,
        IN @tradeSubCode long varchar default null, IN @setaCode long varchar default null,IN @sicCode long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);
    message('About to update for company '||@name);

    -- name
    if (@name is not null) then
        update c_master set name=@name  where company_id=@companyID;
    end if;
    -- registrationNo
    if (@registrationNo is not null) then
        update c_master set COMPANY_REGISTRATION=@registrationNo  where company_id=@companyID;
    end if;
    -- taxNo
    if (@taxNo is not null) then
        update c_master set TAX_NUMBER=@taxNo where company_id=@companyID;
    end if;
    -- phone
    if (@phone is not null) then
        update c_master set TEL=@phone where company_id=@companyID;
    end if;
    -- fax
    if (@fax is not null) then
        update c_master set FAX=@fax where company_id=@companyID;
    end if;
    -- email
    if (@email is not null) then
        update c_master set EMAIL=@email where company_id=@companyID;
    end if;
    -- diplomaticIndemnity
    if (@diplomaticIndemnity is not null) then
        if (@diplomaticIndemnity='1')then
          update c_master set DIPL_INDEMNITY='Y' where company_id=@companyID
        else
          update c_master set DIPL_INDEMNITY='N' where company_id=@companyID
        end if;
    end if;
    -- pensionNo
    if (@pensionNo is not null) then
        update c_master set PENSION_NUMBER=@pensionNo where company_id=@companyID;
    end if;
    -- postalCountry
    if (@postalCountry is not null) then
        insert into c_address (COMPANY_ID, POSTAL_COUNTRY) on existing update values(@companyID, @postalCountry);
    end if;
    -- postalProvince
    if (@postalProvince is not null) then
        insert into c_address (COMPANY_ID, POSTAL_PROVINCE) on existing update values(@companyID, @postalProvince);
    end if;
    -- postalAddress
    if (@postalAddress is not null) then
        insert into c_address (COMPANY_ID, POSTAL_ADDRESS) on existing update values(@companyID, @postalAddress);
    end if;
    -- postalSuburb
    if (@postalSuburb is not null) then
        insert into c_address (COMPANY_ID, POSTAL_SUBURB) on existing update values(@companyID, @postalSuburb);
    end if;
    -- postalCity
    if (@postalCity is not null) then
        insert into c_address (COMPANY_ID, POSTAL_CITY) on existing update values(@companyID, @postalCity);
    end if;
    -- postalCode
    if (@postalCode is not null) then
        insert into c_address (COMPANY_ID, POSTAL_CODE) on existing update values(@companyID, @postalCode);
    end if;
    -- physUnitNo
    if (@physUnitNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_UNITNO) on existing update values(@companyID, @physUnitNo);
    end if;
    -- physComplex
    if (@physComplex is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_COMPLEX) on existing update values(@companyID, @physComplex);
    end if;
    -- physStreetNo
    if (@physStreetNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_STREETNO) on existing update values(@companyID, @physStreetNo);
    end if;
    -- physStreet
    if (@physStreet is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_STREET) on existing update values(@companyID, @physStreet);
    end if;
    -- physSuburb
    if (@physSuburb is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_SUBURB) on existing update values(@companyID, @physSuburb);
    end if;
    -- physCity
    if (@physCity is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_CITY) on existing update values(@companyID, @physCity);
    end if;
    -- physCode
    if (@physCode is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_CODE) on existing update values(@companyID, @physCode);
    end if;
    -- physProvince
    if (@physProvince is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_PROVINCE) on existing update values(@companyID, @physProvince);
    end if;
    -- physCountry
    if (@registrationNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_COUNTRY) on existing update values(@companyID, @physCountry);
    end if;
    -- postalSameAsPhysical
    if (@postalSameAsPhysical is not null) then
       insert into c_address (COMPANY_ID, POSTAL_SAME_AS_PHYSICAL) on existing update values(@companyID, @postalSameAsPhysical);
    end if;
    -- mainBusinessActivity
    if (@mainBusinessActivity is not null) then
        update c_master set MAIN_BUSINESS_ACTIVITY=@mainBusinessActivity  where company_id=@companyID;
    end if;
    -- tradeCode
    if (@tradeCode is not null) then
        update c_master set TRADE_CLASS_ID=(select TRADE_CLASS_ID from A_TRADE_CLASSIFICATION where TRADE_CODE=@tradeCode )  where company_id=@companyID;
    end if;
    -- tradeSubCode
    if (@tradeSubCode is not null) then
        update c_master set TRADE_SUB_CODE=@tradeSubCode where company_id=@companyID;
    end if;
    -- setaCode
    if (@setaCode is not null) then
        update c_master set JOBSECTOR_ID=(select JOBSECTOR_ID from J_JOBSECTOR where SETA=@setaCode )  where company_id=@companyID;
    end if;
    -- sicCode
    if (@sicCode is not null) then
        update c_master set SIC_CODE_ID=(select SIC_CODE_ID from J_SIC_CODE where SIC_CODE=@sicCode )  where company_id=@companyID;
    end if;

    commit;
    return('OK'); 
END;

commit;
----------------------------------------------
-- FUNCTION TO UPDATE COST CENTRE INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCostCentreData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @costID long varchar,IN @name long varchar default null,
        IN @description long varchar default null,IN @number long varchar default null,  IN @parentCostID long varchar default null,
        IN @doDelete char(1) default 'N')
returns varchar(10)
BEGIN
    declare @newCostID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this cost centre?
    if (@costID='-1') then
        select  coalesce(max(cost_id)+1,1) into @newCostID from c_costmain where company_id=@companyID;
        message('New Cost ID:'||@newCostID);
        set @costID = @newCostID;
        insert into C_COSTMAIN (company_id, cost_id, name) values (@companyID, @costID, 'New');
    end if;

    -- Should we delete this cost centre?
    if (@doDelete='Y') then
        -- Don't cascade delete if the parent gets deleted
        update c_costmain set PARENT_COST_ID=null where company_id=@companyID and PARENT_COST_ID=@costID;
        -- Now delete the cost centre
        delete from c_costmain where  company_id=@companyID and cost_id=@costID
    else
        -- name
        if (@name is not null) then
            update c_costmain set name=@name  where company_id=@companyID and cost_id=@costID;
        end if;
        -- description
        if (@description is not null) then
            update c_costmain set DESCRIPTION=@description  where company_id=@companyID and cost_id=@costID;
        end if;
        -- number
        if (@number is not null) then
            update c_costmain set COST_NUMBER=@number where company_id=@companyID and cost_id=@costID;
        end if;
        -- parentCostID
        if (@parentCostID is not null) then
            update c_costmain set PARENT_COST_ID=@parentCostID where company_id=@companyID and cost_id=@costID;
        end if;
    end if;   

    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE ACCOUNT INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeeAccountData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @accountType long VARCHAR default null, IN @accountName long varchar default null, 
            IN @accountDescription long varchar default null, IN @accountBank long varchar default null, IN @accountBranch long VARCHAR default null, 
            IN @accountBankCode long varchar default null, IN @accountBranchCode long varchar default null, IN @accountNumber long varchar default null, 
            IN @accountHolderRelationship long varchar default null, IN @accountLineNumber long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- With the web interface, we ALWAYS only work with one account.  If more than one exist, only retain the first one.
    delete from e_bankdet where company_id=@companyID and employee_id=@employeeID and account_id > 
        (select min(account_id) from e_bankdet where company_id=@companyID and employee_id=@employeeID );

    -- Create an account if it doesn't already exist
    if not exists(select * from e_bankdet where company_id=@companyID and employee_id=@employeeID) then
        insert into e_bankdet (company_id, employee_id, account_id) values (@companyID, @employeeID, 1);
    end if;

    -- Account Type
    if (@accountType is not null) then
        update e_bankdet set ACCOUNT_TYPE=@accountType
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Name
    if (@accountName is not null) then
        update e_bankdet set ACCOUNT_NAME=@accountName
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Description
    if (@accountDescription is not null) then
        update e_bankdet set ACCOUNT_DESCRIPTION=@accountDescription
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Bank
    if (@accountBank is not null) then
        update e_bankdet set BANK=@accountBank
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Branch
    if (@accountBranch is not null) then
        update e_bankdet set BRANCH=@accountBranch
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Bank Code
    if (@accountBankCode is not null) then
        update e_bankdet set BANK_CODE=@accountBankCode
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Branch Code
    if (@accountBranchCode is not null) then
        update e_bankdet set BRANCH_CODE=@accountBranchCode
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Number
    if (@accountNumber is not null) then
        update e_bankdet set ACCOUNT_NUMBER=@accountNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Relationship
    if (@accountHolderRelationship is not null) then
        update e_bankdet set ACCOUNT_HOLDER_RELATIONSHIP=@accountHolderRelationship
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Line Number
    if (@accountLineNumber is not null) then
        update e_bankdet set LINE_NUMBER=@accountLineNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE ADDRESS INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeeAddressData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @physCountry long varchar default null, IN @physProvince long VARCHAR default null, IN @physPostalCode long varchar default null, 
            IN @physUnitNo long varchar default null, IN @physComplex long varchar default null, IN @physStreetNo long VARCHAR default null, 
            IN @physStreetOrFarmName long varchar default null, IN @physSuburb long varchar default null, IN @physCityOrTown long varchar default null, 
            IN @postalSameAsPhysical long varchar default null, IN @postCountry long varchar default null, IN @postProvince long varchar default null, 
            IN @postPostCode long varchar default null, IN @postPostOffice long VARCHAR default null, IN @postPostalAgencyOrSubUnit long varchar default null, 
            IN @postBoxOrBagNumber long varchar default null, IN @postIsPOBox long varchar default null, IN @postIsPrivateBag long varchar default null, 
            IN @postPostalAddressIsCareOf long varchar default null, IN @postCityOrTown long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Physical Address - Country
    if (@physCountry is not null) then
        update e_address set PHYS_HOME_COUNTRY=@physCountry
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Province
    if (@physProvince is not null) then
        update e_address set PHYS_HOME_PROVINCE=@physProvince
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Postal Code
    if (@physPostalCode is not null) then
        update e_address set PHYS_HOME_CODE=@physPostalCode
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Unit
    if (@physUnitNo is not null) then
        update e_address set PHYS_HOME_UNITNO=@physUnitNo
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Complex
    if (@physComplex is not null) then
        update e_address set PHYS_HOME_COMPLEX=@physComplex
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Street No
    if (@physStreetNo is not null) then
        update e_address set PHYS_HOME_STREETNO=@physStreetNo
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - physStreetOrFarmName
    if (@physStreetOrFarmName is not null) then
        update e_address set PHYS_HOME_STREET=@physStreetOrFarmName
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - physSuburb
    if (@physSuburb is not null) then
        update e_address set PHYS_HOME_SUBURB=@physSuburb
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - physCityOrTown
    if (@physCityOrTown is not null) then
        update e_address set PHYS_HOME_CITY=@physCityOrTown
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - postalSameAsPhysical
    if (@postalSameAsPhysical is not null) then
        update e_address set POSTAL_SAMEAS_PHYSICAL=@postalSameAsPhysical
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postCountry
    if (@postCountry is not null) then
        update e_address set POSTAL_COUNTRY=@postCountry
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postProvince
    if (@postProvince is not null) then
        update e_address set POSTAL_PROVINCE=@postProvince
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postPostCode
    if (@postPostCode is not null) then
        update e_address set HOME_POSTAL_CODE=@postPostCode
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postPostOffice
    if (@postPostOffice is not null) then
        update e_address set POSTAL_POST_OFFICE=@postPostOffice
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postPostalAgencyOrSubUnit
    if (@postPostalAgencyOrSubUnit is not null) then
        update e_address set POSTAL_AGENCY_SUBUNIT=@postPostalAgencyOrSubUnit
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postBoxOrBagNumber
    if (@postBoxOrBagNumber is not null) then
        update e_address set POSTAL_BOX_BAG_NUMBER=@postBoxOrBagNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postIsPOBox
    if (@postIsPOBox is not null) then
        update e_address set POSTAL_IS_POBOX=@postIsPOBox
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postIsPrivateBag
    if (@postIsPrivateBag is not null) then
        update e_address set POSTAL_IS_PRIVATEBAG=@postIsPrivateBag
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postPostalAddressIsCareOf
    if (@postPostalAddressIsCareOf is not null) then
        update e_address set POSTAL_CAREOF_INTERMEDIARY=@postPostalAddressIsCareOf
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postCityOrTown
    if (@postCityOrTown is not null) then
        update e_address set POSTAL_CITY_TOWN=@postCityOrTown
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE INFORMATION
----------------------------------------------
create or replace FUNCTION "DBA"."fn_HTTP_SetEmployeeData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar default null, IN @firstname long varchar default null,
            IN @secondname long VARCHAR default null, IN @surname long varchar default null, IN @employeeNumber long varchar default null, 
            IN @preferredName long varchar default null, IN @title long VARCHAR default null, IN @initials long varchar default null, 
            IN @gender long varchar default null, IN @maritalStatus long varchar default null, IN @homeLanguage long varchar default null, 
            IN @groupJoinDate long varchar default null, IN @dateOfBirth long varchar default null, IN @taxNumber long varchar default null,
            IN @idNumber long VARCHAR default null, IN @passportHeld long varchar default null, IN @passportNumber long varchar default null,
            IN @race long varchar default null, IN @dateOfEngagement long varchar default null, IN @dateOfDischarge long varchar default null,
            IN @dischargeReason long varchar default null,
            IN @costCentreID long varchar default null, IN @jobTitle long varchar default null, IN @payroll long varchar default null,
            IN @essLogin long varchar default null, IN @essPassword long varchar default null, IN @adminLogin long varchar default null,
            IN @adminPassword long varchar default null, IN @isAdministrator char(1) default 'N',
            IN @telHome long varchar default null, IN @telWork long varchar default null, IN @telCell long varchar default null,
            IN @eMail long varchar default null,
            IN @undoDischarge char(1) default 'N')
returns varchar(10)
BEGIN
declare @newEmployeeID numeric(10);
declare @reasonID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- No Employee_ID?  Then this should be a new employee
    if ((@employeeID is null) or not exists(select * from e_master where company_id=@companyID and employee_id=@employeeID))
        then
        select coalesce(max(employee_id)+1,1) into @newEmployeeID from e_master where company_id=@companyID;
        set @employeeID=@newEmployeeID;
        // E_MASTER
        insert into e_master (company_id, employee_id, active_yn)
        values (@companyID, @employeeID, 'Y');
    end if;

    
    -- Firstname
    if (@firstname is not null) then
        update e_master set FIRSTNAME=@firstname
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Secondname
    if (@secondname is not null) then
        update e_master set SECONDNAME=@secondname
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Surname
    if (@surname is not null) then
        update e_master set SURNAME=@surname
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Employee Number
    if (@employeeNumber is not null) then
        update e_master set COMPANY_EMPLOYEE_NUMBER=@employeeNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Preferred Name
    if (@preferredName is not null) then
        update e_master set PREFERRED_NAME=@preferredName
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Title
    if (@title is not null) then
        update e_master set TITLE=@title
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Initials
    if (@initials is not null) then
        update e_master set INITIALS=@initials
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- GENDER
    if (@gender is not null) then
        call sp_P_SetEmployeeGender(@companyID, @employeeID, @gender);
    end if;
    -- Marital Status
    if (@maritalStatus is not null) then
        update e_master set MARITAL_STATUS=@maritalStatus
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Home Language
    if (@homeLanguage is not null) then
        update e_master set HOME_LANGUAGE=@homeLanguage
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- GROUP JOIN DATE
    if (@GroupJoinDate is not null) then
        update e_master set GROUP_JOIN_DATE=@groupJoinDate
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Birthdate
    if (@dateOfBirth is not null) then
        update e_master set BIRTHDATE=@dateOfBirth
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Tax Number
    if (@taxNumber is not null) then
        update E_NUMBER set TAX_NUMBER=@taxNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- ID Number
    if (@idNumber is not null) then
        update e_number set ID_NUMBER=@idNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Passport Held
    if (@passportHeld is not null) then
        update E_PASSPORT set PASSPORT_HELD=@passportHeld
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Passport Number
    if (@passportNumber is not null) then
        update E_PASSPORT set PASSPORT_NUMBER=@passportNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- RACE
    if (@race is not null) then
        call sp_P_SetEmployeeRace(@companyID, @employeeID, @race);
    end if;

    -- Start Date
    if (@dateOfEngagement is not null) then
        -- New?
        if not exists (select * from e_on_off where company_id=@companyID and employee_id=@employeeID) then
            insert into e_on_off (company_id, employee_id, start_date) values (@companyID, @employeeID, @dateOfEngagement)
        else
            update e_on_off set START_DATE=@dateOfEngagement where
            company_id=@companyID and employee_id=@employeeID and start_date = (select max(start_date) from e_on_off where company_id=@companyID and employee_id=@employeeID)
        end if;
    end if;
    -- End Date
    if (@dateOfDischarge is not null) then
        update e_on_off set END_DATE=@dateOfDischarge where
        company_id=@companyID and employee_id=@employeeID and start_date = (select max(start_date) from e_on_off where company_id=@companyID and employee_id=@employeeID and start_date<=@dateOfDischarge)
    end if;
    -- Discharge Reason
    if (@dischargeReason is not null) then
        select min(reason_id) into @reasonID from j_jobposition_reason where trim(UIF_REASON)=trim(substring(@dischargeReason,1,locate(@dischargeReason,'[')-1));
        update e_on_off set REASON_ID=@reasonID  where
        company_id=@companyID and employee_id=@employeeID and start_date = (select max(start_date) from e_on_off where company_id=@companyID and employee_id=@employeeID);
    end if;

    -- Cost Centre Path
    if (@costCentreID is not null) then
        update e_master 
        set cost_id = @costCentreID
            where company_id=@companyID and employee_id=@employeeID
    end if;
    -- Job Position
    if (@jobTitle is not null) then
        delete from PL_EMASTER_PJOBPOSITION where company_id=@companyID and employee_id=@employeeID;
        insert into PL_EMASTER_PJOBPOSITION (company_id, employee_id, jobposition_id) 
            select @companyID, @employeeID, jobposition_id from P_JOBPOSITION where trim(JOB_NAME)=trim(@jobTitle)
    end if;      
    -- Payroll
    if (@payroll is not null) then
    	-- Unlink from existing payroll
    	delete from pl_paydef_emaster where company_id=@companyID and employee_id=@employeeID and payroll_id not in (select payroll_id from p_payroll where trim(name)=trim(@payroll));
        -- Link to new payroll
        insert into pl_paydef_emaster (company_id, employee_id, payroll_id) on existing skip 
                select @companyID, @employeeID, payroll_id from p_payroll where trim(name)=trim(@payroll);
        -- Link to open period
        insert into p_calc_employeelist (company_id, employee_id, payroll_id, payrolldetail_id, runtype_id, run_status, closed_yn)
            select company_id, employee_id, payroll_id, fn_P_GetOpenPeriod(company_id, payroll_id), 1, 1, 'N'
            from pl_paydef_emaster where employee_id not in (select employee_id from p_calc_employeelist where company_id=pl_paydef_emaster.company_id and
            payroll_id=pl_paydef_emaster.payroll_id and payrolldetail_id=fn_P_GetOpenPeriod(company_id, payroll_id));
        end if;      
      
    -- ESS username
    if (@essLogin is not null) then
        update e_master set WEB_LOGIN=fn_S_EncryptString(@essLogin)
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- ESS password
    if (@essPassword is not null) then
        update e_master set WEB_PWD=fn_S_EncryptString(@essPassword)
            where company_id=@companyID and employee_id=@employeeID;
    end if;

    -- Administrator
    if (@isAdministrator='N') then
        delete from al_emaster_agroup 
        where company_id=@companyID and employee_id=@employeeID;
    end if;
    if (@isAdministrator='Y') then
        if ((@adminLogin is not null) and (@adminPassword is not null)) then
            insert into AL_EMASTER_AGROUP (company_id, employee_id, group_id, login_name, PASSWORD) on existing update
                values (@companyID, @employeeID, 1, fn_S_EncryptString(@adminLogin),  fn_S_EncryptString(@adminPassword));
        end if;
    end if;

    -- Tel Home
    if (@telHome is not null) then
        update E_CONTACT set TEL_HOME=@telHome
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Tel Work
    if (@telWork is not null) then
        update E_CONTACT set TEL_WORK=@telWork
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Tel Cell
    if (@telCell is not null) then
        update E_CONTACT set TEL_CELL=@telCell
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- EMail
    if (@eMail is not null) then
        update E_CONTACT set E_MAIL=@eMail
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Undo Discharge
    if (@undoDischarge='Y') then
        update e_on_off set END_DATE=null, REASON_ID=null where
        company_id=@companyID and employee_id=@employeeID and start_date = (select max(start_date) from e_on_off where company_id=@companyID and employee_id=@employeeID);
		-- Make sure the person is in P_CALC_EMPLOYEELIST
        insert into p_calc_employeelist (company_id, employee_id, payroll_id, payrolldetail_id, runtype_id, run_status, closed_yn)
            select company_id, employee_id, payroll_id, fn_P_GetOpenPeriod(company_id, payroll_id), 1, 1, 'N'
            from pl_paydef_emaster where employee_id not in (select employee_id from p_calc_employeelist where company_id=pl_paydef_emaster.company_id and
            payroll_id=pl_paydef_emaster.payroll_id and payrolldetail_id=fn_P_GetOpenPeriod(company_id, payroll_id));
		
    end if;

    -- Other stuff
    update p_e_basic set rate_of_pay1=100 where company_id=@companyID and EMPLOYEE_ID=@employeeID and rate_of_pay1 is null;
    update p_e_basic set pay_method='Bank Transfer' where company_id=@companyID and EMPLOYEE_ID=@employeeID and pay_method is null;
    if not exists(select * from p_e_master where company_id=@companyID and EMPLOYEE_ID=@employeeID ) then
        insert into p_e_master (company_id, employee_id, tax_calc_method, NATURE_OF_PERSON_ID)
        values (@companyID, @employeeID, 'NON AVERAGING',1);
    end if;


    -- Link everyone to this workweek profile
    insert into LL_EMASTER_WORKWEEKPROFILE (company_id, employee_id, workweekprofile_id, start_date, end_date) on existing update
      select company_id, employee_id, 1, '2010/01/01', null from e_master;
    
    commit;

    return('OK'); 
END;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE PACKAGE INFO
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeePackage" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @entityID long VARCHAR default null, IN @entityType long varchar default null, 
            IN @value long varchar default null, IN @balance long varchar default null,
            IN @payrollID long VARCHAR default null, IN @referenceNo long varchar default null,
            IN @rfi long varchar default 'N',
            IN @unlink long VARCHAR default 'no', IN @removeTempVal long VARCHAR default 'no' )
returns varchar(10)
BEGIN
  declare @RSLT numeric;
    truncate table P_CALC_LASTCALCED_EMPLOYEE;
    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Make sure the record exists
    if not exists(select * from p_e_basic where company_id=@companyID and employee_id=@employeeID) then
        insert into P_E_BASIC  (company_id, employee_id, pay_per1) values (@companyID, @employeeID, 0);
    end if;

    -- Is this a VARIABLE?
    if ((@entityType='entityType_GenericVariable') or
        (@entityType='entityType_Deduction') or
        (@entityType='entityType_Interim') or
        (@entityType='entityType_PackageComponent') or
        (@entityType='entityType_Earning') or
        (@entityType='entityType_Tax')) then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_pvariable where  company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
        else
            begin
                if (@value is null) then
                    call sp_P_SaveDefaultVariableValues(@companyID, @employeeID, null,null,@entityID,null,'',null,null,null);
                    --insert into PL_EMASTER_PVARIABLE (company_id, employee_id, variable_id, default_value)
                    --values (@companyID, @employeeID,@entityID,'')
                else
                    call sp_P_SaveDefaultVariableValues(@companyID, @employeeID, null,null,@entityID,@value,@value,null,null,null);
                end if;
                -- Reference Number
                if (@referenceNo is not null) then
                    update PL_EMASTER_PVARIABLE set "REFERENCE"=@referenceNo 
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
                -- Remove TEMP value
                if (@removeTempVal='yes') then
                    update PL_EMASTER_PVARIABLE set temp_value=null
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
                -- Set RFI
                update PL_EMASTER_PVARIABLE set RFI_YN='N'
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                if (@rfi='Y') then
                    update PL_EMASTER_PVARIABLE set RFI_YN='Y'
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
            end;
        end if;

    end if;
    -- Is this a LOAN?
    if (@entityType='entityType_Loan') then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_loan where  company_id=@companyID and employee_id=@employeeID and loan_id=@entityID
        else
            select fn_http_SaveEmployeeLoan(@companyID, @employeeID, @entityID, @balance, 
                        fn_P_GetPeriodStartDate(@payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID)),@value,null,null,@payrollID,fn_P_GetOpenPeriod(@companyID, @payrollID),@referenceNo ) into @RSLT;
            -- Remove TEMP value
            if (@removeTempVal='yes') then
                update pl_emaster_loan set premium_temp=null
                where company_id=@companyID and employee_id=@employeeID and loan_id=@entityID;
            end if;
        end if;
    end if;
    -- Is this a SAVING?
    if (@entityType='entityType_Saving') then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_saving where  company_id=@companyID and employee_id=@employeeID and saving_id=@entityID
        else
            select fn_http_SaveEmployeeSaving(@companyID, @employeeID, @entityID, 
                        fn_P_GetPeriodStartDate(@payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID)),@value,null,null,null,@payrollID,fn_P_GetOpenPeriod(@companyID, @payrollID),@referenceNo ) into @RSLT;
            -- Remove TEMP value
            if (@removeTempVal='yes') then
                update pl_emaster_saving set premium_temp=null
                where company_id=@companyID and employee_id=@employeeID and saving_id=@entityID;
            end if;
        end if;
    end if;

    -- We need to make sure the Payroll calculates this person as many times as it needs, without thinking it's the same calculation that simply got stuck
    truncate table P_CALC_LASTCALCED_EMPLOYEE;
    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE PAYROLL INFO
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeePayrollInfo" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @rateOfPay long VARCHAR default null,    IN @hoursPerDay long varchar default null, 
            IN @hoursPerWeek long varchar default null, IN @hoursPerMonth long varchar default null, 
            IN @daysPerWeek long VARCHAR default null,  IN @daysPerMonth long varchar default null, 
            IN @payPer long varchar default null, IN @nbMedDependents varchar default null)
returns varchar(10)
BEGIN
  declare @counter integer;

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Make sure the record exists
    if not exists(select * from p_e_basic where company_id=@companyID and employee_id=@employeeID) then
        insert into P_E_BASIC  (company_id, employee_id, pay_per1) values (@companyID, @employeeID, 0);
    end if;

    -- Rate Of Pay
    if (@rateOfPay is not null) then
        update p_e_basic set DBA.P_E_BASIC.RATE_OF_PAY1=@rateOfPay
            where company_id=@companyID and employee_id=@employeeID;
    end if;
     -- Hours Per Day
    if (@hoursPerDay is not null) then
        update p_e_basic set DBA.P_E_BASIC.HOURS_PER_DAY=@hoursPerDay
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Hours Per Week
    if (@hoursPerWeek is not null) then
        update p_e_basic set DBA.P_E_BASIC.HOURS_PER_WEEK=@hoursPerWeek
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Hours Per Month
    if (@hoursPerMonth is not null) then
        update p_e_basic set DBA.P_E_BASIC.HOURS_PER_MONTH=@hoursPerMonth
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Days Per Week
    if (@daysPerWeek is not null) then
        update p_e_basic set DBA.P_E_BASIC.DAYS_PER_WEEK=@daysPerWeek
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Days Per Month
    if (@daysPerMonth is not null) then
        update p_e_basic set DBA.P_E_BASIC.DAYS_PER_MONTH=@daysPerMonth
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Pay Per
    if (@payPer is not null) then
        update p_e_basic set DBA.P_E_BASIC.PAY_PER1=@payPer
            where company_id=@companyID and employee_id=@employeeID;
    end if;  
    -- Number of Medical Aid Dependents
    if (@nbMedDependents is not null) then
        set @counter=0;
        -- Delete what's already stored in DB
        delete from e_family where company_id=@companyID and employee_id=@employeeID;
        -- Now add new ones
        while @counter < @nbMedDependents loop
            insert into E_FAMILY (company_id, employee_id, family_id, FIRSTNAME, MED_DEPENDENT_YN)
            values (@companyID, @employeeID, @counter, 'Dependent @'||@counter, 'Y');
            set @counter = @counter + 1;
        end loop;
    end if;
    -- We need to make sure the Payroll calculates this person as many times as it needs, without thinking it's the same calculation that simply got stuck
    truncate table P_CALC_LASTCALCED_EMPLOYEE;
    -- Add into the calculation queue
    insert into p_calc_queue( company_id,employee_id,payroll_id,payrolldetail_id,runtype_id,run_status,paused_yn ) on existing update defaults off
    select company_id,employee_id,payroll_id,payrolldetail_id,runtype_id,run_status,'N' from p_calc_employeelist
      where payrolldetail_id = fn_P_GetOpenPeriod(company_id,payroll_id)
      and run_status = 2 and runtype_id = 1;

    commit;
    return('OK'); 
END;

commit;
---------------------------------------------- 
-- FUNCTION TO UPDATE JOB POSITION INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetJobPositionData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @jobPositionID long varchar,  
        IN @jobTitle long varchar,  IN @jobDescription long varchar default null,  IN @jobCode long varchar default null,  
        IN @jobGrade long varchar default null, IN @deleted char(1) default 'N')
returns numeric(10)
BEGIN
    declare @newJobPositionID numeric(10);
    set @newJobPositionID = -1;


    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return -1;
    end if;

    -- Can't have NULL description field
    if (@jobDescription is null) then
        set @jobDescription = @jobTitle
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this Job Position?
    if (@jobPositionID='-1') then
        //
        // Create Job Position
        //
        if not exists(select * from P_JOBPOSITION where trim(job_name)=trim(@jobTitle)) then
            begin
                select  coalesce(max(jobPosition_ID)+1,1) into @newJobPositionID from p_jobposition;
                set @jobPositionID = @newJobPositionID;
                insert into p_jobposition (company_id, jobposition_id, job_name, description, JOB_CODE, GRADE) 
                    values (@companyID, @jobPositionID, @jobTitle, '','','');
            end;
        end if;
    end if;

        
    -- Should we delete this entity?
    if (@deleted='Y') then
       delete from p_jobposition where company_id=@companyID and jobposition_id=@jobPositionID
    else
        //
        // Change VALUES in P_JOBPOSITION
        //
        begin
            if (@jobTitle is not null) then
                if not exists(select * from P_JOBPOSITION where trim(job_name)=trim(@jobTitle) and jobposition_id<>@jobPositionID) then
                    update p_jobposition set job_name=@jobTitle where company_id=@companyID and jobposition_id=@jobPositionID;
                end if;
            end if;
            if (@jobDescription is not null) then
              update p_jobposition set "DESCRIPTION"=@jobDescription where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
            if (@jobCode is not null) then
              update p_jobposition set JOB_CODE=@jobCode where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
            if (@jobGrade is not null) then
              update p_jobposition set GRADE=@jobGrade where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
        end;
    end if;

    return @jobPositionID; 
END;

commit;-------------------------------------------------------------
-- FUNCTION TO SAVE CHANGES TO TITLE DATA
-------------------------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetLanguageData" ( 
        IN @sessionID long VARCHAR,IN @languageID long VARCHAR, IN @language long varchar,IN @deleted char(1) default 'N')
returns numeric(10)
BEGIN
  declare @newLanguageID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return -1;
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this Language Entry?
    if (@languageID ='-1') then
        if not exists(select * from a_language where trim("language")=trim(@language)) then
           select coalesce(max(language_id)+1,1) into @newLanguageID from A_LANGUAGE;
           insert into A_LANGUAGE (language_id, "language") values (@newLanguageID,@language);
           set @languageID = @newLanguageID 
        end if;
    else
        -- Should we delete this Title?
        if (@deleted ='Y') then
           delete from A_LANGUAGE where language_id= @languageID
        ELSE 
            update A_LANGUAGE set "language"=@language where language_id=@languageID
        end if;
    end if;


    return(@languageID); 
END;

commit;---------------------------------------------- 
-- FUNCTION TO UPDATE PAY ENTITY INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetPayEntityData" ( 
        IN @sessionID long VARCHAR, IN @entityID long varchar, IN @entityType long varchar default null,
        IN @name long varchar default null,IN @caption long varchar default null, IN @valueUnit long varchar default null, IN @showOnPayslip char(1) default 'Y',
        IN @showYtdOnPayslip char(1) default 'N',
        IN @calculationFormula long varchar default null, 
        IN @payrollType char(20) default null,
        IN @interest long varchar default null, 
        IN @taxCode long varchar default null, 
        IN @doDelete char(1) default 'N')
returns numeric(10)
BEGIN
    declare @newEntityID numeric(10);
    set @newEntityID = -1;


    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- If the user tries to change from one entity to another 
    --  we need to delete the old entity and create a new one
	-- LOAN (already in P_VARIABLE)
	if ((@entityID <> '-1') and (@entityType='entityType_Loan') and (@name is not null) and 
		exists(select * from p_variable where variable_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_variable where variable_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- LOAN (already in P_SAVING)
	if ((@entityID <> '-1') and (@entityType='entityType_Loan') and (@name is not null) and 
		exists(select * from p_saving where saving_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of saving
		delete from p_saving where saving_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- SAVING (already in P_VARIABLE)
	if ((@entityID <> '-1') and (@entityType='entityType_Saving') and (@name is not null) and 
		exists(select * from p_variable where variable_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_variable where variable_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- SAVING (already in P_LOAN)
	if ((@entityID <> '-1') and (@entityType='entityType_Saving') and (@name is not null) and 
		exists(select * from p_loan where loan_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of loan
		delete from p_loan where loan_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- VARIABLE (already in P_SAVING)
	if ((@entityID <> '-1') and ((@entityType='entityType_Earning') or (@entityType='entityType_Deduction') or (@entityType='entityType_PackageComponent') or (@entityType='entityType_Interim')) and (@name is not null) and 
		exists(select * from p_saving where saving_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_saving where saving_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- VARIABLE (already in P_LOAN)
	if ((@entityID <> '-1') and ((@entityType='entityType_Earning') or (@entityType='entityType_Deduction') or (@entityType='entityType_PackageComponent') or (@entityType='entityType_Interim')) and (@name is not null) and 
		exists(select * from p_loan where loan_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of saving
		delete from p_loan where loan_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;

    -- Should we create this entity?
    if (@entityID='-1') then
        //
        // Create Loan
        //
        if (@entityType='entityType_Loan') then
            select  coalesce(max(loan_id)+1,1) into @newEntityID from p_loan;
            message('New Loan ID:'||@newEntityID);
            set @entityID = @newEntityID;
            insert into p_loan (loan_id, name, interest, COMPOUND_YN, BEFORECALC_YN) values (@entityID, @name, 0, 'N','N');
        else
            //
            // Create Saving
            //
            if (@entityType='entityType_Saving') then
                select  coalesce(max(saving_id)+1,1) into @newEntityID from p_saving;
                message('New Saving ID:'||@newEntityID);
                set @entityID = @newEntityID;
                insert into p_saving (saving_id, name, interest, COMPOUND_YN, BEFORECALC_YN) values (@entityID, @name, 0, 'N','N');
            else
                //
                // Create Variable
                //
                begin
                    select  coalesce(max(variable_id)+1,1) into @newEntityID from p_variable;
                    message('New Variable ID:'||@newEntityID);
                    set @entityID = @newEntityID;
                    insert into p_variable (variable_id, NAME, RESULT_TYPE, VARIABLE_TYPE, THEVALUE, ACTIVE_YN) 
                        values (@entityID, @name, 'EARNING', 'INPUT','0','Y');
                    -- Make sure all variables are available in all payrolls
                    insert into PL_PVARIABLE_ppayroll (payroll_id, variable_id) on existing SKIP 
                        select payroll_id, variable_id
                        from p_variable, p_payroll

                end;
            end if;
        end if;
    end if;

        
    
    -- Should we delete this entity?
    if (@doDelete='Y') then
        //
        // Delete Loan
        //
        if (@entityType='entityType_Loan') then
            delete from P_LOAN where loan_id=@entityID
        else
            //
            // Delete Saving
            //
            if (@entityType='entityType_Saving') then
                delete from P_SAVING where saving_id=@entityID
            else
                //
                // Delete Variable
                //
                delete from P_VARIABLE where variable_id=@entityID
            end if;
        end if;
    else
        begin
        //
        // Change LOANS
        //
        if (@entityType='entityType_Loan') then
            if (@name is not null) then
              update p_loan set name=@name  where loan_id=@entityID;
            end if;
            if (@caption is not null) then
              update p_loan set description=@caption  where loan_id=@entityID;
            end if;
            if (@calculationFormula is not null) then
              update p_loan set PREMIUM_FORMULA=@calculationFormula  where loan_id=@entityID;
            end if;
            if (@interest is not null) then
              update p_loan set INTEREST=convert(numeric(16,2),@interest)  where loan_id=@entityID;
            end if;
        else 
            //
            // Change SAVINGS
            //
            if (@entityType='entityType_Saving') then
                if (@name is not null) then
                  update p_saving set name=@name  where saving_id=@entityID;
                end if;
                if (@caption is not null) then
                  update p_saving set description=@caption  where saving_id=@entityID;
                end if;
                if (@calculationFormula is not null) then
                  update p_saving set PREMIUM_FORMULA=@calculationFormula  where saving_id=@entityID;
                end if;
                if (@interest is not null) then
                  update p_saving set INTEREST=convert(numeric(16,2),@interest) where saving_id=@entityID;
                end if;           
            else
                //
                // Change VARIABLES
                //
                begin
                    if (@name is not null) then
                      update p_variable set name=@name  where variable_id=@entityID;
                    end if;
                    if (@caption is not null) then
                      update p_variable set description=@caption  where variable_id=@entityID;
                    end if;
                    -- If this is a SYSTEM variable, don't touch it further!
                    if not exists(select * from p_variable where variable_id=@entityID and theOptions like '%SYSTEM%') then
                        -- Variable type-specific
                        if (@entityType is not null) then
                            // EARNING
                            if (@entityType='entityType_Earning') then
                                update p_variable set RESULT_TYPE='EARNING', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='YY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;     
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                            // DEDUCTION
                            if (@entityType='entityType_Deduction') then
                                update p_variable set RESULT_TYPE='DEDUCTION', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='YY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;         
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                            // INTERIM
                            if (@entityType='entityType_Interim') then
                                update p_variable set RESULT_TYPE='INTERIM', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='NN', SARS_CODE='' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;         
                                --update p_variable set SARS_CODE=null where variable_id=@entityID;
                            end if;
                            // PACKAGE_COMPONENT
                            if (@entityType='entityType_PackageComponent') then
                                update p_variable set RESULT_TYPE='EARNING', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='NY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;   
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                        end if;
                        if (@showOnPayslip is not null) then
                          update p_variable set DISPLAYONPAYSLIP_YN=@showOnPayslip  where variable_id=@entityID;
                        end if;
                        if (@showYtdOnPayslip is not null) then
                          update p_variable set DISPLAYYTD_YN=@showYtdOnPayslip  where variable_id=@entityID;
                        end if;
                        if (@calculationFormula is not null) then
                          update p_variable set THEVALUE=@calculationFormula, variable_type='FORMULA'  where variable_id=@entityID;
                        end if;
                        if (@payrollType is not null) then
                          update p_variable set PAYROLL_TYPE=@payrollType  where variable_id=@entityID;
                        end if;
                    end if;
           

                   
                end;
            end if;
        end if ;
        end;
    end if;

    return(@entityID); 
END;

commit;CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetPayrollData" ( 
        IN @sessionID long VARCHAR, IN @payrollID long varchar, IN @payrollName long varchar default null,
        IN @payrollType long varchar default null,IN @taxYearStart long varchar default null,  IN @taxYearEnd long varchar default null,
         IN @openPeriod long varchar default null,IN @taxCountry long varchar default null, IN @doDelete char(1) default 'N',
		 IN @taxNumber char(21) default '', IN @tradeClassificationCode char(10) default '',
		 IN @tradeClassificationSubCode char(10) default '',IN @sicCode char(10) default '')
returns varchar(10)
BEGIN
    declare @newPayrollID numeric(10);
    declare @newPayrollDetailID numeric(10);
    set @newPayrollID = -1;
    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this payroll?
    if (@payrollID='-1') then
        select  coalesce(max(payroll_id)+1,1) into @newPayrollID from p_payroll;
        message('New Payroll ID:'||@newPayrollID);
        set @payrollID = @newPayrollID;
        insert into p_payroll (payroll_id, name, description) values (@payrollID, @payrollName, @payrollName);
        -- Make sure all variables are available in all payrolls
        insert into PL_PVARIABLE_ppayroll (payroll_id, variable_id) on existing SKIP 
            select payroll_id, variable_id
            from p_variable, p_payroll
    end if;

    -- Should we delete this payroll?
    if (@doDelete='Y') then
        delete from p_payroll where payroll_id=@payrollID
    else
        -- payrollName
        if (@payrollName is not null) then
            update p_payroll set name=@payrollName  where payroll_id=@payrollID;
        end if;
        -- payrollType
        if (@payrollType is not null) then
            update p_payroll set payroll_type=@payrollType where payroll_id=@payrollID;
        end if;
        -- taxYearStart
        if (@taxYearStart is not null) then
            update p_payroll set tax_year_start=@taxYearStart  where payroll_id=@payrollID;
        end if;
        -- taxYearEnd
        if (@taxYearEnd is not null) then
            update p_payroll set tax_year_end=@taxYearEnd  where payroll_id=@payrollID;
        end if;
        -- taxCountry
        if (@taxCountry is not null) then
            if exists(select * from p_country where name=@taxCountry) then
                update p_payroll set country_id=(select country_id from p_country where name=@taxCountry)  where payroll_id=@payrollID;
            end if;
        end if;
		-- taxNumber
        update p_payroll set tax_number=@taxNumber  where payroll_id=@payrollID;
		-- tradeClassificationCode
        update p_payroll set TRADE_CLASS_ID=(SELECT TRADE_CLASS_ID FROM A_TRADE_CLASSIFICATION where TRADE_CODE = @tradeClassificationCode)  where payroll_id=@payrollID;
		-- tradeClassificationSubCode
        update p_payroll set TRADE_SUB_CODE=@tradeClassificationSubCode  where payroll_id=@payrollID;
		-- sicCode
        update p_payroll set SIC_CODE_ID=(select SIC_CODE_ID from J_SIC_CODE where SIC_CODE = @sicCode)  where payroll_id=@payrollID;
    end if;  
 
    -- Now that we've created/updated the payroll, see if we need to create the periods
    if (@newPayrollID>0) then
         message('fn_P_AutoCreatePayrollPeriods('||@newPayrollID||')');
        call fn_P_AutoCreatePayrollPeriods(@newPayrollID);
        -- Get the 'Open' period id
        select coalesce(payrolldetail_id,1) into @newPayrollDetailID from dba.p_payrolldetail where payroll_id=@newPayrollID and startdate=@openPeriod;
        message('openPeriod:'||@newPayrollDetailID);
        
        -- Open the first period
        insert into dba.P_CALC_MASTER(COMPANY_ID,PAYROLL_ID,PAYROLLDETAIL_ID,RUN_STATUS) values(
          (select company_id from web_session where session_id=@sessionID),@newPayrollID,@newPayrollDetailID,1);
        -- Open the basic run
        insert into P_CALC_DETAIL(COMPANY_ID,PAYROLL_ID,PAYROLLDETAIL_ID,RUNTYPE_ID, RUN_STATUS) values(
          (select company_id from web_session where session_id=@sessionID),@newPayrollID,@newPayrollDetailID,1,2);
        insert into P_CALC_DETAIL(COMPANY_ID,PAYROLL_ID,PAYROLLDETAIL_ID,RUNTYPE_ID, RUN_STATUS) values(
          (select company_id from web_session where session_id=@sessionID),@newPayrollID,@newPayrollDetailID,2,1);

    end if;


    return('OK'); 
END;


commit;

-------------------------------------------------------------
-- FUNCTION TO SAVE CHANGES TO TITLE DATA
-------------------------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetTitleData" ( 
        IN @sessionID long VARCHAR,IN @titleID long VARCHAR, IN @title long varchar,IN @deleted char(1) default 'N')
returns numeric(10)
BEGIN
  declare @newTitleID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return -1;
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this Title?
    
    if (@titleID ='-1') then
        if not exists(select * from WEB_LOOKUP_TITLE where trim(title)=trim(@title)) then
          select coalesce(max(title_id)+1,1) into @newTitleID from  WEB_LOOKUP_TITLE;
          insert into WEB_LOOKUP_TITLE (title_id, Title) values (@newTitleID, @title);
          set @titleID = @newTitleID
        end if
    else
        -- Should we delete this Title?
        if (@deleted ='Y') then
           delete from WEB_LOOKUP_TITLE where title_id = @titleID
        ELSE 
            update web_lookup_title set title=@title where title_id=@titleID
        end if;
    end if;


    return(@titleID); 
END;

commit;-------------------------------------------------
-- PROCEDURE TO UPDATE SELECTED COLUMNS
-- This function requires paramaters in a format 
-- that will allow the dynamic insertion/update
-- of records.
-- Example:
--   fn_HTTP_SetValue(...,"E_NUMBER","COMPANY_ID, EMPLOYEE_ID, ID_NUMBER", "23012,1002, 7007204324323") 
-------------------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_SETVALUE ( IN @sessionid  long VARCHAR, IN @tablename long varchar, IN @columlist long varchar, IN @valuelist long varchar )
returns varchar(10)
BEGIN
    DECLARE @Command LONG VARCHAR;

    -- If this session doesn't exist, return empty-handed
    if not exists(select * from WEB_SESSION where session_id=@sessionid) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    SET @Command = 'INSERT INTO ' || @tablename || ' (' || @columlist || ') on existing update values (' || @valuelist || ');';
    EXECUTE IMMEDIATE @Command;
    return 'OK';
END;
comment on procedure dba.fn_HTTP_SETVALUE is 'Inserts/updates values in a given table with the appropriate columnlist, valuelist pairs.\nE.g. fn_HTTP_SetValue(...,"E_NUMBER","COMPANY_ID, EMPLOYEE_ID, ID_NUMBER", "23012,1002, 7007204324323")';

commit;
--------------------------------------
-- PROCEDURE TO TOUCH SESSION LIVENESS
--------------------------------------
create or replace function dba.fn_HTTP_TouchSession( IN @sessionid  long VARCHAR ) 
returns char(10)
BEGIN
  update dba.WEB_SESSION set update_time=now() where session_id=@sessionid;
return 'OK';
END;
CREATE OR REPLACE FUNCTION "DBA"."fn_P_TC_SouthAfrica_DoTaxCertificate"(in @COMPANY_ID numeric(10),in @PAYROLL_ID numeric(10),in @LIVE_INDICATOR char(4),
in @CONTACT_ID numeric(10),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),in @TYPE_ID numeric(10) default 1,
in @TaxYearEnd date default null,in @CompanyName char(120) default null,in @TaxNumber char(20) default null,
in @SDLNumber char(20) default null,in @UIFNumber char(20) default null,in @TradeCode char(4) default null,in @Run_Id numeric(10) default null)
returns numeric(10)
/*  
<purpose>  
fn_P_TC_SouthAfrica_DoTaxCertificate is called by the Tax Certificate creation
menu in PeopleWare to prepare and validate the data for tax certificates.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2008/05/10  6560    Christo Krause  Move IRP5 creation to stored procedure
</history>
*/
begin
  declare @ResultMessage varchar(150);
  declare @TaxYearStart date;
  declare @Mandatory bit;
  declare @FirstChar char(1);
  declare @ContactName char(30);
  declare @ContactTel char(20);
  declare @Email char(70);
  declare @IsValid bit;
  declare @UIFMatch bit;
  declare @PAYEMatch bit;
  declare @AtPosition tinyint;
  declare @TransactionYear numeric(4);
  declare @ReconPeriod char(6);
  declare @ReconDate date;
  declare @DomainPosition tinyint;
  declare @ExceptionCharacters char(120);
  declare @AddressSaved bit;
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  set @Debug_On=1;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_DoTaxCertificate ... ')
  end if;
  -- Load Payroll Information
  if @TaxYearEnd is null then
    select "DATE"(TAX_YEAR_START),"DATE"(TAX_YEAR_END) into @TaxYearStart,@TaxYearEnd from
      P_PAYROLL where PAYROLL_ID = @Payroll_Id
  else
    select "DATE"(TAX_YEAR_START) into @TaxYearStart from
      P_PAYROLL where PAYROLL_ID = @Payroll_Id
  end if;
  -- Start Copying .. 
  set @LIVE_INDICATOR=upper(@LIVE_INDICATOR);
  if @TYPE_ID = 4 then
    -- ITREG
    set @LIVE_INDICATOR='LIVE';
    set @RECONCILIATION_YEAR=0;
    set @RECONCILIATION_MONTH=0
  end if;
  -- List Company Header
  set @Run_Id=fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues(@COMPANY_ID,@PAYROLL_ID,@LIVE_INDICATOR,@CONTACT_ID,
    @RECONCILIATION_YEAR,@RECONCILIATION_MONTH,@TYPE_ID,@TaxYearEnd,@CompanyName,@TaxNumber,@SDLNumber,@UIFNumber,@TradeCode,@Run_Id);
  if @Run_Id is not null then
    if @TYPE_ID = 1 then
      -- CERTIFICATE TYPE = Standard
      select fn_P_TC_SouthAfrica_ListFileCodeValues(@Run_Id,@COMPANY_ID,E.EMPLOYEE_ID,@PAYROLL_ID,@LIVE_INDICATOR,
        @RECONCILIATION_YEAR,@RECONCILIATION_MONTH,@TYPE_ID,@TaxYearEnd) into #E from
        PL_Paydef_EMaster as P join P_E_Master as E on P.COMPANY_ID = E.COMPANY_ID and P.EMPLOYEE_ID = E.EMPLOYEE_ID where
        P.COMPANY_ID = @COMPANY_ID and PAYROLL_ID = @PAYROLL_ID and(E.IRP5_TYPE = 'S' or E.IRP5_TYPE is null) and
        fn_E_EmployeeActiveInRange_YN(E.COMPANY_ID,E.EMPLOYEE_ID,@TaxYearStart,@TaxYearEnd) = 'Y' order by
        E.EMPLOYEE_ID asc
    end if;
    if @TYPE_ID = 4 then
      -- CERTIFICATE TYPE = ITREG
      select fn_P_TC_SouthAfrica_ListFileCodeValues(@Run_Id,@COMPANY_ID,E.EMPLOYEE_ID,@PAYROLL_ID,@LIVE_INDICATOR,
        @RECONCILIATION_YEAR,@RECONCILIATION_MONTH,@TYPE_ID,@TaxYearEnd) into #E from
        PL_Paydef_EMaster as P join P_E_Master as E on P.COMPANY_ID = E.COMPANY_ID and P.EMPLOYEE_ID = E.EMPLOYEE_ID where
        P.COMPANY_ID = @COMPANY_ID and PAYROLL_ID = @PAYROLL_ID and E.IRP5_TYPE = 'R' order by
        E.EMPLOYEE_ID asc
    end if;
    if exists(select* from P_E_TaxCertificate_EMPLOYEE_REJECTION where FILE_ID = 
        any(select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID)) then
      update P_C_TAXCERTIFICATE_RUN set Run_Status = 1 where Run_id = @Run_Id
    end if; 
	if @LIVE_INDICATOR = 'LIVE' then
      update P_C_TAXCERTIFICATE_RUN set Run_Status = 3 where Run_id = @Run_Id
    else
      update P_C_TAXCERTIFICATE_RUN set Run_Status = 2 where Run_id = @Run_Id
    end if;
    if @Debug_On = 1 then
      message('Listing Employer Trailer values ... ')
    end if;
    set @IsValid=fn_P_TC_SouthAfrica_ListCoTrailerFileCodeValues(@Run_Id,@COMPANY_ID,@PAYROLL_ID,@LIVE_INDICATOR,
      @RECONCILIATION_YEAR,@RECONCILIATION_MONTH,@TYPE_ID,@TaxYearEnd)
  end if;
  return(@Run_Id)
end;
CREATE OR REPLACE FUNCTION "DBA"."fn_P_TC_SouthAfrica_FooterOutputRecord"(in @RUN_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_TC_SouthAfrica_FooterOutputRecord is called from the South African tax 
DLL to create the electronic file output record for the company Footer 
according to the formatting requirements specified by SARS.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/07/06  14705   Francois        Move Tax Certificate extract creation to a function so that PeopleWeb can use it.
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @TaxNumber char(21);
  declare @Counter integer;
  declare @IsValid bit;
  declare @IsStart bit;
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  declare DetailCursor dynamic scroll cursor for select R.FILE_CODE, R.VALUE
    from P_C_TAXCERTIFICATE_EMPLOYER_TRAILER as R
    where R.RUN_ID = @RUN_ID
    order by R.FILE_CODE;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_FooterOutputRecord ... ') to console
  end if;
  
  set @IsStart = 1;
  -- 1 Loop over the rows of the query
  open DetailCursor;
    DetailCode_LOOP: loop
      --
      --
      fetch next DetailCursor into @FileCode,@CodeValue;
      --
      if sqlstate = err_notfound then
        leave DetailCode_LOOP
      end if;
      -- 
      if @Debug_On = 1 then
        message('FileRecord = '||@FileRecord ||' FileCode = '||@FileCode||' CodeValue = '||@CodeValue) to console
      end if;
      -- 
      if @FileCode <> 9999 then
		if @IsStart = 1 then 
			set @FileRecord = @FileCode ||','|| @CodeValue;
			set @IsStart = 0;
		else
			set @FileRecord = @FileRecord ||','|| @FileCode ||','|| @CodeValue;
		end if;
      end if;
	  
	  set @FileCode = null;
      set @CodeValue = null;
    end loop DetailCode_LOOP;
  close DetailCursor;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_FooterOutputRecord: DetailCursor done ... ') to console
  end if;
  -- 2 Add the end code
  set @FileRecord = @FileRecord || ',9999';
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;CREATE OR REPLACE FUNCTION "DBA"."fn_P_TC_SouthAfrica_HeaderOutputRecord"(in @PAYROLL_ID numeric(10), in @RUN_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_TC_SouthAfrica_HeaderOutputRecord is called from the South African tax 
DLL to create the electronic file output record for the company Heading 
according to the formatting requirements specified by SARS.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/07/06  14705   Francois        Move Tax Certificate extract creation to a function so that PeopleWeb can use it.
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @FieldType char(10);
  declare @TaxNumber char(21);
  declare @Counter integer;
  declare @IsValid bit;
  declare @IsStart bit;
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  declare DetailCursor dynamic scroll cursor for select R.FILE_CODE, R.VALUE, C.FIELD_TYPE 
    from P_C_TAXCERTIFICATE_EMPLOYER_HEADER as R join P_SARS_CODE as C on R.FILE_CODE = C.SARS_CODE 
    where R.RUN_ID = @RUN_ID and COUNTRY_ID = 2 and C.RECORD_TYPE = 'Header'
    order by C.FILE_SEQUENCE,R.FILE_CODE;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_HeaderOutputRecord ... ') to console
  end if;
  
  set @IsStart = 1;
  -- 1 Loop over the rows of the query
  open DetailCursor;
    DetailCode_LOOP: loop
      --
      --
      fetch next DetailCursor into @FileCode,@CodeValue,@FieldType;
      --
      if sqlstate = err_notfound then
        leave DetailCode_LOOP
      end if;
      -- 
      if length(@CodeValue) > 0 then
        if @FieldType like 'A%' or @FieldType like 'F%' then
          set @CodeValue = '"'||@CodeValue||'"'
        end if;
        if @Debug_On = 1 then
          message('FileRecord = '||@FileRecord ||' FileCode = '||@FileCode||' CodeValue = '||@CodeValue) to console
        end if;
        -- 
        if @FileCode <> 9999 then
			if @IsStart = 1 then 
				set @FileRecord = @FileCode ||','|| @CodeValue;
				set @IsStart = 0;
			else
				set @FileRecord = @FileRecord ||','|| @FileCode ||','|| @CodeValue;
			end if;
        end if;
      end if;
	  -- Add the Tax, SDL and UIF numbers
	  if @FileCode = 2015 then
		set @TaxNumber = (select TAX_NUMBER from P_PAYROLL where PAYROLL_ID = @PAYROLL_ID);
		set @FileRecord = @FileRecord || ',2020,' || @TaxNumber || ',2022,"L' || substring(@TaxNumber,2,9) || '",2024,"U' || substring(@TaxNumber,2,9) || '"';
      end if;
      set @FileCode = null;
      set @CodeValue = null;
    end loop DetailCode_LOOP;
  close DetailCursor;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_HeaderOutputRecord: DetailCursor done ... ') to console
  end if;
  -- 2 Add the end code
  set @FileRecord = @FileRecord || ',9999';
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;// requires: "fn_P_TC_SouthAfrica_ListRun.txt","fn_P_TC_ListSpecifiedCharactersInANField.txt","fn_P_TC_RemoveListedCharactersFromString.txt","fn_P_TC_SouthAfrica_ListCoAddressFileCodeValues.txt","fn_P_TC_SouthAfrica_ListFileCodeValues.txt","fn_P_TC_SouthAfrica_ListCoTrailerFileCodeValues.txt","fn_P_TC_SouthAfrica_ListTaxCodeChangeWarnings.txt"

create or replace function "DBA"."fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues"( in @COMPANY_ID numeric(10),in @PAYROLL_ID numeric(10),in @LIVE_INDICATOR char(4),in @CONTACT_ID numeric(10),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),
  in @TYPE_ID numeric(10) default 1,in @TaxYearEnd date default null,
  in @CompanyName char(120) default null,in @TaxNumber char(21) default null,in @SDLNumber char(50) default null,in @UIFNumber char(20) default null,
  in @TradeCode char(4) default null,in @Run_Id numeric(10) default null ) 
returns numeric(10)
/*  
<purpose>  
fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues is called by the Tax Certificate
creation menu in PeopleWare to prepare and validate the data for certificates.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2008/05/10  6560    Christo Krause  Move IRP5 creation to stored procedure
2010/04/14  9327    Christo Krause  Fix the validation of Reconciliation periods for wages payrolls
2010/10/19  9002    Christo Krause  Include changes made to the tax codes of variables in the warnings
2010/10/19  13319   Christo Krause  Generate tax certificates for previous tax years
2013/08/21  13745   Christo Krause  Prevent right truncation errors
2014/02/25  14135   Christo Krause  ETI and SIC codes requirements
2014/03/28  13886   Christo Krause  SIC codes did not write to the file
2014/05/19  13772   Christo Krause  Use PAG membership number in the software field
2015/07/08  14705   Francois        PeopleWeb Tax Certificates need to format UIF and SDL numbers from the TAX number
</history>
*/
begin
  declare @ResultMessage varchar(150);
  declare @Archive bit;
  declare @TaxYearStart date;
  declare @PayrollTaxYear date;
  declare @Mandatory bit;
  declare @FirstChar char(1);
  declare @ContactName char(100);
  declare @ContactTel char(20);
  declare @SIC_Code char(10);
  declare @Email char(250);
  declare @IsValid bit;
  declare @UIFMatch bit;
  declare @UIFRequired bit;
  declare @PAYEMatch bit;
  declare @AtPosition tinyint;
  declare @TransactionYear numeric(4);
  declare @ReconPeriod char(6);
  declare @ReconDate date;
  declare @DomainPosition tinyint;
  declare @ExceptionCharacters char(120);
  declare @WarningString char(2500);
  declare @AddressSaved bit;
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues ... ') to console
  end if;
  //
  // Load Company Information
  //
  select isnull(trim(@CompanyName),trim(NAME)),isnull(trim(@TaxNumber),trim(TAX_NUMBER)),
    isnull(trim(@SDLNumber),trim(SDL_REGISTRATION_NUMBER)),isnull(trim(@UIFNumber),trim(UIF_NUMBER)),
    trim(EMAIL),isnull(trim(@TradeCode),trim(TRADE_SUB_CODE)) into @CompanyName,
    @TaxNumber,@SDLNumber,@UIFNumber,@Email,@TradeCode from C_MASTER where COMPANY_ID = @COMPANY_ID;
  //  
  // Load missing reference numbers from Payroll
  //
  select isnull(@TaxNumber,trim(TAX_NUMBER)),isnull(@UIFNumber,trim(UIF_NUMBER)),
    isnull(@TradeCode,trim(TRADE_SUB_CODE)) into @TaxNumber,
    @UIFNumber,@TradeCode from P_PAYROLL where PAYROLL_ID = @PAYROLL_ID;

  if exists (select * from DBA.a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
    set @UIFNumber = 'U'||substring(@TaxNumber,2,9);
    set @SDLNumber = 'L'||substring(@TaxNumber,2,9);
  end if;

  //  
  // Load Payroll Information
  //
  if @TaxYearEnd is null then
    select "DATE"(TAX_YEAR_START),"DATE"(TAX_YEAR_END) into @TaxYearStart,@TaxYearEnd
      from P_PAYROLL where PAYROLL_ID = @Payroll_Id
  else
    if(select datediff(month,@TaxYearEnd,Tax_Year_End) from P_Payroll where payroll_id = @PAYROLL_ID) > 6 then
      set @Archive = 1
    else
      set @Archive = 0
    end if;
    if @Archive = 0 then
      select "DATE"(TAX_YEAR_START),"DATE"(TAX_YEAR_END) into @TaxYearStart,@PayrollTaxYear
        from P_PAYROLL where PAYROLL_ID = @Payroll_Id
    else
      select first "DATE"(TAX_YEAR_START),"DATE"(TAX_YEAR_END) into @TaxYearStart,@PayrollTaxYear
        from P_PAYROLL_OLD where PAYROLL_ID = @Payroll_Id
        order by abs(datediff(day,@TaxYearEnd,TAX_YEAR_END)) asc
    end if
  end if;
  if @PAYROLL_ID is null and @TaxYearEnd is null then
    return null;
    -- Can not store a message without and Run_Id
    call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2030,'The Tax Year End has to be specified if the payroll_id is not specified.',
    @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
  end if;
  if @TaxYearStart is null and @TaxYearEnd is not null then
    set @TaxYearStart = dateadd(dd,1,dateadd(yy,-1,@TaxYearEnd))
  end if;
  if @RECONCILIATION_YEAR is null then
    if @PAYROLL_ID is not null then
      select first datepart(yy,MonthEnd) into @RECONCILIATION_YEAR from P_Payrolldetail
        where Payroll_Id = @PAYROLL_ID and datepart(mm,MonthEnd) = @RECONCILIATION_MONTH
    else
      if @RECONCILIATION_MONTH > 2 then
        set @RECONCILIATION_YEAR = datepart(yy,dateadd(yy,-1,@TaxYearEnd))
      else
        set @RECONCILIATION_YEAR = datepart(yy,@TaxYearEnd)
      end if
    end if end if;
  // Start Copying .. 
  set @LIVE_INDICATOR = upper(@LIVE_INDICATOR);
  if @TYPE_ID = 4 then
    --
    -- ITREG
    --
    set @LIVE_INDICATOR = 'LIVE';
    set @RECONCILIATION_YEAR = 0;
    set @RECONCILIATION_MONTH = 0
  end if;
  if @Debug_On = 1 then
    message('  Deleting existing company File Code values ... ') to console
  end if;
  --
  -- Get the run_id
  --
  if @Run_Id is null then
    set @Run_Id = fn_P_TC_SouthAfrica_ListRun(@COMPANY_ID,@PAYROLL_ID,@TYPE_ID,@TaxYearEnd,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
  end if;
  if @Run_Id is not null then
    --
    -- Clear Previous Entries
    --
    delete from P_C_TaxCertificate_Employer_Rejection where RUN_ID = @Run_Id;
    delete from P_C_TaxCertificate_Employer_Header where RUN_ID = @Run_Id;
    delete from P_C_TaxCertificate_Employer_Trailer where RUN_ID = @Run_Id;
    delete from P_E_TaxCertificate_EMPLOYEE_REJECTION where FILE_ID
       = any(select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID);
    delete from P_E_TaxCertificate_EMPLOYEE_WARNING where FILE_ID
       = any(select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID);
    delete from P_E_TaxCertificate_Employee_Record where FILE_ID
       = any(select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID);
    delete from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID;
    if @Debug_On = 1 then
      message('Listing Company File Code values ... ') to console
    end if;
    -- Code 2010 Trading Name
    if @CompanyName is not null then
      set @ExceptionCharacters = fn_P_TC_ListSpecifiedCharactersInANField(@CompanyName,'/\\*?":><|');
      if @ExceptionCharacters is not null then
        set @CompanyName = fn_P_TC_RemoveListedCharactersFromString(@CompanyName,@ExceptionCharacters)
      end if;
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2010,@CompanyName,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2010,'Company name can not be null.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2015 ... ') to console
    end if;
    -- Code 2015 Test/Live indicator
    if @LIVE_INDICATOR in( 'TEST','LIVE' ) then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2015,@LIVE_INDICATOR,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2015,'Test/Live indicator can not be null.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2020 ... ') to console
    end if;
    -- Code 2020 PAYE Reference Number
    set @IsValid = 0;
    if length(@TaxNumber) = 10 then
      set @FirstChar = substring(@TaxNumber,1,1);
      if @FirstChar = '7' then
        -- Replace first digit with a 4 for Modulus 10 check
        set @IsValid = fn_S_Modulus10Test(4 || substring(@TaxNumber,2,9))
      else
        if @FirstChar in( '0','1','2','3','9' ) then
          set @IsValid = fn_S_Modulus10Test(@TaxNumber)
        end if
      end if end if;
    if @IsValid = 1 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2020,@TaxNumber,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2020,'"' || @TaxNumber || '" is not a valid PAYE Reference Number.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2022 ... ') to console
    end if;
    -- Code 2022 SDL Reference Number
    set @IsValid = 0;
    if length(@SDLNumber) = 10 then
      if length(@TaxNumber) = 10 and @FirstChar = '7' then
        if substring(@TaxNumber,2,9) = substring(@SDLNumber,2,9) then
          set @PAYEMatch = 1
        else
          set @PAYEMatch = 0
        end if
      else set @PAYEMatch = 1
      end if;
      if length(@UIFNumber) = 10 then
        if substring(@UIFNumber,2,9) = substring(@SDLNumber,2,9) then
          set @UIFMatch = 1
        else
          set @UIFMatch = 0
        end if
      else set @UIFMatch = 1
      end if;
      if substring(@SDLNumber,1,1) = 'L' then
        -- Replace first digit with a 4 for Modulus 10 check
        set @IsValid = fn_S_Modulus10Test(4 || substring(@SDLNumber,2,9))
      end if
    else set @UIFMatch = 1
    end if;
    if @IsValid = 1 and @PAYEMatch = 1 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2022,@SDLNumber,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      if not exists(select * from C_SETUP where COMPANY_ID = @COMPANY_ID
          and upper(ITEM) = 'EXEMPT FROM SDL' and upper(DESCRIPTION) = 'Y') then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2022,'"' || @SDLNumber || '" is not a valid SDL Reference Number.',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      end if end if;
    if @Debug_On = 1 then
      message('  Listing 2024 ... ') to console
    end if;
    -- Code 2024 UIF Reference Number
    set @IsValid = 0;
    if length(@UIFNumber) = 10 then
      if length(@TaxNumber) = 10 and @FirstChar = '7' then
        if substring(@TaxNumber,2,9) = substring(@UIFNumber,2,9) then
          set @PAYEMatch = 1
        else
          set @PAYEMatch = 0
        end if
      else set @PAYEMatch = 1
      end if;
      if substring(@UIFNumber,1,1) = 'U' then
        -- Replace first digit with a 4 for Modulus 10 check
        set @IsValid = fn_S_Modulus10Test(4 || substring(@UIFNumber,2,9))
      end if end if;
    if @IsValid = 1 and @PAYEMatch = 1 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2024,@UIFNumber,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      if length(@UIFNumber) > 0 then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2024,'"' || @UIFNumber || '" is not a valid UIF Reference Number.',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      else
        if @PAYROLL_ID is null then
          if exists(select * from P_YTDCHANGE as P join P_VARIABLE as V on P.VARIABLE_ID = V.VARIABLE_ID
              where(V.IND_CODE like '%99010%' or V.IND_CODE like '%99011%') and P.COMPANY_ID = @COMPANY_ID)
            or exists(select * from P_VARIABLES_FOR_EMPLOYEE as P join P_VARIABLE as V on P.VARIABLE_ID = V.VARIABLE_ID
              where(V.IND_CODE like '%99010%' or V.IND_CODE like '%99011%') and P.COMPANY_ID = @COMPANY_ID) then
            set @UIFRequired = 1
          end if
        else if exists(select * from P_YTDCHANGE as P join P_VARIABLE as V on P.VARIABLE_ID = V.VARIABLE_ID
              where(V.IND_CODE like '%99010%' or V.IND_CODE like '%99011%') and P.COMPANY_ID = @COMPANY_ID and P.PAYROLL_ID = @PAYROLL_ID)
            or exists(select * from P_VARIABLES_FOR_EMPLOYEE as P join P_VARIABLE as V on P.VARIABLE_ID = V.VARIABLE_ID
              where(V.IND_CODE like '%99010%' or V.IND_CODE like '%99011%') and P.COMPANY_ID = @COMPANY_ID and P.PAYROLL_ID = @PAYROLL_ID) then
            set @UIFRequired = 1
          end if end if;
        if @UIFRequired = 1 then
          call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2024,'A UIF Reference Number is mandatory.',
          @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
        end if
      end if end if;
    if @UIFMatch = 0 then
      if @IsValid = 1 then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2024,'"' || @SDLNumber || '" does not match UIF Reference Number "' || @UIFNumber || '".',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      else
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2022,'"' || @UIFNumber || '" does not match SDL Reference Number "' || @SDLNumber || '".',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      end if end if;
    -- Codes 2025 to 2027 
    select isnull(trim(NAME),''),isnull(trim(TEL_NUMBER),''),isnull(trim(E_MAIL),@Email) into @ContactName,
      @ContactTel,@Email from C_CONTACT where COMPANY_ID = @COMPANY_ID and CONTACT_ID = @CONTACT_ID;
    -- Code 2025 Contact Name (Mandatory)
    set @ExceptionCharacters = fn_P_TC_ListNonAlphaTypeCharactersInField(@ContactName);
    if @ExceptionCharacters is not null then
      set @ContactName = fn_P_TC_RemoveListedCharactersFromString(@ContactName,@ExceptionCharacters)
    end if;
    if length(@ContactName) > 0 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2025,@ContactName,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2025,'Contact name can not be null.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2026 ... ') to console
    end if;
    -- Code 2026 Contact Number (Mandatory)
    set @IsValid = 0;
    if length(@ContactTel) > 0 then
      if locate(@ContactTel,'/') > 0 then
        set @WarningString = 'The contact telephone number "' || @ContactTel || '" is not valid. Only one phone number can be specified per field, "/" cannot be used.'
      else
        set @ContactTel = fn_S_RemoveNonNumericFromString(@ContactTel);
        if length(@ContactTel) between 9 and 10 then
          call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2026,@ContactTel,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
          set @IsValid = 1
        else
          if length(@ContactTel) < 9 then
            set @WarningString = 'The contact telephone number "' || @ContactTel || '" must be at least 9 numeric characters long.'
          end if;
          if length(@ContactTel) > 10 then
            set @WarningString = 'The contact telephone number "' || @ContactTel || '" cannot be more than 10 characters long.'
          end if
        end if
      end if
    else set @WarningString = 'The contact telephone number cannot be null.'
    end if;
    if @IsValid = 0 then
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2026,@WarningString,
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2027 ... ') to console
    end if;
    -- Code 2027 (Optional)
    if length(@Email) between 1 and 70 then
      set @AtPosition = locate(@Email,'@',1);
      if @AtPosition > 1 then
        set @DomainPosition = locate(@Email,'.',@AtPosition);
        if(@DomainPosition > 1) and(length(substring(@Email,@DomainPosition)) > 2) then
          call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2027,@Email,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
        end if
      end if end if;
    -- Code 2028 (Optional)
    call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2028,'PAG1001',@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
    if @Debug_On = 1 then
      message('  Listing 2030, 2031 ... ') to console
    end if;
    -- Codes 2030 to 2031 
    if @TYPE_ID <> 4 then
      -- Not for ITREG
      if @PayrollTaxYear is not null and year(@TaxYearEnd) <> year(@PayrollTaxYear) then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2030,'The payroll tax year end "' || @PayrollTaxYear || '" does not match the specified tax year end "' || @TaxYearEnd || '".',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      else
        set @TransactionYear = year(@TaxYearEnd);
        if @TransactionYear between 1999 and(year(today())+1) then
          call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2030,@TransactionYear,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
        else
          call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2030,'The Transaction Year is "' || @TransactionYear || '". It may only be between 1999 and next year.',
          @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
        end if end if;
      set @IsValid = 0;
      set @ReconPeriod = @RECONCILIATION_YEAR || "right"('00' || @RECONCILIATION_MONTH,2);
      if length(@ReconPeriod) = 6 then
        set @ReconDate = @RECONCILIATION_YEAR || '-' || "right"('00' || @RECONCILIATION_MONTH,2) || '-01';
        if @ReconDate between @TaxYearStart and @TaxYearEnd then
          call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2031,@ReconPeriod,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
          set @IsValid = 1
        end if end if;
      if @IsValid = 0 then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2031,'"' || @ReconPeriod || '" is an invalid Period of Reconciliation.',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      end if end if;
    if @Debug_On = 1 then
      message('  Listing 2035 ... ') to console;
      message('  TradeCode = "' || @TradeCode || '"') to console
    end if;
    -- Code 2082
    select SIC_Code into @SIC_Code from J_SIC_CODE where SIC_CODE_ID =
      (select SIC_CODE_ID from C_Master where COMPANY_ID = @COMPANY_ID);
    if length(trim(@SIC_Code)) = 5 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2082,@SIC_Code,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2082,'"' || @SIC_Code || '" is an invalid Standard Industry Classification code.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    -- Code 2035
    set @TradeCode = fn_S_RemoveNonNumericFromString(@TradeCode);
    if length(trim(@TradeCode)) = 4 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2035,@TradeCode,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2035,'"' || @TradeCode || '" is an invalid Trade Classification.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    -- Codes 2061 to 2080
    if @Debug_On = 1 then
      message('  Listing Company Address File Code values ... ') to console
    end if;
    set @AddressSaved = fn_P_TC_SouthAfrica_ListCoAddressFileCodeValues(@Run_Id,
      @COMPANY_ID,@PAYROLL_ID,@TYPE_ID,@LIVE_INDICATOR,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
    -- End of Header record
    call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,9999,null,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
    if @Debug_On = 1 then
      message('  Wrote company header values ... ') to console
    end if;
    if exists(select * from P_C_TAXCERTIFICATE_Employer_Rejection where RUN_ID = @RUN_ID) then
      update P_C_TAXCERTIFICATE_RUN set Run_Status = 1,PROCESSED = now() where Run_id = @Run_Id;
      if @Debug_On = 1 then
        message('  Updated PROCESSED and RUN_STATUS ... ') to console
      end if
    else set @IsValid = fn_P_TC_SouthAfrica_ListTaxCodeChangeWarnings(@RUN_ID,@COMPANY_ID,@PAYROLL_ID,@TaxYearStart,@TaxYearEnd)
    end if end if;
  return(@Run_Id)
end;

commit;

comment on procedure fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues is "<h2>Purpose</h2><br/>Compiles the company header data for the South African Tax Certificates.";
// requires: "fn_S_RemoveNonNumericFromString.txt","sp_P_TC_SoutAfrica_WriteEeWarning.txt","sp_P_TC_SoutAfrica_WriteEeRecord.txt","sp_P_TC_SoutAfrica_WriteEeRejection.txt"

create or replace function "DBA"."fn_P_TC_SouthAfrica_ListEEContactFileCodeValues"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10),in @TYPE_ID numeric(10),in @TaxYearEnd date,in @NOP char(1),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),in @FILE_ID numeric(10))
returns bit
/*  
<purpose>  
fn_P_TC_SouthAfrica_ListEEContactFileCodeValues is called by the South African
tax certificate functions to retrieve the File code values for the codes 
3125 to 3138 for the employee and save it in P_E_TAXCERTIFICATE_Employee_Record.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2009/08/27  6021    Christo Krause  Move IRP5 creation to stored procedure
2013/08/21  13745   Christo Krause  Prevent right truncation errors
2015/07/14  14705   Francois        Tax Certificates for PeopleWeb need the business address from C_CONTACT
</history>
*/
begin
  declare @AddressSaved bit;
  declare @Mandatory bit;
  declare @WarningString char(2500);
  declare @Email char(250);  --  3125 FT70
  declare @HomeTelephone char(35);  -- 3135 N11
  declare @BusTelephone char(35);   -- 3136 N11
  declare @Fax char(35);    --  3137 N11
  declare @Cell char(20);   -- 3138 N10
  declare @ValidString bit;
  declare @AtPosition tinyint;
  declare @DomainPosition tinyint;
  -- Employee Contact details
  select isnull(trim(TEL_HOME),''),isnull(trim(TEL_WORK),''),isnull(trim(FAX_HOME),trim(FAX_WORK)),isnull(trim(TEL_CELL),''),
    isnull(trim(E_MAIL),'') into @HomeTelephone,@BusTelephone,@Fax,@Cell,@Email from 
    E_CONTACT where COMPANY_ID= @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID;
  -- PeopleWeb needs to get the business details from C_ADDRESS
  if exists (select * from DBA.a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
    select isnull(trim(TEL_NUMBER),'') into @BusTelephone from 
      C_CONTACT where COMPANY_ID= @COMPANY_ID;
  end if;
  -- 3125 E-mail (Optional)
  if length(@Email) between 1 and 70 then
    set @AtPosition = locate(@Email,'@',1);
    if @AtPosition > 1 then
      set @DomainPosition = locate(@Email,'.',@AtPosition);
      if (@DomainPosition > 1) and (length(substring(@Email,@DomainPosition)) > 2) then
        call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3125,@Email,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
      else
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3125,'The E-mail address must have a domain.');
      end if
    else
      call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3125,'The E-mail address must have an @ sign.');
    end if;
  end if;
  -- 3135 Home telephone (Optional)
  if length(@HomeTelephone) > 0 then
    if locate(@HomeTelephone,'/') > 0 then
      call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3135,'The home telephone number "'|| @HomeTelephone ||'" will not be used. Only one phone number can be specified per field, "/" cannot be used.');
    else
      set @HomeTelephone = fn_S_RemoveNonNumericFromString(@HomeTelephone);
      if length(@HomeTelephone) between 9 and 11 then
        call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3135,@HomeTelephone,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
      else
        if length(@HomeTelephone) < 9 then
          call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3135,'The home telephone number "'|| @HomeTelephone ||'" must be at least 9 numeric characters long.');
        end if;
        if length(@HomeTelephone) > 11 then
          call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3135,'The home telephone number "'|| @HomeTelephone ||'" will not be used, only 11 characters can be used.');
        end if;
      end if;
    end if;
  end if;
  -- 3136 Business telephone (Mandatory except for nature of person N)
  set @ValidString =0;
  if length(@BusTelephone) > 0 then
    set @ValidString =1;
    if locate(@BusTelephone,'/') > 0 then
      set @WarningString = 'The business telephone number "'|| @BusTelephone ||'" is not valid. Only one phone number can be specified per field, "/" cannot be used.';
      set @ValidString =0;
    else
      set @BusTelephone = fn_S_RemoveNonNumericFromString(@BusTelephone);
      if length(@BusTelephone) between 9 and 11 then
        call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3136,@BusTelephone,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
        set @AddressSaved = 1;
      else
        set @ValidString =0;
        if length(@BusTelephone) < 9 then
          set @WarningString = 'The business telephone number "'|| @BusTelephone || '" must be at least 9 characters long.'
        end if;
        if length(@BusTelephone) > 11 then
          set @WarningString = 'The business telephone number "'|| @BusTelephone || '" cannot be more than 11 characters long.'
        end if;
      end if;
    end if;
  end if;
  if @ValidString =0 then
    set @AddressSaved = 0;
    if @NOP <> 'N' then
      set @Mandatory = 1;
      if @WarningString is null then
        set @WarningString = 'The business telephone number is mandatory.'
      end if;
    else
      set @Mandatory = 0;
    end if;
    if @Mandatory = 1 then
      if  year(@TaxYearEnd) < '2011' then
        set @Mandatory = 0;
      end if;
    end if;
    if @Mandatory = 1 then
      call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3136,@WarningString)
    else
      if @WarningString is not null then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3136,@WarningString);
      end if;
    end if
  end if;
  -- 3137 Fax number (Optional)
  if @Fax is not null then
    if locate(@Fax,'/') > 0 then
      call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3137,'The fax number "'|| @Fax ||'" will not be used. Only one number can be specified per field, "/" cannot be used.');
    end if;
    set @Fax = fn_S_RemoveNonNumericFromString(@Fax);
    if length(@Fax) between 9 and 11 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3137,@Fax,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    else
      if length(@Fax) < 9 then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3137,'The fax number "'|| @Fax ||'" must be at least 9 numeric characters long.');
      end if;
      if length(@Fax) > 11 then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3137,'The fax number "'|| @Fax ||'" will not be used, only 11 characters can be used.');
      end if;
    end if;
  end if;
  -- 3138 Cell phone (Optional)
  if length(@Cell) > 0 then
    if locate(@Cell,'/') > 0 then
      call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3138,'The cell phone number "'|| @Cell ||'" will not be used. Only one cell phone number can be specified per field, "/" cannot be used.');
    else
      set @Cell = fn_S_RemoveNonNumericFromString(@Cell);
      if length(@Cell) = 10 then
        call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3138,@Cell,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
      else
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3138,'The cell phone number "'|| @Cell ||'" must be 10 numeric characters long.');
      end if;
    end if;
  end if;
  return(@AddressSaved);
end;

commit;

comment on procedure fn_P_TC_SouthAfrica_ListEEContactFileCodeValues is "<h2>Purpose</h2><br/>Compiles the employee contact details for the South African Tax Certificates.";
// requires: "sp_P_TC_SoutAfrica_WriteEeWarning.txt","sp_P_TC_SoutAfrica_WriteEeRecord.txt","sp_P_TC_SoutAfrica_WriteEeRejection.txt","fn_S_RemoveNonAlphaNumericFromString.txt"

create or replace function "DBA"."fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10),in @TYPE_ID numeric(10),in @TaxYearEnd date,in @NOP char(1),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),in @FILE_ID numeric(10))
returns bit
/*  
<purpose>  
fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues  is called by the South African
tax certificate functions to retrieve the File code values for the codes 
3144 to 3150 for the employee and save it in P_E_TAXCERTIFICATE_Employee_Record.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2009/09/07  6021    Christo Krause  Move IRP5 creation to stored procedure
2011/07/19  12568   Christo Krause  Remove special characters from the tax certificate
2013/08/21  13745   Christo Krause  Prevent right truncation errors
2015/07/08  14705   Francois        PeopleWeb Tax Certificates need to use company work address
</history>
*/
begin
  declare @AddressSaved bit;
  declare @Mandatory bit;
  declare @UnitNumber char(50);
  declare @Complex char(50);
  declare @StreetNumber char(50);
  declare @StreetOrFarm char(50);
  declare @SuburbOrDistrict char(50);
  declare @City char(50);
  declare @PostalCode char(10);
  declare @ValidString bit;
  -- Employee Address
if not exists (select * from DBA.a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
  select isnull(trim(PHYS_WORK_UNITNO),''),isnull(trim(PHYS_WORK_COMPLEX),''),isnull(trim(PHYS_WORK_STREETNO),''),isnull(trim(PHYS_WORK_STREET),''),
    isnull(trim(PHYS_WORK_SUBURB),''),isnull(trim(PHYS_WORK_CITY),''),isnull(trim(PHYS_WORK_CODE),'') into @UnitNumber,
    @Complex,@StreetNumber,@StreetOrFarm,@SuburbOrDistrict,@City,@PostalCode from 
    E_ADDRESS where COMPANY_ID= @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID;
else
  select isnull(trim(PHYSICAL_UNITNO),''),isnull(trim(PHYSICAL_COMPLEX),''),isnull(trim(PHYSICAL_STREETNO),''),isnull(trim(PHYSICAL_STREET),''),
    isnull(trim(PHYSICAL_SUBURB),''),isnull(trim(PHYSICAL_CITY),''),isnull(trim(PHYSICAL_CODE),'') into @UnitNumber,
    @Complex,@StreetNumber,@StreetOrFarm,@SuburbOrDistrict,@City,@PostalCode from 
    C_ADDRESS where COMPANY_ID= @COMPANY_ID;
end if;


  -- Mandatory 
  if length(@StreetOrFarm) = 0 or length(@PostalCode) = 0 or (length(@SuburbOrDistrict) = 0 and length(@City) = 0) then
    if @NOP <> 'N' then
      if  year(@TaxYearEnd) < '2011' then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3147,'Employee business address is not complete.')
      else
        if length(@StreetOrFarm) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3147,'Business address Street or Name of Farm is mandatory.')
        end if;
        if length(@SuburbOrDistrict) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3148,'Business address Suburb/District or City/Town is mandatory.')
        end if;
        if length(@City) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3149,'Business address Suburb/District or City/Town is mandatory.')
        end if;
        if length(@PostalCode) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3150,'Business address Postal Code is mandatory.')
        end if;
      end if;
    end if;
    set @AddressSaved = 0;
  else
    -- 3144 Unit Number (Optional)
    set @UnitNumber = fn_S_RemoveNonAlphaNumericFromString(@UnitNumber);
    if length(@UnitNumber) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3144,@UnitNumber,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3145 Complex (Optional)
    if length(@Complex) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3145,@Complex,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3146 Street Number (Optional)
    set @StreetNumber = fn_S_RemoveNonAlphaNumericFromString(@StreetNumber);
    if length(@StreetNumber) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3146,@StreetNumber,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3147 Street or Farm name (Mandatory)
    if length(@StreetOrFarm) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3147,@StreetOrFarm,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3148 Suburb or District (Conditional)
    if length(@SuburbOrDistrict) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3148,@SuburbOrDistrict,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3149 Suburb or District (Conditional)
    if length(@City) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3149,@City,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3150 Postal Code (Mandatory)
    set @PostalCode = fn_S_RemoveNonAlphaNumericFromString(@PostalCode);
    if length(@PostalCode) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3150,@PostalCode,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    set @AddressSaved = 1;
  end if;
  return(@AddressSaved);
end;

commit;

comment on procedure fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues is "<h2>Purpose</h2><br/>Compiles the employee work address data for the South African Tax Certificates.";
CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_EmployeeOutputRecord"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_EmployeeOutputRecord is called to create the electronic 
file output record for the employee according to the formatting requirements 
specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @IDNumber char(20);
  declare @PassportNumber char(20);
  declare @AlternateNumber char(25);
  declare @Surname char(120);
  declare @FirstNames char(90);
  declare @SecondName char(90);
  declare @DateOfBirth char(8);
  declare @EmploymentStart date;
  declare @EmploymentEnd date;
  declare @UIF_EmpStatusCode char(02);
  declare @UIF_ReasonCode char(02);
  declare @GrossTaxable numeric(15,2);
  declare @UIF_Earning numeric(15,2);
  declare @UIF_Contribution numeric(15,2);
  declare @Counter integer;
  declare @IsValid bit;
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_EmployeeOutputRecord ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- Build up the record per code
  set @FileRecord = '8001,"UIWK"';
  -- 8110 UIF Reference Number
  if (select coalesce(trim(TheValue),'Payroll') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichUIF') = 'Payroll' then
    select trim(UIF_Number) into @CodeValue from P_PAYROLL where Payroll_id = @PAYROLL_ID;
  end if;
  if @CodeValue is null then
    select trim(UIF_Number) into @CodeValue from C_MASTER where company_id = @COMPANY_ID
  end if;
  if length(@CodeValue) > 0 then
    set @CodeValue = fn_S_RemoveNonNumericFromString(@CodeValue);
    set @FileRecord = @FileRecord || ',8110,'||right('000000000'||@CodeValue,9)
  end if;
  set @CodeValue = null;
  -- 8200 ID Number
  select trim(ID_Number) into @IDNumber from E_Number where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  select trim(Passport_Number) into @PassportNumber from E_Passport where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  if length(@IDNumber) > 0 then
    if fn_E_SouthAfrica_IsValidIdNo(@IDNumber) = 0 then
      if length(@PassportNumber) = 0 then
        set @PassportNumber = @IDNumber;
      end if;
      if @IDNumber = fn_S_RemoveNonNumericFromString(@IDNumber) then
        set @IDNumber = left(@IDNumber,13)
      else
        set @IDNumber = null
      end if
    end if;
  end if;
  if length(@IDNumber) > 0 then
    set @FileRecord = @FileRecord || ',8200,'||@IDNumber
  end if;
  -- 8210 Other Number
  if length(@PassportNumber) = 0 then
    select trim(Permit_Number) into @PassportNumber from E_Nationality where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  end if;
  if length(@PassportNumber) > 0 then
    set @FileRecord = @FileRecord || ',8210,"'||left(@PassportNumber,16)||'"'
  end if;
  set @FileCode = null;
  set @CodeValue = null;
  -- Personal Information
  select left(trim(Company_Employee_Number),25),left(trim(Surname),120),left(trim(FirstName),90),left(trim(SecondName),90),
    convert(char(8),BirthDate,112) into @AlternateNumber,@Surname,@FirstNames,@SecondName,@DateOfBirth 
    from E_MASTER where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  -- 8220 Alternate Number (Company Employee Number)
  if length(@AlternateNumber) > 0 then
    set @FileRecord = @FileRecord || ','|| 8220 ||',"'||@AlternateNumber||'"'
  end if;
  -- 8230 Surname
  if length(@Surname) > 0 then
    set @FileRecord = @FileRecord || ','|| 8230 ||',"'||@Surname||'"'
  end if;
  -- 8240 First Names
  if length(@FirstNames) + length(@SecondName) > 0 then
    set @FileRecord = @FileRecord || ','|| 8240 ||',"'||left(trim(@FirstNames||' '||@SecondName),90)||'"'
  end if;
  -- 8250 Date of Birth
  if length(@DateOfBirth) > 0 then
    set @FileRecord = @FileRecord || ','|| 8250 ||','||@DateOfBirth
  end if;
  -- Employment Dates and Status
  select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
    into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
    from E_ON_OFF E
    where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID and E.START_DATE <= @MonthEnd
    order by E.START_DATE desc;
  -- 8260 Date Employed From
  if length(@EmploymentStart) = 0 then
    select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
      into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
      from E_ON_OFF E
      where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID
      order by E.START_DATE desc;
  end if;
  if length(@EmploymentStart) > 0 then
    set @FileRecord = @FileRecord || ','|| 8260 ||','||convert(char(8),@EmploymentStart,112)
  end if;
  -- 8270 Date Employed To
  if @EmploymentEnd between @EmploymentStart and dateadd(month,2,@MonthEnd) then
    set @FileRecord = @FileRecord || ','|| 8270 ||','||convert(char(8),@EmploymentEnd,112)
  end if;
  -- 8280 Employment Status Code
  if @UIF_EmpStatusCode = '01' then
    if exists (select * from L_History where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID
      and Leave_Type_Id in (4,9) and Convert_YN = 'N' and Cancel_Date is null
      and Start_Date < @MonthEnd and End_Date >= @MonthEnd) then
      set @UIF_EmpStatusCode = '09';
    end if
  end if;
  if length(@UIF_EmpStatusCode) > 0 then
    set @FileRecord = @FileRecord || ','|| 8280 ||','||@UIF_EmpStatusCode
  end if;
  -- Earnings and Contributions
  select sum(UIF_GROSSTAXABLE), lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)), sum(UIF_EE) + sum(UIF_ER) 
    into @GrossTaxable, @UIF_Earning, @UIF_Contribution
    from dba.vw_P_UIF where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID 
      and Payroll_Id = @PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
          and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd);
  if @UIF_Contribution = 0 or @GrossTaxable = 0 then
    select coalesce((select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and UIF_Grosstaxable > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc), 
    (select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc)) into @UIF_ReasonCode
  end if;
  -- 8290 Reason Code for Non-Contribution
  if @UIF_ReasonCode > 0 then
    set @UIF_ReasonCode = right('0'||@UIF_ReasonCode,2);
    set @FileRecord = @FileRecord || ','|| 8290 ||','||@UIF_ReasonCode
  end if;
  -- 8300 Gross Taxable Remuneration
  if length(@GrossTaxable) > 0 then
    set @FileRecord = @FileRecord || ','|| 8300 ||','||@GrossTaxable
  end if;
  -- 8310 Remuneration subject to UIF
  if length(@UIF_Earning) > 0 then
    set @FileRecord = @FileRecord || ','|| 8310 ||','||@UIF_Earning
  end if;
  -- 8320 UIF Contribution
  if length(@UIF_Contribution) > 0 then
    set @FileRecord = @FileRecord || ','|| 8320 ||','||@UIF_Contribution
  end if;
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_EmployeeOutputRecord is "<h2>Purpose</h2><br/>Prepares the UIF electronic file output record for the employee according to the formatting requirements specified by the Department of Labour.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_EmployeeWarnings"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_EmployeeWarnings is called to create the electronic 
file output record for the employee according to the formatting requirements 
specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @EeNumber char(25);
  declare @IDNumber char(20);
  declare @PassportNumber char(20);
  declare @AlternateNumber char(25);
  declare @Surname char(120);
  declare @FirstNames char(90);
  declare @SecondName char(90);
  declare @DateOfBirth char(8);
  declare @EmploymentStart date;
  declare @EmploymentEnd date;
  declare @UIF_EmpStatusCode char(02);
  declare @UIF_ReasonCode char(02);
  declare @GrossTaxable numeric(15,2);
  declare @UIF_Earning numeric(15,2);
  declare @UIF_EE numeric(15,2);
  declare @UIF_ER numeric(15,2);
  declare @Counter integer;
  declare @IsValid bit;
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_EmployeeWarnings ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- 8200 ID Number
  select trim(ID_Number) into @IDNumber from E_Number where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  select trim(Passport_Number) into @PassportNumber from E_Passport where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  if length(@IDNumber) > 0 then
    if fn_E_SouthAfrica_IsValidIdNo(@IDNumber) = 0 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; ID number ('|| @IDNumber ||') is not valid.'
      else
        set @FileRecord = 'Warning: ID number ('|| @IDNumber ||') is not valid.'
      end if;
      if length(@PassportNumber) = 0 then
        set @FileRecord = @FileRecord || ' - In the absence of a passport number it will be used in the Other Number field.'
      end if;
    end if;
  end if;
  -- 8210 Other Number
  if length(@PassportNumber) = 0 then
    select trim(Permit_Number) into @PassportNumber from E_Nationality where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  end if;
  -- Personal Information
  select left(trim(Company_Employee_Number),25),left(trim(Surname),120),left(trim(FirstName),90),left(trim(SecondName),90),
    convert(char(8),BirthDate,112) into @AlternateNumber,@Surname,@FirstNames,@SecondName,@DateOfBirth 
    from E_MASTER where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  -- 8220 Alternate Number (Company Employee Number)
  if length(@IDNumber) + length(@PassportNumber) + length(@AlternateNumber) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; No ID number, Other number or Alternate number is present.'
    else
      set @FileRecord = 'Warning: No ID number, Other number or Alternate number is present.'
    end if
  end if;
  -- 8230 Surname
  if length(@Surname) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The surname is omitted.'
    else
      set @FileRecord = 'Warning: The surname is omitted.'
    end if
  end if;
  -- 8240 First Names
  if length(@FirstNames) + length(@SecondName) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The first names are omitted.'
    else
      set @FileRecord = 'Warning: The first names are omitted.'
    end if
  end if;
  -- 8250 Date of Birth
  if length(@DateOfBirth) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The date of birth is omitted.'
    else
      set @FileRecord = 'Warning: The date of birth is omitted.'
    end if
  else
    if datediff(year,@DateOfBirth,today()) < 15 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The employee born on ('||@DateOfBirth||') is younger than 15 years.'
      else
        set @FileRecord = 'Warning: The employee born on ('||@DateOfBirth||') is younger than 15 years.'
      end if
    end if;
  end if;
  -- Employment Dates and Status
  select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
    into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
    from E_ON_OFF E
    where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID and E.START_DATE <= @MonthEnd
    order by E.START_DATE desc;
  -- 8260 Date Employed From
  if length(@EmploymentStart) = 0 then
    select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
      into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
      from E_ON_OFF E
      where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID
      order by E.START_DATE desc;
    if length(@EmploymentStart) > 0 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed from ('||@EmploymentStart||') is after the month end date ('|| @MonthEnd ||').'
      else
        set @FileRecord = 'Warning: The date employed from ('||@EmploymentStart||') is after the month end date ('|| @MonthEnd ||').'
      end if
    else
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed from is omitted.'
      else
        set @FileRecord = 'Warning: The date employed from is omitted.'
      end if
    end if;
  end if;
  -- 8270 Date Employed To
  -- 8280 Employment Status Code
  if @UIF_EmpStatusCode = '01' then
    if exists (select * from L_History where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID
      and Leave_Type_Id in (4,9) and Convert_YN = 'N' and Cancel_Date is null
      and Start_Date < @MonthEnd and End_Date >= @MonthEnd) then
      set @UIF_EmpStatusCode = '09';
    end if
  end if;
  if @EmploymentEnd between @EmploymentStart and dateadd(month,2,@MonthEnd) then
    if @UIF_EmpStatusCode in ('01','07','09') then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed to ('|| @EmploymentEnd ||') is valid but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      else
        set @FileRecord = 'Warning: The date employed to ('|| @EmploymentEnd ||') is valid but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      end if
    end if;
  else
    if @UIF_EmpStatusCode not in ('01','07','09') then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed to is omitted but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      else
        set @FileRecord = 'Warning: The date employed to is omitted but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      end if
    end if;
  end if;
  -- Earnings and Contributions
  select sum(UIF_GROSSTAXABLE), lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)), sum(UIF_EE),  sum(UIF_ER) 
    into @GrossTaxable, @UIF_Earning, @UIF_EE, @UIF_ER
    from dba.vw_P_UIF where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID 
      and Payroll_Id = @PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
          and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd);
  -- 8290 Reason Code for Non-Contribution
  if (@UIF_EE + @UIF_ER) = 0 or @GrossTaxable = 0 then
    select coalesce((select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and UIF_Grosstaxable > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc), 
    (select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc)) into @UIF_ReasonCode;
    if @UIF_ReasonCode is null then
      if (@UIF_EE + @UIF_ER) = 0 then
        if length(@FileRecord) > 0 then
          set @FileRecord = @FileRecord || '; The UIF contribution is zero without a valid reason code.'
        else
          set @FileRecord = 'Warning: The UIF contribution is zero without a valid reason code.'
        end if
      else
        if length(@FileRecord) > 0 then
          set @FileRecord = @FileRecord || '; The gross taxable remuneration is zero without a valid reason code.'
        else
          set @FileRecord = 'Warning: The gross taxable remuneration is zero without a valid reason code.'
        end if
      end if;
    end if;
  end if;
  -- 8310 Remuneration subject to UIF
  if @UIF_ReasonCode is null then
    if convert(numeric(13,0),@UIF_Earning)*0.02 <> (@UIF_EE + @UIF_ER) then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The UIF contribution is not 2% of the remuneration subject to UIF.'
      else
        set @FileRecord = 'Warning: The UIF contribution is not 2% of the remuneration subject to UIF.'
      end if
    end if;
  end if;
  -- 8320 UIF Contribution
  if @UIF_EE <> @UIF_ER then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; Company and Employee Contributions do not match.'
    else
      set @FileRecord = 'Warning: Company and Employee Contributions do not match.'
    end if
  end if;
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_EmployeeWarnings is "<h2>Purpose</h2><br/>Prepares the Employee Warning Messages for the preview of the UIF electronic file creation.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_FooterOutputRecord"(in @COMPANY_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_FooterOutputRecord is called to create the electronic 
file output record for footer/employer section according to the formatting 
requirements specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @GrossTaxable numeric(15,2);
  declare @UIF_Earning numeric(15,2);
  declare @UIF_Contribution numeric(15,2);
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_FooterOutputRecord ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- Build up the record per code
  set @FileRecord = '8002,"UIEM"';
  -- 8115 UIF Reference Number
  if (select coalesce(trim(TheValue),'Payroll') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichUIF') = 'Payroll' then
    select trim(UIF_Number) into @CodeValue from P_PAYROLL where Payroll_id = @PAYROLL_ID;
  end if;
  if @CodeValue is null then
    select trim(UIF_Number) into @CodeValue from C_MASTER where company_id = @COMPANY_ID
  end if;
  if length(@CodeValue) > 0 then
    set @CodeValue = fn_S_RemoveNonNumericFromString(@CodeValue);
    set @FileRecord = @FileRecord || ',8115,'||right('000000000'||@CodeValue,9)
  end if;
  set @CodeValue = null;
  -- 8120 PAYE Employer Number
  if (select coalesce(trim(TheValue),'Payroll') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichPAYE') = 'Payroll' then
    select trim(TAX_Number) into @CodeValue from P_PAYROLL where Payroll_id = @PAYROLL_ID;
  end if;
  if @CodeValue is null then
    select trim(TAX_Number) into @CodeValue from C_MASTER where company_id = @COMPANY_ID
  end if;
  if length(@CodeValue) > 0 then
    set @FileRecord = @FileRecord || ',8120,'||left(@CodeValue,10)
  end if;
  set @CodeValue = null;
  -- Earnings and Contributions Totals
  select sum(S.GrossTaxable),sum(S.UIF_Earning),sum(S.UIF_Contribution)
    into @GrossTaxable, @UIF_Earning, @UIF_Contribution
    from (select employee_id, sum(UIF_GROSSTAXABLE) as GrossTaxable, 
      lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)) as UIF_Earning, 
      sum(UIF_EE) + sum(UIF_ER) as UIF_Contribution
      from dba.vw_P_UIF where company_id = @COMPANY_ID and Payroll_Id = @PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
          and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
        and employee_id in (select employee_id from vw_P_OpenMonthEmployeeList 
          where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID)
      group by employee_id) as S;
  -- 8130 Total Gross Taxable Remuneration
  if length(@GrossTaxable) > 0 then
    set @FileRecord = @FileRecord || ','|| 8130 ||','||@GrossTaxable
  end if;
  -- 8135 Total Remuneration subject to UIF
  if length(@UIF_Earning) > 0 then
    set @FileRecord = @FileRecord || ','|| 8135 ||','||@UIF_Earning
  end if;
  -- 8140 Total UIF Contribution
  if length(@UIF_Contribution) > 0 then
    set @FileRecord = @FileRecord || ','|| 8140 ||','||@UIF_Contribution
  end if;
  -- 8150 Total Employees
  select count(employee_id) into @CodeValue from vw_P_OpenMonthEmployeeList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  if length(@CodeValue) > 0 then
    set @FileRecord = @FileRecord || ','|| 8150 ||','||@CodeValue
  end if;
  set @CodeValue = null;
  -- 8160 Employer email address
  select left(trim(EMail),50) into @CodeValue from C_Master where company_id = @COMPANY_ID;
  if length(@CodeValue) > 0 then
    set @FileRecord = @FileRecord || ','|| 8160 ||',"'||@CodeValue||'"'
  end if;
  set @CodeValue = null;
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_FooterOutputRecord is "<h2>Purpose</h2><br/>Prepares the UIF electronic file output record for the footer/employer according to the formatting requirements specified by the Department of Labour.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_HeaderOutputRecord"(in @COMPANY_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_HeaderOutputRecord is called to create the electronic 
file output record for header/creator section according to the formatting 
requirements specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @ContactName char(30);
  declare @ContactPhone char(16);
  declare @ContactEMail char(50);
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_HeaderOutputRecord ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- Build up the record per code
  set @FileRecord = '8000,"UICR",8010,"U1",8015,"E03"';
  -- 8020 UIF Reference Number
  if (select coalesce(trim(TheValue),'Payroll') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichUIF') = 'Payroll' then
    select trim(UIF_Number) into @CodeValue from P_PAYROLL where Payroll_id = @PAYROLL_ID;
  end if;
  if @CodeValue is null then
    select trim(UIF_Number) into @CodeValue from C_MASTER where company_id = @COMPANY_ID
  end if;
  if length(@CodeValue) > 0 then
    set @CodeValue = fn_S_RemoveNonNumericFromString(@CodeValue);
    set @FileRecord = @FileRecord || ',8020,'||right('000000000'||@CodeValue,9)
  end if;
  set @CodeValue = null;
  -- 8030 Test Live Indicator
  select coalesce(trim(TheValue),'LIVE') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'TestOrLive';
  if length(@CodeValue) > 0 then
    set @FileRecord = @FileRecord || ',8030,"'||@CodeValue||'"'
  end if;
  set @FileCode = null;
  set @CodeValue = null;
  -- Contact Information
  select left(trim(Name),30),left(trim(Tel_Number),16),left(trim(E_Mail),50)
    into @ContactName, @ContactPhone, @ContactEMail from C_CONTACT 
    where company_id = @COMPANY_ID and Contact_Id =
      (select coalesce(trim(TheValue),'1') from dba.P_EFTS_CONSTANT 
        where EFTS_ID = 15 and NAME = 'Contact'||@COMPANY_ID);
  -- 8040 Contact Person
  if length(@ContactName) > 0 then
    set @FileRecord = @FileRecord || ','|| 8040 ||',"'||@ContactName||'"'
  end if;
  -- 8050 Contact Telephone Number
  if length(@ContactPhone) > 0 then
    set @FileRecord = @FileRecord || ','|| 8050 ||',"'||@ContactPhone||'"'
  end if;
  -- 8060 Contact E-mail Address
  if length(@ContactEMail) > 0 then
    set @FileRecord = @FileRecord || ','|| 8060 ||',"'||@ContactEMail||'"'
  end if;
  -- 8070 Payroll Month
  set @FileRecord = @FileRecord || ','|| 8070 ||','||datepart(year,@MonthEnd)||right('0'||datepart(month,@MonthEnd),2);
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_HeaderOutputRecord is "<h2>Purpose</h2><br/>Prepares the UIF electronic file output record for the header/creator according to the formatting requirements specified by the Department of Labour.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
--------------------------
-- Add Credits
--------------------------
create or replace function dba.fn_WEB_AddCredits(@ProcessName char(50), @nbCredits integer)
returns integer
begin
  insert into dba.WEB_CREDIT values (now(),@ProcessName,fn_S_EncryptString('CREDIT|'||datepart(dw, now())||'|'||@nbCredits));
  return @nbCredits;
end;--------------------------
-- Get Credits Available
--------------------------
create or replace function dba.fn_WEB_GetCreditsAvailable(@ProcessName char(50))
returns integer
begin
  declare @rslt integer;
  declare @purchased integer;
  declare @used integer;

  -- How many credits were purchased?
  select coalesce(sum(fn_WEB_GetCreditsPurchased(credit_date, process_name)),0) into @purchased from WEB_CREDIT where process_name=@ProcessName;

  -- How many credits were used?
  select fn_WEB_GetCreditsUsed(@ProcessName) into @used;

  return @purchased - @used; 
end;--------------------------
-- Get Credits Purchased
--------------------------
create or replace function dba.fn_WEB_GetCreditsPurchased(@CreditDate timestamp, @ProcessName char(50))
returns integer
begin
  declare @rslt integer;
  declare @code char(100);
  declare @firstPipe integer;
  declare @secondPipe integer;
  
  -- Calculate the total number of credits purchased
  select fn_S_DecryptString(credit_code_encr) as code, locate(fn_S_DecryptString(credit_code_encr),'|',1) as firstPipe,
        locate(fn_S_DecryptString(credit_code_encr),'|',firstPipe+1) as secondPipe,
        convert(integer, substr(code,secondPipe+1,10)) as creditsPurchased into @code, @firstPipe, @secondPipe, @rslt from WEB_CREDIT
    where datepart(dw, CREDIT_DATE) =convert(integer, substr(code,firstPipe+1,1))
        and PROCESS_NAME=@ProcessName and CREDIT_DATE=@CreditDate;

  return coalesce(@rslt,0); 
end;--------------------------
-- Get Credits Used
--------------------------
create or replace function dba.fn_WEB_GetCreditsUsed(@ProcessName char(50))
returns integer
begin
  declare @rslt integer;

  -- For PAYROLL, we calculate the number of CALCULATED employees in P_CALC_EMPLOYEELIST after the date of the first
  --   time the client started using this payroll (after first CREDIT_DATE in WEB_CREDIT)
  if (@ProcessName='PAYROLL') then
    select count(*) into @RSLT from P_CALC_EMPLOYEELIST where calc_date >= coalesce((select min(CREDIT_DATE) from web_credit), '2000/01/01')
  end if;

  return coalesce(@rslt,0); 
end;// requires: "A_TRADE_CLASSIFICATION.txt", "A_TRADE_SUB_CLASSIFICATION.txt"

// Table P_PAYROLL - TABLE STRUCTURE Definition

if not exists (select * from systable where upper(table_name) = 'P_PAYROLL') then
  create table P_PAYROLL (
    "PAYROLL_ID"                     numeric(10)           not null,
    "NAME"                           char(50)              not null,
    "DESCRIPTION"                    varchar(100)          not null,
    "TAX_YEAR_START"                 date                  null,
    "TAX_YEAR_END"                   date                  null,
    "PAYROLL_TYPE"                   char(15)              null default 'Weekly',
    "COUNTRY_ID"                     numeric(10)           null,
    "TAX_NUMBER"                     char(21)              null,
    "UIF_NUMBER"                     char(15)              null,
	"TRADE_CLASS_ID"                 numeric(10)           null, 
    "TRADE_SUB_CODE"                 char(10)              null,    
	"SIC_CODE_ID"                    integer               null, 
    primary key ("PAYROLL_ID")
  );
  
  CREATE UNIQUE INDEX "idx" ON "DBA"."P_PAYROLL"
  ( "PAYROLL_ID" ASC );
  
end if;

-- CR 9022

if not exists (select * from syscolumn where column_name = 'TRADE_CLASS_ID' and 
  table_id = (select table_id from systable where table_name = 'P_PAYROLL')) then 
   ALTER TABLE "DBA"."P_PAYROLL" ADD "TRADE_CLASS_ID" numeric(10) NULL;
end if;

if not exists (select * from syscolumn where column_name = 'TRADE_SUB_CODE' and 
  table_id = (select table_id from systable where table_name = 'P_PAYROLL')) then 
   ALTER TABLE "DBA"."P_PAYROLL" ADD "TRADE_SUB_CODE" char(10) NULL;
end if;

if not exists (select * from syscolumn where column_name = 'SIC_CODE_ID' and 
  table_id = (select table_id from systable where table_name = 'P_PAYROLL')) then 
   ALTER TABLE "DBA"."P_PAYROLL" ADD "SIC_CODE_ID" integer NULL;
end if;

if not exists (select * from sysforeignkey where upper(role) = 'FK_A_TRADE_SUB_CLASSIFICATION' and
  foreign_table_id in (select table_id from systable where upper(table_name) = 'P_PAYROLL')) then
	alter table "DBA"."P_PAYROLL" add foreign key "FK_A_TRADE_SUB_CLASSIFICATION" ("TRADE_SUB_CODE") 
    references "DBA"."A_TRADE_SUB_CLASSIFICATION" ("TRADE_SUB_CODE") on delete set null on update cascade;
end if;

if exists (select * from sysforeignkey where upper(role) = 'A_TRADE_CLASSIFICATION' and
  foreign_table_id in (select table_id from systable where upper(table_name) = 'P_PAYROLL')) then
	alter table "DBA"."P_PAYROLL" drop foreign key "A_TRADE_CLASSIFICATION"
end if;

commit;
create or replace procedure
DBA.sp_P_SetEmployeeGender(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),aGender char(20))
begin
  -- CR 14233 : PeopleWeb
  declare @GROUP_ID numeric(10);
  update e_master set GENDER=upper(left(aGender,1)) where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  // If I cannot get the 'aGender' from c_group, assign to 'Unconfirmed Gender'
  select GROUP_ID into @GROUP_ID from c_group where parent_group_id = 1 and
    upper(aGender) = upper(DESCRIPTION);
  if @GROUP_ID is null then set @GROUP_ID=70
  end if; // Save into database
  delete from e_group where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and
    group_id = 1;
  insert into e_group(COMPANY_ID,EMPLOYEE_ID,GROUP_ID,SUBGROUP1_ID) values(
    @COMPANY_ID,@EMPLOYEE_ID,1,@GROUP_ID)
end;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE RACE
----------------------------------------------
CREATE OR REPLACE PROCEDURE "DBA"."sp_P_SetEmployeeRace"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10), in @RACE char(50))
begin
  -- If the race is different from what is presently assigned to this employee, replace it
  if not exists (select * from e_group where company_id=@company_id and employee_id=@EMPLOYEE_ID and group_id=2 
        and subgroup1_id=(select group_id from c_group where upper(trim(description))=@RACE)) then
    delete from e_group where company_id=@company_id and employee_id=@EMPLOYEE_ID and group_id=2;
    insert into e_group  (company_id, employee_id, group_id, subgroup1_id) 
        values (@company_id, @EMPLOYEE_ID, 2, (select group_id from c_group where upper(trim(description))=@RACE)) ;
  end if;
end;-------------------------------------------------------
-- Automatically calculate employees
-------------------------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_autocalc')) then
      drop event "DBA"."evt_http_autocalc";

end if;
	  
    CREATE EVENT "DBA"."evt_http_autocalc"
                  SCHEDULE "sch_http_autocalc" BETWEEN '00:01' AND '23:59' EVERY 1 SECONDS
                  HANDLER
      BEGIN
        -- Check, at very short intervals, if anything changed that warrants a re-calculation and then triggers that calculation
        if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
          select sp_P_ResetEmployeeCalcStatus(company_id, employee_id, payroll_id, payrolldetail_id, runtype_id, 0),
            sp_P_CALC_Initiate_PerEmployee(company_id, employee_id, payroll_id, payrolldetail_id, runtype_id, 0) into #B
          from p_calc_employeelist
            where payrolldetail_id=fn_P_GetOpenPeriod(company_id, payroll_id)
            and run_status=2 and runtype_id=1; commit;
        end if;
      END;

COMMENT ON EVENT "DBA"."evt_http_autocalc" IS 'Ensures staff are always calculated (Basic Run only) by automatically adding them into the calculation queue if they are flagged to be calculated.';
-----------------------------------------------------------
-- Automatically make the Administrator the Contact person
-----------------------------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_autoCompanyContact')) then
      drop event "DBA"."evt_http_autoCompanyContact";
end if;

 CREATE EVENT "DBA"."evt_http_autoCompanyContact"
          SCHEDULE "sch_http_autoCompanyContact" BETWEEN '00:01' AND '23:59' EVERY 10 MINUTES
 HANDLER
 BEGIN 
   -- Check, on a semi-regular basis, if the C_CONTACT information is the same as that of the system administrator in PeopleWeb
   if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
     if exists(select * from dba.e_master e join dba.al_emaster_agroup al join e_contact c
        where fn_E_EmployeeActiveOnDate_YN(e.company_id, e.employee_id, now(*))='Y') then    
        -- Make sure we're using the Admin as Contact person                    
        delete from c_contact;
        insert into c_contact (company_id, contact_id, name, position, tel_number, e_mail) (
             select e.company_id, 1, e.FIRSTNAME||' '||e.SURNAME, j.job_name, c.tel_work, c.e_mail from dba.e_master e join dba.al_emaster_agroup al join dba.PL_EMASTER_PJOBPOSITION pl join dba.P_JOBPOSITION j join e_contact c)

     end if;
    end if;
 END;
 COMMENT ON EVENT "DBA"."evt_http_autoCompanyContact" IS 'Makes the PeopleWeb administrator the contact person for the company.';

-------------------------------------------------------
-- Automatically update report tables for calculated employees
-------------------------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_autoupdatereports')) then
      drop event "DBA"."evt_http_autoupdatereports";
end if;	  
	
	CREATE EVENT "DBA"."evt_http_autoupdatereports"
	SCHEDULE "sch_http_autoupdatereports" BETWEEN '00:01' AND '23:59' EVERY 2 SECONDS
	HANDLER
	BEGIN
	  if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
            select sp_P_REPORT_PrepareTables_Preview(company_id, employee_id, payroll_id, payrolldetail_id, 0) into #A
		from p_calc_employeelist pce 
		where payrolldetail_id = fn_P_GetOpenPeriod(company_id, payroll_id) 
			and runtype_id=1 
			and run_status=3 
			and employee_id not in (
				select employee_id from p_report_data 
					where company_id=pce.company_id 
					and payroll_id=pce.payroll_id 
					and payrolldetail_id=pce.payrolldetail_id 
					and runtype_id=pce.runtype_id);
         end if;


	END;
	COMMENT ON EVENT "DBA"."evt_http_autoupdatereports" IS 'Ensures that calculated staff get their reporting tables updated.';

-------------------------------------
-- EVENT TO REMOVE EXPIRED SESSIONS
-------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_sessionexpiry')) then
    drop event "DBA"."evt_http_sessionexpiry";
end if;	
    
CREATE EVENT "DBA"."evt_http_sessionexpiry"
	SCHEDULE "sch_session_expiry" BETWEEN '00:00' AND '23:59' EVERY 2 MINUTES
HANDLER
BEGIN
		if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
	delete from WEB_SESSION where datediff(minute, update_time, now())>timeout_minutes;
		end if;
END;

COMMENT ON EVENT "DBA"."evt_http_sessionexpiry" IS 'Remove unused http sessions';
-------------------------------------------------
-- PROCEDURE TO ADVANCE PAYROLL TO THE NEXT PERIOD
-------------------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_AdvancePayrollPeriod ( IN @sessionid long VARCHAR, IN @companyID long varchar, IN @payrollID long varchar )
returns varchar(10)
BEGIN
    DECLARE @Command LONG VARCHAR;
	DECLARE @openPeriod_ID numeric(10);

    -- If this session doesn't exist, return empty-handed
    if not exists(select * from WEB_SESSION where session_id=@sessionid) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);
	
	-- Set the open period ID
	set @openPeriod_ID = fn_P_GetOpenPeriod(@companyID, @payrollID);

	-- If this is the last period do a tax year roll over
	if (@openPeriod_ID  = (select max(PAYROLLDETAIL_ID) from P_PAYROLLDETAIL where PAYROLL_ID = @payrollID)) then
		-- Close run
		call sp_P_SetCloseRun(@companyID, @payrollID, @openPeriod_ID , 1);
		-- Close period
		call sp_P_SetClosePeriod(@companyID, @payrollID, @openPeriod_ID);
		-- Tax Year Roll Over
		call fn_P_RollOverTaxYear(@payrollID);
		-- Open the first period
		call sp_P_SetOpenPeriod(@companyID, @payrollID, 1);
		-- Open basic run
		call sp_P_SetOpenRunType(@companyID, @payrollID, 1, 1, null, null );
		-- Mark all employees as To-Be-Processed
		update P_CALC_EMPLOYEELIST set RUN_STATUS=2 where company_id=@companyID and payroll_id=@payrollID and payrolldetail_id=1;
	else
	-- Normal roll over
		-- Close run
		call sp_P_SetCloseRun(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID), 1);
		-- Close period
		call sp_P_SetClosePeriod(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID));
		-- Open basic run
		call sp_P_SetOpenRunType(@companyID, @payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID), 1, null, null );
		-- Mark all employees as To-Be-Processed
		update P_CALC_EMPLOYEELIST set RUN_STATUS=2 where company_id=@companyID and payroll_id=@payrollID and payrolldetail_id=fn_P_GetOpenPeriod(@companyID, @payrollID);
	end if;
    return 'OK';
END;
comment on procedure dba.fn_HTTP_AdvancePayrollPeriod is 'Advances the payroll to the next week/month/tax year';

commit;
------------------------------------------------------
-- FUNCTION TO FILTER WHO THIS USER HAS ACCESS TO
------------------------------------------------------
CREATE OR REPLACE FUNCTION "DBA"."fn_http_AllowEmployeeView"(in CompanyID numeric(10),in EmployeeID numeric(10),in sessionID long varchar)
returns char(1)
begin
  /*  
  <purpose>  
  Similar to fn_E_AllowEmployeeView, this function is used to filter the employees accessible through the web services, given a certain session id
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2014/07/31            Liam Terblanche Initial
  </history>
  */
  declare GroupID numeric(10);
  declare CostID numeric(10);
  declare PayrollID numeric(10);
  declare PayrollSecurityDisabled char(3);

  -- IMPORTANT: For now, allow access to EVERYONE.
  -- This is a temporary override to make testing a bit easier :-)  [Liam]
  return ('Y');
  --
  --  Get the group ID for the current http session.  This could slow down the 
  --  vw_E_MASTER view because we call this procedure per employee!
  --
    select min(GROUP_id) into GroupID
    from AL_EMASTER_AGROUP NATURAL JOIN WEB_SESSION 
    where WEB_SESSION.session_id=sessionID;

  if GroupID = -1 then
    return('N')
  end if;
  --
  -- Get the cost ID of the cost centre this employee belongs to
  --
  select COST_ID into CostID
    from E_MASTER where
    COMPANY_ID = CompanyID and
    EMPLOYEE_ID = EmployeeID;
  if CostID is null then
    return('Y')
  end if;
  --
  -- Check if the payroll security is disabled.
  --
  select first(DATA) into PayrollSecurityDisabled from A_SETUP where
    ITEM = 'DisablePayrollSecurity';
  if PayrollSecurityDisabled = 'Yes' then
    --
    -- Only check the Cost Centre and ignore payroll security 
    --
    return(fn_A_CheckCostCentre(GroupID,CompanyID,CostID))
  end if;
  --
  -- Get the payroll ID of the payroll this employee belongs to.
  --
  select PAYROLL_ID into PayrollID
    from PL_PAYDEF_EMASTER where
    COMPANY_ID = CompanyID and
    EMPLOYEE_ID = EmployeeID;
  --
  --
  --
  if PayrollID is null then
    return('Y')
  end if;
  --
  -- Check the cost centre, together with payroll.
  --
  if CostID >= 0 and PayrollID >= 0 then
    if fn_A_CheckCostCentreAndPayroll(GroupID,CompanyID,CostID,PayrollID) = 'N' then
      return('N')
    end if
  end if;
  return('Y')
end;

commit;
if exists (select * from sysprocedure where upper(proc_name) = upper('fn_HTTP_CanCloseTaxYear')) then
  drop procedure DBA.fn_HTTP_CanCloseTaxYear;
end if;

create function
"DBA"."fn_HTTP_CanCloseTaxYear" ( IN @sessionid long VARCHAR, IN @companyID long varchar, IN @payrollID long varchar )
returns varchar(250)
/*
<purpose>
fn_HTTP_CanCloseTaxYear is called by the Payroll Manager to check that all 
requirements have been met before performing a tax-year-end process, that is:
1. No other payroll may be busy closing the tax year
2. All companies linked to the payroll must be closed in the last period
3. No payroll calculation may be in progress
</purpose>
<history>
Date          CR#     Programmer      Changes
----          ---     ----------      -------
2015/03/09	 14693    Francois        Convert to PeopleWeb Script from fn_P_CanCloseTaxYear
</history>
*/
BEGIN
  declare @ResultMessage varchar(250);
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop
  declare @Counter integer; 
  declare @Company_Id numeric(10); 
  declare @ClosingPayroll_Id numeric(10); 
  declare @LastPeriod_ID numeric(10);
  declare @CurrentPeriod_ID numeric(10);
  declare @Company_Name char(100);
  declare @Payroll_Name char(50);
  declare CompanyCursor no scroll cursor for 
    select distinct COMPANY_ID from PL_PAYDEF_EMASTER with (readuncommitted) where PAYROLL_ID = @payrollID;
  declare PayrollCursor no scroll cursor for 
    select distinct PAYROLL_ID from P_CLOSEYEAR_STATUS with (readuncommitted) where PAYROLL_ID <> @payrollID and (PAYROLL_ID <> 0);
  set @Counter = 0;
  
  -- If this session doesn't exist, return empty-handed
  if not exists(select * from WEB_SESSION where session_id=@sessionid) then
      return 'NO SESSION';
  end if;

  -- Touch this session
  call fn_HTTP_TouchSession(@sessionid);
	
  --
  -- 1. Payrolls in the process of closing the tax year.
  --
  set @ResultMessage = 'The following payrolls are currently busy closing their tax years:\x0a';
  open PayrollCursor; 
    -- Loop over the rows of the query
    Payroll_LOOP: loop
      --        
      fetch next PayrollCursor into @ClosingPayroll_Id;
      if sqlstate = err_notfound then
        leave Payroll_LOOP
      end if;
      set @Counter = @Counter + 1; 
      select Name into @Payroll_Name from P_PAYROLL with (readuncommitted) where Payroll_Id = @ClosingPayroll_Id;
      set @ResultMessage = @ResultMessage || '\x0a' ||@Payroll_Name;
    end loop Payroll_LOOP;
  -- Close the cursor
  close PayrollCursor;
  if @Counter > 0 then
    set @ResultMessage = @ResultMessage || '\x0a\x0aPlease wait for them to complete before closing this payroll.';
    return @ResultMessage
  end if;
  --
  -- 2. Outstanding Companies
  --
  select first Payrolldetail_ID into @LastPeriod_ID from P_Payrolldetail with (readuncommitted) where Payroll_Id = @payrollID
    order by EndDate desc; 
  set @ResultMessage = 'Outstanding Companies Warning: The following companies are not in the last period yet:';
  open CompanyCursor; 
    -- Loop over the rows of the query
    Company_LOOP: loop
      --        
      fetch next CompanyCursor into @Company_Id;
      if sqlstate = err_notfound then
        leave Company_LOOP
      end if;
      select max(Payrolldetail_Id) into @CurrentPeriod_ID from P_Calc_Master with (readuncommitted) where Company_Id = @Company_Id and 
        Payroll_Id = @payrollID;
      if @CurrentPeriod_ID < @LastPeriod_ID then
        -- Check that the company has employees who were active during the current tax year
        if (select count(*) from PL_Paydef_EMaster where company_id = @Company_Id and payroll_id = @payrollID and
          fn_P_EmployeeStillEngaged_YN(@Company_Id,PL_Paydef_EMaster.EMPLOYEE_ID,@payrollID,@CurrentPeriod_ID,
            fn_P_GetOpenRunType(@Company_Id,@payrollID,@CurrentPeriod_ID)) = 'Y') > 0 then
          set @Counter = @Counter + 1; 
          select Name into @Company_Name from C_Master with (readuncommitted) where Company_Id = @Company_Id;
          set @ResultMessage = @ResultMessage || '\x0a' ||@Company_Name;
        end if;
      end if;
    end loop Company_LOOP;
  -- Close the cursor
  close CompanyCursor;
  if @Counter > 0 then
    return @ResultMessage
  end if;
  --
  -- 3. Open Companies
  --
  set @ResultMessage = 'Open Companies Warning: The following companies are still open in the last period:';
  open CompanyCursor; 
    -- Loop over the rows of the query
    Company_LOOP: loop
      --        
      fetch next CompanyCursor into @Company_Id;
      if sqlstate = err_notfound then
        leave Company_LOOP
      end if;
      if (Select Run_Status from P_Calc_Master with (readuncommitted) where Company_Id = @Company_Id and 
        Payroll_Id = @payrollID and Payrolldetail_Id = @LastPeriod_ID) <> 3  then
        -- Check that the company has employees who were active during the current tax year
        if (select count(*) from PL_Paydef_EMaster where company_id = @Company_Id and payroll_id = @payrollID and
          fn_E_EmployeeActiveInRange_YN(@Company_Id,PL_Paydef_EMaster.EMPLOYEE_ID,
            (select Tax_Year_Start from P_Payroll where payroll_id = @payrollID),
            (select Tax_Year_End from P_Payroll where payroll_id = @payrollID)) = 'Y') > 0 then
          set @Counter = @Counter + 1; 
          select Name into @Company_Name from C_Master with (readuncommitted) where Company_Id = @Company_Id;
          set @ResultMessage = @ResultMessage || '\x0a' ||@Company_Name;
        end if;
      end if;
    end loop Company_LOOP;
  -- Close the cursor
  close CompanyCursor;
  
  if @Counter > 0 then
    return @ResultMessage
  end if;
  
  if exists (select * from p_calc_queue with (readuncommitted) ) then
    set @ResultMessage = 'Payrolls Calculating Warning: \x0a The tax year cannot be closed while a calculation is in progress';
    return @ResultMessage
  end if;
  
  -- CR 12388 - don't start closing in the can-close call... do it in fn_P_RollOverTaxYear call...
  -- insert into P_CLOSEYEAR_STATUS (Payroll_Id, Status) values (@payrollID,'CLOSING');
  
  set @ResultMessage = 'OK';
    
  return (@ResultMessage)  
END;

commit;
// requires: "P_C_TAXCERTIFICATE_EMPLOYER_REJECTION.txt","P_C_TAXCERTIFICATE_EMPLOYER_WARNING.txt","P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION.txt","P_E_TAXCERTIFICATE_EMPLOYEE_WARNING.txt"
CREATE OR REPLACE FUNCTION "DBA"."fn_HTTP_GetTaxCertificateWarningsAndErrors"(in @RUN_ID numeric(10)) 
returns long varchar
/*  
<purpose>  
fn_HTTP_GetTaxCertificateWarningsAndErrors is called to create the electronic 
file output record for the errors and warnings when generating tax certificates
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/05/18          Francois        Original
</history>
*/
begin
  declare @ErrorsAndWarnings long varchar;
  declare @FileCode numeric(5);
  declare @FileId numeric(10);
  declare @EmployeeName char(100);
  declare @Description varchar(2500);
  declare @CompanyErrorsCount integer;
  declare @CompanyWarningsCount integer;
  declare @EmployeeErrorsCount integer;
  declare @EmployeeWarningsCount integer;
  declare @Debug_On bit;
  
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop 
  declare CompanyErrorsCursor dynamic scroll cursor for select FILE_CODE,ERROR_DESCRIPTION 
	from P_C_TAXCERTIFICATE_EMPLOYER_REJECTION where RUN_ID = @RUN_ID
    order by FILE_CODE asc; 
  declare CompanyWarningsCursor dynamic scroll cursor for select FILE_CODE,WARNING_DESCRIPTION 
	from P_C_TAXCERTIFICATE_EMPLOYER_WARNING where RUN_ID = @RUN_ID
    order by FILE_CODE asc; 
  declare EmployeeErrorsCursor dynamic scroll cursor for select FILE_ID,FILE_CODE,ERROR_DESCRIPTION 
    from P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION 
	where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID)
    order by FILE_CODE asc; 
  declare EmployeeWarningsCursor dynamic scroll cursor for select FILE_ID,FILE_CODE,WARNING_DESCRIPTION 
    from P_E_TAXCERTIFICATE_EMPLOYEE_WARNING 
	where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID)
    order by FILE_CODE asc;

  set @Debug_On = 0;	
  
  -- Count the Errors and Warnings
  set @CompanyErrorsCount = (select count(*) from P_C_TAXCERTIFICATE_EMPLOYER_REJECTION where RUN_ID = @RUN_ID);
  set @CompanyWarningsCount = (select count(*) from P_C_TAXCERTIFICATE_EMPLOYER_WARNING where RUN_ID = @RUN_ID);
  set @EmployeeErrorsCount = (select count(*) from P_E_TAXCERTIFICATE_EMPLOYEE_REJECTION 
							where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID));
  set @EmployeeWarningsCount = (select count(*) from P_E_TAXCERTIFICATE_EMPLOYEE_WARNING 
							where FILE_ID in (select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID));
							
  -- Add Run_ID for future use  -------------------------------------------------------------------------
  
  set @ErrorsAndWarnings = @RUN_ID;
  
  -- Company Errors  ------------------------------------------------------------------------------------
  if (@CompanyErrorsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Company Errors</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@CompanyErrorsCount||'</b> company errors found</br>';
	-- Loop through all the errors
	open CompanyErrorsCursor with hold;
      CompanyErrors_LOOP: loop
        -- Fetch next record
		fetch next CompanyErrorsCursor into @FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave CompanyErrors_LOOP
        end if;
		-- Add the Errors to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description;
	  end loop CompanyErrors_LOOP; -- Close the cursor
    close CompanyErrorsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No company errors found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Company Errors  
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Company Warnings  ------------------------------------------------------------------------------------
  if (@CompanyWarningsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings ||'<b>Company Warnings</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@CompanyWarningsCount||'</b> company warnings found</br>';
	-- Loop through all the warnings
	open CompanyWarningsCursor with hold;
      CompanyWarnings_LOOP: loop
        -- Fetch next record
		fetch next CompanyWarningsCursor into @FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave CompanyWarnings_LOOP
        end if;
		-- Add the Warnings to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description;
	  end loop CompanyWarnings_LOOP; -- Close the cursor
    close CompanyWarningsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No company warnings found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Company Warnings 
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Employee Errors  ------------------------------------------------------------------------------------
  if (@EmployeeErrorsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings ||'<b>Employee Errors</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@EmployeeErrorsCount||'</b> employee errors found</br>';
	-- Loop through all the errors
	open EmployeeErrorsCursor with hold;
      EmployeeErrors_LOOP: loop
        -- Fetch next record
		fetch next EmployeeErrorsCursor into @FileID,@FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave EmployeeErrors_LOOP
        end if;
		-- Set the employee name
		set @EmployeeName = (select SURNAME||', '||FIRSTNAME||' ['||COMPANY_EMPLOYEE_NUMBER||']' from E_MASTER where EMPLOYEE_ID = (select EMPLOYEE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where FILE_ID = @FileId));
		-- Add the Errors to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description || '    (  ' || @EmployeeName || '  )';
	  end loop EmployeeErrors_LOOP; -- Close the cursor
    close EmployeeErrorsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No employee errors found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Employee Errors  
  set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br></br>';  --  Line Break
  -- Employee Warnings  ------------------------------------------------------------------------------------
  if (@EmployeeWarningsCount > 0) then 
    set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Employee Warnings</br>&nbsp;&nbsp;&nbsp;&nbsp;'||@EmployeeWarningsCount||'</b> employee warnings found</br>';
	-- Loop through all the warnings
	open EmployeeWarningsCursor with hold;
      EmployeeWarnings_LOOP: loop
        -- Fetch next record
		fetch next EmployeeWarningsCursor into @FileId,@FileCode,@Description;
		-- If there is no record leave the loop
		if sqlstate = err_notfound then
          leave EmployeeWarnings_LOOP
        end if;
		-- Set the employee name
		set @EmployeeName = (select SURNAME||', '||FIRSTNAME||' ['||COMPANY_EMPLOYEE_NUMBER||']' from E_MASTER where EMPLOYEE_ID = (select EMPLOYEE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where FILE_ID = @FileId));
		-- Add the Warnings to the output string
		set @ErrorsAndWarnings = @ErrorsAndWarnings || '</br>' || @FileCode || ' - ' || @Description || '    (  ' || @EmployeeName || '  )';
	  end loop EmployeeWarnings_LOOP; -- Close the cursor
    close EmployeeWarningsCursor;
  else 
	set @ErrorsAndWarnings = @ErrorsAndWarnings || '<b>Successful - No employee warnings found</b>';
  end if;
  ------------------------------------------------------------------------------------ End Employee Warnings  
  
  if @Debug_On = 1 then
    message(' ErrorsAndWarnings = '||@ErrorsAndWarnings) to console
  end if;
  return(@ErrorsAndWarnings)
end;

commit;

comment on procedure dba.fn_HTTP_GetTaxCertificateWarningsAndErrors is "<h2>Purpose</h2><br/>Prepares the Employee Warning Messages for the preview of the Tax Certificate electronic file creation.<br/><h2>Usage</h2>Payroll Tax Certificate Extract, South African electronic file.";
---------------------------------------
-- PROCEDURE TO RETURN SELECTED COLUMNS
---------------------------------------
CREATE OR REPLACE procedure dba.fn_HTTP_GETVALUE ( IN @sessionid  long VARCHAR, IN @tablename long varchar, IN @colname long varchar, IN @filter long varchar default null, IN @order long varchar default null ) 
BEGIN
    DECLARE Command LONG VARCHAR;


    -- If this session doesn't exist, return empty-handed
    if not exists(select * from WEB_SESSION where session_id=@sessionid) then
        return '';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    SET Command = 'SELECT ' || @colname || ' FROM ' || @tablename;
    message(command) type info to console;
    IF ISNULL( @filter,'') <> '' THEN
         SET Command = Command || ' WHERE ' || @filter;
    END IF;
    IF ISNULL( @order,'') <> '' THEN
         SET Command = Command || ' ORDER BY ' || @order;
    END IF;

    EXECUTE IMMEDIATE WITH RESULT SET ON Command;
END;
commit;---------------------------------------------
-- FUNCTION TO CHECK IF A PERSON IS LOGGED IN
---------------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_ISSESSIONLIVE ( IN @sessionid  long VARCHAR ) 
returns varchar(10)
BEGIN
      
     -- If this session already exists, clear it
    if exists(select * from WEB_SESSION where session_id=@sessionid) then
      return 'OK';
    else
      return 'NO SESSION';
    end if;

END; -------------------------------------
-- FUNCTION TO LOG A PERSON IN
-------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_LOGIN ( IN @web_login long varchar, IN @web_pwd long varchar, IN @old_sessionid  long VARCHAR ) 
returns long varchar
BEGIN
    DECLARE @session_id long VARCHAR;
    DECLARE @company_id numeric(10);
    DECLARE @employee_id numeric(10);
    
    // If this session already exists, clear it
    delete from WEB_SESSION where session_id=@old_sessionid;

    // Who is trying to log in?
    select company_id, employee_id into @company_id, @employee_id from 
        DBA.al_emaster_agroup 
        where 
            upper(trim(LOGIN_NAME_DECRYPTED))=upper(trim(@web_login)) and 
            upper(trim(fn_S_DecryptString(PASSWORD)))=upper(trim(@web_pwd)); 

    // If no matching login/pwd, return empty-handed
    if @@rowcount=0 then return ''; end if;    
 
    // If this person is logged in through another session, clear it
    delete from WEB_SESSION where company_id=@company_id and employee_id=@employee_id;

    // create a new session id
    set @session_id = newid();
    message('LOGIN:New session:'||@session_id);
 
    -- Update WEB_SESSION
    insert into WEB_SESSION (session_id, creation_time, update_time, timeout_minutes, company_id, employee_id)
        values (@session_id, now(), now(), 5, @company_id, @employee_id);

    return @session_id;

END; 
commit;

-------------------------------------
-- FUNCTION TO LOG A PERSON OUT
-------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_LOGOUT ( IN @sessionid  long VARCHAR ) 
returns varchar(10)
BEGIN
      
    // If this session already exists, clear it
    if exists(select * from WEB_SESSION where session_id=@sessionid) then
      delete from WEB_SESSION where session_id=@sessionid;
      message('LOGOUT:Removed session:'||@sessionid);
      return 'OK';
    else
      return 'NO SESSION';
      message('LOGOUT:Session:'||@sessionid||' no longer exists');
    end if;

END; 
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_PrepareTaxCertificateData" ( 
        IN @sessionID long VARCHAR, IN @payrollID long varchar, IN @companyID long varchar,
        IN @reconMonth long varchar, IN @reconYear long varchar, IN @tradeCode long varchar)
returns varchar(5000)
BEGIN
    declare @Run_ID numeric(10);
	declare @RSLT varchar(5000);
	
    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Prepare the Data?
	set @Run_ID = fn_P_TC_SouthAfrica_DoTaxCertificate(@companyID,@payrollID,'LIVE',1,@reconYear,@reconMonth,1,null,null,null,null,null,@tradeCode,null);
	set @RSLT = fn_HTTP_GetTaxCertificateWarningsAndErrors(@Run_ID); 
	
    return(@RSLT); 
END;


commit;

------------------------------------------------
-- Function to mark a person to be recalculated
------------------------------------------------
create or replace function dba.fn_HTTP_RequestCalc(IN @companyID long varchar, IN @employeeID long varchar, IN @payrollID long varchar, IN @payrollDetailID long varchar, IN @runtypeID long varchar)
returns bit
begin
  call sp_P_ResetEmployeeCalcStatus(@companyID, @employeeID, @payrollID, @payrollDetailID, @runtypeID,0);
    commit;
  call sp_P_CALC_Initiate_PerEmployee(@companyID, @employeeID, @payrollID, @payrollDetailID, @runtypeID,0);
    commit;
  return 1; 
end;------------------------------------------------------------------------
-- FUNCTION TO RESET THE LOGIN_NAME and PASSWORD for an ADMINISTRATOR
-- Note: The person must already be a PeopleWeb admin 
--        (have a record in AL_EMASTER_AGROUP)
-- The function returns the first name || '*' || login name || '*' || new_password
------------------------------------------------------------------------
create or replace function dba.fn_HTTP_RESET_ADMINPASSWORD(IN @adminEmail long varchar)
returns long varchar
begin
  declare @newPwd char(10);
  declare @firstName long varchar;
  declare @preferredName long varchar;
  declare @loginName long varchar;
  declare @returnString long varchar;
  declare @adminCompanyID numeric(10);
  declare @adminEmployeeID numeric(10);

    -- Randomise a new password (10 characters)
    set @newPwd = char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);
    set @newPwd = @newPwd||char(ceil(rand()*58)+65);

    -- Get Login Name
    select first PREFERRED_NAME, firstname, 
            fn_S_DecryptString(login_name), al.company_id, al.employee_id 
        into 
           @preferredName, @firstName, @loginName, @adminCompanyID, @adminEmployeeID
        from 
            al_emaster_agroup al join e_master e join e_contact ec
            on al.company_id=e.company_id and al.employee_id=e.employee_id
            and e.company_id=ec.company_id and e.employee_id=ec.employee_id
        where upper(trim(E_MAIL))=upper(trim(@adminEmail))
        order by al.employee_id;
    
    if (@loginName is null) then
        return 'msgNoAdminWithThisEmail'
    end if;

    -- Do we have a preferred name?
    if ((@preferredName is not null) and (trim(@preferredName)>'')) then
        set @firstName = @preferredName
    end if;
    -- OK, now let's update the password.
    update al_emaster_agroup set password = fn_S_EncryptString(@newPwd) where
        company_id=@adminCompanyID and employee_id=@adminEmployeeID;

    -- Return login_name + '*' + password
    return @firstName||'*'||@loginName||'*'||@newPwd;
end;-------------------------------------------------------------
-- FUNCTION TO SAVE CREDIT PURCHASE HISTORY
-------------------------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SaveCreditPurchase" ( 
        in @CREDIT_DATE timestamp, in @PROCESS_NAME char(50),in @NBCREDITS int, in @TRANS_ID char(50), in @TRANS_REF char(50), in @TRANS_STATUS char(50),
        in @RESULT_CODE char(50), in @RESULT_DESCR char(200), in @AUTH_CODE char(50),  in @AMT_IN_CENTS int, in @RISK_INDICATOR char(10), in @CHKSUM char(50), in @INV_TO_CC char(200), in @INV_TO_PERSON char(200))
returns int
BEGIN
   insert into dba.WEB_CREDITTRANSACTION (CREDIT_DATE ,PROCESS_NAME ,NBCREDITS ,TRANS_ID ,TRANS_REF ,TRANS_STATUS ,RESULT_CODE , RESULT_DESCR ,AUTH_CODE ,AMT_IN_CENTS ,RISK_INDICATOR, CHKSUM, INV_TO_CC,INV_TO_PERSON ) on existing update
        values (@CREDIT_DATE ,@PROCESS_NAME ,@NBCREDITS ,@TRANS_ID ,@TRANS_REF ,@TRANS_STATUS ,@RESULT_CODE ,@RESULT_DESCR ,@AUTH_CODE ,@AMT_IN_CENTS ,@RISK_INDICATOR, @CHKSUM, @INV_TO_CC, @INV_TO_PERSON);
    return @@rowcount;
END;create or replace FUNCTION "DBA"."fn_http_SaveEmployeeLoan"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @LOAN_ID numeric(10),in @LOAN_AMMOUNT numeric(16,4),in @LOAN_DATE timestamp,in @PREMIUM numeric(16,4),in @EMLOAN_ID numeric(10),in @PREMIUM_TEMP numeric(16,4),in @PAYROLL_ID numeric(10),in @PAYROLLDETAIL_ID numeric(10),in @REFERENCE char(50))
returns numeric(10)
begin
  /*   
  <purpose>  
  fn_http_SaveEmployeeLoan saves the Employee''s loan information details for PeopleWeb
  </purpose>  

  <history>
  Date          CR#     Programmer        Changes
  ----          ---     ----------        -------
  2014/09/18    14233   Liam Terblanche   Initial script
  
  </history>
  */

  -- No EMLOAN_ID?  Create a new record  
  if (@EMLOAN_ID is null) then
    if not exists(select * from pl_emaster_loan where company_id=@company_id and employee_id=@employee_id and loan_id=@loan_id) then
      insert into PL_EMASTER_LOAN (company_id, employee_id, loan_id, loan_ammount, loan_date, premium) values (@COMPANY_ID, @EMPLOYEE_ID, @LOAN_ID, 0, now(), 0);
    end if;
    select emloan_id into @EMLOAN_ID from pl_emaster_loan where company_id=@company_ID and employee_id=@employee_id and loan_id=@loan_id;
  end if;

  -- Now let's update it
        -- Loan Date
        if @LOAN_DATE is not null then
            update PL_EMASTER_LOAN set
                LOAN_DATE = @LOAN_DATE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- PREMIUM
        if @PREMIUM is not null then
            update PL_EMASTER_LOAN set
                PREMIUM = @PREMIUM
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- PREMIUM_TEMP
        if @PREMIUM_TEMP is not null then
            update PL_EMASTER_LOAN set
                PREMIUM_TEMP = @PREMIUM_TEMP
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- PAYROLL_ID
        if @PAYROLL_ID is not null then
            update PL_EMASTER_LOAN set
                PAYROLL_ID = @PAYROLL_ID
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- LOAN_AMMOUNT
        if @LOAN_AMMOUNT is not null then
            update PL_EMASTER_LOAN set
                LOAN_AMMOUNT = @LOAN_AMMOUNT
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
        -- "REFERENCE"
        if @REFERENCE is not null then
            update PL_EMASTER_LOAN set
                "REFERENCE" = @REFERENCE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and LOAN_ID = @LOAN_ID
        end if;
  
    commit;
  return(@EMLOAN_ID);
end;create or replace function "DBA"."fn_http_SaveEmployeeSaving"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @SAVING_ID numeric(10),in @START_DATE date,in @PREMIUM numeric(16,4),in @EMSAVING_ID numeric(10),in @DEPOSIT numeric(16,4),in @PREMIUM_TEMP numeric(16,4),in @PAYROLL_ID numeric(10),in @PAYROLLDETAIL_ID numeric(10),in @REFERENCE char(50))
returns numeric(10)
begin
  /*   
  <purpose>  
  fn_http_SaveEmployeeSaving saves the Employee''s Saving information details for PeopleWeb
  </purpose>  


  <history>
  Date          CR#     Programmer        Changes
  ----          ---     ----------        -------
  2014/09/18    14233   Liam Terblanche   Initial script
  </history>
  */
  -- No EMsaving_ID?  Create a new record  
  if (@EMsaving_ID is null) then
    if not exists(select * from pl_emaster_saving where company_id=@company_id and employee_id=@employee_id and saving_id=@saving_id) then
      insert into PL_EMASTER_saving (company_id, employee_id, saving_id, start_date, premium) values (@COMPANY_ID, @EMPLOYEE_ID, @saving_ID, now(), 0);
    end if;
    select emsaving_id into @EMsaving_ID from pl_emaster_saving where company_id=@company_ID and employee_id=@employee_id and saving_id=@saving_id;
  end if;

  -- Now let's update it
        -- START_DATE
        if @START_DATE is not null then
            update PL_EMASTER_saving set
                START_DATE = @START_DATE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PREMIUM
        if @PREMIUM is not null then
            update PL_EMASTER_saving set
                PREMIUM = @PREMIUM
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PREMIUM_TEMP
        if @PREMIUM_TEMP is not null then
            update PL_EMASTER_saving set
                PREMIUM_TEMP = @PREMIUM_TEMP
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- PAYROLL_ID
        if @PAYROLL_ID is not null then
            update PL_EMASTER_saving set
                PAYROLL_ID = @PAYROLL_ID
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
        -- DEPOSIT
        if @DEPOSIT is not null then
            update PL_EMASTER_saving set
                DEPOSIT = @DEPOSIT
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID;

        end if;
        -- "REFERENCE"
        if @REFERENCE is not null then
            update PL_EMASTER_saving set
                "REFERENCE" = @REFERENCE
            where
                COMPANY_ID = @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID and saving_ID = @saving_ID
        end if;
  
    commit;
  return(@EMsaving_ID);
end;----------------------------------------------
-- FUNCTION TO UPDATE COMPANY ACCOUNT INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCompanyAccountData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, 
            IN @accountType long VARCHAR default null, IN @accountName long varchar default null, 
            IN @accountDescription long varchar default null, IN @accountBank long varchar default null, IN @accountBranch long VARCHAR default null, 
            IN @accountBankCode long varchar default null, IN @accountBranchCode long varchar default null, IN @accountNumber long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- With the web interface, we ALWAYS only work with one account.  If more than one exist, only retain the first one.
    delete from c_bankdet where company_id=@companyID and account_id > 
        (select min(account_id) from c_bankdet where company_id=@companyID);

    -- Create an account if it doesn't already exist
    if not exists(select * from c_bankdet where company_id=@companyID ) then
        insert into c_bankdet (company_id, account_id) values (@companyID, 1);
    end if;

    -- Account Type
    if (@accountType is not null) then
        update c_bankdet set ACCOUNT_TYPE=@accountType
            where company_id=@companyID;
    end if;
    -- Account Name
    if (@accountName is not null) then
        update c_bankdet set ACCOUNT_NAME=@accountName
            where company_id=@companyID;
    end if;
    -- Account Description
    if (@accountDescription is not null) then
        update c_bankdet set ACCOUNT_DESCRIPTION=@accountDescription
            where company_id=@companyID;
    end if;
    -- Account Bank
    if (@accountBank is not null) then
        update c_bankdet set BANK=@accountBank
            where company_id=@companyID;
    end if;
    -- Account Branch
    if (@accountBranch is not null) then
        update c_bankdet set BRANCH=@accountBranch
            where company_id=@companyID;
    end if;
    -- Account Bank Code
    if (@accountBankCode is not null) then
        update c_bankdet set BANK_CODE=@accountBankCode
            where company_id=@companyID;
    end if;
    -- Account Branch Code
    if (@accountBranchCode is not null) then
        update c_bankdet set BRANCH_CODE=@accountBranchCode
            where company_id=@companyID;
    end if;
    -- Account Number
    if (@accountNumber is not null) then
        update c_bankdet set ACCOUNT_NUMBER=@accountNumber
            where company_id=@companyID;
    end if;
    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE COMPANY INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCompanyData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @name long varchar default null,IN @registrationNo long varchar default null,
        IN @taxNo long varchar default null,IN @phone long varchar default null,  IN @fax long varchar default null,IN @email long varchar default null,
        IN @diplomaticIndemnity char(1) default null,IN @pensionNo long varchar default null,IN @postalCountry long varchar default null,
        IN @postalProvince long varchar default null,IN @postalAddress long varchar default null,IN @postalSuburb long varchar default null,
        IN @postalCity long varchar default null, IN @postalCode long varchar default null, IN @physUnitNo long varchar default null,
        IN @physComplex long varchar default null,IN @physStreetNo long varchar default null, IN @physStreet long varchar default null,IN @physSuburb long varchar default null,
        IN @physCity long varchar default null,IN @physCode long varchar default null, IN @physProvince long varchar default null,IN @physCountry long varchar default null,
        IN @postalSameAsPhysical char(1) default null,IN @mainBusinessActivity long varchar default null,IN @tradeCode long varchar default null,
        IN @tradeSubCode long varchar default null, IN @setaCode long varchar default null,IN @sicCode long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);
    message('About to update for company '||@name);

    -- name
    if (@name is not null) then
        update c_master set name=@name  where company_id=@companyID;
    end if;
    -- registrationNo
    if (@registrationNo is not null) then
        update c_master set COMPANY_REGISTRATION=@registrationNo  where company_id=@companyID;
    end if;
    -- taxNo
    if (@taxNo is not null) then
        update c_master set TAX_NUMBER=@taxNo where company_id=@companyID;
    end if;
    -- phone
    if (@phone is not null) then
        update c_master set TEL=@phone where company_id=@companyID;
    end if;
    -- fax
    if (@fax is not null) then
        update c_master set FAX=@fax where company_id=@companyID;
    end if;
    -- email
    if (@email is not null) then
        update c_master set EMAIL=@email where company_id=@companyID;
    end if;
    -- diplomaticIndemnity
    if (@diplomaticIndemnity is not null) then
        if (@diplomaticIndemnity='1')then
          update c_master set DIPL_INDEMNITY='Y' where company_id=@companyID
        else
          update c_master set DIPL_INDEMNITY='N' where company_id=@companyID
        end if;
    end if;
    -- pensionNo
    if (@pensionNo is not null) then
        update c_master set PENSION_NUMBER=@pensionNo where company_id=@companyID;
    end if;
    -- postalCountry
    if (@postalCountry is not null) then
        insert into c_address (COMPANY_ID, POSTAL_COUNTRY) on existing update values(@companyID, @postalCountry);
    end if;
    -- postalProvince
    if (@postalProvince is not null) then
        insert into c_address (COMPANY_ID, POSTAL_PROVINCE) on existing update values(@companyID, @postalProvince);
    end if;
    -- postalAddress
    if (@postalAddress is not null) then
        insert into c_address (COMPANY_ID, POSTAL_ADDRESS) on existing update values(@companyID, @postalAddress);
    end if;
    -- postalSuburb
    if (@postalSuburb is not null) then
        insert into c_address (COMPANY_ID, POSTAL_SUBURB) on existing update values(@companyID, @postalSuburb);
    end if;
    -- postalCity
    if (@postalCity is not null) then
        insert into c_address (COMPANY_ID, POSTAL_CITY) on existing update values(@companyID, @postalCity);
    end if;
    -- postalCode
    if (@postalCode is not null) then
        insert into c_address (COMPANY_ID, POSTAL_CODE) on existing update values(@companyID, @postalCode);
    end if;
    -- physUnitNo
    if (@physUnitNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_UNITNO) on existing update values(@companyID, @physUnitNo);
    end if;
    -- physComplex
    if (@physComplex is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_COMPLEX) on existing update values(@companyID, @physComplex);
    end if;
    -- physStreetNo
    if (@physStreetNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_STREETNO) on existing update values(@companyID, @physStreetNo);
    end if;
    -- physStreet
    if (@physStreet is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_STREET) on existing update values(@companyID, @physStreet);
    end if;
    -- physSuburb
    if (@physSuburb is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_SUBURB) on existing update values(@companyID, @physSuburb);
    end if;
    -- physCity
    if (@physCity is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_CITY) on existing update values(@companyID, @physCity);
    end if;
    -- physCode
    if (@physCode is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_CODE) on existing update values(@companyID, @physCode);
    end if;
    -- physProvince
    if (@physProvince is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_PROVINCE) on existing update values(@companyID, @physProvince);
    end if;
    -- physCountry
    if (@registrationNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_COUNTRY) on existing update values(@companyID, @physCountry);
    end if;
    -- postalSameAsPhysical
    if (@postalSameAsPhysical is not null) then
       insert into c_address (COMPANY_ID, POSTAL_SAME_AS_PHYSICAL) on existing update values(@companyID, @postalSameAsPhysical);
    end if;
    -- mainBusinessActivity
    if (@mainBusinessActivity is not null) then
        update c_master set MAIN_BUSINESS_ACTIVITY=@mainBusinessActivity  where company_id=@companyID;
    end if;
    -- tradeCode
    if (@tradeCode is not null) then
        update c_master set TRADE_CLASS_ID=(select TRADE_CLASS_ID from A_TRADE_CLASSIFICATION where TRADE_CODE=@tradeCode )  where company_id=@companyID;
    end if;
    -- tradeSubCode
    if (@tradeSubCode is not null) then
        update c_master set TRADE_SUB_CODE=@tradeSubCode where company_id=@companyID;
    end if;
    -- setaCode
    if (@setaCode is not null) then
        update c_master set JOBSECTOR_ID=(select JOBSECTOR_ID from J_JOBSECTOR where SETA=@setaCode )  where company_id=@companyID;
    end if;
    -- sicCode
    if (@sicCode is not null) then
        update c_master set SIC_CODE_ID=(select SIC_CODE_ID from J_SIC_CODE where SIC_CODE=@sicCode )  where company_id=@companyID;
    end if;

    commit;
    return('OK'); 
END;

commit;
----------------------------------------------
-- FUNCTION TO UPDATE COST CENTRE INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCostCentreData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @costID long varchar,IN @name long varchar default null,
        IN @description long varchar default null,IN @number long varchar default null,  IN @parentCostID long varchar default null,
        IN @doDelete char(1) default 'N')
returns varchar(10)
BEGIN
    declare @newCostID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this cost centre?
    if (@costID='-1') then
        select  coalesce(max(cost_id)+1,1) into @newCostID from c_costmain where company_id=@companyID;
        message('New Cost ID:'||@newCostID);
        set @costID = @newCostID;
        insert into C_COSTMAIN (company_id, cost_id, name) values (@companyID, @costID, 'New');
    end if;

    -- Should we delete this cost centre?
    if (@doDelete='Y') then
        -- Don't cascade delete if the parent gets deleted
        update c_costmain set PARENT_COST_ID=null where company_id=@companyID and PARENT_COST_ID=@costID;
        -- Now delete the cost centre
        delete from c_costmain where  company_id=@companyID and cost_id=@costID
    else
        -- name
        if (@name is not null) then
            update c_costmain set name=@name  where company_id=@companyID and cost_id=@costID;
        end if;
        -- description
        if (@description is not null) then
            update c_costmain set DESCRIPTION=@description  where company_id=@companyID and cost_id=@costID;
        end if;
        -- number
        if (@number is not null) then
            update c_costmain set COST_NUMBER=@number where company_id=@companyID and cost_id=@costID;
        end if;
        -- parentCostID
        if (@parentCostID is not null) then
            update c_costmain set PARENT_COST_ID=@parentCostID where company_id=@companyID and cost_id=@costID;
        end if;
    end if;   

    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE ACCOUNT INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeeAccountData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @accountType long VARCHAR default null, IN @accountName long varchar default null, 
            IN @accountDescription long varchar default null, IN @accountBank long varchar default null, IN @accountBranch long VARCHAR default null, 
            IN @accountBankCode long varchar default null, IN @accountBranchCode long varchar default null, IN @accountNumber long varchar default null, 
            IN @accountHolderRelationship long varchar default null, IN @accountLineNumber long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- With the web interface, we ALWAYS only work with one account.  If more than one exist, only retain the first one.
    delete from e_bankdet where company_id=@companyID and employee_id=@employeeID and account_id > 
        (select min(account_id) from e_bankdet where company_id=@companyID and employee_id=@employeeID );

    -- Create an account if it doesn't already exist
    if not exists(select * from e_bankdet where company_id=@companyID and employee_id=@employeeID) then
        insert into e_bankdet (company_id, employee_id, account_id) values (@companyID, @employeeID, 1);
    end if;

    -- Account Type
    if (@accountType is not null) then
        update e_bankdet set ACCOUNT_TYPE=@accountType
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Name
    if (@accountName is not null) then
        update e_bankdet set ACCOUNT_NAME=@accountName
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Description
    if (@accountDescription is not null) then
        update e_bankdet set ACCOUNT_DESCRIPTION=@accountDescription
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Bank
    if (@accountBank is not null) then
        update e_bankdet set BANK=@accountBank
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Branch
    if (@accountBranch is not null) then
        update e_bankdet set BRANCH=@accountBranch
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Bank Code
    if (@accountBankCode is not null) then
        update e_bankdet set BANK_CODE=@accountBankCode
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Branch Code
    if (@accountBranchCode is not null) then
        update e_bankdet set BRANCH_CODE=@accountBranchCode
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Number
    if (@accountNumber is not null) then
        update e_bankdet set ACCOUNT_NUMBER=@accountNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Relationship
    if (@accountHolderRelationship is not null) then
        update e_bankdet set ACCOUNT_HOLDER_RELATIONSHIP=@accountHolderRelationship
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Account Line Number
    if (@accountLineNumber is not null) then
        update e_bankdet set LINE_NUMBER=@accountLineNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE ADDRESS INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeeAddressData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @physCountry long varchar default null, IN @physProvince long VARCHAR default null, IN @physPostalCode long varchar default null, 
            IN @physUnitNo long varchar default null, IN @physComplex long varchar default null, IN @physStreetNo long VARCHAR default null, 
            IN @physStreetOrFarmName long varchar default null, IN @physSuburb long varchar default null, IN @physCityOrTown long varchar default null, 
            IN @postalSameAsPhysical long varchar default null, IN @postCountry long varchar default null, IN @postProvince long varchar default null, 
            IN @postPostCode long varchar default null, IN @postPostOffice long VARCHAR default null, IN @postPostalAgencyOrSubUnit long varchar default null, 
            IN @postBoxOrBagNumber long varchar default null, IN @postIsPOBox long varchar default null, IN @postIsPrivateBag long varchar default null, 
            IN @postPostalAddressIsCareOf long varchar default null, IN @postCityOrTown long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Physical Address - Country
    if (@physCountry is not null) then
        update e_address set PHYS_HOME_COUNTRY=@physCountry
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Province
    if (@physProvince is not null) then
        update e_address set PHYS_HOME_PROVINCE=@physProvince
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Postal Code
    if (@physPostalCode is not null) then
        update e_address set PHYS_HOME_CODE=@physPostalCode
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Unit
    if (@physUnitNo is not null) then
        update e_address set PHYS_HOME_UNITNO=@physUnitNo
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Complex
    if (@physComplex is not null) then
        update e_address set PHYS_HOME_COMPLEX=@physComplex
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - Street No
    if (@physStreetNo is not null) then
        update e_address set PHYS_HOME_STREETNO=@physStreetNo
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - physStreetOrFarmName
    if (@physStreetOrFarmName is not null) then
        update e_address set PHYS_HOME_STREET=@physStreetOrFarmName
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - physSuburb
    if (@physSuburb is not null) then
        update e_address set PHYS_HOME_SUBURB=@physSuburb
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - physCityOrTown
    if (@physCityOrTown is not null) then
        update e_address set PHYS_HOME_CITY=@physCityOrTown
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Physical Address - postalSameAsPhysical
    if (@postalSameAsPhysical is not null) then
        update e_address set POSTAL_SAMEAS_PHYSICAL=@postalSameAsPhysical
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postCountry
    if (@postCountry is not null) then
        update e_address set POSTAL_COUNTRY=@postCountry
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postProvince
    if (@postProvince is not null) then
        update e_address set POSTAL_PROVINCE=@postProvince
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postPostCode
    if (@postPostCode is not null) then
        update e_address set HOME_POSTAL_CODE=@postPostCode
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postPostOffice
    if (@postPostOffice is not null) then
        update e_address set POSTAL_POST_OFFICE=@postPostOffice
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postPostalAgencyOrSubUnit
    if (@postPostalAgencyOrSubUnit is not null) then
        update e_address set POSTAL_AGENCY_SUBUNIT=@postPostalAgencyOrSubUnit
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postBoxOrBagNumber
    if (@postBoxOrBagNumber is not null) then
        update e_address set POSTAL_BOX_BAG_NUMBER=@postBoxOrBagNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postIsPOBox
    if (@postIsPOBox is not null) then
        update e_address set POSTAL_IS_POBOX=@postIsPOBox
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postIsPrivateBag
    if (@postIsPrivateBag is not null) then
        update e_address set POSTAL_IS_PRIVATEBAG=@postIsPrivateBag
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postPostalAddressIsCareOf
    if (@postPostalAddressIsCareOf is not null) then
        update e_address set POSTAL_CAREOF_INTERMEDIARY=@postPostalAddressIsCareOf
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Postal Address - postCityOrTown
    if (@postCityOrTown is not null) then
        update e_address set POSTAL_CITY_TOWN=@postCityOrTown
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE INFORMATION
----------------------------------------------
create or replace FUNCTION "DBA"."fn_HTTP_SetEmployeeData" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar default null, IN @firstname long varchar default null,
            IN @secondname long VARCHAR default null, IN @surname long varchar default null, IN @employeeNumber long varchar default null, 
            IN @preferredName long varchar default null, IN @title long VARCHAR default null, IN @initials long varchar default null, 
            IN @gender long varchar default null, IN @maritalStatus long varchar default null, IN @homeLanguage long varchar default null, 
            IN @groupJoinDate long varchar default null, IN @dateOfBirth long varchar default null, IN @taxNumber long varchar default null,
            IN @idNumber long VARCHAR default null, IN @passportHeld long varchar default null, IN @passportNumber long varchar default null,
            IN @race long varchar default null, IN @dateOfEngagement long varchar default null, IN @dateOfDischarge long varchar default null,
            IN @dischargeReason long varchar default null,
            IN @costCentreID long varchar default null, IN @jobTitle long varchar default null, IN @payroll long varchar default null,
            IN @essLogin long varchar default null, IN @essPassword long varchar default null, IN @adminLogin long varchar default null,
            IN @adminPassword long varchar default null, IN @isAdministrator char(1) default 'N',
            IN @telHome long varchar default null, IN @telWork long varchar default null, IN @telCell long varchar default null,
            IN @eMail long varchar default null,
            IN @undoDischarge char(1) default 'N')
returns varchar(10)
BEGIN
declare @newEmployeeID numeric(10);
declare @reasonID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- No Employee_ID?  Then this should be a new employee
    if ((@employeeID is null) or not exists(select * from e_master where company_id=@companyID and employee_id=@employeeID))
        then
        select coalesce(max(employee_id)+1,1) into @newEmployeeID from e_master where company_id=@companyID;
        set @employeeID=@newEmployeeID;
        // E_MASTER
        insert into e_master (company_id, employee_id, active_yn)
        values (@companyID, @employeeID, 'Y');
    end if;

    
    -- Firstname
    if (@firstname is not null) then
        update e_master set FIRSTNAME=@firstname
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Secondname
    if (@secondname is not null) then
        update e_master set SECONDNAME=@secondname
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Surname
    if (@surname is not null) then
        update e_master set SURNAME=@surname
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Employee Number
    if (@employeeNumber is not null) then
        update e_master set COMPANY_EMPLOYEE_NUMBER=@employeeNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Preferred Name
    if (@preferredName is not null) then
        update e_master set PREFERRED_NAME=@preferredName
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Title
    if (@title is not null) then
        update e_master set TITLE=@title
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Initials
    if (@initials is not null) then
        update e_master set INITIALS=@initials
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- GENDER
    if (@gender is not null) then
        call sp_P_SetEmployeeGender(@companyID, @employeeID, @gender);
    end if;
    -- Marital Status
    if (@maritalStatus is not null) then
        update e_master set MARITAL_STATUS=@maritalStatus
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Home Language
    if (@homeLanguage is not null) then
        update e_master set HOME_LANGUAGE=@homeLanguage
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- GROUP JOIN DATE
    if (@GroupJoinDate is not null) then
        update e_master set GROUP_JOIN_DATE=@groupJoinDate
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Birthdate
    if (@dateOfBirth is not null) then
        update e_master set BIRTHDATE=@dateOfBirth
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Tax Number
    if (@taxNumber is not null) then
        update E_NUMBER set TAX_NUMBER=@taxNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- ID Number
    if (@idNumber is not null) then
        update e_number set ID_NUMBER=@idNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Passport Held
    if (@passportHeld is not null) then
        update E_PASSPORT set PASSPORT_HELD=@passportHeld
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Passport Number
    if (@passportNumber is not null) then
        update E_PASSPORT set PASSPORT_NUMBER=@passportNumber
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- RACE
    if (@race is not null) then
        call sp_P_SetEmployeeRace(@companyID, @employeeID, @race);
    end if;

    -- Start Date
    if (@dateOfEngagement is not null) then
        -- New?
        if not exists (select * from e_on_off where company_id=@companyID and employee_id=@employeeID) then
            insert into e_on_off (company_id, employee_id, start_date) values (@companyID, @employeeID, @dateOfEngagement)
        else
            update e_on_off set START_DATE=@dateOfEngagement where
            company_id=@companyID and employee_id=@employeeID and start_date = (select max(start_date) from e_on_off where company_id=@companyID and employee_id=@employeeID)
        end if;
    end if;
    -- End Date
    if (@dateOfDischarge is not null) then
        update e_on_off set END_DATE=@dateOfDischarge where
        company_id=@companyID and employee_id=@employeeID and start_date = (select max(start_date) from e_on_off where company_id=@companyID and employee_id=@employeeID and start_date<=@dateOfDischarge)
    end if;
    -- Discharge Reason
    if (@dischargeReason is not null) then
        select min(reason_id) into @reasonID from j_jobposition_reason where trim(UIF_REASON)=trim(substring(@dischargeReason,1,locate(@dischargeReason,'[')-1));
        update e_on_off set REASON_ID=@reasonID  where
        company_id=@companyID and employee_id=@employeeID and start_date = (select max(start_date) from e_on_off where company_id=@companyID and employee_id=@employeeID);
    end if;

    -- Cost Centre Path
    if (@costCentreID is not null) then
        update e_master 
        set cost_id = @costCentreID
            where company_id=@companyID and employee_id=@employeeID
    end if;
    -- Job Position
    if (@jobTitle is not null) then
        delete from PL_EMASTER_PJOBPOSITION where company_id=@companyID and employee_id=@employeeID;
        insert into PL_EMASTER_PJOBPOSITION (company_id, employee_id, jobposition_id) 
            select @companyID, @employeeID, jobposition_id from P_JOBPOSITION where trim(JOB_NAME)=trim(@jobTitle)
    end if;      
    -- Payroll
    if (@payroll is not null) then
    	-- Unlink from existing payroll
    	delete from pl_paydef_emaster where company_id=@companyID and employee_id=@employeeID and payroll_id not in (select payroll_id from p_payroll where trim(name)=trim(@payroll));
        -- Link to new payroll
        insert into pl_paydef_emaster (company_id, employee_id, payroll_id) on existing skip 
                select @companyID, @employeeID, payroll_id from p_payroll where trim(name)=trim(@payroll);
        -- Link to open period
        insert into p_calc_employeelist (company_id, employee_id, payroll_id, payrolldetail_id, runtype_id, run_status, closed_yn)
            select company_id, employee_id, payroll_id, fn_P_GetOpenPeriod(company_id, payroll_id), 1, 1, 'N'
            from pl_paydef_emaster where employee_id not in (select employee_id from p_calc_employeelist where company_id=pl_paydef_emaster.company_id and
            payroll_id=pl_paydef_emaster.payroll_id and payrolldetail_id=fn_P_GetOpenPeriod(company_id, payroll_id));
        end if;      
      
    -- ESS username
    if (@essLogin is not null) then
        update e_master set WEB_LOGIN=fn_S_EncryptString(@essLogin)
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- ESS password
    if (@essPassword is not null) then
        update e_master set WEB_PWD=fn_S_EncryptString(@essPassword)
            where company_id=@companyID and employee_id=@employeeID;
    end if;

    -- Administrator
    if (@isAdministrator='N') then
        delete from al_emaster_agroup 
        where company_id=@companyID and employee_id=@employeeID;
    end if;
    if (@isAdministrator='Y') then
        if ((@adminLogin is not null) and (@adminPassword is not null)) then
            insert into AL_EMASTER_AGROUP (company_id, employee_id, group_id, login_name, PASSWORD) on existing update
                values (@companyID, @employeeID, 1, fn_S_EncryptString(@adminLogin),  fn_S_EncryptString(@adminPassword));
        end if;
    end if;

    -- Tel Home
    if (@telHome is not null) then
        update E_CONTACT set TEL_HOME=@telHome
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Tel Work
    if (@telWork is not null) then
        update E_CONTACT set TEL_WORK=@telWork
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Tel Cell
    if (@telCell is not null) then
        update E_CONTACT set TEL_CELL=@telCell
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- EMail
    if (@eMail is not null) then
        update E_CONTACT set E_MAIL=@eMail
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Undo Discharge
    if (@undoDischarge='Y') then
        update e_on_off set END_DATE=null, REASON_ID=null where
        company_id=@companyID and employee_id=@employeeID and start_date = (select max(start_date) from e_on_off where company_id=@companyID and employee_id=@employeeID);
		-- Make sure the person is in P_CALC_EMPLOYEELIST
        insert into p_calc_employeelist (company_id, employee_id, payroll_id, payrolldetail_id, runtype_id, run_status, closed_yn)
            select company_id, employee_id, payroll_id, fn_P_GetOpenPeriod(company_id, payroll_id), 1, 1, 'N'
            from pl_paydef_emaster where employee_id not in (select employee_id from p_calc_employeelist where company_id=pl_paydef_emaster.company_id and
            payroll_id=pl_paydef_emaster.payroll_id and payrolldetail_id=fn_P_GetOpenPeriod(company_id, payroll_id));
		
    end if;

    -- Other stuff
    update p_e_basic set rate_of_pay1=100 where company_id=@companyID and EMPLOYEE_ID=@employeeID and rate_of_pay1 is null;
    update p_e_basic set pay_method='Bank Transfer' where company_id=@companyID and EMPLOYEE_ID=@employeeID and pay_method is null;
    if not exists(select * from p_e_master where company_id=@companyID and EMPLOYEE_ID=@employeeID ) then
        insert into p_e_master (company_id, employee_id, tax_calc_method, NATURE_OF_PERSON_ID)
        values (@companyID, @employeeID, 'NON AVERAGING',1);
    end if;


    -- Link everyone to this workweek profile
    insert into LL_EMASTER_WORKWEEKPROFILE (company_id, employee_id, workweekprofile_id, start_date, end_date) on existing update
      select company_id, employee_id, 1, '2010/01/01', null from e_master;
    
    commit;

    return('OK'); 
END;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE PACKAGE INFO
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeePackage" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @entityID long VARCHAR default null, IN @entityType long varchar default null, 
            IN @value long varchar default null, IN @balance long varchar default null,
            IN @payrollID long VARCHAR default null, IN @referenceNo long varchar default null,
            IN @rfi long varchar default 'N',
            IN @unlink long VARCHAR default 'no', IN @removeTempVal long VARCHAR default 'no' )
returns varchar(10)
BEGIN
  declare @RSLT numeric;
    truncate table P_CALC_LASTCALCED_EMPLOYEE;
    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Make sure the record exists
    if not exists(select * from p_e_basic where company_id=@companyID and employee_id=@employeeID) then
        insert into P_E_BASIC  (company_id, employee_id, pay_per1) values (@companyID, @employeeID, 0);
    end if;

    -- Is this a VARIABLE?
    if ((@entityType='entityType_GenericVariable') or
        (@entityType='entityType_Deduction') or
        (@entityType='entityType_Interim') or
        (@entityType='entityType_PackageComponent') or
        (@entityType='entityType_Earning') or
        (@entityType='entityType_Tax')) then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_pvariable where  company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
        else
            begin
                if (@value is null) then
                    call sp_P_SaveDefaultVariableValues(@companyID, @employeeID, null,null,@entityID,null,'',null,null,null);
                    --insert into PL_EMASTER_PVARIABLE (company_id, employee_id, variable_id, default_value)
                    --values (@companyID, @employeeID,@entityID,'')
                else
                    call sp_P_SaveDefaultVariableValues(@companyID, @employeeID, null,null,@entityID,@value,@value,null,null,null);
                end if;
                -- Reference Number
                if (@referenceNo is not null) then
                    update PL_EMASTER_PVARIABLE set "REFERENCE"=@referenceNo 
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
                -- Remove TEMP value
                if (@removeTempVal='yes') then
                    update PL_EMASTER_PVARIABLE set temp_value=null
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
                -- Set RFI
                update PL_EMASTER_PVARIABLE set RFI_YN='N'
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                if (@rfi='Y') then
                    update PL_EMASTER_PVARIABLE set RFI_YN='Y'
                    where company_id=@companyID and employee_id=@employeeID and variable_id=@entityID;
                end if;
            end;
        end if;

    end if;
    -- Is this a LOAN?
    if (@entityType='entityType_Loan') then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_loan where  company_id=@companyID and employee_id=@employeeID and loan_id=@entityID
        else
            select fn_http_SaveEmployeeLoan(@companyID, @employeeID, @entityID, @balance, 
                        fn_P_GetPeriodStartDate(@payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID)),@value,null,null,@payrollID,fn_P_GetOpenPeriod(@companyID, @payrollID),@referenceNo ) into @RSLT;
            -- Remove TEMP value
            if (@removeTempVal='yes') then
                update pl_emaster_loan set premium_temp=null
                where company_id=@companyID and employee_id=@employeeID and loan_id=@entityID;
            end if;
        end if;
    end if;
    -- Is this a SAVING?
    if (@entityType='entityType_Saving') then
        --
        -- Unlinked?
        --
        if (@unlink='yes') then
            delete from pl_emaster_saving where  company_id=@companyID and employee_id=@employeeID and saving_id=@entityID
        else
            select fn_http_SaveEmployeeSaving(@companyID, @employeeID, @entityID, 
                        fn_P_GetPeriodStartDate(@payrollID, fn_P_GetOpenPeriod(@companyID, @payrollID)),@value,null,null,null,@payrollID,fn_P_GetOpenPeriod(@companyID, @payrollID),@referenceNo ) into @RSLT;
            -- Remove TEMP value
            if (@removeTempVal='yes') then
                update pl_emaster_saving set premium_temp=null
                where company_id=@companyID and employee_id=@employeeID and saving_id=@entityID;
            end if;
        end if;
    end if;

    -- We need to make sure the Payroll calculates this person as many times as it needs, without thinking it's the same calculation that simply got stuck
    truncate table P_CALC_LASTCALCED_EMPLOYEE;
    commit;
    return('OK'); 
END;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE PAYROLL INFO
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetEmployeePayrollInfo" ( 
            IN @sessionID long VARCHAR, IN @companyID long varchar, IN @employeeID long varchar, 
            IN @rateOfPay long VARCHAR default null,    IN @hoursPerDay long varchar default null, 
            IN @hoursPerWeek long varchar default null, IN @hoursPerMonth long varchar default null, 
            IN @daysPerWeek long VARCHAR default null,  IN @daysPerMonth long varchar default null, 
            IN @payPer long varchar default null, IN @nbMedDependents varchar default null)
returns varchar(10)
BEGIN
  declare @counter integer;

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Make sure the record exists
    if not exists(select * from p_e_basic where company_id=@companyID and employee_id=@employeeID) then
        insert into P_E_BASIC  (company_id, employee_id, pay_per1) values (@companyID, @employeeID, 0);
    end if;

    -- Rate Of Pay
    if (@rateOfPay is not null) then
        update p_e_basic set DBA.P_E_BASIC.RATE_OF_PAY1=@rateOfPay
            where company_id=@companyID and employee_id=@employeeID;
    end if;
     -- Hours Per Day
    if (@hoursPerDay is not null) then
        update p_e_basic set DBA.P_E_BASIC.HOURS_PER_DAY=@hoursPerDay
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Hours Per Week
    if (@hoursPerWeek is not null) then
        update p_e_basic set DBA.P_E_BASIC.HOURS_PER_WEEK=@hoursPerWeek
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Hours Per Month
    if (@hoursPerMonth is not null) then
        update p_e_basic set DBA.P_E_BASIC.HOURS_PER_MONTH=@hoursPerMonth
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Days Per Week
    if (@daysPerWeek is not null) then
        update p_e_basic set DBA.P_E_BASIC.DAYS_PER_WEEK=@daysPerWeek
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Days Per Month
    if (@daysPerMonth is not null) then
        update p_e_basic set DBA.P_E_BASIC.DAYS_PER_MONTH=@daysPerMonth
            where company_id=@companyID and employee_id=@employeeID;
    end if;
    -- Pay Per
    if (@payPer is not null) then
        update p_e_basic set DBA.P_E_BASIC.PAY_PER1=@payPer
            where company_id=@companyID and employee_id=@employeeID;
    end if;  
    -- Number of Medical Aid Dependents
    if (@nbMedDependents is not null) then
        set @counter=0;
        -- Delete what's already stored in DB
        delete from e_family where company_id=@companyID and employee_id=@employeeID;
        -- Now add new ones
        while @counter < @nbMedDependents loop
            insert into E_FAMILY (company_id, employee_id, family_id, FIRSTNAME, MED_DEPENDENT_YN)
            values (@companyID, @employeeID, @counter, 'Dependent @'||@counter, 'Y');
            set @counter = @counter + 1;
        end loop;
    end if;
    -- We need to make sure the Payroll calculates this person as many times as it needs, without thinking it's the same calculation that simply got stuck
    truncate table P_CALC_LASTCALCED_EMPLOYEE;
    -- Add into the calculation queue
    insert into p_calc_queue( company_id,employee_id,payroll_id,payrolldetail_id,runtype_id,run_status,paused_yn ) on existing update defaults off
    select company_id,employee_id,payroll_id,payrolldetail_id,runtype_id,run_status,'N' from p_calc_employeelist
      where payrolldetail_id = fn_P_GetOpenPeriod(company_id,payroll_id)
      and run_status = 2 and runtype_id = 1;

    commit;
    return('OK'); 
END;

commit;
---------------------------------------------- 
-- FUNCTION TO UPDATE JOB POSITION INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetJobPositionData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @jobPositionID long varchar,  
        IN @jobTitle long varchar,  IN @jobDescription long varchar default null,  IN @jobCode long varchar default null,  
        IN @jobGrade long varchar default null, IN @deleted char(1) default 'N')
returns numeric(10)
BEGIN
    declare @newJobPositionID numeric(10);
    set @newJobPositionID = -1;


    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return -1;
    end if;

    -- Can't have NULL description field
    if (@jobDescription is null) then
        set @jobDescription = @jobTitle
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this Job Position?
    if (@jobPositionID='-1') then
        //
        // Create Job Position
        //
        if not exists(select * from P_JOBPOSITION where trim(job_name)=trim(@jobTitle)) then
            begin
                select  coalesce(max(jobPosition_ID)+1,1) into @newJobPositionID from p_jobposition;
                set @jobPositionID = @newJobPositionID;
                insert into p_jobposition (company_id, jobposition_id, job_name, description, JOB_CODE, GRADE) 
                    values (@companyID, @jobPositionID, @jobTitle, '','','');
            end;
        end if;
    end if;

        
    -- Should we delete this entity?
    if (@deleted='Y') then
       delete from p_jobposition where company_id=@companyID and jobposition_id=@jobPositionID
    else
        //
        // Change VALUES in P_JOBPOSITION
        //
        begin
            if (@jobTitle is not null) then
                if not exists(select * from P_JOBPOSITION where trim(job_name)=trim(@jobTitle) and jobposition_id<>@jobPositionID) then
                    update p_jobposition set job_name=@jobTitle where company_id=@companyID and jobposition_id=@jobPositionID;
                end if;
            end if;
            if (@jobDescription is not null) then
              update p_jobposition set "DESCRIPTION"=@jobDescription where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
            if (@jobCode is not null) then
              update p_jobposition set JOB_CODE=@jobCode where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
            if (@jobGrade is not null) then
              update p_jobposition set GRADE=@jobGrade where company_id=@companyID and jobposition_id=@jobPositionID;
            end if;
        end;
    end if;

    return @jobPositionID; 
END;

commit;-------------------------------------------------------------
-- FUNCTION TO SAVE CHANGES TO TITLE DATA
-------------------------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetLanguageData" ( 
        IN @sessionID long VARCHAR,IN @languageID long VARCHAR, IN @language long varchar,IN @deleted char(1) default 'N')
returns numeric(10)
BEGIN
  declare @newLanguageID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return -1;
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this Language Entry?
    if (@languageID ='-1') then
        if not exists(select * from a_language where trim("language")=trim(@language)) then
           select coalesce(max(language_id)+1,1) into @newLanguageID from A_LANGUAGE;
           insert into A_LANGUAGE (language_id, "language") values (@newLanguageID,@language);
           set @languageID = @newLanguageID 
        end if;
    else
        -- Should we delete this Title?
        if (@deleted ='Y') then
           delete from A_LANGUAGE where language_id= @languageID
        ELSE 
            update A_LANGUAGE set "language"=@language where language_id=@languageID
        end if;
    end if;


    return(@languageID); 
END;

commit;---------------------------------------------- 
-- FUNCTION TO UPDATE PAY ENTITY INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetPayEntityData" ( 
        IN @sessionID long VARCHAR, IN @entityID long varchar, IN @entityType long varchar default null,
        IN @name long varchar default null,IN @caption long varchar default null, IN @valueUnit long varchar default null, IN @showOnPayslip char(1) default 'Y',
        IN @showYtdOnPayslip char(1) default 'N',
        IN @calculationFormula long varchar default null, 
        IN @payrollType char(20) default null,
        IN @interest long varchar default null, 
        IN @taxCode long varchar default null, 
        IN @doDelete char(1) default 'N')
returns numeric(10)
BEGIN
    declare @newEntityID numeric(10);
    set @newEntityID = -1;


    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- If the user tries to change from one entity to another 
    --  we need to delete the old entity and create a new one
	-- LOAN (already in P_VARIABLE)
	if ((@entityID <> '-1') and (@entityType='entityType_Loan') and (@name is not null) and 
		exists(select * from p_variable where variable_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_variable where variable_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- LOAN (already in P_SAVING)
	if ((@entityID <> '-1') and (@entityType='entityType_Loan') and (@name is not null) and 
		exists(select * from p_saving where saving_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of saving
		delete from p_saving where saving_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- SAVING (already in P_VARIABLE)
	if ((@entityID <> '-1') and (@entityType='entityType_Saving') and (@name is not null) and 
		exists(select * from p_variable where variable_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_variable where variable_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- SAVING (already in P_LOAN)
	if ((@entityID <> '-1') and (@entityType='entityType_Saving') and (@name is not null) and 
		exists(select * from p_loan where loan_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of loan
		delete from p_loan where loan_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- VARIABLE (already in P_SAVING)
	if ((@entityID <> '-1') and ((@entityType='entityType_Earning') or (@entityType='entityType_Deduction') or (@entityType='entityType_PackageComponent') or (@entityType='entityType_Interim')) and (@name is not null) and 
		exists(select * from p_saving where saving_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_saving where saving_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- VARIABLE (already in P_LOAN)
	if ((@entityID <> '-1') and ((@entityType='entityType_Earning') or (@entityType='entityType_Deduction') or (@entityType='entityType_PackageComponent') or (@entityType='entityType_Interim')) and (@name is not null) and 
		exists(select * from p_loan where loan_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of saving
		delete from p_loan where loan_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;

    -- Should we create this entity?
    if (@entityID='-1') then
        //
        // Create Loan
        //
        if (@entityType='entityType_Loan') then
            select  coalesce(max(loan_id)+1,1) into @newEntityID from p_loan;
            message('New Loan ID:'||@newEntityID);
            set @entityID = @newEntityID;
            insert into p_loan (loan_id, name, interest, COMPOUND_YN, BEFORECALC_YN) values (@entityID, @name, 0, 'N','N');
        else
            //
            // Create Saving
            //
            if (@entityType='entityType_Saving') then
                select  coalesce(max(saving_id)+1,1) into @newEntityID from p_saving;
                message('New Saving ID:'||@newEntityID);
                set @entityID = @newEntityID;
                insert into p_saving (saving_id, name, interest, COMPOUND_YN, BEFORECALC_YN) values (@entityID, @name, 0, 'N','N');
            else
                //
                // Create Variable
                //
                begin
                    select  coalesce(max(variable_id)+1,1) into @newEntityID from p_variable;
                    message('New Variable ID:'||@newEntityID);
                    set @entityID = @newEntityID;
                    insert into p_variable (variable_id, NAME, RESULT_TYPE, VARIABLE_TYPE, THEVALUE, ACTIVE_YN) 
                        values (@entityID, @name, 'EARNING', 'INPUT','0','Y');
                    -- Make sure all variables are available in all payrolls
                    insert into PL_PVARIABLE_ppayroll (payroll_id, variable_id) on existing SKIP 
                        select payroll_id, variable_id
                        from p_variable, p_payroll

                end;
            end if;
        end if;
    end if;

        
    
    -- Should we delete this entity?
    if (@doDelete='Y') then
        //
        // Delete Loan
        //
        if (@entityType='entityType_Loan') then
            delete from P_LOAN where loan_id=@entityID
        else
            //
            // Delete Saving
            //
            if (@entityType='entityType_Saving') then
                delete from P_SAVING where saving_id=@entityID
            else
                //
                // Delete Variable
                //
                delete from P_VARIABLE where variable_id=@entityID
            end if;
        end if;
    else
        begin
        //
        // Change LOANS
        //
        if (@entityType='entityType_Loan') then
            if (@name is not null) then
              update p_loan set name=@name  where loan_id=@entityID;
            end if;
            if (@caption is not null) then
              update p_loan set description=@caption  where loan_id=@entityID;
            end if;
            if (@calculationFormula is not null) then
              update p_loan set PREMIUM_FORMULA=@calculationFormula  where loan_id=@entityID;
            end if;
            if (@interest is not null) then
              update p_loan set INTEREST=convert(numeric(16,2),@interest)  where loan_id=@entityID;
            end if;
        else 
            //
            // Change SAVINGS
            //
            if (@entityType='entityType_Saving') then
                if (@name is not null) then
                  update p_saving set name=@name  where saving_id=@entityID;
                end if;
                if (@caption is not null) then
                  update p_saving set description=@caption  where saving_id=@entityID;
                end if;
                if (@calculationFormula is not null) then
                  update p_saving set PREMIUM_FORMULA=@calculationFormula  where saving_id=@entityID;
                end if;
                if (@interest is not null) then
                  update p_saving set INTEREST=convert(numeric(16,2),@interest) where saving_id=@entityID;
                end if;           
            else
                //
                // Change VARIABLES
                //
                begin
                    if (@name is not null) then
                      update p_variable set name=@name  where variable_id=@entityID;
                    end if;
                    if (@caption is not null) then
                      update p_variable set description=@caption  where variable_id=@entityID;
                    end if;
                    -- If this is a SYSTEM variable, don't touch it further!
                    if not exists(select * from p_variable where variable_id=@entityID and theOptions like '%SYSTEM%') then
                        -- Variable type-specific
                        if (@entityType is not null) then
                            // EARNING
                            if (@entityType='entityType_Earning') then
                                update p_variable set RESULT_TYPE='EARNING', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='YY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;     
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                            // DEDUCTION
                            if (@entityType='entityType_Deduction') then
                                update p_variable set RESULT_TYPE='DEDUCTION', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='YY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;         
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                            // INTERIM
                            if (@entityType='entityType_Interim') then
                                update p_variable set RESULT_TYPE='INTERIM', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='NN', SARS_CODE='' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;         
                                --update p_variable set SARS_CODE=null where variable_id=@entityID;
                            end if;
                            // PACKAGE_COMPONENT
                            if (@entityType='entityType_PackageComponent') then
                                update p_variable set RESULT_TYPE='EARNING', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='NY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;   
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                        end if;
                        if (@showOnPayslip is not null) then
                          update p_variable set DISPLAYONPAYSLIP_YN=@showOnPayslip  where variable_id=@entityID;
                        end if;
                        if (@showYtdOnPayslip is not null) then
                          update p_variable set DISPLAYYTD_YN=@showYtdOnPayslip  where variable_id=@entityID;
                        end if;
                        if (@calculationFormula is not null) then
                          update p_variable set THEVALUE=@calculationFormula, variable_type='FORMULA'  where variable_id=@entityID;
                        end if;
                        if (@payrollType is not null) then
                          update p_variable set PAYROLL_TYPE=@payrollType  where variable_id=@entityID;
                        end if;
                    end if;
           

                   
                end;
            end if;
        end if ;
        end;
    end if;

    return(@entityID); 
END;

commit;CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetPayrollData" ( 
        IN @sessionID long VARCHAR, IN @payrollID long varchar, IN @payrollName long varchar default null,
        IN @payrollType long varchar default null,IN @taxYearStart long varchar default null,  IN @taxYearEnd long varchar default null,
         IN @openPeriod long varchar default null,IN @taxCountry long varchar default null, IN @doDelete char(1) default 'N',
		 IN @taxNumber char(21) default '', IN @tradeClassificationCode char(10) default '',
		 IN @tradeClassificationSubCode char(10) default '',IN @sicCode char(10) default '')
returns varchar(10)
BEGIN
    declare @newPayrollID numeric(10);
    declare @newPayrollDetailID numeric(10);
    set @newPayrollID = -1;
    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this payroll?
    if (@payrollID='-1') then
        select  coalesce(max(payroll_id)+1,1) into @newPayrollID from p_payroll;
        message('New Payroll ID:'||@newPayrollID);
        set @payrollID = @newPayrollID;
        insert into p_payroll (payroll_id, name, description) values (@payrollID, @payrollName, @payrollName);
        -- Make sure all variables are available in all payrolls
        insert into PL_PVARIABLE_ppayroll (payroll_id, variable_id) on existing SKIP 
            select payroll_id, variable_id
            from p_variable, p_payroll
    end if;

    -- Should we delete this payroll?
    if (@doDelete='Y') then
        delete from p_payroll where payroll_id=@payrollID
    else
        -- payrollName
        if (@payrollName is not null) then
            update p_payroll set name=@payrollName  where payroll_id=@payrollID;
        end if;
        -- payrollType
        if (@payrollType is not null) then
            update p_payroll set payroll_type=@payrollType where payroll_id=@payrollID;
        end if;
        -- taxYearStart
        if (@taxYearStart is not null) then
            update p_payroll set tax_year_start=@taxYearStart  where payroll_id=@payrollID;
        end if;
        -- taxYearEnd
        if (@taxYearEnd is not null) then
            update p_payroll set tax_year_end=@taxYearEnd  where payroll_id=@payrollID;
        end if;
        -- taxCountry
        if (@taxCountry is not null) then
            if exists(select * from p_country where name=@taxCountry) then
                update p_payroll set country_id=(select country_id from p_country where name=@taxCountry)  where payroll_id=@payrollID;
            end if;
        end if;
		-- taxNumber
        update p_payroll set tax_number=@taxNumber  where payroll_id=@payrollID;
		-- tradeClassificationCode
        update p_payroll set TRADE_CLASS_ID=(SELECT TRADE_CLASS_ID FROM A_TRADE_CLASSIFICATION where TRADE_CODE = @tradeClassificationCode)  where payroll_id=@payrollID;
		-- tradeClassificationSubCode
        update p_payroll set TRADE_SUB_CODE=@tradeClassificationSubCode  where payroll_id=@payrollID;
		-- sicCode
        update p_payroll set SIC_CODE_ID=(select SIC_CODE_ID from J_SIC_CODE where SIC_CODE = @sicCode)  where payroll_id=@payrollID;
    end if;  
 
    -- Now that we've created/updated the payroll, see if we need to create the periods
    if (@newPayrollID>0) then
         message('fn_P_AutoCreatePayrollPeriods('||@newPayrollID||')');
        call fn_P_AutoCreatePayrollPeriods(@newPayrollID);
        -- Get the 'Open' period id
        select coalesce(payrolldetail_id,1) into @newPayrollDetailID from dba.p_payrolldetail where payroll_id=@newPayrollID and startdate=@openPeriod;
        message('openPeriod:'||@newPayrollDetailID);
        
        -- Open the first period
        insert into dba.P_CALC_MASTER(COMPANY_ID,PAYROLL_ID,PAYROLLDETAIL_ID,RUN_STATUS) values(
          (select company_id from web_session where session_id=@sessionID),@newPayrollID,@newPayrollDetailID,1);
        -- Open the basic run
        insert into P_CALC_DETAIL(COMPANY_ID,PAYROLL_ID,PAYROLLDETAIL_ID,RUNTYPE_ID, RUN_STATUS) values(
          (select company_id from web_session where session_id=@sessionID),@newPayrollID,@newPayrollDetailID,1,2);
        insert into P_CALC_DETAIL(COMPANY_ID,PAYROLL_ID,PAYROLLDETAIL_ID,RUNTYPE_ID, RUN_STATUS) values(
          (select company_id from web_session where session_id=@sessionID),@newPayrollID,@newPayrollDetailID,2,1);

    end if;


    return('OK'); 
END;


commit;

-------------------------------------------------------------
-- FUNCTION TO SAVE CHANGES TO TITLE DATA
-------------------------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetTitleData" ( 
        IN @sessionID long VARCHAR,IN @titleID long VARCHAR, IN @title long varchar,IN @deleted char(1) default 'N')
returns numeric(10)
BEGIN
  declare @newTitleID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return -1;
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this Title?
    
    if (@titleID ='-1') then
        if not exists(select * from WEB_LOOKUP_TITLE where trim(title)=trim(@title)) then
          select coalesce(max(title_id)+1,1) into @newTitleID from  WEB_LOOKUP_TITLE;
          insert into WEB_LOOKUP_TITLE (title_id, Title) values (@newTitleID, @title);
          set @titleID = @newTitleID
        end if
    else
        -- Should we delete this Title?
        if (@deleted ='Y') then
           delete from WEB_LOOKUP_TITLE where title_id = @titleID
        ELSE 
            update web_lookup_title set title=@title where title_id=@titleID
        end if;
    end if;


    return(@titleID); 
END;

commit;-------------------------------------------------
-- PROCEDURE TO UPDATE SELECTED COLUMNS
-- This function requires paramaters in a format 
-- that will allow the dynamic insertion/update
-- of records.
-- Example:
--   fn_HTTP_SetValue(...,"E_NUMBER","COMPANY_ID, EMPLOYEE_ID, ID_NUMBER", "23012,1002, 7007204324323") 
-------------------------------------------------
CREATE OR REPLACE function dba.fn_HTTP_SETVALUE ( IN @sessionid  long VARCHAR, IN @tablename long varchar, IN @columlist long varchar, IN @valuelist long varchar )
returns varchar(10)
BEGIN
    DECLARE @Command LONG VARCHAR;

    -- If this session doesn't exist, return empty-handed
    if not exists(select * from WEB_SESSION where session_id=@sessionid) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    SET @Command = 'INSERT INTO ' || @tablename || ' (' || @columlist || ') on existing update values (' || @valuelist || ');';
    EXECUTE IMMEDIATE @Command;
    return 'OK';
END;
comment on procedure dba.fn_HTTP_SETVALUE is 'Inserts/updates values in a given table with the appropriate columnlist, valuelist pairs.\nE.g. fn_HTTP_SetValue(...,"E_NUMBER","COMPANY_ID, EMPLOYEE_ID, ID_NUMBER", "23012,1002, 7007204324323")';

commit;
--------------------------------------
-- PROCEDURE TO TOUCH SESSION LIVENESS
--------------------------------------
create or replace function dba.fn_HTTP_TouchSession( IN @sessionid  long VARCHAR ) 
returns char(10)
BEGIN
  update dba.WEB_SESSION set update_time=now() where session_id=@sessionid;
return 'OK';
END;
CREATE OR REPLACE FUNCTION "DBA"."fn_P_TC_SouthAfrica_DoTaxCertificate"(in @COMPANY_ID numeric(10),in @PAYROLL_ID numeric(10),in @LIVE_INDICATOR char(4),
in @CONTACT_ID numeric(10),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),in @TYPE_ID numeric(10) default 1,
in @TaxYearEnd date default null,in @CompanyName char(120) default null,in @TaxNumber char(20) default null,
in @SDLNumber char(20) default null,in @UIFNumber char(20) default null,in @TradeCode char(4) default null,in @Run_Id numeric(10) default null)
returns numeric(10)
/*  
<purpose>  
fn_P_TC_SouthAfrica_DoTaxCertificate is called by the Tax Certificate creation
menu in PeopleWare to prepare and validate the data for tax certificates.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2008/05/10  6560    Christo Krause  Move IRP5 creation to stored procedure
</history>
*/
begin
  declare @ResultMessage varchar(150);
  declare @TaxYearStart date;
  declare @Mandatory bit;
  declare @FirstChar char(1);
  declare @ContactName char(30);
  declare @ContactTel char(20);
  declare @Email char(70);
  declare @IsValid bit;
  declare @UIFMatch bit;
  declare @PAYEMatch bit;
  declare @AtPosition tinyint;
  declare @TransactionYear numeric(4);
  declare @ReconPeriod char(6);
  declare @ReconDate date;
  declare @DomainPosition tinyint;
  declare @ExceptionCharacters char(120);
  declare @AddressSaved bit;
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  set @Debug_On=1;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_DoTaxCertificate ... ')
  end if;
  -- Load Payroll Information
  if @TaxYearEnd is null then
    select "DATE"(TAX_YEAR_START),"DATE"(TAX_YEAR_END) into @TaxYearStart,@TaxYearEnd from
      P_PAYROLL where PAYROLL_ID = @Payroll_Id
  else
    select "DATE"(TAX_YEAR_START) into @TaxYearStart from
      P_PAYROLL where PAYROLL_ID = @Payroll_Id
  end if;
  -- Start Copying .. 
  set @LIVE_INDICATOR=upper(@LIVE_INDICATOR);
  if @TYPE_ID = 4 then
    -- ITREG
    set @LIVE_INDICATOR='LIVE';
    set @RECONCILIATION_YEAR=0;
    set @RECONCILIATION_MONTH=0
  end if;
  -- List Company Header
  set @Run_Id=fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues(@COMPANY_ID,@PAYROLL_ID,@LIVE_INDICATOR,@CONTACT_ID,
    @RECONCILIATION_YEAR,@RECONCILIATION_MONTH,@TYPE_ID,@TaxYearEnd,@CompanyName,@TaxNumber,@SDLNumber,@UIFNumber,@TradeCode,@Run_Id);
  if @Run_Id is not null then
    if @TYPE_ID = 1 then
      -- CERTIFICATE TYPE = Standard
      select fn_P_TC_SouthAfrica_ListFileCodeValues(@Run_Id,@COMPANY_ID,E.EMPLOYEE_ID,@PAYROLL_ID,@LIVE_INDICATOR,
        @RECONCILIATION_YEAR,@RECONCILIATION_MONTH,@TYPE_ID,@TaxYearEnd) into #E from
        PL_Paydef_EMaster as P join P_E_Master as E on P.COMPANY_ID = E.COMPANY_ID and P.EMPLOYEE_ID = E.EMPLOYEE_ID where
        P.COMPANY_ID = @COMPANY_ID and PAYROLL_ID = @PAYROLL_ID and(E.IRP5_TYPE = 'S' or E.IRP5_TYPE is null) and
        fn_E_EmployeeActiveInRange_YN(E.COMPANY_ID,E.EMPLOYEE_ID,@TaxYearStart,@TaxYearEnd) = 'Y' order by
        E.EMPLOYEE_ID asc
    end if;
    if @TYPE_ID = 4 then
      -- CERTIFICATE TYPE = ITREG
      select fn_P_TC_SouthAfrica_ListFileCodeValues(@Run_Id,@COMPANY_ID,E.EMPLOYEE_ID,@PAYROLL_ID,@LIVE_INDICATOR,
        @RECONCILIATION_YEAR,@RECONCILIATION_MONTH,@TYPE_ID,@TaxYearEnd) into #E from
        PL_Paydef_EMaster as P join P_E_Master as E on P.COMPANY_ID = E.COMPANY_ID and P.EMPLOYEE_ID = E.EMPLOYEE_ID where
        P.COMPANY_ID = @COMPANY_ID and PAYROLL_ID = @PAYROLL_ID and E.IRP5_TYPE = 'R' order by
        E.EMPLOYEE_ID asc
    end if;
    if exists(select* from P_E_TaxCertificate_EMPLOYEE_REJECTION where FILE_ID = 
        any(select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID)) then
      update P_C_TAXCERTIFICATE_RUN set Run_Status = 1 where Run_id = @Run_Id
    end if; 
	if @LIVE_INDICATOR = 'LIVE' then
      update P_C_TAXCERTIFICATE_RUN set Run_Status = 3 where Run_id = @Run_Id
    else
      update P_C_TAXCERTIFICATE_RUN set Run_Status = 2 where Run_id = @Run_Id
    end if;
    if @Debug_On = 1 then
      message('Listing Employer Trailer values ... ')
    end if;
    set @IsValid=fn_P_TC_SouthAfrica_ListCoTrailerFileCodeValues(@Run_Id,@COMPANY_ID,@PAYROLL_ID,@LIVE_INDICATOR,
      @RECONCILIATION_YEAR,@RECONCILIATION_MONTH,@TYPE_ID,@TaxYearEnd)
  end if;
  return(@Run_Id)
end;
CREATE OR REPLACE FUNCTION "DBA"."fn_P_TC_SouthAfrica_FooterOutputRecord"(in @RUN_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_TC_SouthAfrica_FooterOutputRecord is called from the South African tax 
DLL to create the electronic file output record for the company Footer 
according to the formatting requirements specified by SARS.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/07/06  14705   Francois        Move Tax Certificate extract creation to a function so that PeopleWeb can use it.
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @TaxNumber char(21);
  declare @Counter integer;
  declare @IsValid bit;
  declare @IsStart bit;
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  declare DetailCursor dynamic scroll cursor for select R.FILE_CODE, R.VALUE
    from P_C_TAXCERTIFICATE_EMPLOYER_TRAILER as R
    where R.RUN_ID = @RUN_ID
    order by R.FILE_CODE;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_FooterOutputRecord ... ') to console
  end if;
  
  set @IsStart = 1;
  -- 1 Loop over the rows of the query
  open DetailCursor;
    DetailCode_LOOP: loop
      --
      --
      fetch next DetailCursor into @FileCode,@CodeValue;
      --
      if sqlstate = err_notfound then
        leave DetailCode_LOOP
      end if;
      -- 
      if @Debug_On = 1 then
        message('FileRecord = '||@FileRecord ||' FileCode = '||@FileCode||' CodeValue = '||@CodeValue) to console
      end if;
      -- 
      if @FileCode <> 9999 then
		if @IsStart = 1 then 
			set @FileRecord = @FileCode ||','|| @CodeValue;
			set @IsStart = 0;
		else
			set @FileRecord = @FileRecord ||','|| @FileCode ||','|| @CodeValue;
		end if;
      end if;
	  
	  set @FileCode = null;
      set @CodeValue = null;
    end loop DetailCode_LOOP;
  close DetailCursor;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_FooterOutputRecord: DetailCursor done ... ') to console
  end if;
  -- 2 Add the end code
  set @FileRecord = @FileRecord || ',9999';
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;CREATE OR REPLACE FUNCTION "DBA"."fn_P_TC_SouthAfrica_HeaderOutputRecord"(in @PAYROLL_ID numeric(10), in @RUN_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_TC_SouthAfrica_HeaderOutputRecord is called from the South African tax 
DLL to create the electronic file output record for the company Heading 
according to the formatting requirements specified by SARS.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/07/06  14705   Francois        Move Tax Certificate extract creation to a function so that PeopleWeb can use it.
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @FieldType char(10);
  declare @TaxNumber char(21);
  declare @Counter integer;
  declare @IsValid bit;
  declare @IsStart bit;
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  declare DetailCursor dynamic scroll cursor for select R.FILE_CODE, R.VALUE, C.FIELD_TYPE 
    from P_C_TAXCERTIFICATE_EMPLOYER_HEADER as R join P_SARS_CODE as C on R.FILE_CODE = C.SARS_CODE 
    where R.RUN_ID = @RUN_ID and COUNTRY_ID = 2 and C.RECORD_TYPE = 'Header'
    order by C.FILE_SEQUENCE,R.FILE_CODE;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_HeaderOutputRecord ... ') to console
  end if;
  
  set @IsStart = 1;
  -- 1 Loop over the rows of the query
  open DetailCursor;
    DetailCode_LOOP: loop
      --
      --
      fetch next DetailCursor into @FileCode,@CodeValue,@FieldType;
      --
      if sqlstate = err_notfound then
        leave DetailCode_LOOP
      end if;
      -- 
      if length(@CodeValue) > 0 then
        if @FieldType like 'A%' or @FieldType like 'F%' then
          set @CodeValue = '"'||@CodeValue||'"'
        end if;
        if @Debug_On = 1 then
          message('FileRecord = '||@FileRecord ||' FileCode = '||@FileCode||' CodeValue = '||@CodeValue) to console
        end if;
        -- 
        if @FileCode <> 9999 then
			if @IsStart = 1 then 
				set @FileRecord = @FileCode ||','|| @CodeValue;
				set @IsStart = 0;
			else
				set @FileRecord = @FileRecord ||','|| @FileCode ||','|| @CodeValue;
			end if;
        end if;
      end if;
	  -- Add the Tax, SDL and UIF numbers
	  if @FileCode = 2015 then
		set @TaxNumber = (select TAX_NUMBER from P_PAYROLL where PAYROLL_ID = @PAYROLL_ID);
		set @FileRecord = @FileRecord || ',2020,' || @TaxNumber || ',2022,"L' || substring(@TaxNumber,2,9) || '",2024,"U' || substring(@TaxNumber,2,9) || '"';
      end if;
      set @FileCode = null;
      set @CodeValue = null;
    end loop DetailCode_LOOP;
  close DetailCursor;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_HeaderOutputRecord: DetailCursor done ... ') to console
  end if;
  -- 2 Add the end code
  set @FileRecord = @FileRecord || ',9999';
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;// requires: "fn_P_TC_SouthAfrica_ListRun.txt","fn_P_TC_ListSpecifiedCharactersInANField.txt","fn_P_TC_RemoveListedCharactersFromString.txt","fn_P_TC_SouthAfrica_ListCoAddressFileCodeValues.txt","fn_P_TC_SouthAfrica_ListFileCodeValues.txt","fn_P_TC_SouthAfrica_ListCoTrailerFileCodeValues.txt","fn_P_TC_SouthAfrica_ListTaxCodeChangeWarnings.txt"

create or replace function "DBA"."fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues"( in @COMPANY_ID numeric(10),in @PAYROLL_ID numeric(10),in @LIVE_INDICATOR char(4),in @CONTACT_ID numeric(10),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),
  in @TYPE_ID numeric(10) default 1,in @TaxYearEnd date default null,
  in @CompanyName char(120) default null,in @TaxNumber char(21) default null,in @SDLNumber char(50) default null,in @UIFNumber char(20) default null,
  in @TradeCode char(4) default null,in @Run_Id numeric(10) default null ) 
returns numeric(10)
/*  
<purpose>  
fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues is called by the Tax Certificate
creation menu in PeopleWare to prepare and validate the data for certificates.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2008/05/10  6560    Christo Krause  Move IRP5 creation to stored procedure
2010/04/14  9327    Christo Krause  Fix the validation of Reconciliation periods for wages payrolls
2010/10/19  9002    Christo Krause  Include changes made to the tax codes of variables in the warnings
2010/10/19  13319   Christo Krause  Generate tax certificates for previous tax years
2013/08/21  13745   Christo Krause  Prevent right truncation errors
2014/02/25  14135   Christo Krause  ETI and SIC codes requirements
2014/03/28  13886   Christo Krause  SIC codes did not write to the file
2014/05/19  13772   Christo Krause  Use PAG membership number in the software field
2015/07/08  14705   Francois        PeopleWeb Tax Certificates need to format UIF and SDL numbers from the TAX number
</history>
*/
begin
  declare @ResultMessage varchar(150);
  declare @Archive bit;
  declare @TaxYearStart date;
  declare @PayrollTaxYear date;
  declare @Mandatory bit;
  declare @FirstChar char(1);
  declare @ContactName char(100);
  declare @ContactTel char(20);
  declare @SIC_Code char(10);
  declare @Email char(250);
  declare @IsValid bit;
  declare @UIFMatch bit;
  declare @UIFRequired bit;
  declare @PAYEMatch bit;
  declare @AtPosition tinyint;
  declare @TransactionYear numeric(4);
  declare @ReconPeriod char(6);
  declare @ReconDate date;
  declare @DomainPosition tinyint;
  declare @ExceptionCharacters char(120);
  declare @WarningString char(2500);
  declare @AddressSaved bit;
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues ... ') to console
  end if;
  //
  // Load Company Information
  //
  select isnull(trim(@CompanyName),trim(NAME)),isnull(trim(@TaxNumber),trim(TAX_NUMBER)),
    isnull(trim(@SDLNumber),trim(SDL_REGISTRATION_NUMBER)),isnull(trim(@UIFNumber),trim(UIF_NUMBER)),
    trim(EMAIL),isnull(trim(@TradeCode),trim(TRADE_SUB_CODE)) into @CompanyName,
    @TaxNumber,@SDLNumber,@UIFNumber,@Email,@TradeCode from C_MASTER where COMPANY_ID = @COMPANY_ID;
  //  
  // Load missing reference numbers from Payroll
  //
  select isnull(@TaxNumber,trim(TAX_NUMBER)),isnull(@UIFNumber,trim(UIF_NUMBER)),
    isnull(@TradeCode,trim(TRADE_SUB_CODE)) into @TaxNumber,
    @UIFNumber,@TradeCode from P_PAYROLL where PAYROLL_ID = @PAYROLL_ID;

  if exists (select * from DBA.a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
    set @UIFNumber = 'U'||substring(@TaxNumber,2,9);
    set @SDLNumber = 'L'||substring(@TaxNumber,2,9);
  end if;

  //  
  // Load Payroll Information
  //
  if @TaxYearEnd is null then
    select "DATE"(TAX_YEAR_START),"DATE"(TAX_YEAR_END) into @TaxYearStart,@TaxYearEnd
      from P_PAYROLL where PAYROLL_ID = @Payroll_Id
  else
    if(select datediff(month,@TaxYearEnd,Tax_Year_End) from P_Payroll where payroll_id = @PAYROLL_ID) > 6 then
      set @Archive = 1
    else
      set @Archive = 0
    end if;
    if @Archive = 0 then
      select "DATE"(TAX_YEAR_START),"DATE"(TAX_YEAR_END) into @TaxYearStart,@PayrollTaxYear
        from P_PAYROLL where PAYROLL_ID = @Payroll_Id
    else
      select first "DATE"(TAX_YEAR_START),"DATE"(TAX_YEAR_END) into @TaxYearStart,@PayrollTaxYear
        from P_PAYROLL_OLD where PAYROLL_ID = @Payroll_Id
        order by abs(datediff(day,@TaxYearEnd,TAX_YEAR_END)) asc
    end if
  end if;
  if @PAYROLL_ID is null and @TaxYearEnd is null then
    return null;
    -- Can not store a message without and Run_Id
    call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2030,'The Tax Year End has to be specified if the payroll_id is not specified.',
    @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
  end if;
  if @TaxYearStart is null and @TaxYearEnd is not null then
    set @TaxYearStart = dateadd(dd,1,dateadd(yy,-1,@TaxYearEnd))
  end if;
  if @RECONCILIATION_YEAR is null then
    if @PAYROLL_ID is not null then
      select first datepart(yy,MonthEnd) into @RECONCILIATION_YEAR from P_Payrolldetail
        where Payroll_Id = @PAYROLL_ID and datepart(mm,MonthEnd) = @RECONCILIATION_MONTH
    else
      if @RECONCILIATION_MONTH > 2 then
        set @RECONCILIATION_YEAR = datepart(yy,dateadd(yy,-1,@TaxYearEnd))
      else
        set @RECONCILIATION_YEAR = datepart(yy,@TaxYearEnd)
      end if
    end if end if;
  // Start Copying .. 
  set @LIVE_INDICATOR = upper(@LIVE_INDICATOR);
  if @TYPE_ID = 4 then
    --
    -- ITREG
    --
    set @LIVE_INDICATOR = 'LIVE';
    set @RECONCILIATION_YEAR = 0;
    set @RECONCILIATION_MONTH = 0
  end if;
  if @Debug_On = 1 then
    message('  Deleting existing company File Code values ... ') to console
  end if;
  --
  -- Get the run_id
  --
  if @Run_Id is null then
    set @Run_Id = fn_P_TC_SouthAfrica_ListRun(@COMPANY_ID,@PAYROLL_ID,@TYPE_ID,@TaxYearEnd,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
  end if;
  if @Run_Id is not null then
    --
    -- Clear Previous Entries
    --
    delete from P_C_TaxCertificate_Employer_Rejection where RUN_ID = @Run_Id;
    delete from P_C_TaxCertificate_Employer_Header where RUN_ID = @Run_Id;
    delete from P_C_TaxCertificate_Employer_Trailer where RUN_ID = @Run_Id;
    delete from P_E_TaxCertificate_EMPLOYEE_REJECTION where FILE_ID
       = any(select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID);
    delete from P_E_TaxCertificate_EMPLOYEE_WARNING where FILE_ID
       = any(select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID);
    delete from P_E_TaxCertificate_Employee_Record where FILE_ID
       = any(select FILE_ID from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID);
    delete from P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = @RUN_ID;
    if @Debug_On = 1 then
      message('Listing Company File Code values ... ') to console
    end if;
    -- Code 2010 Trading Name
    if @CompanyName is not null then
      set @ExceptionCharacters = fn_P_TC_ListSpecifiedCharactersInANField(@CompanyName,'/\\*?":><|');
      if @ExceptionCharacters is not null then
        set @CompanyName = fn_P_TC_RemoveListedCharactersFromString(@CompanyName,@ExceptionCharacters)
      end if;
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2010,@CompanyName,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2010,'Company name can not be null.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2015 ... ') to console
    end if;
    -- Code 2015 Test/Live indicator
    if @LIVE_INDICATOR in( 'TEST','LIVE' ) then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2015,@LIVE_INDICATOR,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2015,'Test/Live indicator can not be null.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2020 ... ') to console
    end if;
    -- Code 2020 PAYE Reference Number
    set @IsValid = 0;
    if length(@TaxNumber) = 10 then
      set @FirstChar = substring(@TaxNumber,1,1);
      if @FirstChar = '7' then
        -- Replace first digit with a 4 for Modulus 10 check
        set @IsValid = fn_S_Modulus10Test(4 || substring(@TaxNumber,2,9))
      else
        if @FirstChar in( '0','1','2','3','9' ) then
          set @IsValid = fn_S_Modulus10Test(@TaxNumber)
        end if
      end if end if;
    if @IsValid = 1 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2020,@TaxNumber,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2020,'"' || @TaxNumber || '" is not a valid PAYE Reference Number.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2022 ... ') to console
    end if;
    -- Code 2022 SDL Reference Number
    set @IsValid = 0;
    if length(@SDLNumber) = 10 then
      if length(@TaxNumber) = 10 and @FirstChar = '7' then
        if substring(@TaxNumber,2,9) = substring(@SDLNumber,2,9) then
          set @PAYEMatch = 1
        else
          set @PAYEMatch = 0
        end if
      else set @PAYEMatch = 1
      end if;
      if length(@UIFNumber) = 10 then
        if substring(@UIFNumber,2,9) = substring(@SDLNumber,2,9) then
          set @UIFMatch = 1
        else
          set @UIFMatch = 0
        end if
      else set @UIFMatch = 1
      end if;
      if substring(@SDLNumber,1,1) = 'L' then
        -- Replace first digit with a 4 for Modulus 10 check
        set @IsValid = fn_S_Modulus10Test(4 || substring(@SDLNumber,2,9))
      end if
    else set @UIFMatch = 1
    end if;
    if @IsValid = 1 and @PAYEMatch = 1 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2022,@SDLNumber,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      if not exists(select * from C_SETUP where COMPANY_ID = @COMPANY_ID
          and upper(ITEM) = 'EXEMPT FROM SDL' and upper(DESCRIPTION) = 'Y') then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2022,'"' || @SDLNumber || '" is not a valid SDL Reference Number.',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      end if end if;
    if @Debug_On = 1 then
      message('  Listing 2024 ... ') to console
    end if;
    -- Code 2024 UIF Reference Number
    set @IsValid = 0;
    if length(@UIFNumber) = 10 then
      if length(@TaxNumber) = 10 and @FirstChar = '7' then
        if substring(@TaxNumber,2,9) = substring(@UIFNumber,2,9) then
          set @PAYEMatch = 1
        else
          set @PAYEMatch = 0
        end if
      else set @PAYEMatch = 1
      end if;
      if substring(@UIFNumber,1,1) = 'U' then
        -- Replace first digit with a 4 for Modulus 10 check
        set @IsValid = fn_S_Modulus10Test(4 || substring(@UIFNumber,2,9))
      end if end if;
    if @IsValid = 1 and @PAYEMatch = 1 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2024,@UIFNumber,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      if length(@UIFNumber) > 0 then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2024,'"' || @UIFNumber || '" is not a valid UIF Reference Number.',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      else
        if @PAYROLL_ID is null then
          if exists(select * from P_YTDCHANGE as P join P_VARIABLE as V on P.VARIABLE_ID = V.VARIABLE_ID
              where(V.IND_CODE like '%99010%' or V.IND_CODE like '%99011%') and P.COMPANY_ID = @COMPANY_ID)
            or exists(select * from P_VARIABLES_FOR_EMPLOYEE as P join P_VARIABLE as V on P.VARIABLE_ID = V.VARIABLE_ID
              where(V.IND_CODE like '%99010%' or V.IND_CODE like '%99011%') and P.COMPANY_ID = @COMPANY_ID) then
            set @UIFRequired = 1
          end if
        else if exists(select * from P_YTDCHANGE as P join P_VARIABLE as V on P.VARIABLE_ID = V.VARIABLE_ID
              where(V.IND_CODE like '%99010%' or V.IND_CODE like '%99011%') and P.COMPANY_ID = @COMPANY_ID and P.PAYROLL_ID = @PAYROLL_ID)
            or exists(select * from P_VARIABLES_FOR_EMPLOYEE as P join P_VARIABLE as V on P.VARIABLE_ID = V.VARIABLE_ID
              where(V.IND_CODE like '%99010%' or V.IND_CODE like '%99011%') and P.COMPANY_ID = @COMPANY_ID and P.PAYROLL_ID = @PAYROLL_ID) then
            set @UIFRequired = 1
          end if end if;
        if @UIFRequired = 1 then
          call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2024,'A UIF Reference Number is mandatory.',
          @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
        end if
      end if end if;
    if @UIFMatch = 0 then
      if @IsValid = 1 then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2024,'"' || @SDLNumber || '" does not match UIF Reference Number "' || @UIFNumber || '".',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      else
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2022,'"' || @UIFNumber || '" does not match SDL Reference Number "' || @SDLNumber || '".',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      end if end if;
    -- Codes 2025 to 2027 
    select isnull(trim(NAME),''),isnull(trim(TEL_NUMBER),''),isnull(trim(E_MAIL),@Email) into @ContactName,
      @ContactTel,@Email from C_CONTACT where COMPANY_ID = @COMPANY_ID and CONTACT_ID = @CONTACT_ID;
    -- Code 2025 Contact Name (Mandatory)
    set @ExceptionCharacters = fn_P_TC_ListNonAlphaTypeCharactersInField(@ContactName);
    if @ExceptionCharacters is not null then
      set @ContactName = fn_P_TC_RemoveListedCharactersFromString(@ContactName,@ExceptionCharacters)
    end if;
    if length(@ContactName) > 0 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2025,@ContactName,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2025,'Contact name can not be null.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2026 ... ') to console
    end if;
    -- Code 2026 Contact Number (Mandatory)
    set @IsValid = 0;
    if length(@ContactTel) > 0 then
      if locate(@ContactTel,'/') > 0 then
        set @WarningString = 'The contact telephone number "' || @ContactTel || '" is not valid. Only one phone number can be specified per field, "/" cannot be used.'
      else
        set @ContactTel = fn_S_RemoveNonNumericFromString(@ContactTel);
        if length(@ContactTel) between 9 and 10 then
          call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2026,@ContactTel,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
          set @IsValid = 1
        else
          if length(@ContactTel) < 9 then
            set @WarningString = 'The contact telephone number "' || @ContactTel || '" must be at least 9 numeric characters long.'
          end if;
          if length(@ContactTel) > 10 then
            set @WarningString = 'The contact telephone number "' || @ContactTel || '" cannot be more than 10 characters long.'
          end if
        end if
      end if
    else set @WarningString = 'The contact telephone number cannot be null.'
    end if;
    if @IsValid = 0 then
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2026,@WarningString,
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    if @Debug_On = 1 then
      message('  Listing 2027 ... ') to console
    end if;
    -- Code 2027 (Optional)
    if length(@Email) between 1 and 70 then
      set @AtPosition = locate(@Email,'@',1);
      if @AtPosition > 1 then
        set @DomainPosition = locate(@Email,'.',@AtPosition);
        if(@DomainPosition > 1) and(length(substring(@Email,@DomainPosition)) > 2) then
          call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2027,@Email,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
        end if
      end if end if;
    -- Code 2028 (Optional)
    call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2028,'PAG1001',@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
    if @Debug_On = 1 then
      message('  Listing 2030, 2031 ... ') to console
    end if;
    -- Codes 2030 to 2031 
    if @TYPE_ID <> 4 then
      -- Not for ITREG
      if @PayrollTaxYear is not null and year(@TaxYearEnd) <> year(@PayrollTaxYear) then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2030,'The payroll tax year end "' || @PayrollTaxYear || '" does not match the specified tax year end "' || @TaxYearEnd || '".',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      else
        set @TransactionYear = year(@TaxYearEnd);
        if @TransactionYear between 1999 and(year(today())+1) then
          call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2030,@TransactionYear,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
        else
          call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2030,'The Transaction Year is "' || @TransactionYear || '". It may only be between 1999 and next year.',
          @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
        end if end if;
      set @IsValid = 0;
      set @ReconPeriod = @RECONCILIATION_YEAR || "right"('00' || @RECONCILIATION_MONTH,2);
      if length(@ReconPeriod) = 6 then
        set @ReconDate = @RECONCILIATION_YEAR || '-' || "right"('00' || @RECONCILIATION_MONTH,2) || '-01';
        if @ReconDate between @TaxYearStart and @TaxYearEnd then
          call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2031,@ReconPeriod,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
          set @IsValid = 1
        end if end if;
      if @IsValid = 0 then
        call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2031,'"' || @ReconPeriod || '" is an invalid Period of Reconciliation.',
        @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
      end if end if;
    if @Debug_On = 1 then
      message('  Listing 2035 ... ') to console;
      message('  TradeCode = "' || @TradeCode || '"') to console
    end if;
    -- Code 2082
    select SIC_Code into @SIC_Code from J_SIC_CODE where SIC_CODE_ID =
      (select SIC_CODE_ID from C_Master where COMPANY_ID = @COMPANY_ID);
    if length(trim(@SIC_Code)) = 5 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2082,@SIC_Code,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2082,'"' || @SIC_Code || '" is an invalid Standard Industry Classification code.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    -- Code 2035
    set @TradeCode = fn_S_RemoveNonNumericFromString(@TradeCode);
    if length(trim(@TradeCode)) = 4 then
      call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,2035,@TradeCode,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    else
      call sp_P_TC_SouthAfrica_WriteCoRejection(@Run_Id,@COMPANY_ID,2035,'"' || @TradeCode || '" is an invalid Trade Classification.',
      @PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH)
    end if;
    -- Codes 2061 to 2080
    if @Debug_On = 1 then
      message('  Listing Company Address File Code values ... ') to console
    end if;
    set @AddressSaved = fn_P_TC_SouthAfrica_ListCoAddressFileCodeValues(@Run_Id,
      @COMPANY_ID,@PAYROLL_ID,@TYPE_ID,@LIVE_INDICATOR,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
    -- End of Header record
    call sp_P_TC_SouthAfrica_WriteCoHeader(@Run_Id,@COMPANY_ID,9999,null,@PAYROLL_ID,@TYPE_ID,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH);
    if @Debug_On = 1 then
      message('  Wrote company header values ... ') to console
    end if;
    if exists(select * from P_C_TAXCERTIFICATE_Employer_Rejection where RUN_ID = @RUN_ID) then
      update P_C_TAXCERTIFICATE_RUN set Run_Status = 1,PROCESSED = now() where Run_id = @Run_Id;
      if @Debug_On = 1 then
        message('  Updated PROCESSED and RUN_STATUS ... ') to console
      end if
    else set @IsValid = fn_P_TC_SouthAfrica_ListTaxCodeChangeWarnings(@RUN_ID,@COMPANY_ID,@PAYROLL_ID,@TaxYearStart,@TaxYearEnd)
    end if end if;
  return(@Run_Id)
end;

commit;

comment on procedure fn_P_TC_SouthAfrica_ListCoHeaderFileCodeValues is "<h2>Purpose</h2><br/>Compiles the company header data for the South African Tax Certificates.";
// requires: "fn_S_RemoveNonNumericFromString.txt","sp_P_TC_SoutAfrica_WriteEeWarning.txt","sp_P_TC_SoutAfrica_WriteEeRecord.txt","sp_P_TC_SoutAfrica_WriteEeRejection.txt"

create or replace function "DBA"."fn_P_TC_SouthAfrica_ListEEContactFileCodeValues"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10),in @TYPE_ID numeric(10),in @TaxYearEnd date,in @NOP char(1),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),in @FILE_ID numeric(10))
returns bit
/*  
<purpose>  
fn_P_TC_SouthAfrica_ListEEContactFileCodeValues is called by the South African
tax certificate functions to retrieve the File code values for the codes 
3125 to 3138 for the employee and save it in P_E_TAXCERTIFICATE_Employee_Record.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2009/08/27  6021    Christo Krause  Move IRP5 creation to stored procedure
2013/08/21  13745   Christo Krause  Prevent right truncation errors
2015/07/14  14705   Francois        Tax Certificates for PeopleWeb need the business address from C_CONTACT
</history>
*/
begin
  declare @AddressSaved bit;
  declare @Mandatory bit;
  declare @WarningString char(2500);
  declare @Email char(250);  --  3125 FT70
  declare @HomeTelephone char(35);  -- 3135 N11
  declare @BusTelephone char(35);   -- 3136 N11
  declare @Fax char(35);    --  3137 N11
  declare @Cell char(20);   -- 3138 N10
  declare @ValidString bit;
  declare @AtPosition tinyint;
  declare @DomainPosition tinyint;
  -- Employee Contact details
  select isnull(trim(TEL_HOME),''),isnull(trim(TEL_WORK),''),isnull(trim(FAX_HOME),trim(FAX_WORK)),isnull(trim(TEL_CELL),''),
    isnull(trim(E_MAIL),'') into @HomeTelephone,@BusTelephone,@Fax,@Cell,@Email from 
    E_CONTACT where COMPANY_ID= @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID;
  -- PeopleWeb needs to get the business details from C_ADDRESS
  if exists (select * from DBA.a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
    select isnull(trim(TEL_NUMBER),'') into @BusTelephone from 
      C_CONTACT where COMPANY_ID= @COMPANY_ID;
  end if;
  -- 3125 E-mail (Optional)
  if length(@Email) between 1 and 70 then
    set @AtPosition = locate(@Email,'@',1);
    if @AtPosition > 1 then
      set @DomainPosition = locate(@Email,'.',@AtPosition);
      if (@DomainPosition > 1) and (length(substring(@Email,@DomainPosition)) > 2) then
        call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3125,@Email,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
      else
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3125,'The E-mail address must have a domain.');
      end if
    else
      call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3125,'The E-mail address must have an @ sign.');
    end if;
  end if;
  -- 3135 Home telephone (Optional)
  if length(@HomeTelephone) > 0 then
    if locate(@HomeTelephone,'/') > 0 then
      call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3135,'The home telephone number "'|| @HomeTelephone ||'" will not be used. Only one phone number can be specified per field, "/" cannot be used.');
    else
      set @HomeTelephone = fn_S_RemoveNonNumericFromString(@HomeTelephone);
      if length(@HomeTelephone) between 9 and 11 then
        call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3135,@HomeTelephone,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
      else
        if length(@HomeTelephone) < 9 then
          call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3135,'The home telephone number "'|| @HomeTelephone ||'" must be at least 9 numeric characters long.');
        end if;
        if length(@HomeTelephone) > 11 then
          call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3135,'The home telephone number "'|| @HomeTelephone ||'" will not be used, only 11 characters can be used.');
        end if;
      end if;
    end if;
  end if;
  -- 3136 Business telephone (Mandatory except for nature of person N)
  set @ValidString =0;
  if length(@BusTelephone) > 0 then
    set @ValidString =1;
    if locate(@BusTelephone,'/') > 0 then
      set @WarningString = 'The business telephone number "'|| @BusTelephone ||'" is not valid. Only one phone number can be specified per field, "/" cannot be used.';
      set @ValidString =0;
    else
      set @BusTelephone = fn_S_RemoveNonNumericFromString(@BusTelephone);
      if length(@BusTelephone) between 9 and 11 then
        call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3136,@BusTelephone,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
        set @AddressSaved = 1;
      else
        set @ValidString =0;
        if length(@BusTelephone) < 9 then
          set @WarningString = 'The business telephone number "'|| @BusTelephone || '" must be at least 9 characters long.'
        end if;
        if length(@BusTelephone) > 11 then
          set @WarningString = 'The business telephone number "'|| @BusTelephone || '" cannot be more than 11 characters long.'
        end if;
      end if;
    end if;
  end if;
  if @ValidString =0 then
    set @AddressSaved = 0;
    if @NOP <> 'N' then
      set @Mandatory = 1;
      if @WarningString is null then
        set @WarningString = 'The business telephone number is mandatory.'
      end if;
    else
      set @Mandatory = 0;
    end if;
    if @Mandatory = 1 then
      if  year(@TaxYearEnd) < '2011' then
        set @Mandatory = 0;
      end if;
    end if;
    if @Mandatory = 1 then
      call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3136,@WarningString)
    else
      if @WarningString is not null then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3136,@WarningString);
      end if;
    end if
  end if;
  -- 3137 Fax number (Optional)
  if @Fax is not null then
    if locate(@Fax,'/') > 0 then
      call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3137,'The fax number "'|| @Fax ||'" will not be used. Only one number can be specified per field, "/" cannot be used.');
    end if;
    set @Fax = fn_S_RemoveNonNumericFromString(@Fax);
    if length(@Fax) between 9 and 11 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3137,@Fax,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    else
      if length(@Fax) < 9 then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3137,'The fax number "'|| @Fax ||'" must be at least 9 numeric characters long.');
      end if;
      if length(@Fax) > 11 then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3137,'The fax number "'|| @Fax ||'" will not be used, only 11 characters can be used.');
      end if;
    end if;
  end if;
  -- 3138 Cell phone (Optional)
  if length(@Cell) > 0 then
    if locate(@Cell,'/') > 0 then
      call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3138,'The cell phone number "'|| @Cell ||'" will not be used. Only one cell phone number can be specified per field, "/" cannot be used.');
    else
      set @Cell = fn_S_RemoveNonNumericFromString(@Cell);
      if length(@Cell) = 10 then
        call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3138,@Cell,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
      else
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3138,'The cell phone number "'|| @Cell ||'" must be 10 numeric characters long.');
      end if;
    end if;
  end if;
  return(@AddressSaved);
end;

commit;

comment on procedure fn_P_TC_SouthAfrica_ListEEContactFileCodeValues is "<h2>Purpose</h2><br/>Compiles the employee contact details for the South African Tax Certificates.";
// requires: "sp_P_TC_SoutAfrica_WriteEeWarning.txt","sp_P_TC_SoutAfrica_WriteEeRecord.txt","sp_P_TC_SoutAfrica_WriteEeRejection.txt","fn_S_RemoveNonAlphaNumericFromString.txt"

create or replace function "DBA"."fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10),in @TYPE_ID numeric(10),in @TaxYearEnd date,in @NOP char(1),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),in @FILE_ID numeric(10))
returns bit
/*  
<purpose>  
fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues  is called by the South African
tax certificate functions to retrieve the File code values for the codes 
3144 to 3150 for the employee and save it in P_E_TAXCERTIFICATE_Employee_Record.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2009/09/07  6021    Christo Krause  Move IRP5 creation to stored procedure
2011/07/19  12568   Christo Krause  Remove special characters from the tax certificate
2013/08/21  13745   Christo Krause  Prevent right truncation errors
2015/07/08  14705   Francois        PeopleWeb Tax Certificates need to use company work address
</history>
*/
begin
  declare @AddressSaved bit;
  declare @Mandatory bit;
  declare @UnitNumber char(50);
  declare @Complex char(50);
  declare @StreetNumber char(50);
  declare @StreetOrFarm char(50);
  declare @SuburbOrDistrict char(50);
  declare @City char(50);
  declare @PostalCode char(10);
  declare @ValidString bit;
  -- Employee Address
if not exists (select * from DBA.a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
  select isnull(trim(PHYS_WORK_UNITNO),''),isnull(trim(PHYS_WORK_COMPLEX),''),isnull(trim(PHYS_WORK_STREETNO),''),isnull(trim(PHYS_WORK_STREET),''),
    isnull(trim(PHYS_WORK_SUBURB),''),isnull(trim(PHYS_WORK_CITY),''),isnull(trim(PHYS_WORK_CODE),'') into @UnitNumber,
    @Complex,@StreetNumber,@StreetOrFarm,@SuburbOrDistrict,@City,@PostalCode from 
    E_ADDRESS where COMPANY_ID= @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID;
else
  select isnull(trim(PHYSICAL_UNITNO),''),isnull(trim(PHYSICAL_COMPLEX),''),isnull(trim(PHYSICAL_STREETNO),''),isnull(trim(PHYSICAL_STREET),''),
    isnull(trim(PHYSICAL_SUBURB),''),isnull(trim(PHYSICAL_CITY),''),isnull(trim(PHYSICAL_CODE),'') into @UnitNumber,
    @Complex,@StreetNumber,@StreetOrFarm,@SuburbOrDistrict,@City,@PostalCode from 
    C_ADDRESS where COMPANY_ID= @COMPANY_ID;
end if;


  -- Mandatory 
  if length(@StreetOrFarm) = 0 or length(@PostalCode) = 0 or (length(@SuburbOrDistrict) = 0 and length(@City) = 0) then
    if @NOP <> 'N' then
      if  year(@TaxYearEnd) < '2011' then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3147,'Employee business address is not complete.')
      else
        if length(@StreetOrFarm) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3147,'Business address Street or Name of Farm is mandatory.')
        end if;
        if length(@SuburbOrDistrict) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3148,'Business address Suburb/District or City/Town is mandatory.')
        end if;
        if length(@City) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3149,'Business address Suburb/District or City/Town is mandatory.')
        end if;
        if length(@PostalCode) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3150,'Business address Postal Code is mandatory.')
        end if;
      end if;
    end if;
    set @AddressSaved = 0;
  else
    -- 3144 Unit Number (Optional)
    set @UnitNumber = fn_S_RemoveNonAlphaNumericFromString(@UnitNumber);
    if length(@UnitNumber) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3144,@UnitNumber,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3145 Complex (Optional)
    if length(@Complex) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3145,@Complex,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3146 Street Number (Optional)
    set @StreetNumber = fn_S_RemoveNonAlphaNumericFromString(@StreetNumber);
    if length(@StreetNumber) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3146,@StreetNumber,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3147 Street or Farm name (Mandatory)
    if length(@StreetOrFarm) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3147,@StreetOrFarm,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3148 Suburb or District (Conditional)
    if length(@SuburbOrDistrict) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3148,@SuburbOrDistrict,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3149 Suburb or District (Conditional)
    if length(@City) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3149,@City,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3150 Postal Code (Mandatory)
    set @PostalCode = fn_S_RemoveNonAlphaNumericFromString(@PostalCode);
    if length(@PostalCode) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3150,@PostalCode,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    set @AddressSaved = 1;
  end if;
  return(@AddressSaved);
end;

commit;

comment on procedure fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues is "<h2>Purpose</h2><br/>Compiles the employee work address data for the South African Tax Certificates.";
CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_EmployeeOutputRecord"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_EmployeeOutputRecord is called to create the electronic 
file output record for the employee according to the formatting requirements 
specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @IDNumber char(20);
  declare @PassportNumber char(20);
  declare @AlternateNumber char(25);
  declare @Surname char(120);
  declare @FirstNames char(90);
  declare @SecondName char(90);
  declare @DateOfBirth char(8);
  declare @EmploymentStart date;
  declare @EmploymentEnd date;
  declare @UIF_EmpStatusCode char(02);
  declare @UIF_ReasonCode char(02);
  declare @GrossTaxable numeric(15,2);
  declare @UIF_Earning numeric(15,2);
  declare @UIF_Contribution numeric(15,2);
  declare @Counter integer;
  declare @IsValid bit;
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_EmployeeOutputRecord ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- Build up the record per code
  set @FileRecord = '8001,"UIWK"';
  -- 8110 UIF Reference Number
  if (select coalesce(trim(TheValue),'Payroll') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichUIF') = 'Payroll' then
    select trim(UIF_Number) into @CodeValue from P_PAYROLL where Payroll_id = @PAYROLL_ID;
  end if;
  if @CodeValue is null then
    select trim(UIF_Number) into @CodeValue from C_MASTER where company_id = @COMPANY_ID
  end if;
  if length(@CodeValue) > 0 then
    set @CodeValue = fn_S_RemoveNonNumericFromString(@CodeValue);
    set @FileRecord = @FileRecord || ',8110,'||right('000000000'||@CodeValue,9)
  end if;
  set @CodeValue = null;
  -- 8200 ID Number
  select trim(ID_Number) into @IDNumber from E_Number where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  select trim(Passport_Number) into @PassportNumber from E_Passport where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  if length(@IDNumber) > 0 then
    if fn_E_SouthAfrica_IsValidIdNo(@IDNumber) = 0 then
      if length(@PassportNumber) = 0 then
        set @PassportNumber = @IDNumber;
      end if;
      if @IDNumber = fn_S_RemoveNonNumericFromString(@IDNumber) then
        set @IDNumber = left(@IDNumber,13)
      else
        set @IDNumber = null
      end if
    end if;
  end if;
  if length(@IDNumber) > 0 then
    set @FileRecord = @FileRecord || ',8200,'||@IDNumber
  end if;
  -- 8210 Other Number
  if length(@PassportNumber) = 0 then
    select trim(Permit_Number) into @PassportNumber from E_Nationality where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  end if;
  if length(@PassportNumber) > 0 then
    set @FileRecord = @FileRecord || ',8210,"'||left(@PassportNumber,16)||'"'
  end if;
  set @FileCode = null;
  set @CodeValue = null;
  -- Personal Information
  select left(trim(Company_Employee_Number),25),left(trim(Surname),120),left(trim(FirstName),90),left(trim(SecondName),90),
    convert(char(8),BirthDate,112) into @AlternateNumber,@Surname,@FirstNames,@SecondName,@DateOfBirth 
    from E_MASTER where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  -- 8220 Alternate Number (Company Employee Number)
  if length(@AlternateNumber) > 0 then
    set @FileRecord = @FileRecord || ','|| 8220 ||',"'||@AlternateNumber||'"'
  end if;
  -- 8230 Surname
  if length(@Surname) > 0 then
    set @FileRecord = @FileRecord || ','|| 8230 ||',"'||@Surname||'"'
  end if;
  -- 8240 First Names
  if length(@FirstNames) + length(@SecondName) > 0 then
    set @FileRecord = @FileRecord || ','|| 8240 ||',"'||left(trim(@FirstNames||' '||@SecondName),90)||'"'
  end if;
  -- 8250 Date of Birth
  if length(@DateOfBirth) > 0 then
    set @FileRecord = @FileRecord || ','|| 8250 ||','||@DateOfBirth
  end if;
  -- Employment Dates and Status
  select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
    into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
    from E_ON_OFF E
    where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID and E.START_DATE <= @MonthEnd
    order by E.START_DATE desc;
  -- 8260 Date Employed From
  if length(@EmploymentStart) = 0 then
    select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
      into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
      from E_ON_OFF E
      where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID
      order by E.START_DATE desc;
  end if;
  if length(@EmploymentStart) > 0 then
    set @FileRecord = @FileRecord || ','|| 8260 ||','||convert(char(8),@EmploymentStart,112)
  end if;
  -- 8270 Date Employed To
  if @EmploymentEnd between @EmploymentStart and dateadd(month,2,@MonthEnd) then
    set @FileRecord = @FileRecord || ','|| 8270 ||','||convert(char(8),@EmploymentEnd,112)
  end if;
  -- 8280 Employment Status Code
  if @UIF_EmpStatusCode = '01' then
    if exists (select * from L_History where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID
      and Leave_Type_Id in (4,9) and Convert_YN = 'N' and Cancel_Date is null
      and Start_Date < @MonthEnd and End_Date >= @MonthEnd) then
      set @UIF_EmpStatusCode = '09';
    end if
  end if;
  if length(@UIF_EmpStatusCode) > 0 then
    set @FileRecord = @FileRecord || ','|| 8280 ||','||@UIF_EmpStatusCode
  end if;
  -- Earnings and Contributions
  select sum(UIF_GROSSTAXABLE), lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)), sum(UIF_EE) + sum(UIF_ER) 
    into @GrossTaxable, @UIF_Earning, @UIF_Contribution
    from dba.vw_P_UIF where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID 
      and Payroll_Id = @PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
          and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd);
  if @UIF_Contribution = 0 or @GrossTaxable = 0 then
    select coalesce((select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and UIF_Grosstaxable > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc), 
    (select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc)) into @UIF_ReasonCode
  end if;
  -- 8290 Reason Code for Non-Contribution
  if @UIF_ReasonCode > 0 then
    set @UIF_ReasonCode = right('0'||@UIF_ReasonCode,2);
    set @FileRecord = @FileRecord || ','|| 8290 ||','||@UIF_ReasonCode
  end if;
  -- 8300 Gross Taxable Remuneration
  if length(@GrossTaxable) > 0 then
    set @FileRecord = @FileRecord || ','|| 8300 ||','||@GrossTaxable
  end if;
  -- 8310 Remuneration subject to UIF
  if length(@UIF_Earning) > 0 then
    set @FileRecord = @FileRecord || ','|| 8310 ||','||@UIF_Earning
  end if;
  -- 8320 UIF Contribution
  if length(@UIF_Contribution) > 0 then
    set @FileRecord = @FileRecord || ','|| 8320 ||','||@UIF_Contribution
  end if;
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_EmployeeOutputRecord is "<h2>Purpose</h2><br/>Prepares the UIF electronic file output record for the employee according to the formatting requirements specified by the Department of Labour.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_EmployeeWarnings"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_EmployeeWarnings is called to create the electronic 
file output record for the employee according to the formatting requirements 
specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @EeNumber char(25);
  declare @IDNumber char(20);
  declare @PassportNumber char(20);
  declare @AlternateNumber char(25);
  declare @Surname char(120);
  declare @FirstNames char(90);
  declare @SecondName char(90);
  declare @DateOfBirth char(8);
  declare @EmploymentStart date;
  declare @EmploymentEnd date;
  declare @UIF_EmpStatusCode char(02);
  declare @UIF_ReasonCode char(02);
  declare @GrossTaxable numeric(15,2);
  declare @UIF_Earning numeric(15,2);
  declare @UIF_EE numeric(15,2);
  declare @UIF_ER numeric(15,2);
  declare @Counter integer;
  declare @IsValid bit;
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_EmployeeWarnings ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- 8200 ID Number
  select trim(ID_Number) into @IDNumber from E_Number where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  select trim(Passport_Number) into @PassportNumber from E_Passport where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  if length(@IDNumber) > 0 then
    if fn_E_SouthAfrica_IsValidIdNo(@IDNumber) = 0 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; ID number ('|| @IDNumber ||') is not valid.'
      else
        set @FileRecord = 'Warning: ID number ('|| @IDNumber ||') is not valid.'
      end if;
      if length(@PassportNumber) = 0 then
        set @FileRecord = @FileRecord || ' - In the absence of a passport number it will be used in the Other Number field.'
      end if;
    end if;
  end if;
  -- 8210 Other Number
  if length(@PassportNumber) = 0 then
    select trim(Permit_Number) into @PassportNumber from E_Nationality where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  end if;
  -- Personal Information
  select left(trim(Company_Employee_Number),25),left(trim(Surname),120),left(trim(FirstName),90),left(trim(SecondName),90),
    convert(char(8),BirthDate,112) into @AlternateNumber,@Surname,@FirstNames,@SecondName,@DateOfBirth 
    from E_MASTER where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  -- 8220 Alternate Number (Company Employee Number)
  if length(@IDNumber) + length(@PassportNumber) + length(@AlternateNumber) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; No ID number, Other number or Alternate number is present.'
    else
      set @FileRecord = 'Warning: No ID number, Other number or Alternate number is present.'
    end if
  end if;
  -- 8230 Surname
  if length(@Surname) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The surname is omitted.'
    else
      set @FileRecord = 'Warning: The surname is omitted.'
    end if
  end if;
  -- 8240 First Names
  if length(@FirstNames) + length(@SecondName) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The first names are omitted.'
    else
      set @FileRecord = 'Warning: The first names are omitted.'
    end if
  end if;
  -- 8250 Date of Birth
  if length(@DateOfBirth) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The date of birth is omitted.'
    else
      set @FileRecord = 'Warning: The date of birth is omitted.'
    end if
  else
    if datediff(year,@DateOfBirth,today()) < 15 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The employee born on ('||@DateOfBirth||') is younger than 15 years.'
      else
        set @FileRecord = 'Warning: The employee born on ('||@DateOfBirth||') is younger than 15 years.'
      end if
    end if;
  end if;
  -- Employment Dates and Status
  select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
    into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
    from E_ON_OFF E
    where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID and E.START_DATE <= @MonthEnd
    order by E.START_DATE desc;
  -- 8260 Date Employed From
  if length(@EmploymentStart) = 0 then
    select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
      into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
      from E_ON_OFF E
      where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID
      order by E.START_DATE desc;
    if length(@EmploymentStart) > 0 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed from ('||@EmploymentStart||') is after the month end date ('|| @MonthEnd ||').'
      else
        set @FileRecord = 'Warning: The date employed from ('||@EmploymentStart||') is after the month end date ('|| @MonthEnd ||').'
      end if
    else
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed from is omitted.'
      else
        set @FileRecord = 'Warning: The date employed from is omitted.'
      end if
    end if;
  end if;
  -- 8270 Date Employed To
  -- 8280 Employment Status Code
  if @UIF_EmpStatusCode = '01' then
    if exists (select * from L_History where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID
      and Leave_Type_Id in (4,9) and Convert_YN = 'N' and Cancel_Date is null
      and Start_Date < @MonthEnd and End_Date >= @MonthEnd) then
      set @UIF_EmpStatusCode = '09';
    end if
  end if;
  if @EmploymentEnd between @EmploymentStart and dateadd(month,2,@MonthEnd) then
    if @UIF_EmpStatusCode in ('01','07','09') then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed to ('|| @EmploymentEnd ||') is valid but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      else
        set @FileRecord = 'Warning: The date employed to ('|| @EmploymentEnd ||') is valid but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      end if
    end if;
  else
    if @UIF_EmpStatusCode not in ('01','07','09') then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed to is omitted but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      else
        set @FileRecord = 'Warning: The date employed to is omitted but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      end if
    end if;
  end if;
  -- Earnings and Contributions
  select sum(UIF_GROSSTAXABLE), lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)), sum(UIF_EE),  sum(UIF_ER) 
    into @GrossTaxable, @UIF_Earning, @UIF_EE, @UIF_ER
    from dba.vw_P_UIF where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID 
      and Payroll_Id = @PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
          and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd);
  -- 8290 Reason Code for Non-Contribution
  if (@UIF_EE + @UIF_ER) = 0 or @GrossTaxable = 0 then
    select coalesce((select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and UIF_Grosstaxable > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc), 
    (select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc)) into @UIF_ReasonCode;
    if @UIF_ReasonCode is null then
      if (@UIF_EE + @UIF_ER) = 0 then
        if length(@FileRecord) > 0 then
          set @FileRecord = @FileRecord || '; The UIF contribution is zero without a valid reason code.'
        else
          set @FileRecord = 'Warning: The UIF contribution is zero without a valid reason code.'
        end if
      else
        if length(@FileRecord) > 0 then
          set @FileRecord = @FileRecord || '; The gross taxable remuneration is zero without a valid reason code.'
        else
          set @FileRecord = 'Warning: The gross taxable remuneration is zero without a valid reason code.'
        end if
      end if;
    end if;
  end if;
  -- 8310 Remuneration subject to UIF
  if @UIF_ReasonCode is null then
    if convert(numeric(13,0),@UIF_Earning)*0.02 <> (@UIF_EE + @UIF_ER) then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The UIF contribution is not 2% of the remuneration subject to UIF.'
      else
        set @FileRecord = 'Warning: The UIF contribution is not 2% of the remuneration subject to UIF.'
      end if
    end if;
  end if;
  -- 8320 UIF Contribution
  if @UIF_EE <> @UIF_ER then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; Company and Employee Contributions do not match.'
    else
      set @FileRecord = 'Warning: Company and Employee Contributions do not match.'
    end if
  end if;
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_EmployeeWarnings is "<h2>Purpose</h2><br/>Prepares the Employee Warning Messages for the preview of the UIF electronic file creation.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_FooterOutputRecord"(in @COMPANY_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_FooterOutputRecord is called to create the electronic 
file output record for footer/employer section according to the formatting 
requirements specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @GrossTaxable numeric(15,2);
  declare @UIF_Earning numeric(15,2);
  declare @UIF_Contribution numeric(15,2);
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_FooterOutputRecord ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- Build up the record per code
  set @FileRecord = '8002,"UIEM"';
  -- 8115 UIF Reference Number
  if (select coalesce(trim(TheValue),'Payroll') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichUIF') = 'Payroll' then
    select trim(UIF_Number) into @CodeValue from P_PAYROLL where Payroll_id = @PAYROLL_ID;
  end if;
  if @CodeValue is null then
    select trim(UIF_Number) into @CodeValue from C_MASTER where company_id = @COMPANY_ID
  end if;
  if length(@CodeValue) > 0 then
    set @CodeValue = fn_S_RemoveNonNumericFromString(@CodeValue);
    set @FileRecord = @FileRecord || ',8115,'||right('000000000'||@CodeValue,9)
  end if;
  set @CodeValue = null;
  -- 8120 PAYE Employer Number
  if (select coalesce(trim(TheValue),'Payroll') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichPAYE') = 'Payroll' then
    select trim(TAX_Number) into @CodeValue from P_PAYROLL where Payroll_id = @PAYROLL_ID;
  end if;
  if @CodeValue is null then
    select trim(TAX_Number) into @CodeValue from C_MASTER where company_id = @COMPANY_ID
  end if;
  if length(@CodeValue) > 0 then
    set @FileRecord = @FileRecord || ',8120,'||left(@CodeValue,10)
  end if;
  set @CodeValue = null;
  -- Earnings and Contributions Totals
  select sum(S.GrossTaxable),sum(S.UIF_Earning),sum(S.UIF_Contribution)
    into @GrossTaxable, @UIF_Earning, @UIF_Contribution
    from (select employee_id, sum(UIF_GROSSTAXABLE) as GrossTaxable, 
      lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)) as UIF_Earning, 
      sum(UIF_EE) + sum(UIF_ER) as UIF_Contribution
      from dba.vw_P_UIF where company_id = @COMPANY_ID and Payroll_Id = @PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
          and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
        and employee_id in (select employee_id from vw_P_OpenMonthEmployeeList 
          where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID)
      group by employee_id) as S;
  -- 8130 Total Gross Taxable Remuneration
  if length(@GrossTaxable) > 0 then
    set @FileRecord = @FileRecord || ','|| 8130 ||','||@GrossTaxable
  end if;
  -- 8135 Total Remuneration subject to UIF
  if length(@UIF_Earning) > 0 then
    set @FileRecord = @FileRecord || ','|| 8135 ||','||@UIF_Earning
  end if;
  -- 8140 Total UIF Contribution
  if length(@UIF_Contribution) > 0 then
    set @FileRecord = @FileRecord || ','|| 8140 ||','||@UIF_Contribution
  end if;
  -- 8150 Total Employees
  select count(employee_id) into @CodeValue from vw_P_OpenMonthEmployeeList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  if length(@CodeValue) > 0 then
    set @FileRecord = @FileRecord || ','|| 8150 ||','||@CodeValue
  end if;
  set @CodeValue = null;
  -- 8160 Employer email address
  select left(trim(EMail),50) into @CodeValue from C_Master where company_id = @COMPANY_ID;
  if length(@CodeValue) > 0 then
    set @FileRecord = @FileRecord || ','|| 8160 ||',"'||@CodeValue||'"'
  end if;
  set @CodeValue = null;
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_FooterOutputRecord is "<h2>Purpose</h2><br/>Prepares the UIF electronic file output record for the footer/employer according to the formatting requirements specified by the Department of Labour.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_HeaderOutputRecord"(in @COMPANY_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_HeaderOutputRecord is called to create the electronic 
file output record for header/creator section according to the formatting 
requirements specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @ContactName char(30);
  declare @ContactPhone char(16);
  declare @ContactEMail char(50);
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_HeaderOutputRecord ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- Build up the record per code
  set @FileRecord = '8000,"UICR",8010,"U1",8015,"E03"';
  -- 8020 UIF Reference Number
  if (select coalesce(trim(TheValue),'Payroll') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichUIF') = 'Payroll' then
    select trim(UIF_Number) into @CodeValue from P_PAYROLL where Payroll_id = @PAYROLL_ID;
  end if;
  if @CodeValue is null then
    select trim(UIF_Number) into @CodeValue from C_MASTER where company_id = @COMPANY_ID
  end if;
  if length(@CodeValue) > 0 then
    set @CodeValue = fn_S_RemoveNonNumericFromString(@CodeValue);
    set @FileRecord = @FileRecord || ',8020,'||right('000000000'||@CodeValue,9)
  end if;
  set @CodeValue = null;
  -- 8030 Test Live Indicator
  select coalesce(trim(TheValue),'LIVE') into @CodeValue from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'TestOrLive';
  if length(@CodeValue) > 0 then
    set @FileRecord = @FileRecord || ',8030,"'||@CodeValue||'"'
  end if;
  set @FileCode = null;
  set @CodeValue = null;
  -- Contact Information
  select left(trim(Name),30),left(trim(Tel_Number),16),left(trim(E_Mail),50)
    into @ContactName, @ContactPhone, @ContactEMail from C_CONTACT 
    where company_id = @COMPANY_ID and Contact_Id =
      (select coalesce(trim(TheValue),'1') from dba.P_EFTS_CONSTANT 
        where EFTS_ID = 15 and NAME = 'Contact'||@COMPANY_ID);
  -- 8040 Contact Person
  if length(@ContactName) > 0 then
    set @FileRecord = @FileRecord || ','|| 8040 ||',"'||@ContactName||'"'
  end if;
  -- 8050 Contact Telephone Number
  if length(@ContactPhone) > 0 then
    set @FileRecord = @FileRecord || ','|| 8050 ||',"'||@ContactPhone||'"'
  end if;
  -- 8060 Contact E-mail Address
  if length(@ContactEMail) > 0 then
    set @FileRecord = @FileRecord || ','|| 8060 ||',"'||@ContactEMail||'"'
  end if;
  -- 8070 Payroll Month
  set @FileRecord = @FileRecord || ','|| 8070 ||','||datepart(year,@MonthEnd)||right('0'||datepart(month,@MonthEnd),2);
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_HeaderOutputRecord is "<h2>Purpose</h2><br/>Prepares the UIF electronic file output record for the header/creator according to the formatting requirements specified by the Department of Labour.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
--------------------------
-- Add Credits
--------------------------
create or replace function dba.fn_WEB_AddCredits(@ProcessName char(50), @nbCredits integer)
returns integer
begin
  insert into dba.WEB_CREDIT values (now(),@ProcessName,fn_S_EncryptString('CREDIT|'||datepart(dw, now())||'|'||@nbCredits));
  return @nbCredits;
end;--------------------------
-- Get Credits Available
--------------------------
create or replace function dba.fn_WEB_GetCreditsAvailable(@ProcessName char(50))
returns integer
begin
  declare @rslt integer;
  declare @purchased integer;
  declare @used integer;

  -- How many credits were purchased?
  select coalesce(sum(fn_WEB_GetCreditsPurchased(credit_date, process_name)),0) into @purchased from WEB_CREDIT where process_name=@ProcessName;

  -- How many credits were used?
  select fn_WEB_GetCreditsUsed(@ProcessName) into @used;

  return @purchased - @used; 
end;--------------------------
-- Get Credits Purchased
--------------------------
create or replace function dba.fn_WEB_GetCreditsPurchased(@CreditDate timestamp, @ProcessName char(50))
returns integer
begin
  declare @rslt integer;
  declare @code char(100);
  declare @firstPipe integer;
  declare @secondPipe integer;
  
  -- Calculate the total number of credits purchased
  select fn_S_DecryptString(credit_code_encr) as code, locate(fn_S_DecryptString(credit_code_encr),'|',1) as firstPipe,
        locate(fn_S_DecryptString(credit_code_encr),'|',firstPipe+1) as secondPipe,
        convert(integer, substr(code,secondPipe+1,10)) as creditsPurchased into @code, @firstPipe, @secondPipe, @rslt from WEB_CREDIT
    where datepart(dw, CREDIT_DATE) =convert(integer, substr(code,firstPipe+1,1))
        and PROCESS_NAME=@ProcessName and CREDIT_DATE=@CreditDate;

  return coalesce(@rslt,0); 
end;--------------------------
-- Get Credits Used
--------------------------
create or replace function dba.fn_WEB_GetCreditsUsed(@ProcessName char(50))
returns integer
begin
  declare @rslt integer;

  -- For PAYROLL, we calculate the number of CALCULATED employees in P_CALC_EMPLOYEELIST after the date of the first
  --   time the client started using this payroll (after first CREDIT_DATE in WEB_CREDIT)
  if (@ProcessName='PAYROLL') then
    select count(*) into @RSLT from P_CALC_EMPLOYEELIST where calc_date >= coalesce((select min(CREDIT_DATE) from web_credit), '2000/01/01')
  end if;

  return coalesce(@rslt,0); 
end;// requires: "A_TRADE_CLASSIFICATION.txt", "A_TRADE_SUB_CLASSIFICATION.txt"

// Table P_PAYROLL - TABLE STRUCTURE Definition

if not exists (select * from systable where upper(table_name) = 'P_PAYROLL') then
  create table P_PAYROLL (
    "PAYROLL_ID"                     numeric(10)           not null,
    "NAME"                           char(50)              not null,
    "DESCRIPTION"                    varchar(100)          not null,
    "TAX_YEAR_START"                 date                  null,
    "TAX_YEAR_END"                   date                  null,
    "PAYROLL_TYPE"                   char(15)              null default 'Weekly',
    "COUNTRY_ID"                     numeric(10)           null,
    "TAX_NUMBER"                     char(21)              null,
    "UIF_NUMBER"                     char(15)              null,
	"TRADE_CLASS_ID"                 numeric(10)           null, 
    "TRADE_SUB_CODE"                 char(10)              null,    
	"SIC_CODE_ID"                    integer               null, 
    primary key ("PAYROLL_ID")
  );
  
  CREATE UNIQUE INDEX "idx" ON "DBA"."P_PAYROLL"
  ( "PAYROLL_ID" ASC );
  
end if;

-- CR 9022

if not exists (select * from syscolumn where column_name = 'TRADE_CLASS_ID' and 
  table_id = (select table_id from systable where table_name = 'P_PAYROLL')) then 
   ALTER TABLE "DBA"."P_PAYROLL" ADD "TRADE_CLASS_ID" numeric(10) NULL;
end if;

if not exists (select * from syscolumn where column_name = 'TRADE_SUB_CODE' and 
  table_id = (select table_id from systable where table_name = 'P_PAYROLL')) then 
   ALTER TABLE "DBA"."P_PAYROLL" ADD "TRADE_SUB_CODE" char(10) NULL;
end if;

if not exists (select * from syscolumn where column_name = 'SIC_CODE_ID' and 
  table_id = (select table_id from systable where table_name = 'P_PAYROLL')) then 
   ALTER TABLE "DBA"."P_PAYROLL" ADD "SIC_CODE_ID" integer NULL;
end if;

if not exists (select * from sysforeignkey where upper(role) = 'FK_A_TRADE_SUB_CLASSIFICATION' and
  foreign_table_id in (select table_id from systable where upper(table_name) = 'P_PAYROLL')) then
	alter table "DBA"."P_PAYROLL" add foreign key "FK_A_TRADE_SUB_CLASSIFICATION" ("TRADE_SUB_CODE") 
    references "DBA"."A_TRADE_SUB_CLASSIFICATION" ("TRADE_SUB_CODE") on delete set null on update cascade;
end if;

if exists (select * from sysforeignkey where upper(role) = 'A_TRADE_CLASSIFICATION' and
  foreign_table_id in (select table_id from systable where upper(table_name) = 'P_PAYROLL')) then
	alter table "DBA"."P_PAYROLL" drop foreign key "A_TRADE_CLASSIFICATION"
end if;

commit;
create or replace procedure
DBA.sp_P_SetEmployeeGender(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),aGender char(20))
begin
  -- CR 14233 : PeopleWeb
  declare @GROUP_ID numeric(10);
  update e_master set GENDER=upper(left(aGender,1)) where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  // If I cannot get the 'aGender' from c_group, assign to 'Unconfirmed Gender'
  select GROUP_ID into @GROUP_ID from c_group where parent_group_id = 1 and
    upper(aGender) = upper(DESCRIPTION);
  if @GROUP_ID is null then set @GROUP_ID=70
  end if; // Save into database
  delete from e_group where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and
    group_id = 1;
  insert into e_group(COMPANY_ID,EMPLOYEE_ID,GROUP_ID,SUBGROUP1_ID) values(
    @COMPANY_ID,@EMPLOYEE_ID,1,@GROUP_ID)
end;

commit;----------------------------------------------
-- FUNCTION TO UPDATE EMPLOYEE RACE
----------------------------------------------
CREATE OR REPLACE PROCEDURE "DBA"."sp_P_SetEmployeeRace"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10), in @RACE char(50))
begin
  -- If the race is different from what is presently assigned to this employee, replace it
  if not exists (select * from e_group where company_id=@company_id and employee_id=@EMPLOYEE_ID and group_id=2 
        and subgroup1_id=(select group_id from c_group where upper(trim(description))=@RACE)) then
    delete from e_group where company_id=@company_id and employee_id=@EMPLOYEE_ID and group_id=2;
    insert into e_group  (company_id, employee_id, group_id, subgroup1_id) 
        values (@company_id, @EMPLOYEE_ID, 2, (select group_id from c_group where upper(trim(description))=@RACE)) ;
  end if;
end;-------------------------------------------------------
-- Automatically calculate employees
--------// requires: "vw_P_OpenMonthPeriodList.txt"

create or replace view dba.vw_P_OpenMonthEmployeeList
  /*  
  <purpose>  
  vw_P_OpenMonthEmployeeList supplies a list of all employees calculated or 
  skipped in the current open month per company payroll combination.
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2014/12/18    14233   Christo Krause  Move the UIF extract to a view
  </history>
  */
  as select distinct Company_Id,Employee_Id,Payroll_id from dba.P_REPORT_EMPLOYEELIST as U where Payrolldetail_id in 
    (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
      where Company_Id = U.Company_Id and Payroll_Id = U.Payroll_Id);

comment on view dba.vw_P_OpenMonthEmployeeList is "<h2>Purpose</h2><br/>Supplies a list of all employees calculated or skipped in the current open month per company payroll combination.";
create or replace view dba.vw_P_OpenMonthPeriodList
  /*  
  <purpose>  
  vw_P_OpenMonthPeriodList supplies a list of all Payroll Periods in the 
  current open month per company payroll combination.
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2015/01/13    14233   Christo Krause  Move the UIF extract to a view
  </history>
  */
  as select O.Company_id,P.Payroll_id, P.Payrolldetail_id,O.MonthEnd from dba.P_PAYROLLDETAIL as P join
    (select Company_id,Payroll_id, Payrolldetail_id, 
      (select MonthEnd from dba.P_PAYROLLDETAIL where Payroll_id = M.Payroll_id and Payrolldetail_id =M.Payrolldetail_id) as MonthEnd
      from dba.P_CALC_MASTER as M where Run_Status < 3) as O
    on P.Payroll_id = O.Payroll_id and P.MonthEnd = O.MonthEnd;

comment on view dba.vw_P_OpenMonthPeriodList is "<h2>Purpose</h2><br/>Supplies a list of all Payroll Periods in the current open month per company payroll combination."

// requires: "vw_P_OpenMonthEmployeeList.txt","fn_P_UIF_SouthAfrica_EmployeeOutputRecord.txt"

create or replace view dba.vw_P_SouthAfrica_UIFextract
  /*  
  <purpose>  
  vw_P_SouthAfrica_UIFextract supplies data in the format required for 
  South African electronic UIF submission.
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2015/01/06    14233   Christo Krause  Move the UIF extract to a view
  </history>
  */
  as select Company_Id,Employee_Id,Payroll_id,
    dba.fn_P_UIF_SouthAfrica_EmployeeOutputRecord(Company_Id,Employee_Id,Payroll_id) as Employee_Record
  from dba.vw_P_OpenMonthEmployeeList;

comment on view dba.vw_P_SouthAfrica_UIFextract is "<h2>Purpose</h2><br/>Supplies the UIF electronic file output records of all employees calculated or skipped in the current open month per company payroll combination."
// requires: "vw_P_OpenMonthEmployeeList.txt","fn_P_UIF_SouthAfrica_HeaderOutputRecord.txt","fn_P_UIF_SouthAfrica_FooterOutputRecord.txt"

create or replace view dba.vw_P_SouthAfrica_UIFextractHeaderAndFooter
  /*  
  <purpose>  
  vw_P_SouthAfrica_UIFextractHeader supplies header/creator data in the format 
  required for South African electronic UIF submission.
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2015/01/06    14233   Christo Krause  Move the UIF extract to a view
  </history>
  */
  as select distinct Company_Id,Payroll_id,
    dba.fn_P_UIF_SouthAfrica_HeaderOutputRecord(Company_Id,Payroll_id) as Header_Record,
    dba.fn_P_UIF_SouthAfrica_FooterOutputRecord(Company_Id,Payroll_id) as Footer_Record
  from dba.vw_P_OpenMonthEmployeeList;

comment on view dba.vw_P_SouthAfrica_UIFextractHeaderAndFooter is "<h2>Purpose</h2><br/>Supplies the UIF electronic file output header and footer records in the current open month per company payroll combination.";

commit;
create or replace view dba.vw_P_SouthAfrica_UIFPreview
  /*  
  <purpose>  
  vw_P_SouthAfrica_UIFPreview supplies data in the format required for 
  South African electronic UIF submission.
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2015/01/12    14233   Christo Krause  Move the UIF extract to a view
  </history>
  */
  as select vw.Company_Id,vw.Employee_Id,vw.Payroll_id,
  E.Surname ||'('||E.Company_Employee_Number||')' as "Surname (EmpNo)",
  (select sum(UIF_GROSSTAXABLE) from dba.vw_P_UIF 
    where company_id = vw.COMPANY_ID and employee_id = vw.EMPLOYEE_ID 
      and Payroll_Id = vw.PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
          where company_id = vw.COMPANY_ID and Payroll_id = vw.PAYROLL_ID)) as Taxable,
  (select lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)) from dba.vw_P_UIF 
    where company_id = vw.COMPANY_ID and employee_id = vw.EMPLOYEE_ID 
      and Payroll_Id = vw.PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
          where company_id = vw.COMPANY_ID and Payroll_id = vw.PAYROLL_ID)) as "UIF Remuneration",
  (select sum(UIF_ER) from dba.vw_P_UIF 
    where company_id = vw.COMPANY_ID and employee_id = vw.EMPLOYEE_ID 
      and Payroll_Id = vw.PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
          where company_id = vw.COMPANY_ID and Payroll_id = vw.PAYROLL_ID)) as Company,
  (select sum(UIF_EE) from dba.vw_P_UIF 
    where company_id = vw.COMPANY_ID and employee_id = vw.EMPLOYEE_ID 
      and Payroll_Id = vw.PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
          where company_id = vw.COMPANY_ID and Payroll_id = vw.PAYROLL_ID)) as Employee,
  Company + Employee as Total,

    dba.fn_P_UIF_SouthAfrica_EmployeeWarnings(vw.Company_Id,vw.Employee_Id,vw.Payroll_id) as Employee_Warnings
  from dba.vw_P_OpenMonthEmployeeList as vw join dba.E_Master as E
    on E.company_id = vw.COMPANY_ID and E.employee_id = vw.EMPLOYEE_ID;

comment on view dba.vw_P_SouthAfrica_UIFPreview is "<h2>Purpose</h2><br/>Supplies a preview with warnings for the UIF electronic file output data of all employees calculated or skipped in the current open month per company payroll combination.";

create or replace view dba.vw_P_SouthAfrica_UIFPreviewHeaderAndFooter
  /*  
  <purpose>  
  vw_P_SouthAfrica_UIFPreviewHeaderAndFooter supplies data required for 
  South African electronic UIF submission in column format with warnings.
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2015/01/15    14233   Christo Krause  Move the UIF extract to a view
  </history>
  */
  as select distinct vw.Company_Id,vw.Payroll_id,
  (select coalesce(trim(TheValue),'LIVE') from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'TestOrLive') as TestOrLive,
  C.Name as Company, P.Name as Payroll,vw.MonthEnd,
  CC.Name as Contact_Name,CC.Tel_Number as Contact_Phone,CC.E_Mail as Contact_EMail,

  -- 8120 PAYE Employer Number
  if (select coalesce(trim(TheValue),'Payroll') from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichPAYE') = 'Payroll' then
    '(Payroll) '||P.TAX_Number
  else
    '(Company) '||C.TAX_Number
  end if as "PAYE Number",
  -- 8020 UIF Reference Number
  if (select coalesce(trim(TheValue),'Payroll') from dba.P_EFTS_CONSTANT 
    where EFTS_ID = 15 and NAME = 'WhichUIF') = 'Payroll' then
    '(Payroll) '||P.UIF_Number
  else
    '(Company) '||C.UIF_Number
  end if as "UIF Number",
  dba.fn_P_SouthAfrica_IsValidUIFRef("UIF Number") as UIFNumber_IsValid
  from dba.vw_P_OpenMonthPeriodList as vw 
    join dba.C_Master as C on C.company_id = vw.COMPANY_ID
    join dba.P_Payroll as P on P.Payroll_id = vw.Payroll_id
    left outer join dba.C_Contact as CC on CC.company_id = vw.COMPANY_ID
      and Contact_id = (select coalesce(trim(TheValue),'1') from dba.P_EFTS_CONSTANT 
        where EFTS_ID = 15 and NAME = 'Contact'||vw.COMPANY_ID);

comment on view DBA.vw_P_SouthAfrica_UIFPreviewHeaderAndFooter is "<h2>Purpose</h2><br/>Supplies a preview with warnings for the UIF electronic file header and footer data in the current open month per company payroll combination.";

-------------------------
-- WEB_CREDIT
-------------------------
if not exists(select * from systable where table_name='WEB_CREDIT') then
  create table dba.WEB_CREDIT(
	CREDIT_DATE timestamp not null,
	PROCESS_NAME char(50) not null,
	CREDIT_CODE_ENCR char(50) not null,
	primary key (CREDIT_DATE,PROCESS_NAME));

	comment on table dba.WEB_CREDIT is 'Records an encrypted code containing the number of purchased credits per credit type (PAYROLL, TIME, etc).  This code has a built-in checksum to safeguard against unauthorised changes';

	-- Add complimentary credits
	insert into dba.WEB_CREDIT values (now(),'PAYROLL',fn_S_EncryptString('CREDIT|'||datepart(dw, now())||'|'||20));
end if;-------------------------------------------------
-- TABLE USED TO STORE WEB CREDIT TRANSACTIONS --
-------------------------------------------------
if not exists(select * from systable where table_name='WEB_CREDITTRANSACTION') then
  create table dba.WEB_CREDITTRANSACTION(
	    CREDIT_DATE timestamp not null,
	    PROCESS_NAME char(50) not null,
        NBCREDITS int not null,
        TRANS_ID char(50),
        TRANS_REF char(50) not null,
        TRANS_STATUS char(50),
        RESULT_CODE char(50),
        RESULT_DESCR char(200),
        AUTH_CODE char(50),
        AMT_IN_CENTS int,
        RISK_INDICATOR char(10),
        CHKSUM char(50),
		INV_TO_CC char(200),
		INV_TO_PERSON char(200),
		
	primary key (CREDIT_DATE,PROCESS_NAME,TRANS_ID));

	comment on table dba.WEB_CREDIT is 'Records the full history of each payment transaction received from PayGate';

end if;------------------------------------------------------------------------------------
-- Title Lookup Table
------------------------------------------------------------------------------------
if not exists(select * from systable where table_name='WEB_LOOKUP_TITLE')
then
  create table dba.WEB_LOOKUP_TITLE (
    TITLE_ID numeric(10),
    Title char(50) not null,
    primary key (TITLE_ID));
  insert into WEB_LOOKUP_TITLE values (1, 'Mr');
  insert into WEB_LOOKUP_TITLE values (2, 'Ms');
  insert into WEB_LOOKUP_TITLE values (3, 'Mrs');
  insert into WEB_LOOKUP_TITLE values (4, 'Dr');
  insert into WEB_LOOKUP_TITLE values (5, 'Prof');
end if;
commit;---------------------------------------
-- TABLE USED TO STORE WEB PREFERENCES
---------------------------------------
if not exists (select * from systable where upper(table_name) = 'WEB_PREFERENCES') then
CREATE TABLE dba.WEB_PREFERENCES (
   company_id numeric(10) not null,
   employee_id numeric(10) not null,
   pref_key char(50) not null,
   pref_value char(100) not null,
   primary key (company_id, employee_id, pref_key) );

end if;--------------------------------------
-- TABLE USED TO CONTROL WEB SESSIONS
---------------------------------------
if not exists (select * from systable where upper(table_name) = 'WEB_SESSION') then
CREATE TABLE dba.WEB_SESSION (
   session_id  long varchar NOT NULL PRIMARY KEY,
   action_counter  BIGINT NOT NULL DEFAULT 0,
   creation_time timestamp,
   update_time timestamp,
   timeout_minutes int,
   company_id numeric(10),
   employee_id numeric(10) );
end if;---------------------------------------------------
-- WEB SERVICE TO ADD CREDITS
---------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_AddCredits') then
  drop service ws_HTTP_AddCredits
end if;
create service ws_HTTP_AddCredits
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),fn_WEB_AddCredits(:processName, :nbCredits)) as AddedCredits;

comment on service ws_HTTP_AddCredits is 'Adds additional credits';

commit;
------------------------------------------------------
-- WEB SERVICE USED TO ADVANCE A PAYROLL
------------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_AdvancePayrollPeriod') then
  drop service ws_HTTP_AdvancePayrollPeriod
end if;
create service ws_HTTP_AdvancePayrollPeriod
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select fn_HTTP_AdvancePayrollPeriod ( :sessionid, :companyID, :payrollID ) as RESULT ;

comment on service ws_HTTP_AdvancePayrollPeriod is 'Advances the payroll to the next period.  NOTE:  If the payroll is in the last period of the year, A different web services call should be used (tba)';

commit;// requires: "fn_HTTP_CanCloseTaxYear.txt"

------------------------------------------------------
-- WEB SERVICE USED TO CHACK IF A PAYROLL CAN BE ADVANCED TO THE NEXT TAX YEAR
------------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_CanAdvancePayrollTaxYear') then
  drop service ws_HTTP_CanAdvancePayrollTaxYear
end if;
create service ws_HTTP_CanAdvancePayrollTaxYear
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select fn_HTTP_CanCloseTaxYear ( :sessionid, :companyID, :payrollID ) as RESULT ;

comment on service ws_HTTP_CanAdvancePayrollTaxYear is 'Checks to see if the payroll can be advanced to the next Tax Year';

commit;---------------------------------------------------
-- WEB SERVICE TO CHECK IF WE CAN CONNECT
---------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_CanConnect') then
  drop service ws_HTTP_CanConnect
end if;
create service ws_HTTP_CanConnect
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select  property('starttime') StartTime, convert(char(10),datediff(hour, StartTime, now())) HrsRunning,
    (select first version_nr from s_db_ver order by date_of_version desc) PeopleWareVersion;

comment on service ws_HTTP_CanConnect is 'Can we connect to this database?';

commit;



----------------------------------------------
-- WEB SERVICE TO FETCH BANK ACCOUNT TYPES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetAccountTypeList') then
  drop service ws_HTTP_GetAccountTypeList
end if;

create service ws_HTTP_GetAccountTypeList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select ACCOUNT_TYPE as accountType,    
    fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        S_BANK_ACCOUNT_TYPE 
    order by accountType;

comment on service ws_HTTP_GetAccountTypeList is 'Returns a list of all the account types in S_BANK_ACCOUNT_TYPE';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH COMPANY ACCOUNT INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetCompanyAccountData') then
  drop service ws_HTTP_GetCompanyAccountData
end if;

create service ws_HTTP_GetCompanyAccountData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select first
        convert(char(10),ACCOUNT_ID)  as accountID, 
        ACCOUNT_TYPE accountType,
        ACCOUNT_NAME as accountName,
        ACCOUNT_DESCRIPTION as accountDescription,
        BANK as accountBank,
        BRANCH as accountBranch,
        convert(char(10),BANK_CODE)  as accountBankCode,
        convert(char(10),BRANCH_CODE)  as accountBranchCode,
        convert(char(20),ACCOUNT_NUMBER)  as accountNumber,
        fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH
     from c_bankdet e 
    where 
        e.company_id=:companyid and exists(select * from web_session where session_id=:sessionid)
    order by accountID;

comment on service ws_HTTP_GetCompanyAccountData is 'Returns company account information for the given companyID';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH COMPANY DATA
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetCompanyData') then
  drop service ws_HTTP_GetCompanyData
end if;

create service ws_HTTP_GetCompanyData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select 
    coalesce(C.NAME,'') name,
    coalesce(C.COMPANY_REGISTRATION ,'') registrationNo,
    coalesce(C.TAX_NUMBER ,'') taxNo,
    coalesce(C.TEL ,'') phone,
    coalesce(C.FAX ,'') fax,
    coalesce(C.EMAIL ,'') email,
    coalesce(C.DIPL_INDEMNITY ,'') diplomaticIndemnity,
    coalesce(C.PENSION_NUMBER ,'') pensionNo,
    coalesce(A.POSTAL_COUNTRY ,'') postalCountry,
    coalesce(A.POSTAL_PROVINCE ,'') postalProvince,
    coalesce(A.POSTAL_ADDRESS ,'') postalAddress,
    coalesce(A.POSTAL_SUBURB ,'') postalSuburb,
    coalesce(A.POSTAL_CITY ,'') postalCity,
    coalesce(A.POSTAL_CODE ,'') postalCode,
    coalesce(A.PHYSICAL_UNITNO ,'') physUnitNo,
    coalesce(A.PHYSICAL_COMPLEX ,'') physComplex,
    coalesce(A.PHYSICAL_STREETNO ,'') physStreetNo,
    coalesce(A.PHYSICAL_STREET ,'') physStreet,
    coalesce(A.PHYSICAL_SUBURB ,'') physSuburb,
    coalesce(A.PHYSICAL_CITY ,'') physCity,
    coalesce(A.PHYSICAL_CODE ,'') physCode,
    coalesce(A.PHYSICAL_PROVINCE ,'') physProvince,
    coalesce(A.PHYSICAL_COUNTRY ,'') physCountry,
    coalesce(convert(char(10),A.POSTAL_SAME_AS_PHYSICAL)  ,'') postalSameAsPhysical,
    coalesce(C.MAIN_BUSINESS_ACTIVITY ,'') mainBusinessActivity,
    coalesce(T.TRADE_CODE ,'') tradeCode,
    coalesce(T.TRADE_CLASSIFICATION ,'') tradeClassification,
    coalesce(C.TRADE_SUB_CODE ,'') tradeSubCode,
    coalesce(TS.TRADE_SUB_CLASSIFICATION ,'') tradeSubClassification,
    coalesce(convert(char(10), J.SETA) ,'') setaCode,
    coalesce(J.DESCRIPTION ,'') setaDescription,
    coalesce(convert(char(10), S.SIC_CODE) ,'') sicCode,
    coalesce(S.NAME ,'') sicName,
    fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
  from (((C_Master as C left outer join C_Address as A on C.COMPANY_ID = A.COMPANY_ID ) left outer join
    (A_TRADE_SUB_CLASSIFICATION as TS join A_TRADE_CLASSIFICATION as T on TS.TRADE_CLASS_ID = T.TRADE_CLASS_ID) 
    on C.TRADE_SUB_CODE = TS.TRADE_SUB_CODE) left outer join
    J_JOBSECTOR as J on C.JOBSECTOR_ID = J.JOBSECTOR_ID) left outer join
    J_SIC_CODE as S on C.SIC_CODE_ID = S.SIC_CODE_ID

    where 
        c.company_id=:companyID and
        exists (select * from web_session where session_id=:sessionid);

comment on service  ws_HTTP_GetCompanyData is 'Returns all the statutory information from a given companyID';----------------------------------------------
-- WEB SERVICE TO FETCH COST CENTRE LIST (paths, actually)
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetCostCentreList') then
  drop service ws_HTTP_GetCostCentreList
end if;
create service ws_HTTP_GetCostCentreList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select name costName, 
    description costDescription, 
    COST_NUMBER costNumber, 
    convert(char(10),cost_id) as costID, 
    convert(char(10),parent_cost_id) as parentCostID, 
    fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        c_costmain 
    where company_id=:companyID and exists(select * from web_session where session_id=:sessionid)
    order by costName;

comment on service ws_HTTP_GetCostCentreList is 'Returns a list of all the cost centres for the given companyID';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH list of COUNTRIES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetCountryList') then
  drop service ws_HTTP_GetCountryList
end if;
create service ws_HTTP_GetCountryList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select COUNTRY countryName, COUNTRY_CODE countryCode,  fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        A_PASSPORT_COUNTRYLIST 
    order by countryName;

comment on service ws_HTTP_GetCountryList is 'Returns a list of all countries in A_PASSPORT_COUNTRYLIST';

commit;---------------------------------------------------
-- WEB SERVICE TO RETRIEVE THE AVAILABLE CREDITS
---------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetCredits') then
  drop service ws_HTTP_GetCredits
end if;
create service ws_HTTP_GetCredits
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),fn_WEB_GetCreditsAvailable('Payroll')) as Payroll,
    '0' as TimeModule;

comment on service ws_HTTP_GetCredits is 'Returns the number of credits available';

commit;----------------------------------------------------------------------
-- WEB SERVICE TO CALCULATE/ESTIMATE THE MONTHLY CREDIT UTILISATION --
----------------------------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetCreditUsageEstimate') then
  drop service ws_HTTP_GetCreditUsageEstimate
end if;
create service ws_HTTP_GetCreditUsageEstimate
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),count(*)) as Payroll, '0' as TimeModule
    from dba.pl_paydef_emaster pl join dba.p_payrolldetail pd on pl.payroll_id=pd.payroll_id 
    where dba.fn_P_EmployeeStillActive_YN(company_id, employee_id, pd.payroll_id, dba.fn_P_GetOpenPeriod(pl.company_id, pl.payroll_id))='Y'
    and pd.monthend = (select monthend from p_payrolldetail where payroll_id=pd.payroll_id and payrolldetail_id=dba.fn_P_GetOpenPeriod(pl.company_id, pl.payroll_id));

comment on service ws_HTTP_GetCreditUsageEstimate is 'Returns an estimate of the number of credits that will be used in this month';

commit;
----------------------------------------------
-- WEB SERVICE TO FETCH Discharge Reasons
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetDischargeCodeAndReasonList') then
  drop service ws_HTTP_GetDischargeCodeAndReasonList
end if;
create service ws_HTTP_GetDischargeCodeAndReasonList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select distinct "UIF_CODE","UIF_REASON",
  "fn_HTTP_TouchSession"(:sessionid) as "SESSION_TOUCH"
  from "J_JOBPOSITION_REASON" order by "UIF_CODE" asc;
    
comment on service ws_HTTP_GetDischargeCodeAndReasonList is 'Returns the UIF Discharge Reasons and codes';
commit;----------------------------------------------
-- WEB SERVICE TO FETCH EMPLOYEE ACCOUNT INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeeAccountData') then
  drop service ws_HTTP_GetEmployeeAccountData
end if;

create service ws_HTTP_GetEmployeeAccountData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select first
        convert(char(10),ACCOUNT_ID)  as accountID, 
        ACCOUNT_TYPE accountType,
        ACCOUNT_NAME as accountName,
        ACCOUNT_DESCRIPTION as accountDescription,
        BANK as accountBank,
        BRANCH as accountBranch,
        convert(char(10),BANK_CODE)  as accountBankCode,
        convert(char(10),BRANCH_CODE)  as accountBranchCode,
        convert(char(20),ACCOUNT_NUMBER)  as accountNumber,
        ACCOUNT_HOLDER_RELATIONSHIP as accountHolderRelationship,
        convert(char(10),LINE_NUMBER)  as accountLineNumber,
        fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH
     from e_bankdet e 
    where 
        e.company_id=:companyid and e.employee_id=:employeeid and exists(select * from web_session where session_id=:sessionid)
    order by accountID;

comment on service ws_HTTP_GetEmployeeAccountData is 'Returns comprehensive employee account information for the given companyID and employeeID';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH EMPLOYEE ADDRESS INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeeAddressData') then
  drop service ws_HTTP_GetEmployeeAddressData
end if;

create service ws_HTTP_GetEmployeeAddressData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select 
        // Physical
        PHYS_HOME_COUNTRY physCountry,
        PHYS_HOME_PROVINCE physProvince,
        PHYS_HOME_CODE physPostalCode,
        PHYS_HOME_UNITNO physUnitNo,
        PHYS_HOME_COMPLEX physComplex,
        PHYS_HOME_STREETNO physStreetNo,
        PHYS_HOME_STREET physStreetOrFarmName,
        PHYS_HOME_SUBURB physSuburb,
        PHYS_HOME_CITY physCityOrTown,
        convert(char(10),POSTAL_SAMEAS_PHYSICAL) postalSameAsPhysical,
        // Postal
        POSTAL_COUNTRY postCountry,
        POSTAL_PROVINCE postProvince,
        HOME_POSTAL_CODE postPostCode,
        POSTAL_POST_OFFICE postPostOffice,
        POSTAL_AGENCY_SUBUNIT postPostalAgencyOrSubUnit,
        POSTAL_BOX_BAG_NUMBER postBoxOrBagNumber,
        convert(char(10),POSTAL_IS_POBOX) postIsPOBox,
        convert(char(10),POSTAL_IS_PRIVATEBAG) postIsPrivateBag,
        POSTAL_CAREOF_INTERMEDIARY postPostalAddressIsCareOf,
        POSTAL_CITY_TOWN postCityOrTown ,        
        fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH
     from e_address e
    where 
        e.company_id=:companyid and e.employee_id=:employeeid   and exists(select * from web_session where session_id=:sessionid);

comment on service ws_HTTP_GetEmployeeAddressData is 'Returns comprehensive employee address information for the given companyID and employeeID';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH P_CALCREPORT
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeeCalcDebugInfo') then
  drop service ws_HTTP_GetEmployeeCalcDebugInfo
end if;

create service ws_HTTP_GetEmployeeCalcDebugInfo
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select DESCRIPTION 
    from 
        P_CALCREPORT with (nolock) 
    where company_id=:companyID and employee_id=:employeeID
        and category_id not in (11)
    order by CALCREPORT_ID;
    
comment on service ws_HTTP_GetEmployeeCalcDebugInfo is 'Returns the P_CALCREPORT content for this employee''s last calculation';
commit;----------------------------------------------
-- WEB SERVICE TO FETCH EMPLOYEE INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeeData') then
  drop service ws_HTTP_GetEmployeeData
end if;

create service ws_HTTP_GetEmployeeData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select 
        FIRSTNAME firstname, SECONDNAME secondname, 
        SURNAME surname, COMPANY_EMPLOYEE_NUMBER employeeNumber, 
	    PREFERRED_NAME preferredName,  TITLE title, INITIALS initials, 
        fn_P_GetEmployeeGender(e.company_id, e.employee_id) as gender, 
	    MARITAL_STATUS maritalStatus, HOME_LANGUAGE homeLanguage, 
        GROUP_JOIN_DATE groupJoinDate, BIRTHDATE dateOfBirth,
        TAX_NUMBER taxNumber, ID_NUMBER idNumber, PASSPORT_HELD passportHeld, 
        PASSPORT_NUMBER passportNumber, 
	    fn_P_GetEmployeeRace(e.company_id, e.employee_id) as race, 
        (select max(start_date) from e_on_off where company_id=e.company_id and employee_id=e.employee_id) as dateOfEngagement,
        (select end_date from e_on_off where company_id=e.company_id and employee_id=e.employee_id and start_date=dateOfEngagement) as dateOfDischarge,
        convert(char(10),e.cost_id)  as costCentreID,
        (select job_name from p_jobposition join pl_emaster_pjobposition where pl_emaster_pjobposition.company_id=e.company_id and pl_emaster_pjobposition.employee_id=e.employee_id) as jobTitle,
        convert(char(10),(select count(*) from P_VARIABLES_FOR_EMPLOYEE where company_id=e.company_id and employee_id=e.employee_id and payroll_id in 
            	(select payroll_id from p_payroll where trim(name)=payroll))) as nbCalcs,
	    (select name from p_payroll p join PL_PAYDEF_EMASTER pl where company_id=e.company_id and employee_id=e.employee_id) as payroll,
    	(case nbCalcs when 0 then 'Y' else 'N' end) as canTransfer,
        (select fn_S_DecryptString(web_login)) as essLogin,
        convert(char(10),((select count(*) from al_emaster_agroup where company_id=e.company_id and employee_id=e.employee_id))) as nbAdmin,
    	(case nbAdmin when 1 then 'Y' else 'N' end) as isAdministrator,
        (select login_name_decrypted from al_emaster_agroup where company_id=e.company_id and employee_id=e.employee_id) as adminLogin,
        (select fn_S_DecryptString(password) from al_emaster_agroup where company_id=e.company_id and employee_id=e.employee_id) as adminPwd,
        tel_home as telHome, tel_work as telWork, tel_cell as telCell, e_mail as eMail,
        (select UIF_REASON||' ['||UIF_CODE||']' from j_jobposition_reason jr join e_on_off er on jr.reason_id=er.reason_id 
            where er.company_id=e.company_id and er.employee_id=e.employee_id and er.end_date=(select max(end_date) from e_on_off where company_id=e.company_id and employee_id=e.employee_id  )) as dischargeReason,
        fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH
     from e_master e join e_number en join e_passport ep join e_contact
    where 
        e.company_id=:companyid and e.employee_id=:employeeid   and exists(select * from web_session where session_id=:sessionid)
    order by Surname, Firstname;

comment on service ws_HTTP_GetEmployeeData is 'Returns comprehensive employee information for the given companyID and employeeID';

commit;
----------------------------------------------
-- WEB SERVICE TO FETCH EMPLOYEE LIST
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeeList') then
  drop service ws_HTTP_GetEmployeeList
end if;
create service ws_HTTP_GetEmployeeList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select Preferred_name, Surname, Firstname, COMPANY_EMPLOYEE_NUMBER as EmployeeNumber, 
        cast(e.company_id as char(10)) as COMPANY_ID, 
        cast(e.employee_id as char(10)) as EMPLOYEE_ID, 
        cast(p.payroll_id as char(10)) as PAYROLL_ID, 
        fn_E_EmployeeActiveInRange_YN(e.company_id, e.employee_id, 
            (select startdate from dba.p_payrolldetail where payroll_id=p.payroll_id and payrolldetail_id=fn_P_GetOpenPeriod(e.company_id, p.payroll_id)), 
            dateadd(year,10,(select startdate from dba.p_payrolldetail where payroll_id=p.payroll_id and payrolldetail_id=fn_P_GetOpenPeriod(e.company_id, p.payroll_id)))
        ) as stillEngaged,
        fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from e_master e join pl_paydef_emaster p
    order by Surname, Firstname;

comment on service ws_HTTP_GetEmployeeList is 'Returns a list of all the employees (firstname, surname, preferred_name, etc) for which the current session user has access to';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH EMPLOYEE PACKAGE INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeePackage') then
  drop service ws_HTTP_GetEmployeePackage
end if;

create service ws_HTTP_GetEmployeePackage
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 

    -- Variables --
    select 
        convert(char(10),pl.variable_id)  entityID, 'entityType_GenericVariable' entityType, 
	    convert(char(10),DEFAULT_VALUE)  as defaultValue, 
	    convert(char(10),TEMP_VALUE) as tempValue,
        convert(char(10),coalesce((select last_calc_value from p_variables_for_employee 
            where company_id=:companyID and employee_id=:employeeID and variable_id=entityID
            and payroll_id=:payrollID and payrolldetail_id=fn_P_GetOpenPeriod(:companyID, :payrollID)
            and runtype_id=1),0)) as calcValue,
    	pa.START_DATE as activeFrom,
    	pa.END_DATE as activeTo,
        convert(char(200),
            (select sp_P_CALCExtractVariableValueWithFormula(:companyID,:employeeID,v.theValue,:payrollID,fn_P_GetOpenPeriod(:companyID, :payrollID),1) 
             from p_variable v where variable_id=pl.variable_id)) interpretedFormula,
	    null as balance,
        pl.RFI_YN as rfi,

        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        pl_emaster_pvariable pl left outer join pl_emaster_pvariable_activation pa
    where 
        pl.company_id=:companyID and pl.employee_id=:employeeID
        and exists(select * from web_session where session_id=:sessionID)
    -- Loans --
    union all
    select 
        convert(char(10),pl.loan_id)  entityID, 'entityType_Loan' entityType, 
	    convert(char(10),PREMIUM) as defaultValue, 
	    convert(char(10),PREMIUM_TEMP) as tempValue,
        convert(char(10),coalesce((select ammount_deducted from p_loan_log 
            where emloan_id=pl.emloan_id and payroll_id=:payrollID 
            and payrolldetail_id=fn_P_GetOpenPeriod(:companyID, :payrollID)
            and runtype_id=1),0)) as calcValue,
    	LOAN_DATE as activeFrom,
    	null as activeTo, 
        convert(char(200),
            (select sp_P_CALCExtractVariableValueWithFormula(:companyID,:employeeID,p.premium_formula,:payrollID,
                    fn_P_GetOpenPeriod(:companyID, :payrollID),1) 
             from p_loan p where loan_id=pl.loan_id)) interpretedFormula,
	    convert(char(10),fn_P_LOAN_GetCurrentBalance( pl.emloan_id, pl.company_id, pl.employee_id, pl.payroll_id, 
				     fn_P_GetOpenPeriod(:companyID, :payrollID), 1, 0)) as balance,
        '' as rfi,

        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        pl_emaster_loan pl  
    where 
        pl.company_id=:companyID and pl.employee_id=:employeeID and pl.payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    -- Savings --
    union all
    select 
        convert(char(10),pl.saving_id)  entityID, 'entityType_Saving' entityType, 
	    convert(char(10),PREMIUM) as defaultValue, 
	    convert(char(10),PREMIUM_TEMP) as tempValue,
        convert(char(10),coalesce((select ammount_saved from p_saving_log 
            where emsaving_id=pl.emsaving_id and payroll_id=:payrollID 
            and payrolldetail_id=fn_P_GetOpenPeriod(:companyID, :payrollID)
            and runtype_id=1),0)) as calcValue,
    	START_DATE as activeFrom,
    	null as activeTo, 
        convert(char(200),  
            (select sp_P_CALCExtractVariableValueWithFormula(:companyID,:employeeID,p.premium_formula,:payrollID,
             fn_P_GetOpenPeriod(:companyID, :payrollID),1) 
             from p_saving p where saving_id=pl.saving_id)) interpretedFormula,
	    convert(char(10),fn_P_SAVING_GetCurrentBalance( pl.emsaving_id, pl.company_id, pl.employee_id, pl.payroll_id, 
				     fn_P_GetOpenPeriod(:companyID, :payrollID), 1, 0)) as balance,
        '' as rfi,

        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        pl_emaster_saving pl
    where 
      pl.company_id=:companyID and pl.employee_id=:employeeID and pl.payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    order by entityType, entityID;

comment on service ws_HTTP_GetEmployeePackage is 'Returns the entityID, entityType, permanent-, temp-, and calculated values of the list of earnings/deductions/loans/etc. for this employee for this period';
----------------------------------------------
-- WEB SERVICE TO FETCH EMPLOYEE BASIC SALARY INFO (P_E_MASTER and P_E_BASIC)
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeePayrollInfo') then
  drop service ws_HTTP_GetEmployeePayrollInfo
end if;

create service ws_HTTP_GetEmployeePayrollInfo
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 

    -- Variables --
    select 
        convert(char(20),rate_of_pay1) as rateOfPay,
        convert(char(20),pay_per1) as payPer,
        convert(char(20),hours_per_day) as hoursPerDay,
        convert(char(20),hours_per_week) as hoursPerWeek,
        convert(char(20),hours_per_month) as hoursPerMonth,
        convert(char(20),DAYS_PER_WEEK) as daysPerWeek,
        convert(char(20),DAYS_PER_MONTH) as daysPerMonth,
        convert(char(5),
            (select count(*) from e_family where MED_DEPENDENT_YN='Y' and company_id=:companyID and employee_ID=:employeeID) ) as nbMedDependents,
	    convert(char(10),sp_P_GetNettSalary(:companyID,:employeeID,:payrollID,fn_P_GetOpenPeriod(:companyID, :payrollID), 1)) as nettPay,
        convert(char(10),fn_P_GetDeductedTax(:companyID,:employeeID,:payrollID,fn_P_GetOpenPeriod(:companyID, :payrollID), 1)) as tax,
        convert(char(10),fn_P_GetDeductedTaxOnBonus(:companyID,:employeeID,:payrollID,fn_P_GetOpenPeriod(:companyID, :payrollID), 1)) as taxOnBonus,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_e_basic 
    where 
        company_id=:companyID and employee_id=:employeeID and
        exists(select * from web_session where session_id=:sessionID);

comment on service ws_HTTP_GetEmployeePayrollInfo is 'Returns the rates of pay, working days, and nettpay ';
commit;----------------------------------------------
-- WEB SERVICE TO RETREIVE CALC STATUS
----------------------------------------------

if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeeRunStatus') then
  drop service ws_HTTP_GetEmployeeRunStatus
end if;

create service ws_HTTP_GetEmployeeRunStatus
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select 
        convert(char(10),RUN_STATUS) as RUN_STATUS,
        CLOSED_YN
    from 
        p_calc_employeelist with (nolock)
    where 
        company_id=:companyID and employee_id=:employeeID and payroll_id=:payrollID and payrolldetail_id=fn_P_GetOpenPeriod(:companyID, :payrollID)
        and runtype_id=1;


comment on service ws_HTTP_GetEmployeeRunStatus is 'Returns the run_status of a person';
----------------------------------------------
-- WEB SERVICE TO FETCH P_DEBUGREPORT
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetEmployeeTaxDiagnostics') then
  drop service ws_HTTP_GetEmployeeTaxDiagnostics
end if;

create service ws_HTTP_GetEmployeeTaxDiagnostics
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select DESCRIPTION 
    from 
        P_DEBUGREPORT with (nolock) 
    where company_id=:companyID and employee_id=:employeeID and payroll_id=:payrollID 
        and payrolldetail_id=fn_P_GetOpenPeriod(:companyID, :payrollID) and runtype_id=1
        and message_type=0
    order by CALCREPORT_ID;
    
comment on service ws_HTTP_GetEmployeeTaxDiagnostics is 'Returns the P_DEBUGREPORT content for this employee''s last calculation';
commit;----------------------------------------------
-- WEB SERVICE TO FETCH JOB TITLES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetJobTitleList') then
  drop service ws_HTTP_GetJobTitleList
end if;
create service ws_HTTP_GetJobTitleList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),jobPosition_ID) jobPositionID, job_name jobTitle, description jobDescription, job_code jobCode, grade jobGrade,
    fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        p_jobposition 
    where company_id=:companyID
    order by jobTitle;

comment on service ws_HTTP_GetJobTitleList is 'Returns a list of all the job positions stored in P_JOBPOSITION';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH LANGUAGES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetLanguageList') then
  drop service ws_HTTP_GetLanguageList
end if;

create service ws_HTTP_GetLanguageList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),language_id) as languageID,  LANGUAGE "language",
    fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        A_LANGUAGE 
    order by "language";

comment on service ws_HTTP_GetLanguageList is 'Returns a list of languges stored in A_LANGUAGE';

commit;
----------------------------------------------
-- WEB SERVICE TO FETCH next EMPLOYEE_ID
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetNewEmployeeID') then
  drop service ws_HTTP_GetNewEmployeeID
end if;
create service ws_HTTP_GetNewEmployeeID
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select cast(coalesce(max(employee_id)+1,1) as char(10)) as EmployeeID, 
        fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        e_master 
    where company_id=:companyID;
comment on service ws_HTTP_GetNewEmployeeID is 'Returns the next available ID for creating a new Employee.  Should be called immediately before saving the new employee into the datastore.';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH LOANS/SAVINGS/VARIABLES LIST 
-- Rules:
--   Variables: Active
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayEntityList') then
  drop service ws_HTTP_GetPayEntityList
end if;


create service ws_HTTP_GetPayEntityList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    -- Earnings --
    select 
        convert(char(10),variable_id)  entityID, 'entityType_Earning' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code)  taxCode, s.description taxCodeCaption, c.name taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'') as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p join p_sars_code s on p.sars_code=s.sars_code join p_country c on s.country_id=c.country_id
    where 
        active_yn='Y' and p.result_type='EARNING' and COST_DETAIL='YY' and p.sars_code >1 
        and exists(select * from web_session where session_id=:sessionID)
    -- Package Component (Company Contribution - with tax codes) --
    union all
    select 
        convert(char(10),variable_id)  entityID, 'entityType_PackageComponent' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code)   taxCode, s.description taxCodeCaption, c.name taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p join p_sars_code s on p.sars_code=s.sars_code join p_country c on s.country_id=c.country_id
    where 
        active_yn='Y' and p.result_type='EARNING' and COST_DETAIL='NY' and p.sars_code >1  
        and exists(select * from web_session where session_id=:sessionID)
    -- Package Component (Company Contribution - without tax codes) --
    union all
    select 
        convert(char(10),variable_id)  entityID, 'entityType_PackageComponent' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code)   taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p 
    where 
        (sars_code is null or sars_code='') and
        active_yn='Y' and p.result_type='EARNING' and COST_DETAIL='NY' 
        and exists(select * from web_session where session_id=:sessionID)
    -- Deductions (with tax codes)--
    union all
    select 
        convert(char(10),variable_id)  entityID, 'entityType_Deduction' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code)   taxCode, s.description taxCodeCaption, c.name taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p join p_sars_code s on p.sars_code=s.sars_code join p_country c on s.country_id=c.country_id
    where 
        active_yn='Y' and p.result_type='DEDUCTION' and COST_DETAIL='YY' 
        and exists(select * from web_session where session_id=:sessionID)
    -- Deductions (without tax codes)--
    union all
    
    select 
        convert(char(10),variable_id)  entityID, 'entityType_Deduction' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code) taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
     from 
        p_variable p 
    where 
        (sars_code is null or sars_code='') and
        active_yn='Y' and p.result_type='DEDUCTION' and COST_DETAIL='YY' 
        and exists(select * from web_session where session_id=:sessionID)
    -- Interims --
    union all
    select 
        convert(char(10),variable_id)  entityID, 'entityType_Interim' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),'')   taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p 
    where 
        active_yn='Y' and p.cost_detail='NN' 
        and exists(select * from web_session where session_id=:sessionID)
    -- Loans --
    union all
    select 
        convert(char(10),loan_id)  entityID, 'entityType_Loan' entityType, p.NAME name, p.DESCRIPTION caption, 
        '' unit, 'Y' showOnPayslip, 'N' showYtdOnPayslip, 
         coalesce(premium_formula,'INPUT') as calculationFormula, 
        convert(char(10),interest) interest, convert(char(10),'')   taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from Pl_emaster_loan where loan_id = p.loan_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        '0'  as sysOption,
        sp_P_CALCExtractFormulaFromVariable(premium_formula) as theFormula,
        'All' as payrollType, 'All' as payPerType, '' as indicatorCodes,
        'NO' as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_loan p 
    where exists(select * from web_session where session_id=:sessionID)
    -- Savings --
    union all
    select 
        convert(char(10),saving_id)  entityID, 'entityType_Saving' entityType, p.NAME name, p.DESCRIPTION caption, 
        '' unit, 'Y' showOnPayslip, 'N' showYtdOnPayslip, 
        coalesce(premium_formula,'INPUT') as calculationFormula, 
        convert(char(10),interest) interest, convert(char(10),'') taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from Pl_emaster_saving where saving_id = p.saving_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        '0'  as sysOption,
        sp_P_CALCExtractFormulaFromVariable(premium_formula) as theFormula,
        'All' as payrollType, 'All' as payPerType, '' as indicatorCodes,
        'NO' as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_saving p
    where exists(select * from web_session where session_id=:sessionID)
    order by taxCode, caption;

comment on service ws_HTTP_GetPayEntityList is 'Returns a list of all the earnings, deductions, loans, savings, etc';
----------------------------------------------
-- WEB SERVICE TO FETCH PAYROLL LIST 
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayrollList') then
  drop service ws_HTTP_GetPayrollList
end if;
create service ws_HTTP_GetPayrollList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
	select convert(char(10),payroll_id) as payrollID,
	  p.name as payrollName,
	  payroll_type as payrollType,
	  tax_year_start as taxYearStart,
	  tax_year_end as taxYearEnd,
	  c.name as taxCountry,
       	  convert(char(10),(select count() from P_CALC_employeelist where payroll_id = p.payroll_id)) as nbUsedPeriods,
	  (case nbUsedPeriods
	  when 0 then '1'
	  else '0'
	  end) as canDelete,
	  (select StartDate || ' - ' || EndDate from dba.p_payrolldetail where payroll_id = p.payroll_id and payrolldetail_id
		 = fn_P_GetOpenPeriod(
		(select company_id from web_session where session_id = :sessionID),p.payroll_id)) as openPeriod,
	  CURRENCY as currency,
	  p.tax_number as taxNumber,
	  (SELECT TRADE_CODE||' - '||TRADE_CLASSIFICATION FROM A_TRADE_CLASSIFICATION where TRADE_CLASS_ID = p.trade_class_id) as tradeClassificationCode,
	  (select TRADE_SUB_CODE||' - '||TRADE_SUB_CLASSIFICATION from A_TRADE_SUB_CLASSIFICATION where TRADE_SUB_CODE = p.trade_sub_code) as tradeClassificationSubCode,
	  (select SIC_CODE||' - '||NAME from J_SIC_CODE where SIC_CODE_ID = p.sic_code_id) as sicCode,
	  fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH
	  from p_payroll as p left outer join p_country as c on p.country_id = c.country_id
	  where exists(select * from web_session where session_id = :sessionID)
	  order by payrollName asc;

comment on service ws_HTTP_GetPayrollList is 'Returns a list of all the payrolls';

commit;

----------------------------------------------
-- WEB SERVICE TO FETCH PAYROLL STATUS
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayrollOverviewAndStatus') then
  drop service ws_HTTP_GetPayrollOverviewAndStatus
end if;

create service ws_HTTP_GetPayrollOverviewAndStatus
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select 
    convert(char(10),p.payroll_id) as payrollID,
    convert(char(10),fn_P_GetOpenPeriod(:companyID, payroll_id)) as openPayrollDetailID,
    p.name as payroll,
    convert(char(10),(select count(*) from pl_paydef_emaster 
        where company_id=:companyID and payroll_id=p.payroll_id and fn_P_EmployeeStillActiveEndOfPeriod_YN(company_id, employee_id, payroll_id, fn_P_GetOpenPeriod(company_id, payroll_id))='Y')) as nbActiveEmployees,
    convert(char(10),(select count(*) from pl_paydef_emaster 
        where company_id=:companyID and payroll_id=p.payroll_id and fn_P_EmployeeStillActiveEndOfPeriod_YN(company_id, employee_id, payroll_id, fn_P_GetOpenPeriod(company_id, payroll_id))='N')) as nbInactiveEmployees,
    convert(char(10),(select count(*) from pl_paydef_emaster 
        where company_id=:companyID and payroll_id=p.payroll_id and fn_P_EmployeeStillActiveEndOfPreviousPeriod_YN(company_id, employee_id, payroll_id, fn_P_GetOpenPeriod(company_id, payroll_id))='Y')) as nbPreviousActiveEmployees,
    convert(char(10),convert(int,greater(nbActiveEmployees-nbPreviousActiveEmployees,0))) as nbEngaged,
    convert(char(10),convert(int,greater(nbPreviousActiveEmployees-nbActiveEmployees,0))) as nbDischarged,
    (select startdate||' - '||enddate from dba.p_payrolldetail where payroll_id=p.payroll_id and payrolldetail_id=fn_P_GetOpenPeriod(:companyID, payroll_id)) as openPeriod,
    convert(char(10),(select count(*) from p_calc_employeelist 
        where company_id=:companyID and payroll_id=p.payroll_id and payrolldetail_id = fn_P_GetOpenPeriod(:companyID, p.payroll_id) and runtype_id=1 and run_status=3)) as nbCalculated,
    convert(char(10),(select count(*) from p_calc_employeelist 
        where company_id=:companyID and payroll_id=p.payroll_id and payrolldetail_id = fn_P_GetOpenPeriod(:companyID, p.payroll_id) and runtype_id=1 and run_status=5)) as nbSkipped,
    convert(char(10),(select count(*) from p_calc_employeelist 
        where company_id=:companyID and payroll_id=p.payroll_id and payrolldetail_id = fn_P_GetOpenPeriod(:companyID, p.payroll_id) and runtype_id=1 and run_status in (1,2,4))) as nbInQueue,
     fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
from p_payroll p order by name;

comment on service ws_HTTP_GetPayrollOverviewAndStatus is 'Returns the current status of all payrolls';

----------------------------------------------
-- WEB SERVICE TO FETCH PAYROLL PERIOD LIST 
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayrollPeriodList') then
  drop service ws_HTTP_GetPayrollPeriodList
end if;
create service ws_HTTP_GetPayrollPeriodList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
      select convert(char(10),payrolldetail_id) as payrollDetailID,
      STARTDATE startDate,
      ENDDATE endDate,
      MONTHEND monthEnd,
      (case (fn_P_GetOpenPeriod(:companyID, :payrollID)) when payrolldetail_id then 'Y' else 'N' end) openPeriod,
 	  fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH
	  from dba.p_payrolldetail 
	  where 
        payroll_id = :payrollID and
        exists(select * from web_session where session_id = :sessionID)
	  order by startDate asc;

comment on service ws_HTTP_GetPayrollPeriodList is 'Returns a list of all the payroll periods for a given payrollID';

--------------------------------------------------------------
-- WEB SERVICE TO FETCH TAX CERTIFICATE EXTRACT FOR SDF FILE
--------------------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayrollTCExtract') then
  drop service ws_HTTP_GetPayrollTCExtract
end if;

create service ws_HTTP_GetPayrollTCExtract
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
     select V.Employee_Record 
     from
       (select 
	      replace(fn_P_TC_SouthAfrica_HeaderOutputRecord(:payrollid,:runid),'"','~') as Employee_Record
        union
        select 
	      replace(fn_P_TC_SouthAfrica_EmployeeOutputRecord(FILE_ID),'"','~') as Employee_Record
	      FROM P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = :runid
        union
        select 
	      replace(fn_P_TC_SouthAfrica_FooterOutputRecord(:runid),'"','~') as Employee_Record) 
    as V
    order by V.Employee_Record;

comment on service ws_HTTP_GetPayrollTCExtract is 'Returns the list of string values for the Tax Certificate extract for the given runID and payrollID';

commit;

----------------------------------------------
-- WEB SERVICE TO FETCH UIF EXTRACT FOR CSV
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayrollUifExtract') then
  drop service ws_HTTP_GetPayrollUifExtract
end if;

create service ws_HTTP_GetPayrollUifExtract
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
     select V.Employee_Record 
     from
       (select 
	      replace(Header_Record,'"','~') as Employee_Record
	      from vw_P_SouthAfrica_UIFextractHeaderAndFooter vw 
          where  vw.company_id=:companyid and vw.payroll_id=:payrollid
        union
        select 
	      replace(Employee_Record,'"','~') as Employee_Record
	      from vw_P_SouthAfrica_UIFextract vw 
          where vw.company_id=:companyid and vw.payroll_id=:payrollid and exists(select * from web_session where session_id=:sessionid)
        union
        select 
	      replace(Footer_Record,'"','~') as Employee_Record
	      from vw_P_SouthAfrica_UIFextractHeaderAndFooter vw
          where vw.company_id=:companyid and vw.payroll_id=:payrollid) 
    as V
    order by V.Employee_Record;

comment on service ws_HTTP_GetPayrollUifExtract is 'Returns the list of string values for the UIF extract for the given companyID and payrollID';

commit;

----------------------------------------------
-- WEB SERVICE TO FETCH list of PROVINCES (SA ONLY)
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetProvinceList') then
  drop service ws_HTTP_GetProvinceList
end if;
create service ws_HTTP_GetProvinceList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select NAME SAProvince,  fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        GEN_ADDRESS_PROVINCE
    order by SAProvince;

comment on service ws_HTTP_GetProvinceList is 'Returns a list of all the provinces listed in GEN_ADDRESS_PROVINCE';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH SIC Codes
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetSICCodes') then
  drop service ws_HTTP_GetSICCodes
end if;
create service ws_HTTP_GetSICCodes
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select distinct "SIC_CODE", "NAME",
  "fn_HTTP_TouchSession"(:sessionid) as "SESSION_TOUCH"
  from "J_SIC_CODE" order by "SIC_CODE" asc;
    
comment on service ws_HTTP_GetSICCodes is 'Returns the SIC codes';
commit;----------------------------------------------
-- WEB SERVICE TO GET TAX CERTIFICATE SUBMISSION PERIODS
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetTaxCertificatePeriods') then
  drop service ws_HTTP_GetTaxCertificatePeriods
end if;

create service ws_HTTP_GetTaxCertificatePeriods
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
     select max(a.submissionperiod) as SubmissionPeriod
		from (SELECT  distinct vwe.company_id, vwe.employee_id, vwe.payroll_id, vwc.TRANSACTION_YEAR, left(RECONCILIATION_DATE,4)||'/'||right(left(RECONCILIATION_DATE,7),2)
		as SubmissionPeriod FROM ((((((C_MASTER C INNER JOIN vw_P_Company_TaxCertificate vwc ON C.COMPANY_ID=vwc.COMPANY_ID)
		INNER JOIN vw_P_Employee_TaxCertificate vwe ON (vwc.RUN_ID=vwe.RUN_ID) AND (vwc.PAYROLL_ID=vwe.PAYROLL_ID))
		INNER JOIN E_MASTER E_MASTER ON (vwe.COMPANY_ID=E_MASTER.COMPANY_ID) AND (vwe.EMPLOYEE_ID=E_MASTER.EMPLOYEE_ID))
		INNER JOIN P_PAYROLL P_PAYROLL ON vwe.PAYROLL_ID=P_PAYROLL.PAYROLL_ID)))
		where vwe.company_id = :companyID
		and vwe.employee_id = :employeeID
		and vwe.payroll_id = :payrollID
		) as a group by a.company_id, a.employee_id, a.payroll_id, a.TRANSACTION_YEAR  order by SubmissionPeriod desc;

comment on service ws_HTTP_GetTaxCertificatePeriods is 'Returns a list of the tax certificate submission periods for the employee';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH TAX CODES and Descriptions
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetTaxCodeAndDescriptionList') then
  drop service ws_HTTP_GetTaxCodeAndDescriptionList
end if;

create service ws_HTTP_GetTaxCodeAndDescriptionList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select c.name taxCountry, 
        convert(char(10),t.sars_code) taxCode, 
        t.description description, 
        (case result_type 
            when 'EARNING' then 'entityType_Earning' 
            when 'DEDUCTION' then 'entityType_Deduction' 
            ELSE 'Unknown'
         end) entityTypeString,
        fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        p_sars_code t natural join p_country c
    where 
        entityTypeString is not null    
        and t.category in ('IncomeSource','Deduction')       
    order by 
        taxCountry, taxCode;


comment on service ws_HTTP_GetTaxCodeAndDescriptionList is 'Returns a list of all the supported tax countries,  with the tax codes for each type of earning / deduction';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH TAX COUNTRIES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetTaxCountryList') then
  drop service ws_HTTP_GetTaxCountryList
end if;


create service ws_HTTP_GetTaxCountryList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select name as countryName,    
    fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        p_country 
    order by countryName;

comment on service ws_HTTP_GetTaxCountryList is 'Returns a list of all the supported tax countries';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH TITLES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetTitleList') then
  drop service ws_HTTP_GetTitleList
end if;

create service ws_HTTP_GetTitleList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),title_id) as titleID,  Title title,
    fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        WEB_LOOKUP_TITLE 
    order by title;

comment on service ws_HTTP_GetTitleList is 'Returns a list of titles stored in WEB_LOOKUP_TITLE';

commit;----------------------------------------------
-- WEB SERVICE TO FETCH Trade Classifications
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetTradeClassificationCodes') then
  drop service ws_HTTP_GetTradeClassificationCodes
end if;
create service ws_HTTP_GetTradeClassificationCodes
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select distinct "TRADE_CODE", "TRADE_CLASSIFICATION",
  "fn_HTTP_TouchSession"(:sessionid) as "SESSION_TOUCH"
  from "A_TRADE_CLASSIFICATION" order by "TRADE_CODE" asc;
    
comment on service ws_HTTP_GetTradeClassificationCodes is 'Returns the Trade Classification Codes';
commit;----------------------------------------------
-- WEB SERVICE TO FETCH Trade Classification Sub Codes
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetTradeClassificationSubCodes') then
  drop service ws_HTTP_GetTradeClassificationSubCodes
end if;
create service ws_HTTP_GetTradeClassificationSubCodes
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select distinct "TRADE_SUB_CODE", "TRADE_SUB_CLASSIFICATION",
  "fn_HTTP_TouchSession"(:sessionid) as "SESSION_TOUCH"
  from "A_TRADE_SUB_CLASSIFICATION" order by "TRADE_SUB_CODE" asc;
    
comment on service ws_HTTP_GetTradeClassificationSubCodes is 'Returns Trade Classification Sub Codes';
commit;----------------------------------------------
-- WEB SERVICE TO FETCH USER INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetUserInfo') then
  drop service ws_HTTP_GetUserInfo
end if;
create service ws_HTTP_GetUserInfo
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
select Preferred_name, Surname, cast(e_master.company_id as char(10)) as COMPANY_ID, cast(e_master.employee_id as char(10)) as EMPLOYEE_ID, fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from e_master natural join web_session where session_id=:sessionid;
    
comment on service ws_HTTP_GetUserInfo is 'Returns the name, surname, and employee_id of the user with the given sessionID';
commit;----------------------------------------------
-- WEB SERVICE USED TO FETCH SELECTED COLUMN
-- Note: Multiple columns should be piped together
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GETVALUE') then
  drop service ws_HTTP_GETVALUE
end if;
create service ws_HTTP_GETVALUE
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select * from fn_HTTP_GETVALUE ( :sessionid, :tablename, :colname, :filter, :order)  WITH (RESULT long varchar) ;
comment on service ws_HTTP_LOGIN is 'Returns specific data from dynamic sql statement';
----------------------------------------------
-- WEB SERVICE TO FETCH EMPLOYEE YTD PACKAGE INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetYTDEmployeePackage') then
  drop service ws_HTTP_GetYTDEmployeePackage
end if;

create service ws_HTTP_GetYTDEmployeePackage
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 

    -- Variables --
    select 
        convert(char(10),pl.variable_id)  entityID, 'entityType_GenericVariable' entityType, 
	    convert(char(10),sum(last_calc_value))  as ytdValue,
        convert(char(10),count(*)) as nbCalculations,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variables_for_employee pl join p_variable p
    where 
        pl.company_id=:companyID and pl.employee_id=:employeeID and pl.payroll_id=:payrollID
        and p.DISPLAYYTD_YN='Y'
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType
    -- Loans --
    union all
    select 
        convert(char(10),pl.loan_id)  entityID, 'entityType_Loan' entityType, 
	    convert(char(10),sum(ammount_deducted)) as ytdValue, 
	    convert(char(10),count(*)) as nbCalculations,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_loan_log l join PL_EMASTER_LOAN pl
    where 
        pl.company_id=:companyID and pl.employee_id=:employeeID and pl.payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType
    -- Savings --
    union all
    select 
        convert(char(10),pl.saving_id)  entityID, 'entityType_Saving' entityType, 
	    convert(char(10),sum(ammount_saved)) as ytdValue, 
	    convert(char(10),count(*)) as nbCalculations,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_saving_log l join PL_EMASTER_SAVING pl
    where 
        pl.company_id=:companyID and pl.employee_id=:employeeID and pl.payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType
    -- RFI --
    union all
    select 
        '-10' as entityID, 'entityType_GenericVariable' entityType, 
	    convert(char(10),sum(RFI))  as ytdValue,
        convert(char(10),count(RFI)) as nbCalculations,
        fn_HTTP_TouchSession('5cbf4d98-ef3b-4ef4-8b34-205c13456351') as SESSION_TOUCH 
    from 
        vw_P_RFI_NonRFI
    where 
        company_id=:companyID and employee_id=:employeeID and payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType
    -- Non-RFI --
    union all
    select 
        '-20' as entityID, 'entityType_GenericVariable' entityType, 
	    convert(char(10),sum("non-RFI"))  as ytdValue,
        convert(char(10),count("non-RFI")) as nbCalculations,
        fn_HTTP_TouchSession('5cbf4d98-ef3b-4ef4-8b34-205c13456351') as SESSION_TOUCH 
    from 
        vw_P_RFI_NonRFI
    where 
        company_id=:companyID and employee_id=:employeeID and payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType

    order by entityType, entityID;

comment on service ws_HTTP_GetYTDEmployeePackage is 'Returns the YearToDate figures for all variables ';

commit;----------------------------------------------
-- WEB SERVICE TO CHECK IF SESSION IS ALIVE
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_ISSESSIONLIVE') then
  drop service ws_HTTP_ISSESSIONLIVE
end if;
create service ws_HTTP_ISSESSIONLIVE
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select fn_HTTP_ISSESSIONLIVE ( :sessionid ) as RESULT;
comment on service ws_HTTP_ISSESSIONLIVE is 'Checks if a session is still live';-------------------------------------
-- WEB SERVICE USED TO LOG IN
-------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_LOGIN') then
  drop service ws_HTTP_LOGIN
end if;

create service ws_HTTP_LOGIN
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select fn_HTTP_LOGIN ( :web_login, :web_pwd, :sessionid ) as SESSION_ID;
comment on service ws_HTTP_LOGIN is 'Creates a login and establishes a session ID';-------------------------------------
-- WEB SERVICE USED TO LOG OUT
-------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_LOGOUT') then
  drop service ws_HTTP_LOGOUT
end if;
create service ws_HTTP_LOGOUT
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select fn_HTTP_LOGOUT ( :sessionid ) as STATUS;
comment on service ws_HTTP_LOGOUT is 'Removes SESSION and logs out';----------------------------------------------
-- WEB SERVICE TO Clean out the database and set it up with a new user
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_PrepareDBForFirstUse') then
  drop service ws_HTTP_PrepareDBForFirstUse
end if;
create service ws_HTTP_PrepareDBForFirstUse
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10), fn_HTTP_InitiateDatabase(:companyID, :companyName, :userFirstName, :userSurname, :userLogin, :userPwd, :userEmail, :userPhone, :payrollName, :payrollType, :payrollCountry, :taxYearStart, :taxYearEnd, :openPeriod)) as DB_RESULT;
    

comment on service ws_HTTP_PrepareDBForFirstUse is 'Clears out employees, payrolls and creates a new user linked to new payroll, with login capabilities for PeopleWeb';
----------------------------------------------
-- WEB SERVICE TO PREPARE TAX CERTIFICATE DATA
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_PrepareTaxCertificateData') then
  drop service ws_HTTP_PrepareTaxCertificateData
end if;

create service ws_HTTP_PrepareTaxCertificateData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_PrepareTaxCertificateData(:sessionID,:payrollID,:companyID,:reconMonth,:reconYear,:tradeCode) as RESULT;

comment on service ws_HTTP_PrepareTaxCertificateData is 'Populate the neccesary tables and views with tax certificate data for reporting';

commit;----------------------------------------------
-- WEB SERVICE TO RECALCULATE
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_RequestRecalc') then
  drop service ws_HTTP_RequestRecalc
end if;

create service ws_HTTP_RequestRecalc
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(6),fn_HTTP_RequestCalc(:companyID, :employeeID, :payrollID, fn_P_GetOpenPeriod(:companyID, :payrollID), 1)) as RSLT;

comment on service ws_HTTP_RequestRecalc is 'Initiates a recalculation of given employee';
----------------------------------------
-- WEB SERVICE USED TO RESET PASSWORDS
-- Note: The only way we can reset a password
--   is if the person is a PeopleWeb admin
----------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_RESET_ADMINPASSWORD') then
  drop service ws_HTTP_RESET_ADMINPASSWORD
end if;
create service ws_HTTP_RESET_ADMINPASSWORD
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select fn_HTTP_RESET_ADMINPASSWORD ( :adminEmail ) as resetResult;
comment on service ws_HTTP_RESET_ADMINPASSWORD is 'Resets the password of the Administrator with this email address';

----------------------------------------------
-- WEB SERVICE TO STORE WEB CREDIT PURCHASES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SaveCreditPurchase') then
  drop service ws_HTTP_SaveCreditPurchase
end if;

create service ws_HTTP_SaveCreditPurchase
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),dba.fn_HTTP_SaveCreditPurchase(:creditDate ,:module ,:nbCredits ,:transID ,:transRef ,:transStatus ,:rsltCode , :rsltDescr ,:authCode ,:amtInCents ,:riskIndicator, :chkSum, :invToCC, :invToPerson)) as DB_RSLT;


comment on service ws_HTTP_SaveCreditPurchase is 'Records the full history of each payment transaction received from PayGate';

commit;
----------------------------------------------
-- WEB SERVICE TO UPDATE COMPANY ACCOUNT INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetCompanyAccountData') then
  drop service ws_HTTP_SetCompanyAccountData
end if;

create service ws_HTTP_SetCompanyAccountData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetCompanyAccountData(:sessionID,:companyID,:accountType,:accountName,
    :accountDescription,:accountBank,:accountBranch,:accountBankCode,:accountBranchCode,:accountNumber) as RESULT;

comment on service ws_HTTP_SetEmployeeAccountData is 'Inserts/updates company account information';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE COMPANY INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetCompanyData') then
  drop service ws_HTTP_SetCompanyData
end if;

create service ws_HTTP_SetCompanyData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetCompanyData(:sessionID,:companyID,:name,:registrationNo,:taxNo,:phone,
    :fax,:email,:diplomaticIndemnity,:pensionNo,:postalCountry,:postalProvince,:postalAddress,:postalSuburb,
    :postalCity,:postalCode,:physUnitNo,:physComplex,:physStreetNo, :physStreet,:physSuburb,:physCity,:physCode,
    :physProvince,:physCountry,:postalSameAsPhysical,:mainBusinessActivity,:tradeCode,:tradeSubCode,
    :setaCode,:sicCode) as RESULT;

comment on service ws_HTTP_SetCompanyData is 'Updates company information';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE COST CENTER INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetCostCentreData') then
  drop service ws_HTTP_SetCostCentreData
end if;

create service ws_HTTP_SetCostCentreData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetCostCentreData(:sessionID,:companyID,:costID,:name,:description,:number,:parentCostID,:doDelete) as RESULT;

comment on service ws_HTTP_SetCostCentreData is 'Updates cost centre information';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE EMPLOYEE ACCOUNT INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetEmployeeAccountData') then
  drop service ws_HTTP_SetEmployeeAccountData
end if;

create service ws_HTTP_SetEmployeeAccountData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetEmployeeAccountData(:sessionID,:companyID,:employeeID,:accountType,:accountName,
    :accountDescription,:accountBank,:accountBranch,:accountBankCode,:accountBranchCode,:accountNumber,:accountHolderRelationship,
    :accountLineNumber) as RESULT;

comment on service ws_HTTP_SetEmployeeAccountData is 'Inserts/updates employee account information';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE EMPLOYEE ADDRESS INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetEmployeeAddressData') then
  drop service ws_HTTP_SetEmployeeAddressData
end if;

create service ws_HTTP_SetEmployeeAddressData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetEmployeeAddressData(:sessionID,:companyID,:employeeID,:physCountry,:physProvince,:physPostalCode,
    :physUnitNo,:physComplex,:physStreetNo,:physStreetOrFarmName,:physSuburb,:physCityOrTown,:postalSameAsPhysical,
    :postCountry, :postProvince,:postPostCode,:postPostOffice,:postPostalAgencyOrSubUnit,:postBoxOrBagNumber, :postIsPOBox,:postIsPrivateBag,:postPostalAddressIsCareOf, :postCityOrTown) as RESULT;

comment on service ws_HTTP_SetEmployeeAddressData is 'Inserts/updates employee address information';

commit;
----------------------------------------------
-- WEB SERVICE TO UPDATE EMPLOYEE INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetEmployeeData') then
  drop service ws_HTTP_SetEmployeeData
end if;


create service ws_HTTP_SetEmployeeData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetEmployeeData(:sessionID,:companyID,:employeeID,:firstname,:secondname,:surname,
    :employeeNumber,:preferredName,:title,:initials,:gender,:maritalStatus,:homeLanguage,:groupJoinDate,
    :dateOfBirth,:taxNumber,:idNumber,:passportHeld,:passportNumber, :race,:dateOfEngagement,:dateOfDischarge, 
    :dischargeReason, :costCentreID, :jobTitle, :payroll, :essLogin, :essPassword, :adminLogin, :adminPassword, :isAdministrator, 
    :telHome, :telWork, :telCell, :eMail, :undoDischarge) as RESULT;

comment on service ws_HTTP_SetEmployeeData is 'Inserts/updates employee information';

commit;
----------------------------------------------
-- WEB SERVICE TO UPDATE EMPLOYEE PACKAGE INFO
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetEmployeePackage') then
  drop service ws_HTTP_SetEmployeePackage
end if;

create service ws_HTTP_SetEmployeePackage
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetEmployeePackage(:sessionID,:companyID,:employeeID,:entityID,:entityType,
    :value, :balance, :payrollID, :referenceNo, :rfi, :unlink, :removeTempVal) as RESULT;

comment on service ws_HTTP_SetEmployeePackage is 'Links variables/loans/savings';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE EMPLOYEE PAYROLL INFO
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetEmployeePayrollInfo') then
  drop service ws_HTTP_SetEmployeePayrollInfo
end if;

create service ws_HTTP_SetEmployeePayrollInfo
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetEmployeePayrollInfo(:sessionID,:companyID,:employeeID,:rateOfPay,:hoursPerDay,
    :hoursPerWeek,:hoursPerMonth,:daysPerWeek,:daysPerMonth,:payPer,:nbMedDependents) as RESULT;

comment on service ws_HTTP_SetEmployeePayrollInfo is 'Inserts/updates employee pay information in P_E_BASIC ';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE JOB POSITIONS
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetJobPositionData') then
  drop service ws_HTTP_SetJobPositionData
end if;


create service ws_HTTP_SetJobPositionData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10), fn_HTTP_SetJobPositionData(:sessionID,:companyID,:jobPositionID,:jobTitle,:jobDescription,:jobCode,:jobGrade,:deleted)) as jobPositionID;

comment on service ws_HTTP_SetJobPositionData is 'Updates Job Position (P_JOBPOSITION) information';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE TITLES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetLanguageData') then
  drop service ws_HTTP_SetLanguageData
end if;

create service ws_HTTP_SetLanguageData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10), fn_HTTP_SetLanguageData(:sessionID,:languageID,:language,:deleted)) as languageID;

comment on service ws_HTTP_SetLanguageData is 'Updates A_LANGUAGE  information';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE PAY ENTITIES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetPayEntityData') then
  drop service ws_HTTP_SetPayEntityData
end if;

create service ws_HTTP_SetPayEntityData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10), fn_HTTP_SetPayEntityData(:sessionID,:entityID,:entityType,:name,:caption,:valueUnit,:showOnPayslip,:showYtdOnPayslip,:calculationFormula,:payrollType, :interest,:taxCode,:doDelete)) as entityID;

comment on service ws_HTTP_SetPayEntityData is 'Updates pay entity (loans/savings/variables) information';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE COST CENTER INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetPayrollData') then
  drop service ws_HTTP_SetPayrollData
end if;

create service ws_HTTP_SetPayrollData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetPayrollData(:sessionID,:payrollID,:payrollName,:payrollType,:taxYearStart,:taxYearEnd,:openPeriod, :taxCountry,
								  :doDelete,:taxNumber,:tradeClassificationCode,:tradeClassificationSubCode,:sicCode) as RESULT;

comment on service ws_HTTP_SetPayrollData is 'Updates payroll information';

commit;----------------------------------------------
-- WEB SERVICE TO UPDATE TITLES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetTitleData') then
  drop service ws_HTTP_SetTitleData
end if;


create service ws_HTTP_SetTitleData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10), fn_HTTP_SetTitleData(:sessionID,:titleID,:title, :deleted)) as titleID;

comment on service ws_HTTP_SetTitleData is 'Updates WEB_LOOKUP_TITLE  information';

commit;------------------------------------------------------
-- WEB SERVICE USED TO INSERT/UPDATE SELECTED COLUMNS
------------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SETVALUE') then
  drop service ws_HTTP_SETVALUE
end if;
create service ws_HTTP_SETVALUE
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select fn_HTTP_SETVALUE ( :sessionid, :tablename, :columnlist, :valuelist ) as RESULT ;
comment on service ws_HTTP_SETVALUE is 'Inserts/updates values in a given table with the appropriate columnlist, valuelist pairs.\nE.g. ws_HTTP_SETVALUE(...,"E_NUMBER","COMPANY_ID, EMPLOYEE_ID, ID_NUMBER", "23012,1002, 7007204324323")';

commit;