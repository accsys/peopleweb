/*
 * The SvrGeneralPrefs class is used to store all the general (paths, OS, etc.) information stored in the settings.xml file
 */
package za.co.accsys.web.helperclasses;

/**
 *
 * @author lterblanche
 */
public class SvrGeneralPrefs {

    public SvrGeneralPrefs() {
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(String smtpPort) {
        this.smtpPort = smtpPort;
    }

    public String getSmtpAuthUser() {
        return smtpAuthUser;
    }

    public void setSmtpAuthUser(String smtpAuthUser) {
        this.smtpAuthUser = smtpAuthUser;
    }

    public String getSmtpAuthPwd() {
        return smtpAuthPwd;
    }

    public void setSmtpAuthPwd(String smtpAuthPwd) {
        this.smtpAuthPwd = smtpAuthPwd;
    }

    public String getExternalURL() {
        return externalURL;
    }

    public void setExternalURL(String externalURL) {
        this.externalURL = externalURL;
    }

    public int getCreditUnitCost_Payroll() {
        return creditUnitCost_Payroll;
    }

    public void setCreditUnitCost_Payroll(int creditUnitCost_Payroll) {
        this.creditUnitCost_Payroll = creditUnitCost_Payroll;
    }

    
    
    private String smtpServer;
    private String smtpPort;
    private String smtpAuthUser;
    private String smtpAuthPwd;
    private String externalURL;
    private int creditUnitCost_Payroll=1;

}
