/*
 * PayGatePurchaseRequest is used to populate the 
 * HTML <form></form> that gets forwarded to the PayGateRequest servers
 */
package za.co.accsys.web.helperclasses;

import java.text.NumberFormat;
import java.util.Locale;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.model.SessionUser;

/**
 *
 * @author liam
 */
public class PayGatePurchaseRequest extends PayGateBaseClass {

    /**
     * Creates an instance of a PayGateRequest class
     *
     * @param sessionUser
     * @param nbCredits
     * @param creditType "payroll" or "time"
     */
    public PayGatePurchaseRequest(SessionUser sessionUser, int nbCredits, String creditType) {
        this.sessionUser = sessionUser;
        this.nbCredits = nbCredits;
        this.creditType = creditType;
    }

    private String action;

    public String getAction() {
        return Constants.PAYGATE_REQUEST_URL;
    }

    private String payGateID;

    public String getPayGateID() {
        return Constants.PAYGATE_ID;
    }

    private String reference;

    public String getReference() {
        return getTransactionReference();
    }

    private String email;

    public String getEmail() {
        return ServerPrefs.getInstance().getAccount(sessionUser.getAccountID()).getOwnerEmail();
    }

    private int amountInCents;

    public int getAmountInCents() {
        return nbCredits * ServerPrefs.getInstance().getCreditUnitCost_Payroll();
    }

    private String amountInZAR;

    public String getAmountInZAR() {
        NumberFormat formatter;
        formatter = NumberFormat.getCurrencyInstance(new Locale("en", "ZA"));
        amountInZAR = formatter.format(nbCredits * ServerPrefs.getInstance().getCreditUnitCost_Payroll() / 100);
        return amountInZAR;
    }

    public String getCurrency() {
        return this.currency;
    }

    private String returnURL;

    /**
     * Returns the URL of the REPONSE jsp that payGate will send the results to
     *
     * @return
     */
    public String getReturnURL() {
        ServerPrefs prefs = ServerPrefs.getInstance();

        if (prefs.getExternalURL().trim().endsWith("/") || prefs.getExternalURL().trim().endsWith("\\")) {
            returnURL = prefs.getExternalURL().trim() + (Constants.PAYGATE_RETURNURL_JSP);
        } else {
            returnURL = prefs.getExternalURL().trim() + "/" + (Constants.PAYGATE_RETURNURL_JSP);
        }
        return returnURL;
    }

    private String transactionTimeStamp;

    public String getTransactionTimeStamp() {
        return DateUtils.getTimeStampShort();
    }

    private String md5Checksum;

    /**
     * Generates the MD5 checksum for PayGate requests, as per their documentation
     *
     * @return
     */
    public String getMd5Checksum() {

        StringBuilder preChecksum = new StringBuilder();
        // paygate_id
        preChecksum.append(Constants.PAYGATE_ID).append("|");
        // reference
        preChecksum.append(getReference()).append("|");
        preChecksum.append(getAmountInCents()).append("|");
        preChecksum.append(getCurrency()).append("|");
        preChecksum.append(getReturnURL()).append("|");
        preChecksum.append(getTransactionTimeStamp()).append("|");
        preChecksum.append(getEmail()).append("|");
        preChecksum.append(Constants.PAYGATE_ENCRYPTION_KEY);
        return (toMD5(preChecksum.toString()));

    }

    private String getTransactionReference() {
        // "sessionUser" bought "creditsPurchased" credits for "payroll" 
        return sessionUser.getAccountID() + "_" + creditType + "_" + nbCredits;
    }

    private final SessionUser sessionUser;
    private final int nbCredits;
    private final String currency = "ZAR";
    private final String creditType;

}
