
/*
 * The ListManager, as the name implies, is used to generate the drop-down lists used in the various xhtml screens
 */
package za.co.accsys.web.helperclasses;

import za.co.accsys.web.beans.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.model.CostCentre;
import za.co.accsys.web.model.CostCentreManager;
import za.co.accsys.web.model.LocaleDefinition;
import za.co.accsys.web.model.Payroll;
import za.co.accsys.web.model.PayrollManager;
import za.co.accsys.web.model.SessionUser;

/**
 *
 * @author lterblanche The ListManager, as the name implies, is used to generate
 * the drop-down lists used in the various xhtml screens
 *
 */
public class ListManager {

    SessionUser sessionUser;

    /**
     * Constructor for the ListManager.
     *
     * @param sessionUser
     */
    public ListManager(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        getListItems_TradeClassificationCodes();
        getListItems_TradeClassificationSubCodes();
    }
    
    private String[] listItems_CostCentreName;
    private String[] listItems_PayrollName;
    private String[] listItems_UIFDischargeCodeAndReason;
    private String[] listItems_TradeClassificationCodes;
    private String[] listItems_TradeClassificationSubCodes;
    private List<SelectItem> listItems_TradeClassificationSubCodesHierarchy;
    private String[] listItems_SICCodes;
    private String[] listItems_Country;
    private String[] listItems_Province;
    private String[] listItems_TaxCountry;
    private String[] listItems_TaxCodeAndDescription;

    /* ---- DROP_DOWN OPTIONS ----- */
    private final String[] listItems_Gender
            = {"genderOther", "genderFemale", "genderMale"};
    private final String[] listItems_MaritalStatus
            = {"maritalStatusSingle", "maritalStatusDivorced", "maritalStatusMarried"};
    private final String[] listItems_YesNo //Languages spoken by employees
            = {"ynYes", "ynNo"};
    private final String[] listItems_TrueFalse //Languages spoken by employees
            = {"tfTrue", "tfFalse"};
    private final String[] listItems_PayPer
            = {"payperMonth", "payperWeek", "payperDay", "payperHour"};
    private final String[] listItems_PayrollType
            = {"payrolltypeMonthly", "payrolltypeWeekly"};
    private final String[] listItems_PayrollTypeForVariable
            = {"payrolltypeMonthly", "payrolltypeWeekly", "payrolltypeAll"};
    private final String[] listItems_BankAccountType
            = {"accountTypeCurrentAndCheque", "accountTypeSaving", "accountTypeTransmission",
                "accountTypeBondAccount", "accountTypeCreditCard", "accountTypeSubscription", "accountTypeForeignBank"};
   
    public String[] getListItems_PayPer() {
        return translateList(listItems_PayPer);
    }

    public String[] getListItems_TaxCodeAndDescription() {
        return listItems_TaxCodeAndDescription;
    }

    /**
     * Returns a String[] comprising UIF Discharge Reasons and Codes
     *
     * @return reason [code]
     */
    public String[] getListItems_UIFDischargeCodeAndReason() {
        if (listItems_UIFDischargeCodeAndReason == null) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

            List<String> uifReasonAndCodeList = new ArrayList<>();

            // Call the appropriate service
            ArrayList<JSONRecord> listRetreived;
            String uifCode = null;
            String uifReason = null;

            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetUIFCodeAndReasonList, listToSend);

            // What did we get back?
            // The result is a JSON Array listing all UIF discharge codes and reasons
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the cost centre
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                // Step through each record, and construct an EMPLOYEE instance
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase("UIF_CODE")) {
                        uifCode = nvp.getValue();
                    }
                    if (nvp.getName().equalsIgnoreCase("DESCRIPTION")) {
                        uifReason = nvp.getValue();
                    }
                }
                if (uifCode != null && uifReason != null) {
                    uifReasonAndCodeList.add(uifReason + " [" + uifCode + "]");
                }
            }

            String[] rslt = uifReasonAndCodeList.toArray(new String[uifReasonAndCodeList.size()]);
            listItems_UIFDischargeCodeAndReason = rslt;
            return rslt;
        } else {
            return listItems_UIFDischargeCodeAndReason;
        }
    }

    /**
     * Returns a String[] comprising Trade Classification Codes
     *
     * @return reason [code]
     */
    public String[] getListItems_TradeClassificationCodes() {
        if (listItems_TradeClassificationCodes == null) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

            List<String> tradeClassificationCodeList = new ArrayList<>();

            // Call the appropriate service
            ArrayList<JSONRecord> listRetreived;
            String tradeCode = null;
            String tradeClassification = null;

            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetTradeClassificationCodesList, listToSend);

            // What did we get back?
            // The result is a JSON Array listing all UIF discharge codes and reasons
            // Add blank entry
            tradeClassificationCodeList.add("-- None --");
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the cost centre
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                // Step through each record, and construct an EMPLOYEE instance
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase("TRADE_CODE")) {
                        tradeCode = nvp.getValue();
                    }
                    if (nvp.getName().equalsIgnoreCase("TRADE_CLASSIFICATION")) {
                        tradeClassification = nvp.getValue();
                    }
                }
                if (tradeCode != null && tradeClassification != null) {
                    tradeClassificationCodeList.add(tradeCode + " - " + tradeClassification);
                }
            }

            String[] rslt = tradeClassificationCodeList.toArray(new String[tradeClassificationCodeList.size()]);
            listItems_TradeClassificationCodes = rslt;
            return rslt;
        } else {
            return listItems_TradeClassificationCodes;
        }
    }

    /**
     * Returns a String[] comprising Trade Classification Codes
     *
     * @param payroll
     * @return reason [code]
     */
    public String[] getListItems_TradeClassificationSubCodes() {
        if (listItems_TradeClassificationSubCodes == null) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

            List<String> tradeClassificationSubCodeList = new ArrayList<>();

            // Call the appropriate service
            ArrayList<JSONRecord> listRetreived;
            String tradeSubCode = null;
            String tradeSubClassification = null;

            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetTradeClassificationSubCodesList, listToSend);

            // What did we get back?
            // The result is a JSON Array listing all UIF discharge codes and reasons
            // Add blank entry
            tradeClassificationSubCodeList.add("-- None --");
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the cost centre
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                // Step through each record, and construct an EMPLOYEE instance
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase("TRADE_SUB_CODE")) {
                        tradeSubCode = nvp.getValue();
                    }
                    if (nvp.getName().equalsIgnoreCase("TRADE_SUB_CLASSIFICATION")) {
                        tradeSubClassification = nvp.getValue();
                    }
                }
                if (tradeSubCode != null && tradeSubClassification != null) {
                    tradeClassificationSubCodeList.add(tradeSubCode + " - " + tradeSubClassification);
                }
            }

            String[] rslt = tradeClassificationSubCodeList.toArray(new String[tradeClassificationSubCodeList.size()]);
            listItems_TradeClassificationSubCodes = rslt;
            return rslt;
        } else {
            return listItems_TradeClassificationSubCodes;
        }
    }

    /**
     * Returns a String[] comprising Trade Classification Codes
     *
     * @return reason [code]
     */
    public List<SelectItem> getListItems_TradeClassificationSubCodesHierarchy() {
        //Trade Classification Hierarchy
        listItems_TradeClassificationSubCodesHierarchy = new ArrayList<>();
        for (String tradeClassification : listItems_TradeClassificationCodes) {
            if (tradeClassification.equalsIgnoreCase("-- None --")) {
                SelectItemGroup tradeGroup = new SelectItemGroup(tradeClassification);
                tradeGroup.setSelectItems(new SelectItem[]{new SelectItem("-- None --", "-- None --")});
                listItems_TradeClassificationSubCodesHierarchy.add(tradeGroup);
            } else {
                SelectItemGroup tradeGroup = new SelectItemGroup(tradeClassification);

                String[] fullList = getListItems_TradeClassificationSubCodes();
                String filter = tradeClassification.subSequence(0, 2).toString();
                List<SelectItem> filteredList = new ArrayList();

                for (String listItem : fullList) {
                    if (listItem.subSequence(0, 2).toString().equalsIgnoreCase(filter)) {
                        filteredList.add(new SelectItem(listItem, listItem));
                    }
                }
                SelectItem[] selectItemArray = new SelectItem[filteredList.size()];
                filteredList.toArray(selectItemArray);
                
                tradeGroup.setSelectItems(selectItemArray);
                listItems_TradeClassificationSubCodesHierarchy.add(tradeGroup);
            }
        }
        return listItems_TradeClassificationSubCodesHierarchy;
    }

    /**
     * Returns a String[] comprising SIC Codes
     *
     * @return reason [code]
     */
    public String[] getListItems_SICCodes() {
        if (listItems_SICCodes == null) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

            List<String> sicCodeList = new ArrayList<>();

            // Call the appropriate service
            ArrayList<JSONRecord> listRetreived;
            String sicCode = null;
            String sicCodeName = null;

            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetSICCodesList, listToSend);

            // What did we get back?
            // The result is a JSON Array listing all UIF discharge codes and reasons
            // Add blank entry
            sicCodeList.add("-- None --");
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the cost centre
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                // Step through each record, and construct an EMPLOYEE instance
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase("SIC_CODE")) {
                        sicCode = nvp.getValue();
                    }
                    if (nvp.getName().equalsIgnoreCase("NAME")) {
                        sicCodeName = nvp.getValue();
                    }
                }
                if (sicCode != null && sicCodeName != null) {
                    sicCodeList.add(sicCode + " - " + sicCodeName);
                }
            }

            String[] rslt = sicCodeList.toArray(new String[sicCodeList.size()]);
            listItems_SICCodes = rslt;
            return rslt;
        } else {
            return listItems_SICCodes;
        }
    }

    public void setListItems_TaxCodeAndDescription(String[] listItems_TaxCodeAndDescription) {
        this.listItems_TaxCodeAndDescription = listItems_TaxCodeAndDescription;
    }

    /**
     * Retrieves the tax countries from P_COUNTRY
     *
     * @return
     */
    public String[] getListItems_TaxCountry() {
        if (listItems_TaxCountry == null) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

            List<String> accountTypeList = new ArrayList<>();

            // Call the appropriate service
            ArrayList<JSONRecord> listRetreived;
            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetTaxCountryList, listToSend);

            // What did we get back?
            // The result is a JSON Array listing all supported tax countries
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the cost centre
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                String jTaxCountry = null;
                // Step through each record, and construct an EMPLOYEE instance
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase("countryName")) {
                        jTaxCountry = nvp.getValue();
                    }
                }
                if (jTaxCountry != null) {
                    if (jTaxCountry.trim().length() > 0) {
                        accountTypeList.add(jTaxCountry);
                    }
                }
            }

            String[] rslt = accountTypeList.toArray(new String[accountTypeList.size()]);
            listItems_TaxCountry = rslt;
            return rslt;
        } else {
            return listItems_TaxCountry;
        }
    }

    public String[] getListItems_TrueFalse() {
        return translateList(listItems_TrueFalse);
    }

    public String[] getListItems_YesNo() {
        return translateList(listItems_YesNo);
    }

    public SessionUser getSessionUser() {
        return sessionUser;
    }

    public String[] getListItems_PayEntityType() {
        return listItems_PayEntityType;
    }

    public String[] getListItems_PayrollType() {
        return translateList(listItems_PayrollType);
    }

    public String[] getListItems_PayrollTypeForVariable() {
        return translateList(listItems_PayrollTypeForVariable);
    }

    public void setSessionUser(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
    }

    public String[] getListItems_LocaleDisplay() {
        return listItems_LocaleDisplay;
    }

    public String[] getListItems_LocaleLanguage() {
        return listItems_LocaleLanguage;
    }

    public String[] getListItems_LocaleCountry() {
        return listItems_LocaleCountry;
    }

    public String[] getListItems_Theme() {
        return listItems_Theme;
    }

    public String[] getListItems_CostCentreName() {
        List<String> costCentreList = new ArrayList<>();
        for (CostCentre costCentre : sessionUser.getPageManager().getCompany().getCostCentreManager().getGlobalCostCentres()) {
            costCentreList.add(costCentre.getName());
        }

        listItems_CostCentreName = costCentreList.toArray(new String[costCentreList.size()]);
        return listItems_CostCentreName;
    }

    public String[] getListItems_PayrollName() {
        List<String> payrollList = new ArrayList<>();
        for (Payroll payroll : sessionUser.getPageManager().getCompany().getPayrollManager().getGlobalPayrolls()) {
            payrollList.add(payroll.getPayrollName());
        }

        listItems_PayrollName = payrollList.toArray(new String[payrollList.size()]);
        return listItems_PayrollName;
    }

    public ArrayList<String> getListItems_PayEntityType_Localised() {
        ArrayList<String> rslt = new ArrayList<>();
        for (String listItems_PayEntityType1 : getListItems_PayEntityType()) {
            String localeT = PageManager.translate(listItems_PayEntityType1);
            rslt.add(localeT);
        }
        return rslt;
    }

    public static final String getDefaultTheme() {
        return defaultTheme;
    }

    public String[] getListItems_MaritalStatus() {
        return translateList(listItems_MaritalStatus);
    }

    /**
     * Returns the List[] array with translated values
     *
     * @param aList
     * @return
     */
    private String[] translateList(String[] aList) {
        // Translate the list first
        ArrayList<String> translatedItems = new ArrayList<>();
        for (String val : aList) {
            translatedItems.add(PageManager.translate(val));
        }
        String[] rslt = translatedItems.toArray(new String[translatedItems.size()]);
        return rslt;
    }

    public String[] getListItems_Gender() {
        return translateList(listItems_Gender);
    }

    public String[] getListItems_BankAccountType() {
        return translateList(listItems_BankAccountType);
    }

    public String[] getListItems_Country() {
        if (listItems_Country == null) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

            List<String> countryList = new ArrayList<>();

            // Call the appropriate service
            ArrayList<JSONRecord> listRetreived;
            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetCountryList, listToSend);

            // What did we get back?
            // The result is a JSON Array listing countries in A_PASSPORT_COUNTRYLIST
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the cost centre
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                String jCountry = null;
                // Step through each record
                //
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase("countryName")) {
                        jCountry = nvp.getValue();
                    }
                }
                if (jCountry != null) {
                    if (jCountry.trim().length() > 0) {
                        countryList.add(jCountry);
                    }
                }
            }

            listItems_Country = countryList.toArray(new String[countryList.size()]);
        }
        return listItems_Country;
    }

    public String[] getListItems_Province() {
        if (listItems_Province == null) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

            List<String> provinceList = new ArrayList<>();

            // Call the appropriate service
            ArrayList<JSONRecord> listRetreived;
            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetProvinceList, listToSend);

            // What did we get back?
            // The result is a JSON Array listing countries in GEN_ADDRESS_PROVINCE
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the cost centre
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                String jProvince = null;
                // Step through each record
                //
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase("SAProvince")) {
                        jProvince = nvp.getValue();
                    }
                }
                if (jProvince != null) {
                    if (jProvince.trim().length() > 0) {
                        provinceList.add(jProvince);
                    }
                }
            }
            // Add an additional option
            provinceList.add("-- Other --");

            listItems_Province = provinceList.toArray(new String[provinceList.size()]);
        }
        return listItems_Province;
    }

    /* ---- SUPPORTED THEMES ----- */
    private static final String defaultTheme = "aristo";
    private final String[] listItems_Theme
            = {"aristo", "bcx", "accsys"};

    /* ---- SUPPORTED LOCALES ---- */
    private final String[] listItems_LocaleDisplay
            = {"English", "Afrikaans", "Français", "Português", "isiZulu"};
    private final String[] listItems_LocaleLanguage
            = {"en", "af", "fr", "pt", "zu"};
    private final String[] listItems_LocaleCountry
            = {"", "ZA", "FR", "PT", "ZA"};
    private final String[] listItems_PayEntityType
            = {"entityType_Earning", "entityType_Deduction", "entityType_PackageComponent", "entityType_Interim", "entityType_Loan", "entityType_Saving"};

    private final LocaleDefinition[] listItems_LocalDefinitions
            = {
                new LocaleDefinition("English", "", "en"),
                new LocaleDefinition("Afrikaans", "ZA", "af"),
                new LocaleDefinition("Français", "FR", "fr"),
                new LocaleDefinition("Português", "PT", "pt"),
                new LocaleDefinition("isiZulu", "ZA", "zu")};

    public LocaleDefinition[] getListItems_LocalDefinitions() {
        return listItems_LocalDefinitions;
    }

}
