/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.helperclasses;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import za.co.accsys.web.beans.PageManager;

/**
 *
 * @author lterblanche
 */
public final class NewAccount {

    /**
     * Creates a new instance of NewAccount
     */
    public NewAccount() {
        this.payrollType = "Monthly";
        // Create default start date for new payrolls as 1st of March of the current year
        setPayrollTaxYearStart(DateUtils.formatDate("2015/03/01"));
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String UserFirstName) {
        this.userFirstName = UserFirstName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String UserSurname) {
        this.userSurname = UserSurname;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String UserLogin) {
        this.userLogin = UserLogin;
    }

    public String getUserPwd() {
        return userPwd;
    }

    /**
     * @param userPwd
     */
    public void setUserPwd(String userPwd) {
        if (userPwd != null && userPwd.length() > 0) {
            if ((this.userPwd == null) || (!this.userPwd.equals(userPwd))) {
                this.userPwd = userPwd;
                // Compare with Password
                verifyPassword(userPwd, userPwdVerified);
            }
        }
    }

    public String getUserPwdVerified() {
        return userPwdVerified;
    }

    public void setUserPwdVerified(String userPwdVerified) {
        if (userPwdVerified != null && userPwdVerified.length() > 0) {
            if ((this.userPwdVerified == null) || (!this.userPwdVerified.equals(userPwdVerified))) {
                this.userPwdVerified = userPwdVerified;
                // Compare with Password
                verifyPassword(userPwd, userPwdVerified);
            }
        }
    }

    private boolean verifyPassword(String password, String verifPassword) {
        if (password == null || verifPassword == null) {
            return false;
        }

        if (password.equals(verifPassword)) {
            return true;
        } else {
            FacesContext.getCurrentInstance().addMessage(
                    "", new FacesMessage(FacesMessage.SEVERITY_WARN,
                            PageManager.translate("PasswordsNotMatching"), PageManager.translate("PasswordsNotMatching")));
            RequestContext.getCurrentInstance().update("frmCentrePage:outerPanel");
            return false;
        }
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String UserEmail) {
        this.userEmail = UserEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getPayrollName() {
        return payrollName;
    }

    public void setPayrollName(String PayrollName) {
        this.payrollName = PayrollName;
    }

    public String getPayrollType() {
        return payrollType;
    }

    public void setPayrollType(String PayrollType) {
        this.payrollType = PayrollType;
    }

    public String getPayrollCountry() {
        return payrollCountry;
    }

    public void setPayrollCountry(String PayrollCountry) {
        this.payrollCountry = PayrollCountry;
    }

    public Date getPayrollTaxYearStart() {
        return payrollTaxYearStart;
    }

    public void setPayrollTaxYearStart(Date PayrollTaxYearStart) {
        this.payrollTaxYearStart = PayrollTaxYearStart;
        this.requestedOpenPeriod = DateUtils.formatDate(PayrollTaxYearStart);
    }

    public String getAccountID() {
        return accountID;
    }

    /**
     * Returns a list of possible period start dates, based on the payroll type
     * (weekly/monthly), and tax year start date
     *
     * @return
     */
    private List<String> periodStartDates;

    public List<String> getPeriodStartDates() {
        List<String> rslt = new ArrayList<>();

        if ((payrollTaxYearStart == null) || (payrollType == null)) {
            return rslt;
        }

        Date firstDate = payrollTaxYearStart;
        // Monthly
        if (PageManager.translateFromLocaleToEnglish(payrollType).equalsIgnoreCase("Monthly")) {
            for (int i = 0; i <= 11; i++) {
                rslt.add(DateUtils.formatDate(DateUtils.addNMonths(firstDate, i)));
            }
        }
        return rslt;
    }

    public String getRequestedOpenPeriod() {
        return requestedOpenPeriod;
    }

    public void setRequestedOpenPeriod(String requestedOpenPeriod) {
        this.requestedOpenPeriod = requestedOpenPeriod;
    }
    private String accountID;
    private String companyName;
    private String userFirstName;
    private String userSurname;
    private String userLogin;
    private String userPwd;
    private String userPwdVerified;
    private String userEmail;
    private String userPhone;
    private String payrollName;
    private String payrollType;
    private String payrollCountry;
    private Date payrollTaxYearStart;
    private String requestedOpenPeriod;

}
