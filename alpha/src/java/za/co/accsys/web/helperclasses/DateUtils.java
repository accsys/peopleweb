package za.co.accsys.web.helperclasses;

import java.util.*;
import java.text.*;

/**
 * Singleton, only to be used by {@link za.co.accsys.*} classes
 *
 * @author Liam Terblanche
 */
public class DateUtils {

    private static synchronized void createInstance() {
        if (theObject == null) {
            theObject = new DateUtils();
        }
    }

    /**
     * Get single instance of this class.
     *
     * @return
     */
    public static DateUtils getInstance() {
        if (theObject == null) {
            DateUtils.createInstance();
        }
        return theObject;
    }

    /**
     * Returns the current date in yyyy/MM/dd format
     *
     * @return
     */
    public static String getCurrentDate() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            return df.format(new java.util.Date());
        } catch (Exception e) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            return df.format(new java.util.Date());

        }
    }

    /**
     * Returns the current date and time in yyyy/MM/dd hh:mm:ss.SSS format
     *
     * @return
     */
    public static String getTimeStamp() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss.SSS");
            return df.format(new java.util.Date());
        } catch (Exception e) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            return df.format(new java.util.Date());

        }
    }
    
    /**
     * Returns the current date and time in yyyy-MM-dd hh:mm:ss format
     *
     * @return
     */
    public static String getTimeStampShort() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            return df.format(new java.util.Date());
        } catch (Exception e) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            return df.format(new java.util.Date());

        }
    }    

    /**
     * Returns name of month
     *
     * @param referenceDate
     * @return
     */
    public static String getNameOfMonth(java.util.Date referenceDate) {
        String[] monthName = {"January", "February",
            "March", "April", "May", "June", "July",
            "August", "September", "October", "November",
            "December"};

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(referenceDate);

        String month = monthName[calendar.get(Calendar.MONTH)];

        return month;
    }

    /**
     * Returns an array with days of the month in which the referenceDate is
     *
     * @param referenceDate
     * @return
     */
    public static int getDayOfMonth(java.util.Date referenceDate) {
        GregorianCalendar calendar = new GregorianCalendar();

        // set calendar to the beginning of the month
        calendar.setTime(referenceDate);
        return (calendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Returns an array with days of the month in which the referenceDate is
     *
     * @param referenceDate
     * @return
     */
    public static java.util.Date[] getDaysOfMonth(java.util.Date referenceDate) {
        GregorianCalendar calendar = new GregorianCalendar();

        // set calendar to the beginning of the month
        calendar.setTime(referenceDate);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        int N = 31;
        java.util.Date[] result = new java.util.Date[N];

        // Step over 31 days
        for (int i = 0; i < N; i++) {
            java.util.Date aDay = new java.util.Date(calendar.getTimeInMillis());
            result[i] = aDay;
            // move one day forward
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        return result;
    }

    /**
     * Returns the total number of months between two given dates (inclusive)
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    public static int getNumberOfMonthsBetween(java.util.Date fromDate, java.util.Date toDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(toDate);

        // Step through months, generating a database-specific
        // Date format
        while (calendar.getTime().getTime() > fromDate.getTime()) {
            // move one month back in time
            calendar.add(Calendar.MONTH, -1);
            result += 1;
        }
        return result;
    }

    /**
     * Returns the total number of weeks between two given dates (inclusive)
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    public static int getNumberOfWeeksBetween(java.util.Date fromDate, java.util.Date toDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(toDate);

        // Step through weeks, generating a database-specific
        // Date format
        while (calendar.getTime().getTime() > fromDate.getTime()) {
            // move one month back in time
            calendar.add(Calendar.WEEK_OF_YEAR, -1);
            result += 1;
        }
        return result;
    }

    /**
     * Returns the total number of days between two given dates (inclusive)
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    public static int getNumberOfDaysBetween(java.util.Date fromDate, java.util.Date toDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(toDate);

        // Step through months, generating a database-specific
        // Date format
        while (calendar.getTime().getTime() > fromDate.getTime()) {
            // move one month back in time
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            result += 1;
        }
        return result;
    }

    /**
     * Returns the total number of minutes between two given dates (inclusive)
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    public static int getNumberOfMinutesBetween(java.util.Date fromDate, java.util.Date toDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(toDate);

        // Step through months, generating a database-specific
        // Date format
        while (calendar.getTime().getTime() > fromDate.getTime()) {
            // move one month back in time
            calendar.add(Calendar.MINUTE, -1);
            result += 1;
        }
        return result;
    }

    /**
     * Adds one day to the given date
     *
     * @param aDate
     * @return
     */
    public static java.util.Date addOneDay(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        return calendar.getTime();
    }

    /**
     * Adds/deducts nbDays to/from the given date
     *
     * @param aDate
     * @param nbDays
     * @return
     */
    public static java.util.Date addNDays(java.util.Date aDate, int nbDays) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.DAY_OF_YEAR, nbDays);

        return calendar.getTime();
    }

    /**
     * Adds/deducts nbWeeks to/from the given date
     *
     * @param aDate
     * @param nbWeeks
     * @return
     */
    public static java.util.Date addNWeeks(java.util.Date aDate, int nbWeeks) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.WEEK_OF_YEAR, nbWeeks);

        return calendar.getTime();
    }

    /**
     * Adds/deducts nbMonths to/from the given date
     *
     * @param aDate
     * @param nbMonths
     * @return
     */
    public static java.util.Date addNMonths(java.util.Date aDate, int nbMonths) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.MONTH, nbMonths);

        return calendar.getTime();
    }

    /**
     * Adds one month to the given date
     *
     * @param aDate
     * @return
     */
    public static java.util.Date addOneMonth(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.MONTH, 1);

        return calendar.getTime();
    }

    public static java.util.Date addOneYear(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.YEAR, 1);

        return calendar.getTime();
    }

    /**
     * Reverse one month from the given date
     *
     * @param aDate
     * @return
     */
    public static java.util.Date removeOneMonth(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.MONTH, -1);

        return calendar.getTime();
    }

    /**
     * Reverse one month from the given date
     *
     * @param aDate
     * @return
     */
    public static java.util.Date removeOneYear(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.YEAR, -1);

        return calendar.getTime();
    }

    /**
     * Converts 'month yr' to date, e.g. 'January 2004' -> '2004/01/01'
     *
     * @param stringDate
     * @return
     */
    public static java.util.Date formatMonthYearToDate(String stringDate) {
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MMMM yyyy");
        try {
            return df.parse(stringDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns true if the two dates are in the same year and the same month
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean inSameMonth(java.util.Date date1, java.util.Date date2) {
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(date1);

        GregorianCalendar calendar2 = new GregorianCalendar();
        calendar2.setTime(date2);

        return (calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR))
                && (calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH));
    }

    /**
     * Returns true if the two dates are in the same year and the same week
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean inSameWeek(java.util.Date date1, java.util.Date date2) {
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(date1);

        GregorianCalendar calendar2 = new GregorianCalendar();
        calendar2.setTime(date2);

        return (calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR))
                && (calendar1.get(Calendar.WEEK_OF_YEAR) == calendar2.get(Calendar.WEEK_OF_YEAR));
    }

    /**
     * Returns true if the two dates are on the same day
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean onSameDay(java.util.Date date1, java.util.Date date2) {
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(date1);

        GregorianCalendar calendar2 = new GregorianCalendar();
        calendar2.setTime(date2);

        return (calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR))
                && (calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR));
    }

    /**
     * Returns true if refDate is between (inclusive) fromDate and toDate
     *
     * @param refDate
     * @param fromDate
     * @param toDate
     * @return
     */
    public static boolean betweenDates(java.util.Date refDate, java.util.Date fromDate, java.util.Date toDate) {
        if ((onSameDay(refDate, fromDate)) || (onSameDay(refDate, toDate))) {
            return true;
        }
        return (refDate.after(fromDate)) && (refDate.before(toDate));
    }

    /**
     * Formats a date to the format 'month yr', e.g. 'January 2004'
     *
     * @param date
     * @return
     */
    public static String formatDateToMonthYear(java.util.Date date) {
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MMMM yyyy");
        return df.format(date);
    }

    /**
     * Converts a date to the Accsys standard yyyy/mm/dd formatted String
     *
     * @param date
     * @return
     */
    public static String formatDate(java.util.Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        return df.format(date);
    }

    /**
     * Converts a date to the Accsys standard yyyy/mm/dd hh:mm:ss formatted String
     *
     * @param date
     * @return
     */
    public static String formatDateTime(java.util.Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd H:m:s");
        return df.format(date);
    }

    /**
     * Converts a date to the Accsys standard yyyy/mm/dd formatted String
     */
    private static String formatTime(java.util.Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("H:m:s:S");
        return df.format(date);
    }

    /**
     * Converts a String representation of a date to a Date type
     *
     * @param date
     * @return
     */
    public static java.util.Date formatDate(String date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");

        try {
            return df1.parse(date);
        } catch (ParseException e1) {
            SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                return df2.parse(date);
            } catch (ParseException e2) {
            }
        }
        return null;
    }

    public String getCurrentDateAndTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(new java.util.Date());
    }

    public static boolean isValidSAIDNumber(String idNumber) {
        /*
         The format of a normal South-African ID number is as follows
         {YYMMDD}{G}{SSS}{C}{A}{Z}
         YYMMDD: Date of birth
         G : Gender. 0-4 Female; 5-9 Male.
         SSS : Sequence No. for DOB/G combination.
         C : Citizenship. 0 SA; 1 Other.
         A : Usually 8, or 9 (can be other values)
         Z : Control digit.
         The following logic explains how the control digit works:
 
         For this explanation I am going to use ID number 860506 5 397 08 3
         a) Add all the digits of the ID number in the odd positions (except for the last number, which is the control digit):
         8+0+0+5+9+0 = 22
         b) Take all the even digits as one number and multiply that by 2:
         656378 * 2 = 1312756
         c) Add the digits of this number together (in b)
         1+3+1+2+7+5+6 = 25
         d) Add the answer of C to the answer of A
         22+25 = 47
         e) Subtract the second character from D from 10, this number should now equal the control character
         10-7 = 3 = control character (3)
 
         */
        int iOdd = Integer.valueOf(idNumber.substring(0, 1)) + Integer.valueOf(idNumber.substring(2, 3))
                + Integer.valueOf(idNumber.substring(4, 5)) + Integer.valueOf(idNumber.substring(6, 7))
                + Integer.valueOf(idNumber.substring(8, 9)) + Integer.valueOf(idNumber.substring(10, 11));
        String sEven = idNumber.substring(1, 2) + idNumber.substring(3, 4) + idNumber.substring(5, 6)
                + idNumber.substring(7, 8) + idNumber.substring(9, 10) + idNumber.substring(11, 12);
        // ToDo: Complete ID Validation
        return true;
    }
    private static DateUtils theObject;
}
