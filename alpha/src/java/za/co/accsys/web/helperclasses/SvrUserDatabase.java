/*
 * Represents a single Sybase database
 */
package za.co.accsys.web.helperclasses;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.controller.WebServiceClient;

/**
 *
 * @author lterblanche
 */
public class SvrUserDatabase {

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getDbID() {
        return dbID;
    }

    public void setDbID(String dbID) {
        this.dbID = dbID;
    }

    public String getDbURL() {
        return dbURL;
    }

    public void setDbURL(String dbURL) {
        this.dbURL = dbURL;
    }

    public String getJdbcURL() {
        return jdbcURL;
    }

    public void setJdbcURL(String jdbcURL) {
        this.jdbcURL = jdbcURL;
    }

    /**
     * Can I access this database?
     *
     * @return
     */
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Is this database in use by a client?
     *
     * @return
     */
    public boolean isInUse() {
        return (accountID != null);
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    /**
     * Returns TRUE if a successful connection was established
     *
     * @return
     */
    public boolean canConnect() {
        boolean rslt = false;
        this.hrsRunning = "0";
        this.pwVersion = "unknown";
        ArrayList<NameValuePair> listToSend = new ArrayList<>();

        // Call the ws_HTTP_GetCreditsLeft service
        ArrayList<NameValuePair> wsResult;
        try {
            wsResult = WebServiceClient.getInstance().getOneLineResponse(null, dbURL, Constants.serviceName_CanConnect, listToSend);

            // What did we get back?
            for (NameValuePair nvp : wsResult) {

                if (nvp.getName().equalsIgnoreCase("HrsRunning")) {
                    this.hrsRunning = nvp.getValue();
                    rslt = true;
                }
                if (nvp.getName().equalsIgnoreCase("PeopleWareVersion")) {
                    this.pwVersion = nvp.getValue();
                    rslt = true;
                }
                
            }
        } catch (Exception e){
            return false;
        }

        return rslt;
    }

    public String getHrsRunning() {
        return hrsRunning;
    }

    public String getPwVersion() {
        return pwVersion;
    }

    
    private String serverName;
    private String dbID;
    private String dbURL;
    private String jdbcURL;
    private String accountID;
    private String hrsRunning;
    private String pwVersion;
    private boolean active;

}
