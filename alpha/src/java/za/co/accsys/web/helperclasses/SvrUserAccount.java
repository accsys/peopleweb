/*
 * The SvrUserAccount class is used to structure all the account information stored - per account - in the settings.xml file
 */
package za.co.accsys.web.helperclasses;

import java.util.Date;
import za.co.accsys.web.controller.WebServiceClient;

/**
 *
 * @author lterblanche
 */
public class SvrUserAccount {

    public SvrUserAccount() {
    }

    public SvrUserAccount(String accountID, String dbID, String smtpFrom, String companyName, String ownerFirstName, String ownerSurname, String ownerEmail, String ownerPhone) {
        this.accountID = accountID;
        this.accountNumber = accountID;
        this.active = true;
        this.creationDate = new java.util.Date();
        this.dbID = dbID;
        this.smtpFrom = smtpFrom;
        this.companyName = companyName;
        this.ownerFirstName = ownerFirstName;
        this.ownerSurname = ownerSurname;
        this.ownerEmail = ownerEmail;
        this.ownerPhone = ownerPhone;
    }
    
    

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDbURL() {
        return dbURL;
    }

    public void setDbURL(String dbURL) {
        this.dbURL = dbURL;
    }

    public String getJdbcURL() {
        return jdbcURL;
    }

    public void setJdbcURL(String jdbcURL) {
        this.jdbcURL = jdbcURL;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerSurname() {
        return ownerSurname;
    }

    public void setOwnerSurname(String ownerSurname) {
        this.ownerSurname = ownerSurname;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }
    
    public Date getLastAccessDate() {
        return lastAccessDate;
    }

    public void setLastAccessDate(Date lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getSmtpFrom() {
        return smtpFrom;
    }

    public void setSmtpFrom(String smtpFrom) {
        this.smtpFrom = smtpFrom;
    }

    /** 
     * Returns the number of available/usable payroll credits
     * 
     * @return 
     */
    public int getPayrollCredits() {
        try{
        return Integer.parseInt(WebServiceClient.getInstance().GLOBAL_getCreditsAvailable(accountID, "Payroll"));
        } catch (Exception e){
            return -1;
        }
    }
    
   /** 
     * Returns the projected credit usage of this account
     * 
     * @return 
     */
    public int getPayrollMonthlyCreditUsageProjection() {
        try{
        return Integer.parseInt(WebServiceClient.getInstance().GLOBAL_getMonthlyCreditUsageProjection(accountID, "Payroll"));
        } catch (Exception e){
            return -1;
        }
    }    
    
    /**
     * Returns the number of days since this account was last accessed
     * @return 
     */
    public int getIdleDays(){
        Date fromDate = lastAccessDate;
        if (fromDate == null){
            fromDate = creationDate;
        }
        Date toDate = za.co.accsys.web.helperclasses.DateUtils.formatDate(za.co.accsys.web.helperclasses.DateUtils.getCurrentDate());
        return za.co.accsys.web.helperclasses.DateUtils.getNumberOfDaysBetween(fromDate, toDate);
    }

    public String getDbID() {
        return dbID;
    }

    public void setDbID(String dbID) {
        this.dbID = dbID;
    }

    
    private String accountID;
    private String dbID;
    private String accountNumber;
    private String dbURL;
    private String jdbcURL;
    private String smtpFrom;
    private Date creationDate;
    private String companyName;
    private String ownerFirstName;
    private String ownerSurname;
    private String ownerEmail;
    private String ownerPhone;
    private Date lastAccessDate;
    private boolean active;

}
