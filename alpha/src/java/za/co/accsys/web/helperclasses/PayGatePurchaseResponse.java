/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.helperclasses;

import za.co.accsys.web.beans.Constants;

/**
 *
 * @author liam
 */
public class PayGatePurchaseResponse extends PayGateBaseClass {

    /**
     * Constructs an instance of a PayGate transaction response
     *
     * @param pgID // Internal registered PayGate ID
     * @param pgReference // Internally generated reference
     * @param pgTransactionStatus
     * @param pgResultCode
     * @param pgAuthCode
     * @param pgAmountInCents
     * @param pgResultDescr
     * @param pgTransactionID
     * @param pgRiskIndicator
     * @param pgChecksum
     */
    public PayGatePurchaseResponse(String pgID, String pgReference, String pgTransactionStatus, String pgResultCode, String pgAuthCode, int pgAmountInCents, String pgResultDescr, String pgTransactionID, String pgRiskIndicator, String pgChecksum) {
        this.pgID = pgID;
        this.pgReference = pgReference;
        this.pgTransactionStatus = pgTransactionStatus;
        this.pgResultCode = pgResultCode;
        this.pgAuthCode = pgAuthCode;
        this.pgAmountInCents = pgAmountInCents;
        this.pgResultDescr = pgResultDescr;
        this.pgTransactionID = pgTransactionID;
        this.pgRiskIndicator = pgRiskIndicator;
        this.pgChecksum = pgChecksum;
    }

    @Override
    public String toString() {
        return "PayGatePurchaseResponse [pgID=" + pgID + "; pgReference=" + pgReference + "; pgTransactionStatus=" + pgTransactionStatus + "; pgResultCode=" + pgResultCode + "; pgAuthCode="
                + pgAuthCode + "; pgAmount=" + pgAmountInCents + "; pgResultDescr=" + pgResultDescr + "; pgTransactionID=" + pgTransactionID + "; pgRiskIndicator=" + pgRiskIndicator + "; pgChecksum=" + pgChecksum;
    }

    /**
     * Persists the exact response to the SQLite database.
     */
    public void logReponseToDB() {
        ServerPrefs prefs = ServerPrefs.getInstance();
        prefs.logPurchaseResponseToDB(DateUtils.getTimeStampShort(), pgReference, pgTransactionStatus, pgResultCode,
                pgAuthCode, pgAmountInCents, pgResultDescr, pgTransactionID, pgRiskIndicator, pgChecksum);
    }

    /**
     * Call pareseResponse() to evaluate the response and decide on the next
     * step of action.
     *
     * @return TRUE if transaction was valid, legal, and successful
     */
    public boolean parseResponse() {
        // 1. Verify the checksum.  If not OK, this response has been tampered with
        if (!getMd5Checksum().equals(pgChecksum)) {
            authenticationResponse = trRESPONSE_FAIL_INVALIDCHECKSUM;
            return false;
        }

        // 2. Transaction Status
        if (pgTransactionStatus.equals(trSTATUS_OK)) {
            authenticationResponse = trRESPONSE_SUCCESS;
            return true;
        }

        // 3. Processing errors, let's enumerate it
        if (pgTransactionStatus.equals(trSTATUS_ERRORS)) {
            authenticationResponse = pgResultCode + ": " + getAuthenticationResponseForCode(Integer.parseInt(pgResultCode));
            return false;
        }
        
        // 4. Processing 'other problems', let's enumerate it
        if (pgTransactionStatus.equals(trSTATUS_OTHER)) {
            authenticationResponse = pgResultCode + ": " + getAuthenticationResponseForCode(Integer.parseInt(pgResultCode));
            return false;
        }
        
        return false;
    }

    /**
     * Generates the MD5 checksum for PayGate response, as per their
     * documentation
     *
     * @return
     */
    public String getMd5Checksum() {

        StringBuilder preChecksum = new StringBuilder();
        // paygate_id
        preChecksum.append(pgID).append("|");
        // reference
        preChecksum.append(pgReference).append("|");
        preChecksum.append(pgTransactionStatus).append("|");
        preChecksum.append(pgResultCode).append("|");
        preChecksum.append(pgAuthCode).append("|");
        preChecksum.append(pgAmountInCents).append("|");
        preChecksum.append(pgResultDescr).append("|");
        preChecksum.append(pgTransactionID).append("|");
        preChecksum.append(pgRiskIndicator).append("|");
        preChecksum.append(Constants.PAYGATE_ENCRYPTION_KEY);
        return (toMD5(preChecksum.toString()));

    }

    /**
     * Once the PayGate response has been parsed, this will return a readable
     * string to display to the user.
     *
     * @return
     */
    public String getAuthenticationResponse() {
        return authenticationResponse;
    }

    private String getAuthenticationResponseForCode(int code) {
        switch (code) {
            case 900001:
                return "Call your financial institution for approval";
            case 900002:
                return "Your card expired";
            case 900003:
                return "Insufficient Funds";
            case 900004:
                return "Invalid Card Number";
            case 900005:
                return "Bank Interface Timeout";
            case 900006:
                return "Invalid Card";
            case 900007:
                return "Declined";
            case 900009:
                return "Lost Card";
            case 900010:
                return "Invalid Card Length";
            case 900011:
                return "Suspected Fraud";
            case 900012:
                return "Card Reported As Stolen";
            case 900013:
                return "Restricted Card";
            case 900014:
                return "Excessive Card Usage";
            case 900015:
                return "Card Blacklisted Indicates that the transaction has been flagged as High Risk (possibly fraudulent) by PayProtector Online fraud screening system.";
            case 900207:
                return "Declined - authentication failed";
            case 900220:
                return "Incorrect PIN";
            case 991001:
                return "Invalid expiry date";
            case 991002:
                return "Invalid Amount";
            case 900209:
                return "Transaction verification failed";
            case 900210:
                return "Authentication complete; transaction must be restarted";
            case 990024:
                return "Duplicate Transaction Detected. Please check";
            case 990028:
                return "Transcation cancelled";
            case 900205:
                return "Unexpected authentication result (phase 1)";
            case 900206:
                return "Unexpected authentication result (phase 1)";
            case 990001:
                return "Could not insert into Database";
            case 990022:
                return "Bank not available";
            case 990029:
                return "Transaction Not Completed";
            case 990053:
                return "Error processing transaction";
        }
        return "Unknown Authentication Error:" + code;
    }

    /**
     * Decouples the reference AccountID_NbCredits_Module and returns the number
     * of credits purchased
     *
     * @param reference
     * @return
     */
    public String getCreditsFromReference(String reference) {
        // Format: accountID_module_nbCredits
        return reference.substring(reference.lastIndexOf("_") + 1);
    }

    /**
     * Decouples the reference AccountID_NbCredits_Module and returns the module
     * for which credits were purchased
     *
     * @param reference
     * @return
     */
    public String getModuleFromReference(String reference) {
        // Format: accountID_module_nbCredits
        return reference.substring(reference.indexOf("_") + 1, reference.lastIndexOf("_"));
    }

    /**
     * Decouples the reference AccountID_NbCredits_Module and returns the
     * AccountID for which credits were purchased
     *
     * @param reference
     * @return
     */
    public String getAccountIDFromReference(String reference) {
        // Format: accountID_module_nbCredits
        return reference.substring(0, reference.indexOf("_"));
    }

    private final String pgID; // Number(11)
    private final String pgReference; // Varchar (80)
    private final String pgTransactionStatus; // Char(1)
    private final String pgResultCode;
    private final String pgAuthCode;
    private final int pgAmountInCents;
    private final String pgResultDescr;
    private final String pgTransactionID;
    private final String pgRiskIndicator;
    private final String pgChecksum;
    private String authenticationResponse;

    // Transaction Parsing Reponses
    private static final String trRESPONSE_FAIL_INVALIDCHECKSUM = "The PayGate checksum seems to have been changed.  Although your account may have been debited, we cannot accept the payment notification.  Please contact our Support Desk for further assistance";
    private static final String trRESPONSE_SUCCESS = "Your purchase was successful.  PeopleWeb will automatically update your credits accordingly.";

    // Transaction Parsing Constants
    private static final String trSTATUS_OK = "1";
    private static final String trSTATUS_ERRORS = "2";
    private static final String trSTATUS_OTHER = "0";
}
