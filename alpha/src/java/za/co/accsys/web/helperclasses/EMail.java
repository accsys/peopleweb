package za.co.accsys.web.helperclasses;

import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

/**
 * Used to send out email messages (with attachments)
 */
public class EMail {

    /**
     * Constructor If authUser and/or authPwd is null or empty, no user
     * authentication will be used.
     *
     * @param smtpServer
     * @param smtpPort
     * @param authUser
     * @param authPwd
     * @param fromSender
     */
    public EMail(String smtpServer, String smtpPort, String authUser, String authPwd, String fromSender) {
        this.smtpServer = smtpServer;
        this.smtpPort = smtpPort;
        smtpAuthUser = authUser;
        smtpAuthPwd = authPwd;
        smtpFrom = fromSender;
    }

    /**
     * Sends an email without any attachments
     *
     * @param smtpTo
     * @param smtpCC
     * @param subject
     * @param body
     * @return
     */
    public boolean send(String smtpTo, String smtpCC, String subject, String body) {
        try {
            return send(smtpTo, smtpCC, subject, body, null);
        } catch (Exception e) {
            System.out.println("Error on sending email...\n\tsend(" + smtpTo + "," + smtpCC + "," + subject + "," + body);
            System.out.println("\tException:" + e.getMessage());
            return false;
        }
    }

    /**
     * Sends an email <b>with</b> an attachment Returns true if successful.
     *
     * @param smtpTo
     * @param smtpCC
     * @param subject
     * @param body
     * @param attachedFile
     * @return
     */
    public boolean send(String smtpTo, String smtpCC, String subject, String body, java.io.File attachedFile) {
        try {
            Properties props = new Properties();
            System.out.println("**********************************************");
            System.out.println("Send email...\nEMail.java\tmethod:send(" + smtpTo + "," + smtpCC + "," + subject + "," + body);
            System.out.println("\tSMTP Server:" + smtpServer);
            System.out.println("\tSMTP Port:" + smtpPort);
            System.out.println("\tSMTP From:" + smtpFrom);
            if (doUseAuthentication()) {
                System.out.println("\tSMTP - user authentication applied");
                System.out.println("\tSMTP User:" + smtpAuthUser);
                System.out.println("\tSMTP Pwd:" + smtpAuthPwd);
            } else {
                System.out.println("\tSMTP - not using user authentication");
            }

            System.out.println("**********************************************");
            // Do I have a recipient?
            if (smtpTo.trim().isEmpty()) {
                System.out.println("Notification Failure: Undefined recipient for Message...");
                System.out.println("Subject:\"" + subject + "\"");
                System.out.println("Body\"" + body + "\"");
                return false;
            }
            // Try to attach to a default session
            //        props.put("mail.smtp.host", smtpServer);

            props.put("mail.smtp.host", smtpServer);
            props.put("mail.smtp.port", smtpPort);
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(false);

            //Create a new message
            MimeMessage msg = new MimeMessage(mailSession);

            // Set FROM and TO
            msg.setFrom(new InternetAddress(smtpFrom));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(smtpTo, false));
            if (smtpCC != null) {
                msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(smtpCC, false));
            }
            // Set Subject and Body
            msg.setSubject(subject);
            // msg.setText(body);
            //msg.setContent(body,"text/html");
            msg.setDataHandler(new DataHandler(new ByteArrayDataSource(body, "text/html")));
            Transport transport = mailSession.getTransport("smtp");
            if (doUseAuthentication()) {
                transport.connect(smtpServer, smtpAuthUser, smtpAuthPwd);
            }
            if ((attachedFile != null) && (attachedFile.exists())) {

                // create and fill the first message part
                MimeBodyPart mbp1 = new MimeBodyPart();
                mbp1.setText(body);

                // create the second message part
                MimeBodyPart mbp2 = new MimeBodyPart();

                // attach the file to the message
                FileDataSource fds = new FileDataSource(attachedFile);
                mbp2.setDataHandler(new DataHandler(fds));
                mbp2.setFileName(fds.getName());

                // create the Multipart and add its parts to it
                Multipart mp = new MimeMultipart();
                mp.addBodyPart(mbp1);
                mp.addBodyPart(mbp2);

                // add the Multipart to the message
                msg.setContent(mp);

                // set the Date: header
                msg.setSentDate(new Date());

            }
            Transport.send(msg);

            return true;
        } catch (MessagingException ex) {
            System.out.println("MessagingException on sending email...\n\tsend(" + smtpTo + "," + smtpCC + "," + subject + "," + body);
            System.out.println("\tException:" + ex.getMessage());
            return false;
        } catch (Exception e) {
            System.out.println("Error on sending email...\n\tsend(" + smtpTo + "," + smtpCC + "," + subject + "," + body);
            System.out.println("\tException:" + e.getMessage());
            return false;
        }
    }

    private boolean doUseAuthentication() {
        Boolean userNameAuthentication = false;
        Boolean passwordAuthentication = false;

        if ((smtpAuthUser != null)) {
            userNameAuthentication = (!smtpAuthUser.trim().isEmpty());
        }
        if ((smtpAuthPwd != null)) {
            passwordAuthentication = (!smtpAuthPwd.trim().isEmpty());
        }
        return (userNameAuthentication || passwordAuthentication);
    }

    String smtpServer;
    String smtpPort;
    String smtpFrom;
    String smtpAuthUser;
    String smtpAuthPwd;
}
