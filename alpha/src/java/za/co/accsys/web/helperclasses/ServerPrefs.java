/*
 * ServerPrefs.java
 *
 * Created on 07 September 2014, 07:23
 *
 * Used to store all the values required for the different users, databases, etc.
 */
package za.co.accsys.web.helperclasses;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import za.co.accsys.peopleweb.MailChimpClientAccount;
import za.co.accsys.peopleweb.MailChimpMailer;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.model.SessionUser;

/**
 *
 * @author lwt
 */
public final class ServerPrefs {

    private ServerPrefs() {

        reloadAccountPreferences();

    }

    /**
     * Reloads the server configuration for all the accounts and databases (and combinations thereof)
     */
    public void reloadAccountPreferences() {

        createDB();
        accounts = loadAccountSettingsFromDB();
        systemPrefs = loadServerSettingsFromDB();
        databases = loadDatabaseInfoFromDB();

    }

    /**
     * As part of the initialization process, we create the necessary SQLite tables if they don't exist
     * already.
     *
     */
    private void createDB() {
        SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);
            // Create the account table if it does not already exist
            db.exec("BEGIN TRANSACTION;");
            db.exec("CREATE TABLE if NOT EXISTS account ("
                    + "accID INTEGER PRIMARY KEY, account_number INTEGER, creation_date TEXT, company_name TEXT, "
                    + "owner_firstname TEXT, owner_surname TEXT, owner_email TEXT, owner_phone TEXT, last_access TEXT, smtp_from TEXT, "
                    + "dbID INTEGER, active INTEGER DEFAULT 0"
                    + ")");
            db.exec("COMMIT;");

            // Create the database table if it does not already exist
            db.exec("BEGIN TRANSACTION;");
            db.exec("CREATE TABLE if NOT EXISTS database ("
                    + "dbID INTEGER PRIMARY KEY, db_url TEXT, jdbc_url TEXT"
                    + ")");
            db.exec("COMMIT;");

            // Create the global_settings table if it does not already exist
            db.exec("BEGIN TRANSACTION;");
            db.exec("CREATE TABLE if NOT EXISTS global_settings ("
                    + "smtp_server TEXT PRIMARY KEY, smtp_port TEXT, smtp_auth_user TEXT, smtp_auth_pwd TEXT, external_url TEXT"
                    + ")");
            db.exec("COMMIT;");

            // Create the log table if it does not already exist
            db.exec("BEGIN TRANSACTION;");
            db.exec("CREATE TABLE if NOT EXISTS log ("
                    + "timestamp TEXT PRIMARY KEY, session_hash TEXT, pagemanager_hash TEXT, session_user TEXT, detail TEXT  "
                    + ")");
            db.exec("COMMIT;");

            // Create the credit purchase history table if it does not already exist
            db.exec("BEGIN TRANSACTION;");
            db.exec("CREATE TABLE if NOT EXISTS purchase_response ("
                    + "response_date TEXT PRIMARY KEY, transaction_reference TEXT, transaction_status TEXT, "
                    + "result_code TEXT, auth_code TEXT, amount_in_cents INTEGER, result_desc TEXT, "
                    + "transaction_id TEXT, risk_indicator TEXT, checksum TEXT"
                    + ")");
            db.exec("COMMIT;");

            // Create the credit cost table if it does not already exist
            db.exec("BEGIN TRANSACTION;");
            db.exec("CREATE TABLE if NOT EXISTS credit_cost ("
                    + "module TEXT PRIMARY KEY, unit_cost_in_cents INTEGER"
                    + ")");
            db.exec("COMMIT;");

            db.dispose();

        } catch (SQLiteException ex) {
            Logger.getLogger(ServerPrefs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns the first unused database from the SQLite database
     *
     * @return
     */
    public SvrUserDatabase getFirstUnusedDatabase() {
        for (SvrUserDatabase db : databases) {
            if (!db.isInUse()) {
                return db;
            }
        }
        return null;
    }

    /**
     * Returns the next integer to be used for accID in table 'account'
     *
     * @return
     */
    private int getNextUnusedUserAccountID() {
        int maxID = 0;
        for (SvrUserAccount account : accounts) {
            if (Integer.parseInt(account.getAccountID()) > maxID) {
                maxID = Integer.parseInt(account.getAccountID());
            }
        }
        return maxID + 1;
    }

    /**
     * Checks the existing accounts to see if this email address has already been used
     *
     * @param newEmail
     * @return
     */
    public boolean accountEmailUnique(String newEmail) {
        for (SvrUserAccount account : accounts) {
            if (account.getOwnerEmail().trim().equalsIgnoreCase(newEmail.trim())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Uses the NewAccount instance to set up the SQLite database with all the necessary changes
     *
     * @param newAccount
     * @return
     */
    public boolean registerNewAccount(NewAccount newAccount) {
        System.out.println("------------------------------------------------");
        System.out.println(" Registering new account...");
        SvrUserDatabase db = getFirstUnusedDatabase();
        if (db == null) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            System.out.println("Unable to create a new user account.  No ununsed databases available for allocation.");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            return false;
        }

        // Create a new SvrUserAccount
        String accID = Integer.toString(getNextUnusedUserAccountID());
        System.out.println(" AccountID = " + accID);
        SvrUserAccount user = new SvrUserAccount(accID, db.getDbID(), Constants.SMTP_FROM_ADDRESS, newAccount.getCompanyName(),
                newAccount.getUserFirstName(), newAccount.getUserSurname(), newAccount.getUserEmail(), newAccount.getUserPhone());

        // Save changes into SQLite database
        saveAccountSettingsToDB(user);
        // Once completed, reload all account settings
        reloadAccountPreferences();

        // After reloading accounts and databases, use the internal classes to continue further.
        SvrUserAccount newUserAcc = getAccount(accID);
        // Update initial NewAccount
        newAccount.setAccountID(accID);
        // Now that we've linked the new account to an available database, we need to 
        // 1. Connect to that database
        // 2. Prepare the database for the new user by calling the 'fn_HTTP_InitiateDatabase()' stored-procedure
        try {
            System.out.println(" Preparing PeopleWeb for first use...");
            if (preparePWDbForFirstUse(newUserAcc.getAccountID(), newAccount)) {
                // 3. Send a welcome email to the new account user.
                //emailWelcomeToNewAccount(newAccount);
                System.out.println(" Initiate MailChimp notification...");
                MailChimpClientAccount mcAccount;
                mcAccount = new MailChimpClientAccount(newAccount.getAccountID(),
                        newAccount.getCompanyName(), newAccount.getUserFirstName(),
                        newAccount.getUserSurname(), newAccount.getUserEmail(),
                        newAccount.getUserLogin());
                ArrayList<MailChimpClientAccount> mcAccounts = new ArrayList<>();
                mcAccounts.add(mcAccount);

                System.out.println(" Sending MailChimp notification...");
                if (MailChimpMailer.getInstance().sendMail(mcAccounts, MANDRILL_WELCOME_TEMPLATE, MANDRILL_WELCOME_SUBJECT, MANDRILL_WELCOME_FROMEMAIL, MANDRILL_WELCOME_FROMNAME)) {
                    System.out.println(" Successfully sent MailChimp notification...");
                } else {
                    System.out.println(" Problems encountered with MailChimp notification");
                }

                return true;

            }
        } catch (Exception e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            System.out.println("Unable to create a new user account.  ");
            System.out.println("Error:" + e.getMessage());
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            return false;
        }
        System.out.println("------------------------------------------------");

        return true;
    }

//    /**
//     * Sends an email to the newly created account owner To-Do: We need to move this into a more dynamic
//     * configuration where we can change the email structure and message without recompiling
//     *
//     * @param userAccount
//     */
//    private boolean emailWelcomeToNewAccount(NewAccount newAccount) {
//        // Create Email instance
//        EMail newAccountMailer = new EMail(getSMTPServer(),
//                getSMTPPort(), getSMTPAuthUser(), getSMTPAuthPwd(), getSMTPFromAddress(newAccount.getAccountID()));
//        // Welcome Message
//        String htmlMessage = Constants.EMAIL_WELCOME_NEW_CLIENT_MESSAGE;
//        // Replace the sections
//        htmlMessage = htmlMessage.replace(Constants.EMAIL_RPL_FIRSTNAME, newAccount.getUserFirstName());
//        htmlMessage = htmlMessage.replace(Constants.EMAIL_RPL_LOGINNAME, newAccount.getUserLogin());
//        htmlMessage = htmlMessage.replace(Constants.EMAIL_RPL_ACCOUNTID, newAccount.getAccountID());
//        htmlMessage = htmlMessage.replace(Constants.EMAIL_RPL_LOGINURL, ServerPrefs.getInstance().getExternalURL());
//
//        return (newAccountMailer.send(newAccount.getUserEmail(), Constants.EMAIL_CCLIST, "PeopleWeb - New Account Notification", htmlMessage));
//
//    }
    /**
     * Connects to the appropriate database and 1. Clears out all existing payrolls, variables, employees, and
     * companies 2. Creates a new payroll, and opens it up in the first period 3. Creates all the default
     * variables and links it to this payroll 4. Creates a new company and employee and link him/her to this
     * payroll
     *
     * @param newUserAcc The Account representation in the SQLite database. This account is already associated
     * with an available database.
     * @param newUser More information about the user, retrieved from the New User Registration step in
     * PeopleWeb.
     * @return true if successful
     */
    private boolean preparePWDbForFirstUse(String accountID, NewAccount newUser) {
        // Year End is one day less than a full year. (+ 12 months) (- 1 day)
        Date tys = newUser.getPayrollTaxYearStart();
        Date tye = DateUtils.addNMonths(tys, 12);
        tye = DateUtils.addNDays(tye, -1);

        /*
         :companyID, :companyName, :userFirstName, :userSurname, :userLogin, :userPwd, :userEmail, :userPhone :payrollName, :payrollType, :payrollCountry, :taxYearStart, :taxYearEnd
         */
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair("companyID", accountID));
        listToSend.add(new BasicNameValuePair("companyName", newUser.getCompanyName()));
        listToSend.add(new BasicNameValuePair("userFirstName", newUser.getUserFirstName()));
        listToSend.add(new BasicNameValuePair("userSurname", newUser.getUserSurname()));
        listToSend.add(new BasicNameValuePair("userLogin", newUser.getUserLogin()));
        listToSend.add(new BasicNameValuePair("userPwd", newUser.getUserPwd()));
        listToSend.add(new BasicNameValuePair("userEmail", newUser.getUserEmail()));
        listToSend.add(new BasicNameValuePair("userPhone", newUser.getUserPhone()));
        listToSend.add(new BasicNameValuePair("payrollName", newUser.getPayrollName()));
        listToSend.add(new BasicNameValuePair("payrollType", newUser.getPayrollType()));
        listToSend.add(new BasicNameValuePair("payrollCountry", newUser.getPayrollCountry()));
        listToSend.add(new BasicNameValuePair("taxYearStart", DateUtils.formatDate(newUser.getPayrollTaxYearStart())));
        listToSend.add(new BasicNameValuePair("taxYearEnd", DateUtils.formatDate(tye)));
        listToSend.add(new BasicNameValuePair("openPeriod", newUser.getRequestedOpenPeriod()));
        /*
         // Add 12 months
         rslt = DateUtils.addNMonths(taxYearStart, 12);
         // Minus one day
         rslt = DateUtils.addNDays(rslt, -1);
         */

        // Call the GetValue service
        ArrayList<NameValuePair> resetAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(accountID);
        resetAttributes = WebServiceClient.getInstance().getOneLineResponse(null, URL, Constants.serviceName_InitiateNewDB, listToSend);

        // Note: This web service call returns the first person that is
        // a. An Administrator (record in al_emaster_agroup)
        // b. In this server (resetAccountID)
        // c. Using this email address (resetEmail)
        //
        // The result is a single string, comprising login_name + '*' + new_password
        // What did we get back?
        for (NameValuePair nvp : resetAttributes) {

            if (nvp.getName().equalsIgnoreCase("DB_RESULT")) {
                return (nvp.getValue().trim().equals("1"));
            }
        }
        return false;
    }

    /**
     * Updates the database with the current account preferences
     *
     * @param accPrefs
     * @return
     */
    public boolean saveAccountSettingsToDB(SvrUserAccount accPrefs) {
        boolean rslt = false;

        SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);

            // Insert/Update the preferences
            SQLiteStatement insertdata;
            db.exec("BEGIN TRANSACTION;");
            {
                insertdata = db.prepare("INSERT OR REPLACE INTO account "
                        + "( accID, account_number, creation_date, company_name, owner_firstname, owner_surname, owner_email, owner_phone, last_access, smtp_from, dbID, active)"
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?,1);");

                insertdata.bind(1, accPrefs.getAccountID());
                insertdata.bind(2, accPrefs.getAccountNumber());
                insertdata.bind(3, DateUtils.formatDate(accPrefs.getCreationDate()));
                insertdata.bind(4, accPrefs.getCompanyName());
                insertdata.bind(5, accPrefs.getOwnerFirstName());
                insertdata.bind(6, accPrefs.getOwnerSurname());
                insertdata.bind(7, accPrefs.getOwnerEmail());
                insertdata.bind(8, accPrefs.getOwnerPhone());
                insertdata.bind(9, DateUtils.formatDate(accPrefs.getLastAccessDate()));
                insertdata.bind(10, accPrefs.getSmtpFrom());
                insertdata.bind(11, accPrefs.getDbID());
                insertdata.step(); //executes the prepared statement, (a row has been inserted)
                System.out.println("\n-- AccountPreferences for ID:" + accPrefs.getAccountID() + " updated in database. \n\n");
            }
            db.exec("COMMIT;");

            db.dispose();

        } catch (SQLiteException ex) {
            Logger.getLogger(ServerPrefs.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return rslt;
    }

    /**
     * Removes the account from the sqLite database as well as from the internal account list
     *
     * @param accountID
     * @return
     */
    public boolean removeAccount(String accountID) {

        // First remove account from SQLite
        SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);

            // Insert/Update the preferences
            SQLiteStatement deleteData = db.prepare("delete from account where accID=?");
            deleteData.bind(1, accountID);
            deleteData.step(); //executes the prepared statement, (a row has been inserted)
            //db.exec("COMMIT;");

            db.dispose();

        } catch (SQLiteException ex) {
            Logger.getLogger(ServerPrefs.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        // Now remove this account from the list too
        for (SvrUserAccount account : accounts) {
            if (account.getAccountID().trim().equalsIgnoreCase(accountID)) {
                accounts.remove(account);
                return true;
            }
        }

        return false;
    }

    private ArrayList<SvrUserAccount> loadAccountSettingsFromDB() {
        ArrayList<SvrUserAccount> rslt = new ArrayList<>();

        SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);

            // Insert/Update the preferences
            SQLiteStatement retrievedata = db.prepare("select accID, account_number, creation_date, company_name, "
                    + "owner_firstname, owner_surname, owner_email, owner_phone, last_access, smtp_from, "
                    + "db_url, jdbc_url, account.dbID from account join database on account.dbID = database.dbID;");
            while (retrievedata.step()) {
                SvrUserAccount pref = new SvrUserAccount();
                pref.setAccountID(retrievedata.columnString(0));
                pref.setAccountNumber(retrievedata.columnString(1));
                pref.setCreationDate(DateUtils.formatDate(retrievedata.columnString(2)));
                pref.setCompanyName(retrievedata.columnString(3));
                pref.setOwnerFirstName(retrievedata.columnString(4));
                pref.setOwnerSurname(retrievedata.columnString(5));
                pref.setOwnerEmail(retrievedata.columnString(6));
                pref.setOwnerPhone(retrievedata.columnString(7));
                pref.setLastAccessDate(DateUtils.formatDate(retrievedata.columnString(8)));
                pref.setSmtpFrom(retrievedata.columnString(9));
                pref.setDbURL(retrievedata.columnString(10));
                pref.setJdbcURL(retrievedata.columnString(11));
                pref.setDbID(retrievedata.columnString(12));
                rslt.add(pref);
            }

            db.dispose();

        } catch (SQLiteException ex) {
            Logger.getLogger(ServerPrefs.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return rslt;
    }

    /**
     * Updates the general system preferences
     *
     * @param sysPrefs
     * @return
     */
    public boolean saveServerSettingsToDB(SvrGeneralPrefs sysPrefs) {
        boolean rslt = false;

        SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);

            // Insert/Update the preferences
            SQLiteStatement insertdata;
            db.exec("BEGIN TRANSACTION;");
            {
                insertdata = db.prepare("INSERT OR REPLACE INTO global_settings "
                        + "( smtp_server, smtp_port, smtp_auth_user, smtp_auth_pwd,external_url)"
                        + "VALUES (?,?,?,?,?);");

                insertdata.bind(1, sysPrefs.getSmtpServer());
                insertdata.bind(2, sysPrefs.getSmtpPort());
                insertdata.bind(3, sysPrefs.getSmtpAuthUser());
                insertdata.bind(4, sysPrefs.getSmtpAuthPwd());
                insertdata.bind(4, sysPrefs.getExternalURL());
                insertdata.step(); //executes the prepared statement, (a row has been inserted)
                System.out.println("\n-- System Preferences updated in database. \n\n");
            }
            db.exec("COMMIT;");

            db.dispose();

        } catch (SQLiteException ex) {
            Logger.getLogger(ServerPrefs.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return rslt;
    }

    /**
     * Updates the internal database with the login date. Used to follow up on clients not using their payroll
     *
     * @param sessionUser
     */
    public void recordLastAccess(SessionUser sessionUser) {
        boolean rslt = false;

        if (sessionUser != null) {
            SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
            try {
                db.open(true);

                // Insert/Update the preferences
                SQLiteStatement insertdata;
                db.exec("BEGIN TRANSACTION;");
                {
                    insertdata = db.prepare("update account set last_access = ? where accID = ?;");
                    insertdata.bind(1, DateUtils.getTimeStamp());
                    insertdata.bind(2, sessionUser.getAccountID());
                    insertdata.step(); //executes the prepared statement, (a row has been updated)
                }
                db.exec("COMMIT;");

                db.dispose();
            } catch (SQLiteException ex) {
                System.err.println(ex.toString());
            }
        }
    }

    /**
     * Loads list of available/configured Sybase databases from the sqlite database
     *
     * @return
     */
    private ArrayList<SvrUserDatabase> loadDatabaseInfoFromDB() {
        ArrayList<SvrUserDatabase> rslt = new ArrayList<>();

        SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);

            // Insert/Update the preferences
            SQLiteStatement retrievedata = db.prepare("select db.dbID, db.db_url, db.jdbc_url, acc.accID "
                    + "from database db left outer join account acc on db.dbID=acc.dbID order by db.dbID;");
            while (retrievedata.step()) {
                SvrUserDatabase aDB = new SvrUserDatabase();
                aDB.setDbID(retrievedata.columnString(0));
                aDB.setDbURL(retrievedata.columnString(1));
                aDB.setJdbcURL(retrievedata.columnString(2));
                aDB.setAccountID(retrievedata.columnString(3));

                rslt.add(aDB);
            }

            db.dispose();

        } catch (SQLiteException ex) {
            Logger.getLogger(ServerPrefs.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return rslt;
    }

    /**
     * Loads the standard (global) list of settings applicable to this server and all the databases it serves.
     *
     * @return
     */
    private SvrGeneralPrefs loadServerSettingsFromDB() {
        SvrGeneralPrefs pref = new SvrGeneralPrefs();

        //
        // Retreive information from table 'global_settings'
        //
        SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);

            // Fetch the preferences
            SQLiteStatement retrievedata = db.prepare("select smtp_server, smtp_port, smtp_auth_user, smtp_auth_pwd, external_url from global_settings;");
            if (retrievedata.step()) {
                pref.setSmtpServer(retrievedata.columnString(0));
                pref.setSmtpPort(retrievedata.columnString(1));
                pref.setSmtpAuthUser(retrievedata.columnString(2));
                pref.setSmtpAuthPwd(retrievedata.columnString(3));
                pref.setExternalURL(retrievedata.columnString(4));
            }
            db.dispose();

        } catch (SQLiteException ex) {
            Logger.getLogger(ServerPrefs.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        //
        // Retreive information from table 'credit_cost'
        //
        db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);

            // Fetch the preferences
            SQLiteStatement retrievedata = db.prepare("select module, unit_cost_in_cents from credit_cost;");
            while (retrievedata.step()) {
                String module = retrievedata.columnString(0);
                int unitCostInCents = retrievedata.columnInt(1);
                if (module.trim().equals(Constants.MODULE_PAYROLL)) {
                    pref.setCreditUnitCost_Payroll(unitCostInCents);
                }
            }
            db.dispose();

        } catch (SQLiteException ex) {
            Logger.getLogger(ServerPrefs.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return pref;
    }

    public static ServerPrefs getInstance() {
        if (serverPrefs == null) {
            serverPrefs = new ServerPrefs();
        }
        return serverPrefs;
    }

    /**
     * For possible debugging purposes, we'd like to record a number of events as and when it occurs
     *
     * @param sessionUser
     * @param message
     * @param pmInstance
     */
    public void logEventToDB(SessionUser sessionUser, String message, PageManager pmInstance) {
        if (Constants.DEBUG) {
            boolean rslt = false;

            if (sessionUser != null) {
                SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
                try {
                    db.open(true);

                    // Insert/Update the preferences
                    SQLiteStatement insertdata;
                    db.exec("BEGIN TRANSACTION;");
                    {
                        insertdata = db.prepare("INSERT INTO log VALUES (?,?,?,?,?);");
                        insertdata.bind(1, DateUtils.getTimeStamp());
                        insertdata.bind(2, sessionUser.hashCode());
                        insertdata.bind(3, pmInstance.hashCode());
                        insertdata.bind(4, sessionUser.toString());
                        insertdata.bind(5, message);
                        insertdata.step(); //executes the prepared statement, (a row has been inserted)
                    }
                    db.exec("COMMIT;");

                    db.dispose();
                } catch (SQLiteException ex) {
                    System.err.println(ex.toString());
                }
            }
        }
    }

    /**
     * All credit purchases through PayGate should be recorded
     *
     * @param responseDate Time that the response was received from PayGate
     * @param reference This should be the same reference that was passed in the request
     * @param status The final status of the transaction.
     * @param resultCode This field contains a code indicating the result of the transaction.
     * @param authCode If the bank approves the credit card transaction, then the authorisation code will be
     * placed in this field. This is the merchants’ guarantee that the funds are available on the customer’s
     * credit card.
     * @param amountInCents This should be the same amount that was passed in the request.
     * @param description This field contains a description for the result of the transaction.
     * @param transactionID This field contains the PayGate unique reference number for the transaction.
     * @param riskIndicator This is a 2-character field containing a risk indicator for this transaction. The
     * first character describes the Verified-by-Visa / MasterCard SecureCode authentication;
     * @param checksum The MD5 hash calculation of all the response fields including the key known only to the
     * merchant and PayGate.
     */
    public void logPurchaseResponseToDB(String responseDate, String reference,
            String status, String resultCode, String authCode, int amountInCents, String description,
            String transactionID, String riskIndicator, String checksum) {

        //
        // First off, we record this transaction into the global sqLite datagbase
        //
        SQLiteConnection db = new SQLiteConnection(new File(System.getProperty("user.dir") + getOSFileSeperator() + Constants.SQLITE_FILE));
        try {
            db.open(true);

            // Insert/Update the preferences
            SQLiteStatement insertdata;
            db.exec("BEGIN TRANSACTION;");
            {

                insertdata = db.prepare("INSERT INTO purchase_response VALUES (?,?,?,?,?,?,?,?,?,?);");
                insertdata.bind(1, responseDate);
                insertdata.bind(2, reference);
                insertdata.bind(3, status);
                insertdata.bind(4, resultCode);
                insertdata.bind(5, authCode);
                insertdata.bind(6, amountInCents);
                insertdata.bind(7, description);
                insertdata.bind(8, transactionID);
                insertdata.bind(9, riskIndicator);
                insertdata.bind(10, checksum);
                insertdata.step(); //executes the prepared statement, (a row has been inserted)
            }
            db.exec("COMMIT;");

            db.dispose();
        } catch (SQLiteException ex) {
            System.err.println(ex.toString());
        }

    }

    /**
     * Returns the Operating System dependent File Separator OS_WINDOWS = "\\" OS_LINUX = "/"
     *
     * @return
     */
    private static String getOSFileSeperator() {
        if (getOS() == OS_WINDOWS) {
            return "\\";
        } else {
            return "/";
        }
    }

    /**
     * Returns an enumerated value for the Operating System currently installed OS_WINDOWS = 0 OS_LINUX = 1
     *
     * @return
     */
    private static int getOS() {
        if (System.getProperty("os.name").toUpperCase().contains("WINDOWS")) {
            return OS_WINDOWS;
        } else {
            return OS_LINUX;
        }
    }

    /**
     * Returns the URL for connecting to this account's database
     *
     * @param accountID
     * @return
     */
    public String getDbURL(String accountID) {
        for (SvrUserAccount prefs : accounts) {
            if (prefs.getAccountID().compareToIgnoreCase(accountID) == 0) {
                String rslt = prefs.getDbURL();
                return rslt;
            }
        }
        return "";
    }

    /**
     * Returns the jdbc URL for Jasper Reports to access this account's database
     *
     * @param accountID
     * @return
     */
    public String getJdbcURL(String accountID) {
        for (SvrUserAccount prefs : accounts) {
            if (prefs.getAccountID().compareToIgnoreCase(accountID) == 0) {
                String rslt = prefs.getJdbcURL();
                return rslt;
            }
        }
        return "";
    }

    /**
     * Returns the Company Name associated with this accountID
     *
     * @param accountID
     * @return
     */
    public String getCompanyName(String accountID) {
        for (SvrUserAccount prefs : accounts) {
            if (prefs.getAccountID().compareToIgnoreCase(accountID) == 0) {
                String rslt = prefs.getCompanyName();
                return rslt;
            }
        }
        return "";
    }

    /**
     * Returns the Firstname + Surname associated with this accountID
     *
     * @param accountID
     * @return
     */
    public String getOperatorName(String accountID) {
        for (SvrUserAccount prefs : accounts) {
            if (prefs.getAccountID().compareToIgnoreCase(accountID) == 0) {
                String rslt = prefs.getOwnerFirstName() + " " + prefs.getOwnerSurname();
                return rslt;
            }
        }
        return "";
    }

    /**
     * Returns the SMTP server for given AccountID
     *
     * @return
     */
    public String getSMTPServer() {
        return systemPrefs.getSmtpServer();
    }

    /**
     * Returns the SMTP port for given AccountID
     *
     * @return
     */
    public String getSMTPPort() {
        return systemPrefs.getSmtpPort();
    }

    /**
     * Returns the SMTP authentication User for given AccountID
     *
     * @return
     */
    public String getSMTPAuthUser() {
        return systemPrefs.getSmtpAuthUser();
    }

    /**
     * Returns the SMTP authentication Password for given AccountID
     *
     * @return
     */
    public String getSMTPAuthPwd() {
        return systemPrefs.getSmtpAuthPwd();
    }

    /**
     * Returns the unit price for a Payroll Credit
     *
     * @return
     */
    public int getCreditUnitCost_Payroll() {
        return systemPrefs.getCreditUnitCost_Payroll();
    }

    /**
     * Returns the External URL for this tomcat server (stored in the global_settings table)
     *
     * @return
     */
    public String getExternalURL() {
        return systemPrefs.getExternalURL();
    }

    /**
     * Returns the SMTP FROM address for given AccountID
     *
     * @param accountID
     * @return
     */
    public String getSMTPFromAddress(String accountID) {
        for (SvrUserAccount prefs : accounts) {
            if (prefs.getAccountID().compareToIgnoreCase(accountID) == 0) {
                String rslt = prefs.getSmtpFrom();
                return rslt;
            }
        }
        return "";
    }

    //
    //
    //      INTERNAL SYSTEM INFO
    //
    //
    /**
     * Returns an ArrayList of all the database preferences
     *
     * @return
     */
    public ArrayList<SvrUserDatabase> getClientDatabases() {
        return ServerPrefs.databases;
    }

    /**
     * Returns an ArrayList of all the account preferences
     *
     * @return
     */
    public ArrayList<SvrUserAccount> getAccounts() {
        return accounts;
    }

    public SvrUserAccount getAccount(String accountID) {
        for (SvrUserAccount account : accounts) {
            if (account.getAccountID().equals(accountID)) {
                return account;
            }
        }
        return null;
    }

    /**
     * Returns the number of available databases that are not already in use
     *
     * @return
     */
    public int getNumberOfUsedDatabases() {
        int rslt = 0;
        for (SvrUserDatabase db : databases) {
            if (db.isInUse()) {
                rslt++;
            }
        }
        return rslt;
    }

    /**
     * Tries to connect to all database and see if one can, at least connect to one of them
     *
     * @return
     */
    public boolean databasesRunning() {
        for (SvrUserAccount acc : accounts) {
            for (SvrUserDatabase db : databases) {
                if (acc.getDbID() != null && db.getDbID() != null) {
                    if (acc.getDbID().equals(db.getDbID())) {
                        // Can we connect?
                        if (db.canConnect()) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Returns the number of databases that can still be allocated to new clients
     *
     * @return
     */
    public int getNumberOfUnusedDatabases() {
        return databases.size() - getNumberOfUsedDatabases();
    }

    private static ServerPrefs serverPrefs;
    private static SvrGeneralPrefs systemPrefs;
    private static ArrayList<SvrUserAccount> accounts;
    private static ArrayList<SvrUserDatabase> databases;

    public static int OS_WINDOWS = 0;
    public static int OS_LINUX = 1;

    private static final String MANDRILL_WELCOME_TEMPLATE = "welcome-to-peopleweb";
    private static final String MANDRILL_WELCOME_SUBJECT = "PeopleWeb - Welcome";
    private static final String MANDRILL_WELCOME_FROMEMAIL = "peopleweb@accsys.co.za";
    private static final String MANDRILL_WELCOME_FROMNAME = "PeopleWeb Team";
}
