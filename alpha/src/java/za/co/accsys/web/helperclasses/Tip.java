/*
 * The purpose of a 'Tip' is to guide the user into what he/she should look out for or address in the software
 */
package za.co.accsys.web.helperclasses;

/**
 *
 * @author lterblanche
 */
public class Tip {
    private int type;
    private String message;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public Tip(int tipType, String tipMessage){
        this.type = tipType;
        this.message = tipMessage;
    }
    
    public static int INFO = 1;
    public static int WARNING = 2;
    public static int ERROR = 3;
    
}
