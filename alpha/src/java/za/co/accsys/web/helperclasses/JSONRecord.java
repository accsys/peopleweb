/*
 * The JSONRecord class represents a single record (row) from the result set of a JSon web service call
 * This is similar to a single row in a relational database table.  
 * I.e. {'Name','Peter'}{'Surname','Smith'}{'Gender','Male'}
 */

package za.co.accsys.web.helperclasses;

import java.util.ArrayList;
import org.apache.http.NameValuePair;

/**
 *
 * @author Liam
 */
public class JSONRecord {
    private final int index;
    private ArrayList<NameValuePair> nameValuePairs;

    public JSONRecord(int index, ArrayList<NameValuePair> nameValuePairs) {
        this.index = index;
        this.nameValuePairs = nameValuePairs;
    }

    /**
     * The index of this record in relation to the complete result set.
     * @return 
     */
    public int getIndex() {
        return index;
    }

    /**
     * Returns the ArrayList of Name|Value pairs (typically a row in a table with the column-name|column-value representation)
     * @return 
     */
    public ArrayList<NameValuePair> getNameValuePairs() {
        return nameValuePairs;
    }

    public void setNameValuePairs(ArrayList<NameValuePair> nameValuePairs) {
        this.nameValuePairs = nameValuePairs;
    }
    
    
}
