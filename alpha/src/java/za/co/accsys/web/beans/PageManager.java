
/*
 * The PageManager is responsible for managing the different sections that has to be infused into the mailPage.xhtml
 * An enumerated type (EnumNavigation) is used by the front-end to indicate what selections has been made.
 */
package za.co.accsys.web.beans;

import za.co.accsys.web.helperclasses.NewAccount;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.context.RequestContext;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.helperclasses.DateUtils;
import za.co.accsys.web.helperclasses.EMail;
import za.co.accsys.web.helperclasses.ListManager;
import za.co.accsys.web.helperclasses.PayGatePurchaseRequest;
import za.co.accsys.web.helperclasses.ServerPrefs;
import za.co.accsys.web.helperclasses.Tip;
import za.co.accsys.web.model.Company;
import za.co.accsys.web.model.Employee;
import za.co.accsys.web.model.PayEntityManager;
import za.co.accsys.web.model.SessionUser;

/**
 *
 * @author lterblanche The PageManager handles the decision making of which
 * *.xhtml page to infuse into the mainPage.xml
 *
 */
public final class PageManager {

    private EnumNavigation selectedAction;
    private SessionUser sessionUser;
    private Company company;
    private PayEntityManager payEntityManager;
    private ListManager listManager;
    private String userName;
    private String password;
    private String accountID;
    private boolean renderWest; // Should the WEST section be displayed or not?
    private boolean renderNorth;
    private String connectionURL;
    private String jdbcURL;
    private boolean changesOnPage = false;
    private boolean includeEmployeeInHeader;
    private String theme;
    private DbConnection jasperConnection;
    private ReportPrinter reportPrinter;
    private int payrollCredits;
    private String appTitle;
    private String appVersion;
    private boolean loadingNewEmployee = false;
    private String passwordResetEmail;
    private String passwordResetAccountID;
    private EMail mailer;
    private ArrayList<Tip> tips;
    private Date auditStartDate = new Date();
    private Date auditEndDate = new Date();
    private NewAccount newAccount;

    public Date getAuditStartDate() {
        return auditStartDate;
    }

    public void setAuditStartDate(Date auditStartDate) {
        this.auditStartDate = auditStartDate;
    }

    public Date getAuditEndDate() {
        return auditEndDate;
    }

    public void setAuditEndDate(Date auditEndDate) {
        this.auditEndDate = auditEndDate;
    }

    /**
     * Generates a Master File Audit Report
     */
    public void printMasterAudit() {
        ReportPrinter printer = new ReportPrinter();
        printer.printMasterAudit(sessionUser, auditStartDate, auditEndDate);
    }

    /**
     * Generates a Credit History Report
     */
    public void printCreditHistory() {
        ReportPrinter printer = new ReportPrinter();
        printer.printCreditHistory(sessionUser);
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isChangesOnPage() {
        return changesOnPage;
    }

    public boolean isIncludeEmployeeInHeader() {
        return ((selectedAction == EnumNavigation.EMPLOYEE_ADDRESSINFO)
                || (selectedAction == EnumNavigation.EMPLOYEE_ACCOUNTINFO)
                || (selectedAction == EnumNavigation.EMPLOYEE_PAYINFO)
                || (selectedAction == EnumNavigation.EMPLOYEE_PERSONALINFO));
    }

    /**
     * An ArrayList of tips to display on the screen
     *
     * @return
     */
    public ArrayList<Tip> getTips() {
        return tips;
    }

    public void addTip(int tipType, String tipMessage) {
        tips.add(new Tip(tipType, tipMessage));
        reloadFullPage();
    }

    private boolean showTips;

    public boolean isShowTips() {
        if (tips == null) {
            return false;
        } else {
            showTips = tips.size() > 0;
            return showTips;
        }
    }

    public boolean isLoadingNewEmployee() {
        return loadingNewEmployee;
    }

    public String getPasswordResetEmail() {
        return passwordResetEmail;
    }

    public void setPasswordResetEmail(String passwordResetEmail) {
        this.passwordResetEmail = passwordResetEmail;
    }

    public String getPasswordResetAccountID() {
        return passwordResetAccountID;
    }

    public void setPasswordResetAccountID(String passwordResetAccountID) {
        this.passwordResetAccountID = passwordResetAccountID;
    }

    public void setLoadingNewEmployee(boolean loadingNewEmployee) {
        this.loadingNewEmployee = loadingNewEmployee;
    }

    public void startLoadingNewEmployee() {
        this.loadingNewEmployee = true;
    }

    /**
     * Returns the AppTitle as stored in the web.xml file
     *
     * @return
     */
    public String getAppTitle() {
        ServletContext servletContext = (ServletContext) FacesContext
                .getCurrentInstance().getExternalContext().getContext();
        appTitle = servletContext.getInitParameter("APP-TITLE");
        return appTitle;
    }

    /**
     * Returns the AppVersion as stored in the web.xml file
     *
     * @return
     */
    public String getAppVersion() {
        ServletContext servletContext = (ServletContext) FacesContext
                .getCurrentInstance().getExternalContext().getContext();
        appVersion = servletContext.getInitParameter("WAR-VERSION");
        return appVersion;
    }

    public ReportPrinter getReportPrinter() {
        return reportPrinter;
    }

    public DbConnection getJasperConnection() {
        return jasperConnection;
    }

    public String getStyleClassForHeader() {
        if (changesOnPage) {
            return "editFormHeaderToolbar";
        } else {
            return "";
        }
    }

    private Locale locale;

    public Locale getLocale() {
        if (sessionUser != null) {
            locale = sessionUser.getUserLocale();
        } else {
            locale = new Locale("en", "");
        }
        return locale;
    }

    /**
     * Returns the given string in a locale-dependent currency format (without
     * the currency symbol)
     *
     * @param aValue
     * @param locale
     * @return
     */
    public static String formatStringAsCurrency(String aValue, Locale locale) {
        if (aValue == null || aValue.trim().length() == 0) {
            aValue = "0";
        }

        NumberFormat formatter = NumberFormat.getInstance(new Locale("en", "US"));
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        formatter.setGroupingUsed(false);
        double f = Double.parseDouble(aValue);
        return formatter.format(f);
    }

    public void setIncludeEmployeeInHeader(boolean includeEmployeeInHeader) {
        this.includeEmployeeInHeader = includeEmployeeInHeader;
    }

    public PayEntityManager getPayEntityManager() {
        return payEntityManager;
    }

    public void setPayEntityManager(PayEntityManager payEntityManager) {
        this.payEntityManager = payEntityManager;
    }

    public void setChangesOnPage(boolean changesOnPage) {
        if (this.changesOnPage != changesOnPage) {
            this.changesOnPage = changesOnPage;
            // Do we need to inform the operator of anything?
        }
        RequestContext.getCurrentInstance().update("frmCentrePage:outerPanel");
    }

    /**
     * Used to send a page reload from anywhere in the model
     */
    public void reloadOuterPanel() {
        RequestContext.getCurrentInstance().update("frmCentrePage:outerPanel");
    }

    /**
     * Used to send a page reload from anywhere in the model
     */
    public void reloadFullPage() {
        RequestContext.getCurrentInstance().update("@all");
    }

    public String getConnectionURL() {
        if (sessionUser == null) {
            return "Not connected";
        } else {
            return ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        }
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }

    private String getSMTPServer() {
        return ServerPrefs.getInstance().getSMTPServer();
    }

    private String getSMTPPort() {
        return ServerPrefs.getInstance().getSMTPPort();
    }

    private String getSMTPAuthUser() {
        return ServerPrefs.getInstance().getSMTPAuthUser();
    }

    private String getSMTPAuthPwd() {
        return ServerPrefs.getInstance().getSMTPAuthPwd();
    }

    private String getSMTPFromAddress(String accountID) {
        return ServerPrefs.getInstance().getSMTPFromAddress(accountID);
    }

    public String getJdbcURL() {
        if (sessionUser == null) {
            return "Not connected";
        } else {
            return ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        }
    }

    /**
     * Returns the number of available credits for Payroll usage.
     *
     * @return
     */
    public int getPayrollCredits() {
        if (this.payrollCredits == 0) {
            reloadPayrollCreditsFromDB();
        }
        return this.payrollCredits;
    }

    private boolean hasMorePayrollCredits;

    public boolean getHasMorePayrollCredits() {
        hasMorePayrollCredits = getPayrollCredits() > 0;
        return hasMorePayrollCredits;
    }

    /**
     * Retrieves the latest credits from the database;
     *
     * @return
     */
    public int reloadPayrollCreditsFromDB() {
        this.payrollCredits = ServerPrefs.getInstance().getAccount(sessionUser.getAccountID()).getPayrollCredits();
        return this.payrollCredits;
    }

    /**
     * Returns true if the number of available credits are less than the
     * threshold warning (constant value)
     *
     * @return
     */
    public boolean getCreditBelowThreshold_Payroll() {
        return getPayrollCredits() < Constants.creditWarningThreshold;
    }

    public void setJdbcURL(String jdbcURL) {
        this.jdbcURL = jdbcURL;
    }

    /**
     * Constructor
     */
    public PageManager() {
        selectedAction = EnumNavigation.LOGIN;
        Calendar c = Calendar.getInstance();
        c.setTime(auditEndDate);
        c.add(Calendar.DATE, -7);
        auditStartDate = c.getTime();
        newAccount = new NewAccount();
    }

    public ListManager getListManager() {
        return listManager;
    }

    public void setListManager(ListManager listManager) {
        this.listManager = listManager;
    }

    /**
     * Connects to the relevant HTTP server and removes the current session_id
     * from WEB_SESSION table
     */
    public void logout() {
        ServerPrefs.getInstance().logEventToDB(sessionUser, "LOG OUT", this);
        if (sessionUser != null) {
            sessionUser.terminateSession();
            sessionUser = null;
        }
        company = null;
        payEntityManager = null;
        listManager = null;
        mailer = null;
        tips = null;

        setSelectedAction(EnumNavigation.LOGIN);
    }

    public String getTheme() {
        if (sessionUser != null) {
            return sessionUser.getTheme();
        } else {
            return ListManager.getDefaultTheme();
        }
    }

    /**
     * Connects to the relevant HTTP server and retrieves a session_id
     */
    public void login() {
        tips = new ArrayList<>();
        WebServiceClient cl = WebServiceClient.getInstance();
        String sessionID = cl.logIn(accountID, userName, password);
        if (sessionID != null) {
            setSelectedAction(EnumNavigation.EMPLOYEE);
            sessionUser = new SessionUser(sessionID, accountID, this);
            company = new Company(sessionUser);
            payEntityManager = new PayEntityManager(sessionUser);
            company.initialiseContainerClasses();
            listManager = new ListManager(sessionUser);
            mailer = new EMail(getSMTPServer(),
                    getSMTPPort(),
                    getSMTPAuthUser(),
                    getSMTPAuthPwd(),
                    getSMTPFromAddress(accountID));
            this.setChangesOnPage(false);
            this.checkStatusForTips();
            // Locate the person that was logged in
            for (Employee employee : company.getEmployeeManager().getAllEmployees()) {
                if (employee.isAdministrator() && employee.getAdminLogin().equalsIgnoreCase(userName)) {
                    company.getEmployeeManager().setSelectedEmployee(employee);
                }
            }

            ServerPrefs.getInstance().logEventToDB(sessionUser, "LOG IN", this);
            ServerPrefs.getInstance().recordLastAccess(sessionUser);

        } else {
            sessionUser = null;
            notifyUser(FacesMessage.SEVERITY_WARN, "Invalid Login!", "Please Try Again!");
        }

    }

    /**
     * This routine is used to run a full diagnostics and inform the user of
     * certain scenarios he/she needs to be aware of
     */
    public void checkStatusForTips() {
        if (tips == null || company == null) {
            return;
        }

        tips.clear();
        if (company.getEmployeeManager() != null) {
            // 
            // USAGE TIP: Are there any employees loaded?
            // 
            if (company.getEmployeeManager().getAllEmployees().size() <= 1) {
                addTip(Tip.INFO, translate("tooltipAddMoreEmployees"));
            }
            // 
            // USAGE TIP: Are there any packages loaded for staff?
            // 
            int nbUninitiatedStaff = 0;
            for (Employee employee : company.getEmployeeManager().getActiveEmployees()) {
                if (employee.getPayEntityInstances().isEmpty()) {
                    nbUninitiatedStaff++;
                }
            }
            if (nbUninitiatedStaff > 0) {
                addTip(Tip.WARNING, translate("tooltipMissingEarningsAndDeductions"));
            }

            // 
            // USAGE TIP: Are there any problems with the calculations?
            // 
            for (Employee employee : company.getEmployeeManager().getActiveEmployees()) {
                if (employee.getCalcDebugReport().length > 0) {
                    addTip(Tip.ERROR, translate("tooltipInvestigateInconsistenciesFor", employee));
                }
            }
            // 
            // USAGE TIP: Are there any missing Rates Of Pay?
            // 
            for (Employee employee : company.getEmployeeManager().getActiveEmployees()) {
                try {
                    NumberFormat nf = NumberFormat.getNumberInstance(company.getSessionUser().getUserLocale());
                    if (nf.parse(employee.getRateOfPay()).floatValue() == 0.0) {
                        addTip(Tip.ERROR, translate("tooltipEmployeeHasZeroRateOfPay", employee));
                    }
                } catch (ParseException ex) {
                    addTip(Tip.ERROR, translate("tooltipEmployeeHasZeroRateOfPay", employee));
                }
            }

            // 
            // USAGE TIP: Are there missing company information?
            // 
            if (company.getRegistrationNo().trim().length() == 0) {
                addTip(Tip.WARNING, translate("tooltipMissingInfoInCompanySettings"));
            }

            //
            // USAGE TIP: Duplicate ID Numbers?
            //
            for (Employee emp1 : sessionUser.getPageManager().getCompany().getEmployeeManager().getAllEmployees()) {
                for (Employee emp2 : sessionUser.getPageManager().getCompany().getEmployeeManager().getAllEmployees()) {
                    if (emp1.getIdNumber() != null && emp2.getIdNumber() != null) {
                        if (!emp1.equals(emp2) && emp1.getIdNumber().trim().equals(emp2.getIdNumber().trim())) {
                            addTip(Tip.WARNING, emp1.getFirstname() + " " + emp1.getSurname() + ": " + translate("tooltipDuplicateIDNumber", emp2));
                        }
                    }
                }
            }

            //
            // USAGE TIP: Low credit count
            //
            if (getCreditBelowThreshold_Payroll() && getPayrollCredits() > 0) {
                addTip(Tip.WARNING, translate("tooltipPayrollCreditsRunningLow"));
            }

            //
            // USAGE TIP: Zero credit count
            //
            if (getPayrollCredits() <= 0) {
                addTip(Tip.ERROR, translate("tooltipPayrollCreditsDepleted"));
            }

        }
        reloadFullPage();
    }

    /**
     * Redirects to the Password Reset screen
     */
    public void navigateToPasswordReset() {
        setSelectedAction(EnumNavigation.PASSWORD_RESET);
    }

    /**
     * Redirects to the Password Reset screen
     */
    public void navigateToRegisterAccount() {
        setSelectedAction(EnumNavigation.ACCOUNT_REGISTER);
    }

    /**
     * Redirects to the
     */
    public void navigateToPayCycles() {
        setSelectedAction(EnumNavigation.PAY_CYCLE);
    }

    public void navigateToPayslipScreen() {
        setSelectedAction(EnumNavigation.EMPLOYEE_PAYINFO);
    }

    public void navigateToGeneralInfoScreen() {
        setSelectedAction(EnumNavigation.EMPLOYEE_PERSONALINFO);
    }

    public void navigateToLoginScreen() {
        setSelectedAction(EnumNavigation.LOGIN);
    }

    public void navigateToLogoutScreen() {
        setSelectedAction(EnumNavigation.LOGOUT);
    }
    
    public void navigateToPayEntityScreen() {
        setSelectedAction(EnumNavigation.SETTINGS_PAYROLLPAYENTITY);
    }

    /**
     * Calls a web service to reset the user's PeopleWare password.
     *
     * @param resetAccountID
     * @param resetEmail
     * @return login_name + '*' + new_password
     */
    private String getResetUsernameAndNewPassword(String resetAccountID, String resetEmail) {
        String nameAndNewPwd = "";

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair("adminEmail", resetEmail));

        // Call the GetValue service
        ArrayList<NameValuePair> resetAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(resetAccountID);
        resetAttributes = WebServiceClient.getInstance().getOneLineResponse(null, URL, Constants.serviceName_ResetAdminPassword, listToSend);

        // Note: This web service call returns the first person that is
        // a. An Administrator (record in al_emaster_agroup)
        // b. In this server (resetAccountID)
        // c. Using this email address (resetEmail)
        //
        // The result is a single string, comprising login_name + '*' + new_password
        // What did we get back?
        for (NameValuePair nvp : resetAttributes) {

            if (nvp.getName().equalsIgnoreCase("resetResult")) {
                nameAndNewPwd = (nvp.getValue());
            }
        }
        return nameAndNewPwd;
    }

    /**
     * Displays a message
     *
     * @param severity
     * @param summary
     * @param detail
     */
    public void notifyUser(Severity severity, String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(
                "", new FacesMessage(severity,
                        summary, detail));
        reloadOuterPanel();
        //RequestContext.getCurrentInstance().update("frmCentrePage:outerPanel");
    }

    /**
     * This is where we will create a random username and password and email it
     * to the user
     */
    public void processPasswordReset() {
        // Reset password
        String nameAndNewPwd = getResetUsernameAndNewPassword(passwordResetAccountID, passwordResetEmail);
        if (nameAndNewPwd.equalsIgnoreCase(Constants.PASSWORD_RESET_EMAILNOTEXISTS)) {
            // Notify user
            notifyUser(FacesMessage.SEVERITY_ERROR, "No user with this email was found.  Unable to reset your password.", null);
            // Navigate to Login Screen
            setSelectedAction(EnumNavigation.PASSWORD_RESET);
        } else {// Do we have a valid firstname/loginname/password pair?
            if (nameAndNewPwd.contains("*")) {
                // Now that we have the name/password pair
                StringTokenizer token = new StringTokenizer(nameAndNewPwd, "*");

                String firstName = token.nextToken();
                String loginName = token.nextToken();
                String newPassword = token.nextToken();

                // Create Email instance
                EMail passwordResetMailer = new EMail(getSMTPServer(), getSMTPPort(), getSMTPAuthUser(), getSMTPAuthPwd(), getSMTPFromAddress(passwordResetAccountID));
                // The password has been changed.  We now need to notify the user
                String htmlMessage = Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE;
                // Replace the sections
                htmlMessage = htmlMessage.replace(Constants.EMAIL_RPL_FIRSTNAME, firstName);
                htmlMessage = htmlMessage.replace(Constants.EMAIL_RPL_LOGINNAME, loginName);
                htmlMessage = htmlMessage.replace(Constants.EMAIL_RPL_PASSWORD, newPassword);
                // Now for the language-dependent sections
                htmlMessage = htmlMessage.replace(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_PWDHASCHANGED, translate(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_PWDHASCHANGED));
                htmlMessage = htmlMessage.replace(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_YOURNEWPWD, translate(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_YOURNEWPWD));
                htmlMessage = htmlMessage.replace(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_YOURLOGINNAME, translate(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_YOURLOGINNAME));
                htmlMessage = htmlMessage.replace(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_PLEASECHANGE, translate(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_PLEASECHANGE));
                htmlMessage = htmlMessage.replace(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_REGARDS, translate(Constants.EMAIL_PASSWORD_RESET_HTML_MESSAGE_REGARDS));

                if (passwordResetMailer.send(passwordResetEmail, Constants.EMAIL_CCLIST, "PeopleWeb Password Reset", htmlMessage)) {
                    // Notify user
                    notifyUser(FacesMessage.SEVERITY_INFO, translate(Constants.EMAIL_PASSWORD_RESET_DISPLAY_CONFIRMATION), null);
                } else {
                    notifyUser(FacesMessage.SEVERITY_ERROR, Constants.EMAIL_ERROR, null);
                }

                // Navigate to Login Screen
                setSelectedAction(EnumNavigation.LOGIN);
            }
        }
    }

    public void onIdle() {
        if (sessionUser != null) {
            // Notify user
            notifyUser(FacesMessage.SEVERITY_WARN, "Your session has expired.", " Please log in again.");
            // Undo any changes currently being processed.
            this.cancelChanges();

            System.out.println(DateUtils.getTimeStamp() + " *** On Idle event - logout for user:" + sessionUser.getSurname());
            logout();
        }
    }


    /* **************************************
     * GETTERS AND SETTERS 
     * **************************************/
    /**
     * Is this person still logged in
     *
     * @return
     */
    public boolean isLoggedIn() {
        if (sessionUser == null) {
            return false;
        }

        // If the server timed-out but the front-end is still logged in, do something about it.
        if (sessionUser.isSessionExpired()) {
            sessionUser = null;
            setSelectedAction(EnumNavigation.LOGIN);

            // Notify user
            notifyUser(FacesMessage.SEVERITY_INFO, "Your session has timed-out.", " Please log in again.");

            RequestContext.getCurrentInstance().update("body");
            return false;
        }
        return true;
    }

    /**
     * Internal identified that ties a user to an Accsys Client Account. For
     * each account, a different URL can be set up, using the settings.xml file
     * in the CATALINA_HOME directory
     *
     * @return
     */
    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    /**
     * Setter for the selection of which page to work on next
     *
     * @param pageAction
     */
    private void setSelectedAction(EnumNavigation pageAction) {
        ServerPrefs.getInstance().logEventToDB(sessionUser, "Navigate to: " + pageAction.getCaption(), this);
        this.selectedAction = pageAction;
        // Log Out
        if (pageAction == EnumNavigation.LOGOUT) {
            logout();
        }
        // We need to know if we're in the process of loading a new employee
        if (pageAction == EnumNavigation.EMPLOYEE_NEW) {
            setLoadingNewEmployee(true);
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String uname) {
        this.userName = uname;
    }

    public SessionUser getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
    }

    /**
     * When changing a person's name, surname, or number, the employee filter
     * display should be updated too
     */
    private void refreshEmployeeFilterContent(Employee employee) {
        // Reset filter list of employees on display
        getCompany().getEmployeeManager().completeEmployeeSelection(employee.getToString());
        getCompany().getEmployeeManager().setTxtSelectedEmployee(employee.getToString());
    }

    // Saves the appropriate data
    public void saveChanges() {
        ServerPrefs.getInstance().logEventToDB(sessionUser, "Save changes: " + selectedAction.getCaption(), this);
        switch (selectedAction) {
            case EMPLOYEE_ACCOUNTINFO:
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeeAccountData();
                break;
            case EMPLOYEE_ADDRESSINFO:
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeeAddressData();
                break;
            case EMPLOYEE_PERSONALINFO:
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeePersonalData();
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeePayrollInfo();
                refreshEmployeeFilterContent(getCompany().getEmployeeManager().getSelectedEmployee());
                // Changes here may affect other metrics too
                company.getPayrollManager().reloadPayrollsStatus();
                RequestContext.getCurrentInstance().update("@all");
                break;
            case EMPLOYEE_PAYINFO:
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeePayPackage();
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeePayrollInfo();
                break;
            case EMPLOYEE_NEW:
                setLoadingNewEmployee(false);
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeePersonalData();
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeePayrollInfo();
                getCompany().getEmployeeManager().getSelectedEmployee().saveEmployeeAddressData();

                refreshEmployeeFilterContent(getCompany().getEmployeeManager().getSelectedEmployee());
                // Reload his/her payslip info
                getCompany().getEmployeeManager().getSelectedEmployee().quickLoadPayslipData();
                // Navigate to General Info 
                setSelectedAction(EnumNavigation.EMPLOYEE_PERSONALINFO);
                RequestContext.getCurrentInstance().update("ltuSouth");
                break;
            case SETTINGS_COMPANYGENERAL:
                getCompany().saveCompanyInfo();
                break;
            case SETTINGS_COMPANYCOSTCENTRE:
                getCompany().getCostCentreManager().saveCostCentres();
                break;
            case SETTINGS_PAYROLLGENERAL:
                getCompany().getPayrollManager().savePayrolls();
                break;
            case SETTINGS_COMPANYACCOUNT:
                getCompany().saveCompanyAccountData();
                break;
            case SETTINGS_PAYROLLPAYENTITY:
                getCompany().getPayEntityManager().saveSelectedPayEntity();
                getCompany().getPayEntityManager().loadPayEntities();
                break;
            case SETTINGS_LISTSMANAGEMENT:
                break;
            case USER_PREFERENCES:
                // Assign the current locale to the session user
                getSessionUser().saveSessionUser();
                // Log out of PeopleWeb
                navigateToLogoutScreen();
                break;
            case ACCOUNT_REGISTER:
                processNewAccountCreation();
        }
        this.setChangesOnPage(false);
        checkStatusForTips();
    }

    public NewAccount getNewAccount() {
        return newAccount;
    }

    public void setNewAccount(NewAccount newAccount) {
        this.newAccount = newAccount;
    }

    /**
     * The constructor already created an instance of NewAccount. We now use
     * that bean to create the actual PeopleWeb account. Once completed, we'll
     * create a brand new instance for the next time.
     */
    private void processNewAccountCreation() {
        // Has this person's email address already been used?
        if (ServerPrefs.getInstance().accountEmailUnique(newAccount.getUserEmail())) {
            if (!ServerPrefs.getInstance().registerNewAccount(newAccount)) {
                notifyUser(FacesMessage.SEVERITY_WARN, "Unable to create a new account.", "Please check the system log for more detail.");
            } else {
                // Default to the new account ID
                setAccountID(newAccount.getAccountID());
                notifyUser(FacesMessage.SEVERITY_INFO, "Welcome to Accsys PeopleWeb. You can now proceed to log in.", "Please check the system log for more detail.");
            }
            navigateToLoginScreen();
            newAccount = new NewAccount();
        } else {
            notifyUser(FacesMessage.SEVERITY_WARN, "Unable to create a new account.  This email address has already been used.", "This email address has already been used.");
            navigateToLogoutScreen();
        }
    }

    private boolean canCreateNewAccounts;

    /**
     * Returns true if there are unused PeopleWeb databases that we can allocate
     * to new clients.
     *
     * @return
     */
    public boolean isCanCreateNewAccounts() {
        canCreateNewAccounts = (ServerPrefs.getInstance().getNumberOfUnusedDatabases() > 0);
        return canCreateNewAccounts;
    }

    private boolean databasesRunning;

    public boolean isDatabasesRunning() {
        return (ServerPrefs.getInstance().databasesRunning());
    }

    /**
     * Used to retrieve the appropriate LOCALE string for the given key
     *
     * @param dictionaryKey
     * @return
     */
    public static String translate(String dictionaryKey) {
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);
        String rslt = messages.getString(dictionaryKey);

        return rslt;
    }

    /**
     * Used to retrieve the appropriate LOCALE string for the given key
     *
     * @param dictionaryKey
     * @param employee Optional attribute to allow for in-string replacements.
     * Where the translation string contains key words, i.e. #FIRSTNAME#
     * @return
     */
    public static String translate(String dictionaryKey, Employee employee) {
        String rslt = translate(dictionaryKey);

        // Any in-string replacements?
        if (employee != null) {
            rslt = rslt.replace("#FIRSTNAME#", employee.getFirstname().trim());
            rslt = rslt.replace("#SURNAME#", employee.getSurname().trim());
        }
        return rslt;
    }

    /**
     * Used to retrieve the appropriate translation for the given key and locale
     *
     * @param dictionaryKey
     * @param locale
     * @return
     */
    private static String translate(String dictionaryKey, Locale locale) {
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);
        String rslt = messages.getString(dictionaryKey);
        return rslt;
    }

    /**
     * Finds the key in the dictionary for this given word, according to the
     * current locale. The routine then looks up the English version for that
     * word
     *
     * @param wordInLocale the word that we need to find the original key for
     * @return
     */
    public static String translateFromLocaleToEnglish(String wordInLocale) {
        if (wordInLocale == null) {
            return null;
        }
        Locale lClientLanguage = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;
        String wordKey = null;

        // Find the key for wordInLocale
        messages = ResourceBundle.getBundle("resources.messages", lClientLanguage);
        Enumeration enumKeys = messages.getKeys();
        while (enumKeys.hasMoreElements()) {
            String key = (String) enumKeys.nextElement();
            // Now that we have a key, check to see if this word is associated with it
            String lookup = messages.getString(key);
            if (lookup.equalsIgnoreCase(wordInLocale)) {
                // This is the key
                wordKey = key;
                break;
            }
        }

        // Now find the English version for it
        if (wordKey != null) {
            Locale lEnglish = new Locale("en");
            return translate(wordKey, lEnglish);
        } else {
            // Unable to translate.  Use the original word
            return wordInLocale;
        }
    }

    /**
     * Finds the key in the dictionary for this English word, according to the
     * current locale. The routine then looks up the current locale and
     * translates that word accordingly
     *
     * @param wordInEnglish the word that we need to find the original key for
     * @return
     */
    public static String translateFromEnglishToLocale(String wordInEnglish) {
        if (wordInEnglish == null) {
            return null;
        }

        Locale lEnglish = new Locale("en");
        ResourceBundle messages;
        String wordKey = null;
        // Find the key for wordInLocale
        messages = ResourceBundle.getBundle("resources.messages", lEnglish);
        Enumeration enumKeys = messages.getKeys();
        while (enumKeys.hasMoreElements()) {
            String key = (String) enumKeys.nextElement();
            // Now that we have a key, check to see if this word is associated with it
            String lookup = messages.getString(key);
            if (lookup.equalsIgnoreCase(wordInEnglish)) {
                // This is the key
                wordKey = key;
                break;
            }
        }

        if (wordKey != null) {
            // Now find the English version for it
            Locale lClientLanguage = FacesContext.getCurrentInstance().getViewRoot().getLocale();
            return translate(wordKey, lClientLanguage);
        } else {
            // Unable to translate.  Use the original word
            return wordInEnglish;
        }
    }

    /**
     * Aborts all recent changes by reloading the original content from the
     * database.
     */
    public void cancelChanges() {
        ServerPrefs.getInstance().logEventToDB(sessionUser, "Cancel Changes: " + selectedAction.getCaption(), this);
        switch (selectedAction) {
            case EMPLOYEE_ACCOUNTINFO:
                getCompany().getEmployeeManager().getSelectedEmployee().loadEmployeeAccountData();
                break;
            case EMPLOYEE_ADDRESSINFO:
                getCompany().getEmployeeManager().getSelectedEmployee().loadEmployeeAddressData();
                break;
            case EMPLOYEE_PAYINFO:
                getCompany().getEmployeeManager().getSelectedEmployee().loadEmployeePayPackage();
                getCompany().getEmployeeManager().getSelectedEmployee().loadEmployeePayrollInfo();
                break;
            case EMPLOYEE_PERSONALINFO:
                getCompany().getEmployeeManager().getSelectedEmployee().loadEmployeePersonalData();
                break;
            case EMPLOYEE_NEW:
                setLoadingNewEmployee(false);

                // Delete the newly created employee from the list
                getCompany().getEmployeeManager().getEmployees().remove(getCompany().getEmployeeManager().getSelectedEmployee());

                // Select the previously selected employee
                if (getCompany().getEmployeeManager().getPreviouslySelectedEmployee() != null) {
                    getCompany().getEmployeeManager().setSelectedEmployee(getCompany().getEmployeeManager().getPreviouslySelectedEmployee());
                }

                // Reset filter list of employees on display
                getCompany().getEmployeeManager().completeEmployeeSelection("");

                setSelectedAction(EnumNavigation.EMPLOYEE);

                break;
            case SETTINGS_COMPANYGENERAL:
                getCompany().saveCompanyInfo();
                break;
            case SETTINGS_COMPANYCOSTCENTRE:
                getCompany().getCostCentreManager().loadCostCentres();
                break;
            case SETTINGS_PAYROLLGENERAL:
                getCompany().getPayrollManager().loadPayrolls();
                break;
            case SETTINGS_COMPANYACCOUNT:
                getCompany().loadCompanyAccountData();
                break;
            case SETTINGS_PAYROLLPAYENTITY:
                getCompany().getPayEntityManager().loadPayEntities();
                break;
            case SETTINGS_LISTSMANAGEMENT:
                getCompany().getJobPositionManager().loadJobPositions();
                getCompany().getTitleManager().loadTitles();
                getCompany().getLanguageManager().loadLanguages();
                break;
            case USER_PREFERENCES:
                // Assign the current locale to the session user
                getSessionUser().loadSessionUser();
                break;
            case ACCOUNT_REGISTER:
                navigateToLogoutScreen();
                break;
        }
        this.setChangesOnPage(false);
        checkStatusForTips();
    }

    /* ******************************************************************
     *
     * PAGE NAVIGATION LOGIC
     *
     * 
     ** *******************************************************************/
    private EnumNavigation navEmployee = EnumNavigation.EMPLOYEE;

    public EnumNavigation getNavEmployeePersonalInfo() {
        return navEmployeePersonalInfo;
    }

    public void setNavEmployeePersonalInfo(EnumNavigation navEmployeePersonalInfo) {
        this.navEmployeePersonalInfo = navEmployeePersonalInfo;
    }

    public EnumNavigation getNavLogin() {
        return navLogin;
    }

    public void setNavLogin(EnumNavigation navLogin) {
        this.navLogin = navLogin;
    }

    public EnumNavigation getNavLogout() {
        return navLogout;
    }

    public EnumNavigation getNavPasswordReset() {
        return navPasswordReset;
    }

    public void setNavPasswordReset(EnumNavigation navPasswordReset) {
        this.navPasswordReset = navPasswordReset;
    }

    public EnumNavigation getNavAccountRegister() {
        return navAccountRegister;
    }

    public void setNavAccountRegister(EnumNavigation navAccountRegister) {
        this.navAccountRegister = navAccountRegister;
    }

    public void setNavLogout(EnumNavigation navLogout) {
        this.navLogout = navLogout;
    }

    public EnumNavigation getNavPayCycle() {
        return navPayCycle;
    }

    public void setNavPayCycle(EnumNavigation navPayCycle) {
        this.navPayCycle = navPayCycle;
    }

    public EnumNavigation getNavReporting() {
        return navReporting;
    }

    public void setNavReporting(EnumNavigation navReporting) {
        this.navReporting = navReporting;
    }

    public EnumNavigation getNavSettings() {
        return navSettings;
    }

    public void setNavSettings(EnumNavigation navSettings) {
        this.navSettings = navSettings;
    }

    public EnumNavigation getNavPreferences() {
        return navPreferences;
    }

    public EnumNavigation getNavEmployeeNew() {
        return navEmployeeNew;
    }

    public void setNavEmployeeNew(EnumNavigation navEmployeeNew) {
        this.navEmployeeNew = navEmployeeNew;
    }

    public void setNavPreferences(EnumNavigation navPreferences) {
        this.navPreferences = navPreferences;
    }
    private EnumNavigation navLogin = EnumNavigation.LOGIN;
    private EnumNavigation navLogout = EnumNavigation.LOGOUT;
    private EnumNavigation navPayCycle = EnumNavigation.PAY_CYCLE;
    private EnumNavigation navReporting = EnumNavigation.REPORTING;
    private EnumNavigation navSettings = EnumNavigation.SETTINGS;
    private EnumNavigation navPreferences = EnumNavigation.USER_PREFERENCES;
    private EnumNavigation navPasswordReset = EnumNavigation.PASSWORD_RESET;
    private EnumNavigation navAccountRegister = EnumNavigation.ACCOUNT_REGISTER;
    /* Employee Navigation */
    private EnumNavigation navEmployeeNew = EnumNavigation.EMPLOYEE_NEW;
    private EnumNavigation navEmployeePersonalInfo = EnumNavigation.EMPLOYEE_PERSONALINFO;
    private EnumNavigation navEmployeeAddressInfo = EnumNavigation.EMPLOYEE_ADDRESSINFO;
    private EnumNavigation navEmployeeAccountInfo = EnumNavigation.EMPLOYEE_ACCOUNTINFO;
    private EnumNavigation navEmployeePayInfo = EnumNavigation.EMPLOYEE_PAYINFO;
    /* Settings Navigation */
    private EnumNavigation navSettingsServerInfo = EnumNavigation.SETTINGS_SERVER;
    private EnumNavigation navSettingsCompanyGeneralInfo = EnumNavigation.SETTINGS_COMPANYGENERAL;
    private EnumNavigation navSettingsCompanyAddressInfo = EnumNavigation.SETTINGS_COMPANYADDRESS;
    private EnumNavigation navSettingsCompanyAccountInfo = EnumNavigation.SETTINGS_COMPANYACCOUNT;
    private EnumNavigation navSettingsCompanyCostCentreInfo = EnumNavigation.SETTINGS_COMPANYCOSTCENTRE;
    private EnumNavigation navSettingsPayrollGeneralInfo = EnumNavigation.SETTINGS_PAYROLLGENERAL;
    private EnumNavigation navSettingsPayrollPayEntityInfo = EnumNavigation.SETTINGS_PAYROLLPAYENTITY;
    private EnumNavigation navSettingsListsManagement = EnumNavigation.SETTINGS_LISTSMANAGEMENT;
    /* Reporting and Extract Navigation */
    private EnumNavigation navReportMasterAudit = EnumNavigation.REPORTS_MASTERAUDIT;
    private EnumNavigation navReportCreditHistory = EnumNavigation.REPORTS_CREDITHISTORY;
    private EnumNavigation navReportsEmployeePayslip = EnumNavigation.REPORTS_EMPLOYEEPAYSLIP;
    private EnumNavigation navReportsEmployeeTaxCertificate = EnumNavigation.REPORTS_EMPLOYEETAXCERTIFICATE;
    private EnumNavigation navReportsEmployeeEMP201 = EnumNavigation.REPORTS_EMPLOYEEEMP201;
    private EnumNavigation navReportsUifExtract = EnumNavigation.REPORTS_UIFEXTRACT;
    private EnumNavigation navReportsPayrollTaxReturn = EnumNavigation.REPORTS_PAYROLLTAXRETURN;
    private EnumNavigation navReportsPayrollRegister = EnumNavigation.REPORTS_PAYROLLREGISTER;
    private EnumNavigation navReportsPayrollRegisterYTD = EnumNavigation.REPORTS_PAYROLLREGISTERYTD;
    private EnumNavigation navReportsPayrollSDL = EnumNavigation.REPORTS_PAYROLLSDL;
    private EnumNavigation navReportsPayrollVariance = EnumNavigation.REPORTS_PAYROLLVARIANCE;
    private EnumNavigation navReportsPayrollTaxcertificate = EnumNavigation.REPORTS_PAYROLLTAXCERTIFICATE;
    private EnumNavigation navExtractHeadcountAndStats = EnumNavigation.EXTRACTS_HEADCOUNTANDSTATS;
    private EnumNavigation navExtractTakeonAndDischarges = EnumNavigation.EXTRACTS_TAKEONANDDISCHARGES;
    private EnumNavigation navExtractAllEmployeeAttributes = EnumNavigation.EXTRACTS_ALLEMPLOYEEATTRIBUTES;
    private EnumNavigation navExtractAllEmployeeExceptions = EnumNavigation.EXTRACTS_ALLEMPLOYEEEXCEPTIONS;
    private EnumNavigation navExtractEmployeeVarDetail = EnumNavigation.EXTRACTS_EMPLOYEEVARDETAIL;

    public EnumNavigation getNavSettingsCompanyGeneralInfo() {
        return navSettingsCompanyGeneralInfo;
    }

    public void setNavSettingsCompanyGeneralInfo(EnumNavigation navSettingsCompanyGeneralInfo) {
        this.navSettingsCompanyGeneralInfo = navSettingsCompanyGeneralInfo;
    }

    public EnumNavigation getNavSettingsCompanyAddressInfo() {
        return navSettingsCompanyAddressInfo;
    }

    public EnumNavigation getNavReportsPayrollRegister() {
        return navReportsPayrollRegister;
    }

    public void setNavReportsPayrollRegister(EnumNavigation navReportsPayrollRegister) {
        this.navReportsPayrollRegister = navReportsPayrollRegister;
    }

    public void setNavSettingsCompanyAddressInfo(EnumNavigation navSettingsCompanyAddressInfo) {
        this.navSettingsCompanyAddressInfo = navSettingsCompanyAddressInfo;
    }

    public EnumNavigation getNavReportsPayrollSDL() {
        return navReportsPayrollSDL;
    }

    public void setNavReportsPayrollSDL(EnumNavigation navReportsPayrollSDL) {
        this.navReportsPayrollSDL = navReportsPayrollSDL;
    }

    public EnumNavigation getNavReportsPayrollVariance() {
        return navReportsPayrollVariance;
    }

    public void setNavReportsPayrollVariance(EnumNavigation navReportsPayrollVariance) {
        this.navReportsPayrollVariance = navReportsPayrollVariance;
    }

    public EnumNavigation getNavReportsPayrollTaxcertificate() {
        return navReportsPayrollTaxcertificate;
    }

    public void setNavReportsPayrollTaxcertificate(EnumNavigation navReportsPayrollTaxcertificate) {
        this.navReportsPayrollTaxcertificate = navReportsPayrollTaxcertificate;
    }

    public EnumNavigation getNavReportsPayrollRegisterYTD() {
        return navReportsPayrollRegisterYTD;
    }

    public void setNavReportsPayrollRegisterYTD(EnumNavigation navReportsPayrollRegisterYTD) {
        this.navReportsPayrollRegisterYTD = navReportsPayrollRegisterYTD;
    }

    public EnumNavigation getNavExtractAllEmployeeAttributes() {
        return navExtractAllEmployeeAttributes;
    }

    public void setNavExtractAllEmployeeAttributes(EnumNavigation navExtractAllEmployeeAttributes) {
        this.navExtractAllEmployeeAttributes = navExtractAllEmployeeAttributes;
    }

    public EnumNavigation getNavExtractAllEmployeeExceptions() {
        return navExtractAllEmployeeExceptions;
    }

    public void setNavExtractAllEmployeeExceptions(EnumNavigation navExtractAllEmployeeExceptions) {
        this.navExtractAllEmployeeExceptions = navExtractAllEmployeeExceptions;
    }

    public EnumNavigation getNavReportsUifExtract() {
        return navReportsUifExtract;
    }

    public void setNavReportsUifExtract(EnumNavigation navReportsUifExtracts) {
        this.navReportsUifExtract = navReportsUifExtracts;
    }

    public EnumNavigation getNavSettingsCompanyAccountInfo() {
        return navSettingsCompanyAccountInfo;
    }

    public void setNavSettingsCompanyAccountInfo(EnumNavigation navSettingsCompanyAccountInfo) {
        this.navSettingsCompanyAccountInfo = navSettingsCompanyAccountInfo;
    }

    public EnumNavigation getNavSettingsCompanyCostCentreInfo() {
        return navSettingsCompanyCostCentreInfo;
    }

    public void setNavSettingsCompanyCostCentreInfo(EnumNavigation navSettingsCompanyCostCentreInfo) {
        this.navSettingsCompanyCostCentreInfo = navSettingsCompanyCostCentreInfo;
    }

    public EnumNavigation getNavSettingsPayrollGeneralInfo() {
        return navSettingsPayrollGeneralInfo;
    }

    public EnumNavigation getNavExtractEmployeeVarDetail() {
        return navExtractEmployeeVarDetail;
    }

    public void setNavExtractEmployeeVarDetail(EnumNavigation navExtractEmployeeVarDetail) {
        this.navExtractEmployeeVarDetail = navExtractEmployeeVarDetail;
    }

    public void setNavSettingsPayrollGeneralInfo(EnumNavigation navSettingsPayrollGeneralInfo) {
        this.navSettingsPayrollGeneralInfo = navSettingsPayrollGeneralInfo;
    }

    public EnumNavigation getNavSettingsPayrollPayEntityInfo() {
        return navSettingsPayrollPayEntityInfo;
    }

    public void setPayrollCredits(int payrollCredits) {
        this.payrollCredits = payrollCredits;
    }

    public EnumNavigation getNavReportsPayrollTaxReturn() {
        return navReportsPayrollTaxReturn;
    }

    public void setNavReportsPayrollTaxReturn(EnumNavigation navReportsPayrollTaxReturn) {
        this.navReportsPayrollTaxReturn = navReportsPayrollTaxReturn;
    }

    public EnumNavigation getNavSettingsListsManagement() {
        return navSettingsListsManagement;
    }

    public void setNavSettingsListsManagement(EnumNavigation navSettingsListsManagement) {
        this.navSettingsListsManagement = navSettingsListsManagement;
    }

    public void setNavSettingsPayrollPayEntityInfo(EnumNavigation navSettingsPayrollPayEntityInfo) {
        this.navSettingsPayrollPayEntityInfo = navSettingsPayrollPayEntityInfo;
    }

    public EnumNavigation getNavSettingsServerInfo() {
        return navSettingsServerInfo;
    }

    public void setNavSettingsServerInfo(EnumNavigation navSettingsServerInfo) {
        this.navSettingsServerInfo = navSettingsServerInfo;
    }

    public EnumNavigation getNavEmployeeAccountInfo() {
        return navEmployeeAccountInfo;
    }

    public void setNavEmployeeAccountInfo(EnumNavigation navEmployeeAccountInfo) {
        this.navEmployeeAccountInfo = navEmployeeAccountInfo;
    }

    public EnumNavigation getNavEmployeeAddressInfo() {
        return navEmployeeAddressInfo;
    }

    public void setNavEmployeeAddressInfo(EnumNavigation navEmployeeAddressInfo) {
        this.navEmployeeAddressInfo = navEmployeeAddressInfo;
    }

    public EnumNavigation getNavEmployee() {
        return navEmployee;
    }

    public void setNavEmployee(EnumNavigation navEmployee) {
        this.navEmployee = navEmployee;
    }

    public EnumNavigation getNavEmployeePayInfo() {
        return navEmployeePayInfo;
    }

    public void setNavEmployeePayInfo(EnumNavigation navEmployeePayInfo) {
        this.navEmployeePayInfo = navEmployeePayInfo;
    }

    public EnumNavigation getNavReportMasterAudit() {
        return navReportMasterAudit;
    }

    public EnumNavigation getNavReportCreditHistory() {
        return navReportCreditHistory;
    }

    public EnumNavigation getNavExtractHeadcountAndStats() {
        return navExtractHeadcountAndStats;
    }

    public EnumNavigation getNavReportsEmployeePayslip() {
        return navReportsEmployeePayslip;
    }

    public void setNavReportsEmployeePayslip(EnumNavigation navReportsEmployeePayslip) {
        this.navReportsEmployeePayslip = navReportsEmployeePayslip;
    }

    public EnumNavigation getNavReportsEmployeeTaxCertificate() {
        return navReportsEmployeeTaxCertificate;
    }

    public void setNavReportsEmployeeTaxCertificate(EnumNavigation navReportsEmployeeTaxCertificate) {
        this.navReportsEmployeeTaxCertificate = navReportsEmployeeTaxCertificate;
    }

    public EnumNavigation getNavReportsEmployeeEMP201() {
        return navReportsEmployeeEMP201;
    }

    public void setNavReportsEmployeeEMP201(EnumNavigation navReportsEmployeeEMP201) {
        this.navReportsEmployeeEMP201 = navReportsEmployeeEMP201;
    }

    public void setNavExtractHeadcountAndStats(EnumNavigation navExtractHeadcountAndStats) {
        this.navExtractHeadcountAndStats = navExtractHeadcountAndStats;
    }

    public EnumNavigation getNavExtractTakeonAndDischarges() {
        return navExtractTakeonAndDischarges;
    }

    public void setNavExtractTakeonAndDischarges(EnumNavigation navExtractTakeonAndDischarges) {
        this.navExtractTakeonAndDischarges = navExtractTakeonAndDischarges;
    }

    public void setReportMasterAudit(EnumNavigation navReportMasterAudit) {
        this.navReportMasterAudit = navReportMasterAudit;
    }

    public void setReportCreditHistory(EnumNavigation navReportCreditHistory) {
        this.navReportCreditHistory = navReportCreditHistory;
    }

    /**
     * Instructs the interface to navigate to the a specific section. We're
     * using the VALUE of the commandButton to determine where to navigate to.
     *
     * @param actionEvent
     */
    public void setNavigation(ActionEvent actionEvent) {

        // Before we navigate away from a page, let's apply all changes on that page since the last SAVE
        if (changesOnPage) {
            saveChanges();
        }

        String buttonID = actionEvent.getComponent().getClientId();
        if (sessionUser != null) {
            EnumNavigation eNav = EnumNavigation.getPageActionForButton(buttonID);
            setSelectedAction(eNav);
        } else {
            setSelectedAction(EnumNavigation.LOGIN);
        }

        RequestContext.getCurrentInstance().update("ltuSouth");
    }

    public boolean isRenderWest() {
        this.renderWest = (this.selectedAction.getSectionNavigationPage().length() > 0)
                && (!this.loadingNewEmployee);
        return renderWest;
    }

    public boolean isRenderNorth() {
        this.renderNorth = (!this.loadingNewEmployee);
        return renderNorth;
    }

    /**
     * Returns the EnumNavigation we're currently working with
     *
     * @return
     */
    public EnumNavigation getSelectedAction() {
        return this.selectedAction;
    }

    /**
     * ************************************************************************
     * PURCHASING CREDITS
     * ************************************************************************
     */
    private PayGatePurchaseRequest pgPurchase_Payroll_10;

    public PayGatePurchaseRequest getPgPurchase_Payroll_10() {
        // make sure we reload the credits properly
        this.payrollCredits = 0;
        return new PayGatePurchaseRequest(sessionUser, 10, Constants.MODULE_PAYROLL);
    }

    private PayGatePurchaseRequest pgPurchase_Payroll_50;

    public PayGatePurchaseRequest getPgPurchase_Payroll_50() {
        // make sure we reload the credits properly
        this.payrollCredits = 0;
        return new PayGatePurchaseRequest(sessionUser, 50, Constants.MODULE_PAYROLL);
    }

    private PayGatePurchaseRequest pgPurchase_Payroll_100;

    public PayGatePurchaseRequest getPgPurchase_Payroll_100() {
        // make sure we reload the credits properly
        this.payrollCredits = 0;
        return new PayGatePurchaseRequest(sessionUser, 100, Constants.MODULE_PAYROLL);
    }

    private String message, emailAddress;
    private final static String DEFAULT_MESSAGE_1 = "(None: Message Not Sent)";
    private final static String DEFAULT_EMAIL_1 = "(No Reply Email Given)";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String showMessage2() {
        if (message.isEmpty()) {
            message = DEFAULT_MESSAGE_1;
        }
        if (emailAddress.isEmpty()) {
            emailAddress = DEFAULT_EMAIL_1;
        }
        if (!message.isEmpty()) {
            sendMessageToCustomerSupport();
        }
        return (null); //So works if Ajax is disabled
    }

    private void sendMessageToCustomerSupport() {
        // Ignore message and tell customer
        // it has been filed for consideration :-)
    }

}
