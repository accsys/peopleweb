/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import za.co.accsys.web.helperclasses.DateUtils;
import za.co.accsys.web.helperclasses.EMail;
import za.co.accsys.web.helperclasses.ServerPrefs;
import za.co.accsys.web.model.Employee;
import za.co.accsys.web.model.Payroll;
import za.co.accsys.web.model.SessionUser;

/**
 *
 * @author fkleynhans
 */
public class ReportPrinter {

    /**
     * Creates a PDF version of the payslip report (ESSP1102.jasper)
     *
     * @param sessionUser
     * @param employee
     */
    public void printPayslip(SessionUser sessionUser, Employee employee) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_Payslip(sessionUser, employee);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "Payslip_" + employee.getEmployeeNumber() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the IRp5 report (ESS_IRP5.jasper)
     *
     * @param sessionUser
     * @param employee
     */
    public void printTaxCertificate(SessionUser sessionUser, Employee employee) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_TaxCertificate(sessionUser, employee);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "TaxCertificate_" + employee.getEmployeeNumber() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the payslip report (ESSP1102.jasper) for
     * multiple employees in one PDF document
     *
     * @param sessionUser
     * @param filteredEmployees
     * @return
     */
    public StreamedContent getMultiplePayslips(SessionUser sessionUser, ArrayList<Employee> filteredEmployees) {
        try {
            // Initialise the JasperPrint instance with all the necessary parameters for each employee in the list
            List<JasperPrint> jasperPrintList = new ArrayList();
            JasperPrint jPrint;
            for (Employee employee : filteredEmployees) {
                if (employee.isValidPayslip() == true) {
                    jPrint = getJasperPrint_PayslipMultiple(sessionUser, employee);
                    jasperPrintList.add(jPrint);
                    jPrint = null;
                }
            }

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "Payslip_Multiple_Employees.pdf");
            httpServletResponse.setContentType("application/pdf");

            JRPdfExporter exporter = new JRPdfExporter();

            exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
            File file = new java.io.File("Payslip_Multiple_Employees.pdf");
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            configuration.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configuration);
            exporter.exportReport();

            String filePath = file.getAbsolutePath();
            InputStream ios = new FileInputStream(filePath);
            StreamedContent rslt = new DefaultStreamedContent(ios, "application/pdf", "Payslip_Multiple_Employees.pdf");
            return rslt;

        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Creates a PDF version of the TaxCertificate report (ESS_IRP5.jasper) for
     * multiple employees in one PDF document
     *
     * @param sessionUser
     * @param filteredEmployees
     * @return
     */
    public StreamedContent getMultipleTaxCertificates(SessionUser sessionUser, ArrayList<Employee> filteredEmployees) {
        try {
            // Initialise the JasperPrint instance with all the necessary parameters for each employee in the list
            List<JasperPrint> jasperPrintList = new ArrayList();
            JasperPrint jPrint;
            for (Employee employee : filteredEmployees) {
                if (employee.isValidTaxCertificate() == true) {
                    jPrint = getJasperPrint_TaxCertificateMultiple(sessionUser, employee);
                    jasperPrintList.add(jPrint);
                    jPrint = null;
                }
            }

            if (!jasperPrintList.isEmpty()) {
                // Set up the destination of this report
                HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "TaxCertificate_Multiple_Employees.pdf");
                httpServletResponse.setContentType("application/pdf");

                JRPdfExporter exporter = new JRPdfExporter();

                exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
                File file = new java.io.File("TaxCertificate_Multiple_Employees.pdf");
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
                SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
                configuration.setCreatingBatchModeBookmarks(true);
                exporter.setConfiguration(configuration);
                exporter.exportReport();

                String filePath = file.getAbsolutePath();
                InputStream ios = new FileInputStream(filePath);
                StreamedContent rslt = new DefaultStreamedContent(ios, "application/pdf", "TaxCertificate_Multiple_Employees.pdf");
                return rslt;
            } else {
                return null;
            }

        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Creates a PDF version of the payslip report (ESSP1102.jasper) and emails
     * it to the employee
     *
     * @param sessionUser
     * @param employee
     */
    public void emailPayslip(SessionUser sessionUser, Employee employee) {
        FileOutputStream fileOutputStream = null;
        try {

            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_Payslip(sessionUser, employee);

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            // FacesContext.getCurrentInstance().responseComplete();
            String fileName = "Payslip_" + employee.getEmployeeNumber() + ".pdf";

            File payslip = new java.io.File(".\\" + fileName);
            //System.out.println("PDF to " + payslip.getAbsolutePath());
            fileOutputStream = new FileOutputStream(payslip);
            JasperExportManager.exportReportToPdfStream(jPrint, fileOutputStream);

            // Now let's send this email
            EMail payslipMailer = new EMail(
                    // SMTP Server
                    ServerPrefs.getInstance().getSMTPServer(),
                    // SMTP Port
                    ServerPrefs.getInstance().getSMTPPort(),
                    // SMTP AuthUsr
                    ServerPrefs.getInstance().getSMTPAuthUser(),
                    // SMTP AuthPwd
                    ServerPrefs.getInstance().getSMTPAuthPwd(),
                    // SMTP FromAddress
                    ServerPrefs.getInstance().getSMTPFromAddress(sessionUser.getAccountID()));

            if (payslipMailer.send(employee.getEmail(), "", "Electronic Payslip - " + employee.getPayroll().getOpenPeriod().toString(),
                    "Dear " + employee.getPreferredName()
                    + "\n\nPlease find attached your payslip for " + employee.getPayroll().getOpenPeriod().toString(), payslip)) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_INFO, "Payslip for " + employee.getPreferredName() + " " + employee.getSurname() + " sent to " + employee.getEmail(), "Payslip was sent");
            } else {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, Constants.EMAIL_ERROR, null);
            }

        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, "Unable to send payslip for " + employee.toString() + "\n" + ex.getMessage(), "Payslip was not sent");
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the IRp5 report (ESS_IRP5.jasper) and emails it
     * to the employee
     *
     * @param sessionUser
     * @param employee
     */
    public void emailTaxCertificate(SessionUser sessionUser, Employee employee) {
        FileOutputStream fileOutputStream = null;
        try {

            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_TaxCertificate(sessionUser, employee);

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            // FacesContext.getCurrentInstance().responseComplete();
            String fileName = "TaxCertificate_" + employee.getEmployeeNumber() + ".pdf";

            File irp5 = new java.io.File(".\\" + fileName);
            //System.out.println("PDF to " + payslip.getAbsolutePath());
            fileOutputStream = new FileOutputStream(irp5);
            JasperExportManager.exportReportToPdfStream(jPrint, fileOutputStream);

            // Now let's send this email
            EMail irp5Mailer = new EMail(
                    // SMTP Server
                    ServerPrefs.getInstance().getSMTPServer(),
                    // SMTP Port
                    ServerPrefs.getInstance().getSMTPPort(),
                    // SMTP AuthUsr
                    ServerPrefs.getInstance().getSMTPAuthUser(),
                    // SMTP AuthPwd
                    ServerPrefs.getInstance().getSMTPAuthPwd(),
                    // SMTP FromAddress
                    ServerPrefs.getInstance().getSMTPFromAddress(sessionUser.getAccountID()));

            if (irp5Mailer.send(employee.getEmail(), "", "TaxCertificate - " + employee.getPayroll().getOpenPeriod().toString(),
                    "Dear " + employee.getPreferredName()
                    + "\n\nPlease find attached your TaxCertificate for " + employee.getPayroll().getOpenPeriod().toString(), irp5)) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_INFO, "TaxCertificate for " + employee.getPreferredName() + " " + employee.getSurname() + " sent to " + employee.getEmail(), "TaxCertificate was sent");
            } else {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, Constants.EMAIL_ERROR, null);
            }

        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, "Unable to send TaxCertificate for " + employee.toString() + "\n" + ex.getMessage(), "TaxCertificate was not sent");
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the payslip report (ESSP1102.jasper)
     *
     * @param sessionUser
     * @param payroll
     */
    public void printTaxReturn(SessionUser sessionUser, Payroll payroll) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_TaxReturn(sessionUser, payroll);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "TaxReturn_" + payroll.getPayrollName() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the Payroll Register (P1404.jasper)
     *
     * @param sessionUser
     * @param payroll
     */
    public void printPayrollRegister(SessionUser sessionUser, Payroll payroll) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_PayrollRegister(sessionUser, payroll);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "PayrollRegister_" + payroll.getPayrollName() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the Payroll Register (P1404.jasper)
     *
     * @param sessionUser
     * @param payroll
     */
    public void printUifExtractReport(SessionUser sessionUser, Payroll payroll) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_UifExtract(sessionUser, payroll);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "UIF_Extract_Report_" + payroll.getPayrollName() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the Payroll Register (P1404.jasper)
     *
     * @param sessionUser
     * @param payroll
     */
    public void printPayrollVariance(SessionUser sessionUser, Payroll payroll) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_PayrollVariance(sessionUser, payroll);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "PayrollVariance_" + payroll.getPayrollName() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the Master Audit (P1081.jasper)
     *
     * @param FromDate
     * @param sessionUser
     * @param ToDate
     */
    public void printMasterAudit(SessionUser sessionUser, Date FromDate, Date ToDate) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_MasterAudit(sessionUser, FromDate, ToDate);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "MasterAudit_" + FromDate + "-" + ToDate + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the Credit History Report
     *
     * @param sessionUser
     */
    public void printCreditHistory(SessionUser sessionUser) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_CreditHistory(sessionUser);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "CreditHistory.pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the SDL Report (P1415.jasper)
     *
     * @param sessionUser
     * @param payroll
     */
    public void printPayrollSDL(SessionUser sessionUser, Payroll payroll) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_PayrollSDL(sessionUser, payroll);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "PayrollSDL_" + payroll.getPayrollName() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the Year To Date Payroll Register (P1302.jasper)
     *
     * @param sessionUser
     * @param payroll
     */
    public void printPayrollRegisterYTD(SessionUser sessionUser, Payroll payroll) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_PayrollRegisterYTD(sessionUser, payroll);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "PayrollRegisterYTD_" + payroll.getPayrollName() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Creates a PDF version of the EMP201 report (P1470.jasper)
     *
     * @param sessionUser
     * @param payroll
     */
    public void printEMP201(SessionUser sessionUser, Payroll payroll) {
        ServletOutputStream servletOutputStream = null;
        try {
            // Initialise the JasperPrint instance with all the necessary parameters
            JasperPrint jPrint = getJasperPrint_EMP201(sessionUser, payroll);

            // Set up the destination of this report
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=" + "EMP201_" + payroll.getPayrollName() + ".pdf");
            httpServletResponse.setContentType("application/pdf");

            // Call responseComplete on the FacesContext to signal to the JSF runtime 
            //   that it should short-circuit the response lifecycle, handing over control to you
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jPrint, servletOutputStream);
        } catch (IOException | JRException ex) {
            Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportPrinter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Initialisation of JasperPrint for the payslip report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_Payslip(SessionUser sessionUser, Employee employee) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("EMPLOYEE_ID", employee.getEmployeeID());
        parameters.put("PAYROLL_ID", employee.getPayroll().getPayrollID());
        //parameters.put("PAYROLLDETAIL_ID", employee.getPayroll().getPrintForPeriod());
        parameters.put("PERIOD_START_DATE", employee.getPayslipPrintForPeriod());
        parameters.put("BATCH_COUNT", "0");
        parameters.put("RUNTYPE_ID", "1");
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_PAYSLIP), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the TaxCertificate report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_TaxCertificate(SessionUser sessionUser, Employee employee) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("EMPLOYEE_ID", employee.getEmployeeID());
        parameters.put("PAYROLL_ID", employee.getPayroll().getPayrollID());
        parameters.put("TRANSACTION_YEAR", employee.getTaxCertificatePrintForPeriod().substring(0, 4));
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_IRP5), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the payslip report for multiple
     * employees
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_PayslipMultiple(SessionUser sessionUser, Employee employee) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("EMPLOYEE_ID", employee.getEmployeeID());
        parameters.put("PAYROLL_ID", employee.getPayrollID());
        //parameters.put("PAYROLLDETAIL_ID", employee.getPayroll().getOpenPeriodID());
        parameters.put("PERIOD_START_DATE", employee.getPayroll().getOpenPeriodDateAsString());
        parameters.put("BATCH_COUNT", "0");
        parameters.put("RUNTYPE_ID", "1");
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);
        parameters.put("IS_MULTIPLE", true);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_PAYSLIP), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the tax certificate report for multiple
     * employees
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_TaxCertificateMultiple(SessionUser sessionUser, Employee employee) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("EMPLOYEE_ID", employee.getEmployeeID());
        parameters.put("PAYROLL_ID", employee.getPayroll().getPayrollID());
        parameters.put("TRANSACTION_YEAR", employee.getTaxCertificatePrintForPeriod().substring(0, 4));
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);
        parameters.put("IS_MULTIPLE", true);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_IRP5), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the TAx Return report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_TaxReturn(SessionUser sessionUser, Payroll payroll) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("PAYROLLDETAIL_ID", payroll.getOpenPeriodID());
        parameters.put("PAYROLL_ID", payroll.getPayrollID());
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_TAXRETURN), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the Payroll Register report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_PayrollRegister(SessionUser sessionUser, Payroll payroll) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("PAYROLLDETAIL_ID", payroll.getOpenPeriodID());
        parameters.put("PAYROLL_ID", payroll.getPayrollID());
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_PAYROLLREGISTER), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the Payroll Register report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_UifExtract(SessionUser sessionUser, Payroll payroll) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("PAYROLL_ID", payroll.getPayrollID());
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_UIFEXTRACT), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the Payroll Variance report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_PayrollVariance(SessionUser sessionUser, Payroll payroll) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        //parameters.put("PAYROLLDETAIL_ID", payroll.getOpenPeriodID());
        parameters.put("PERIOD_START_DATE", payroll.getPrintForPeriod());
        parameters.put("PAYROLL_ID", payroll.getPayrollID());
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_PAYROLLVARIANCE), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the Payroll Variance report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_MasterAudit(SessionUser sessionUser, Date FromDate, Date ToDate) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        String FD = "'" + DateUtils.formatDate(FromDate) + "'";
        String TD = "'" + DateUtils.formatDate(ToDate) + "'";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("FROM_DATE", FD);
        parameters.put("TO_DATE", TD);
        parameters.put("FROM_DATE_STRING", DateUtils.formatDate(FromDate));
        parameters.put("TO_DATE_STRING", DateUtils.formatDate(ToDate));
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_MASTERAUDIT), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the Credit History report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_CreditHistory(SessionUser sessionUser) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_CREDITHISTORY), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the SDL report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_PayrollSDL(SessionUser sessionUser, Payroll payroll) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("PAYROLLDETAIL_ID", payroll.getOpenPeriodID());
        parameters.put("PAYROLL_ID", payroll.getPayrollID());
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_PAYROLLSDL), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the Payroll Register (Year To Date)
     * report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_PayrollRegisterYTD(SessionUser sessionUser, Payroll payroll) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("PAYROLLDETAIL_ID", payroll.getOpenPeriodID());
        parameters.put("PAYROLL_ID", payroll.getPayrollID());
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_PAYROLLREGISTERYTD), parameters, jasperConn);
        return jasperPrint;
    }

    /**
     * Initialisation of JasperPrint for the EMP201 report
     *
     * @throws JRException
     */
    private JasperPrint getJasperPrint_EMP201(SessionUser sessionUser, Payroll payroll) throws JRException {
        HashMap parameters = new HashMap();
        String jdbcURL = ServerPrefs.getInstance().getJdbcURL(sessionUser.getAccountID());
        Connection jasperConn = new DbConnection(jdbcURL).getConnection();

        //  Make the reports multi-lingual
        //  Get the locale and pass the translation file to the report
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle messages;

        messages = ResourceBundle.getBundle("resources.messages", locale);

        // What is the path (actual, not relative) to where this web app is running?
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
        String reportPath = realPath + "/reports/";

        parameters.put("REPORT_LOGO", "../images/CompanyLogo.png");
        parameters.put("COMPANY_ID", sessionUser.getCompanyId());
        parameters.put("PAYROLLDETAIL_ID", payroll.getOpenPeriodID());
        parameters.put("PAYROLL_ID", payroll.getPayrollID());
        parameters.put("SUBREPORT_DIR", reportPath);
        parameters.put("REPORT_LOCALE", locale);
        parameters.put("REPORT_RESOURCE_BUNDLE", messages);

        JasperPrint jasperPrint = JasperFillManager.fillReport((reportPath + Constants.REPORTNAME_EMP201), parameters, jasperConn);
        return jasperPrint;
    }

}
