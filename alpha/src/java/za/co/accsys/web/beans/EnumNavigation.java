package za.co.accsys.web.beans;

/**
 * An Enumerator class to define the different page selections
 *
 * @author lterblanche
 *
 */
public enum EnumNavigation {

    /*
     |*********************************************************************|
     |                             ltuNorth                                |
     |*********************************************************************|
     | |                         |                                       | |
     | |  SectionNavigationPage  |               MainNavigationPage      | |
     | |                         |                                       | |
     | |                         |                                       | |
     | |*****************************************************************| |
     |*********************************************************************|

     -*/
    EMPLOYEE("navManageStaff", "btnManageStaff", "_centre_employee_overview.xhtml", "_navigation_employee.xhtml"),
    PAY_CYCLE("navPayPeriods", "btnPayPeriods", "_centre_paycycles_overview.xhtml", ""),
    REPORTING("navReporting", "btnReporting", "_centre_reporting_overview.xhtml", "_navigation_reporting.xhtml"),
    SETTINGS("navSystemSettings", "btnSystemSettings", "_centre_settings_overview.xhtml", "_navigation_settings.xhtml"),
    LOGOUT("navLogout", "btnLogout", "", ""),
    LOGIN("navLogin", "btnLogin", "_centre_login.xhtml", ""),
    PASSWORD_RESET("navPasswordReset", "btnReset", "_centre_resetpassword.xhtml", ""),
    ACCOUNT_REGISTER("navAccountRegister", "btnAccountRegister", "_centre_registeraccount.xhtml", ""),
    USER_PREFERENCES("navUserPreferences", "btnPreferences", "_centre_preferences.xhtml", ""),
    /* EMPLOYEE INFO */
    EMPLOYEE_NEW("navNewEmployee", "btnNewEmployee", "_employee_new.xhtml", "_navigation_employee.xhtml"),
    EMPLOYEE_PERSONALINFO("navPersonalInformation", "btnPersonalInfo", "_employee_change_personalInfo.xhtml", "_navigation_employee.xhtml"),
    EMPLOYEE_ADDRESSINFO("navAddressInformation", "btnAddressInfo", "_employee_change_addressInfo.xhtml", "_navigation_employee.xhtml"),
    EMPLOYEE_ACCOUNTINFO("navAccountInformation", "btnAccountInfo", "_employee_change_accountInfo.xhtml", "_navigation_employee.xhtml"),
    EMPLOYEE_PAYINFO("navPayInformation", "btnPayInfo", "_employee_change_payInfo.xhtml", "_navigation_employee.xhtml"),
    /* SETTINGS */
    SETTINGS_SERVER("navServerSettings", "btnServerSettings", "_settings_change_serverInfo.xhtml", "_navigation_settings.xhtml"),
    SETTINGS_COMPANYGENERAL("navCompanyGeneralSettings", "btnCompanySettings", "_settings_change_companyInfo.xhtml", "_navigation_settings.xhtml"),
    SETTINGS_COMPANYADDRESS("navCompanyAddressSettings", "btnCompanyAddressSettings", "_settings_change_serverInfo.xhtml", "_navigation_settings.xhtml"),
    SETTINGS_COMPANYACCOUNT("navCompanyAccountSettings", "btnCompanyAccountSettings", "_settings_change_companyAccount.xhtml", "_navigation_settings.xhtml"),
    SETTINGS_COMPANYCOSTCENTRE("navCompanyCostCentreSettings", "btnCompanyCostCentreSettings", "_settings_change_costCentreInfo.xhtml", "_navigation_settings.xhtml"),
    SETTINGS_PAYROLLGENERAL("navPayrollGeneralSettings", "btnPayrollSettings", "_settings_change_payrollInfo.xhtml", "_navigation_settings.xhtml"),
    SETTINGS_PAYROLLPAYENTITY("navPayrollPayEntitySettings", "btnPayEntitySettings", "_settings_change_payEntities.xhtml", "_navigation_settings.xhtml"),
    SETTINGS_LISTSMANAGEMENT("navListsSettings", "btnListsSettings", "_settings_change_lists.xhtml", "_navigation_settings.xhtml"),
    /* REPORTS */
    REPORTS_MASTERAUDIT("navReportMasterAudit", "btnReportMasterAudit", "_report_master_audit.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_CREDITHISTORY("navReportCreditHistory", "btnReportCreditHistory", "_report_credit_history.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_EMPLOYEEPAYSLIP("navReportEmployeePayslip", "btnReportEmployeePayslip", "_report_employee_payslip.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_EMPLOYEETAXCERTIFICATE("navReportEmployeeTaxCertificate", "btnReportEmployeeTaxCertificate", "_report_employee_taxcertificate.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_EMPLOYEEEMP201("navReportEmployeeEMP201", "btnReportEmployeeEMP201", "_report_employee_emp201.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_PAYROLLTAXRETURN("navReportPayrollTaxReturn", "btnReportPayrollTaxReturn", "_report_payroll_taxreturn.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_PAYROLLREGISTER("navReportPayrollRegister", "btnReportPayrollRegister", "_report_payroll_register.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_PAYROLLREGISTERYTD("navReportPayrollRegisterYTD", "btnReportPayrollRegisterYTD", "_report_payroll_register_ytd.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_PAYROLLSDL("navReportPayrollSDL", "btnReportPayrollSDL", "_report_payroll_sdl.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_PAYROLLVARIANCE("navReportPayrollVariance", "btnReportPayrollVariance", "_report_payroll_variance.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_PAYROLLTAXCERTIFICATE("navReportPayrollTaxcertificate", "btnReportPayrollTaxcertificate", "_report_payroll_taxcertificate.xhtml", "_navigation_reporting.xhtml"),
    REPORTS_UIFEXTRACT("navReportUifExtract", "btnReportUifExtract", "_report_uif_extract.xhtml", "_navigation_reporting.xhtml"),
    EXTRACTS_HEADCOUNTANDSTATS("navExtractHeadcountAndStats", "btnHeadcountAndStatsReport", "_extract_headcount_and_stats.xhtml", "_navigation_reporting.xhtml"),
    EXTRACTS_EMPLOYEEVARDETAIL("navExtractEmployeeVarDetail", "btnExtractEmplyeeVarDetail", "_extract_employee_variabledetails.xhtml", "_navigation_reporting.xhtml"),
    EXTRACTS_TAKEONANDDISCHARGES("navExtractTakeonAndDischarges", "btnTakeonAndDischarges", "_extract_takeon_and_discharges.xhtml", "_navigation_reporting.xhtml"),
    EXTRACTS_ALLEMPLOYEEATTRIBUTES("navExtractAllEmployeeAttributes", "btnAllAttributes", "_extract_employee_allattributes.xhtml", "_navigation_reporting.xhtml"),
    EXTRACTS_ALLEMPLOYEEEXCEPTIONS("navExtractAllEmployeeExceptions", "btnAllExceptions", "_extract_employee_exceptions.xhtml", "_navigation_reporting.xhtml");
    /* Headings */

    private final String name;
    private final String buttonId;
    private final String mainNavigationPage;
    private final String sectionNavigationPage;

    /**
     * A Navigation Enumerated type structures a link's name, caption, which
     * page its linked to and which section navigation it belongs to.
     *
     * @param aName Name of the component. This name MUST exist under the
     * locale-dependent resource files in order to translate the text of the
     * button properly.
     * @param aButtonID Used to create a PrimeFaces ID for the button that
     * actions this page navigation
     * @param aMainNavigationPage The page that this button will be pointing to
     * (on the middle-center section of the screen)
     * @param aSectionNavigationPage The navigation page that this button finds
     * itself in.
     */
    private EnumNavigation(String aName, String aButtonID, String aMainNavigationPage, String aSectionNavigationPage) {
        name = aName;
        buttonId = aButtonID;
        mainNavigationPage = aMainNavigationPage;
        sectionNavigationPage = aSectionNavigationPage;
    }

    public String getName() {
        return name;
    }

    public String getButtonId() {
        return buttonId;
    }

    public String getCaption() {
        return PageManager.translate(name);

    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    public String getMainNavigationPage() {
        return mainNavigationPage;
    }

    public String getSectionNavigationPage() {
        return sectionNavigationPage;
    }

    @Override
    public String toString() {
        return name;
    }

    public static EnumNavigation getPageActionForButton(String buttonID) {
        // The ButtonID can look like ':frmdddd:sasd:btnABX'.
        //  We're only interested in the last part
        buttonID = buttonID.substring(buttonID.lastIndexOf(':') + 1);
        if (buttonID.equalsIgnoreCase(EMPLOYEE.getButtonId())) {
            return EMPLOYEE;
        }
        if (buttonID.equalsIgnoreCase(PAY_CYCLE.getButtonId())) {
            return PAY_CYCLE;
        }
        if (buttonID.equalsIgnoreCase(REPORTING.getButtonId())) {
            return REPORTING;
        }
        if (buttonID.equalsIgnoreCase(SETTINGS.getButtonId())) {
            return SETTINGS;
        }
        if (buttonID.equalsIgnoreCase(LOGOUT.getButtonId())) {
            return LOGOUT;
        }
        if (buttonID.equalsIgnoreCase(LOGIN.getButtonId())) {
            return LOGIN;
        }
        if (buttonID.equalsIgnoreCase(USER_PREFERENCES.getButtonId())) {
            return USER_PREFERENCES;
        }
        if (buttonID.equalsIgnoreCase(PASSWORD_RESET.getButtonId())) {
            return PASSWORD_RESET;
        }
        if (buttonID.equalsIgnoreCase(ACCOUNT_REGISTER.getButtonId())) {
            return ACCOUNT_REGISTER;
        }

        /* EMPLOYEE NAVIGATION */
        if (buttonID.equalsIgnoreCase(EMPLOYEE_NEW.getButtonId())) {
            return EMPLOYEE_NEW;
        }
        if (buttonID.equalsIgnoreCase(EMPLOYEE_PERSONALINFO.getButtonId())) {
            return EMPLOYEE_PERSONALINFO;
        }
        if (buttonID.equalsIgnoreCase(EMPLOYEE_ADDRESSINFO.getButtonId())) {
            return EMPLOYEE_ADDRESSINFO;
        }
        if (buttonID.equalsIgnoreCase(EMPLOYEE_ACCOUNTINFO.getButtonId())) {
            return EMPLOYEE_ACCOUNTINFO;
        }
        if (buttonID.equalsIgnoreCase(EMPLOYEE_PAYINFO.getButtonId())) {
            return EMPLOYEE_PAYINFO;
        }
        /* SETTINGS NAVIGATION */
        if (buttonID.equalsIgnoreCase(SETTINGS_SERVER.getButtonId())) {
            return SETTINGS_SERVER;
        }
        if (buttonID.equalsIgnoreCase(SETTINGS_COMPANYACCOUNT.getButtonId())) {
            return SETTINGS_COMPANYACCOUNT;
        }
        if (buttonID.equalsIgnoreCase(SETTINGS_COMPANYADDRESS.getButtonId())) {
            return SETTINGS_COMPANYADDRESS;
        }
        if (buttonID.equalsIgnoreCase(SETTINGS_COMPANYCOSTCENTRE.getButtonId())) {
            return SETTINGS_COMPANYCOSTCENTRE;
        }
        if (buttonID.equalsIgnoreCase(SETTINGS_COMPANYGENERAL.getButtonId())) {
            return SETTINGS_COMPANYGENERAL;
        }
        if (buttonID.equalsIgnoreCase(SETTINGS_PAYROLLGENERAL.getButtonId())) {
            return SETTINGS_PAYROLLGENERAL;
        }
        if (buttonID.equalsIgnoreCase(SETTINGS_PAYROLLPAYENTITY.getButtonId())) {
            return SETTINGS_PAYROLLPAYENTITY;
        }
        if (buttonID.equalsIgnoreCase(SETTINGS_LISTSMANAGEMENT.getButtonId())) {
            return SETTINGS_LISTSMANAGEMENT;
        }

        /* REPORTS and EXTRACTS NAVIGATION */
        if (buttonID.equalsIgnoreCase(REPORTS_MASTERAUDIT.getButtonId())) {
            return REPORTS_MASTERAUDIT;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_CREDITHISTORY.getButtonId())) {
            return REPORTS_CREDITHISTORY;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_PAYROLLTAXRETURN.getButtonId())) {
            return REPORTS_PAYROLLTAXRETURN;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_PAYROLLREGISTER.getButtonId())) {
            return REPORTS_PAYROLLREGISTER;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_PAYROLLREGISTERYTD.getButtonId())) {
            return REPORTS_PAYROLLREGISTERYTD;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_PAYROLLSDL.getButtonId())) {
            return REPORTS_PAYROLLSDL;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_PAYROLLVARIANCE.getButtonId())) {
            return REPORTS_PAYROLLVARIANCE;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_PAYROLLTAXCERTIFICATE.getButtonId())) {
            return REPORTS_PAYROLLTAXCERTIFICATE;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_UIFEXTRACT.getButtonId())) {
            return REPORTS_UIFEXTRACT;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_EMPLOYEEPAYSLIP.getButtonId())) {
            return REPORTS_EMPLOYEEPAYSLIP;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_EMPLOYEETAXCERTIFICATE.getButtonId())) {
            return REPORTS_EMPLOYEETAXCERTIFICATE;
        }
        if (buttonID.equalsIgnoreCase(REPORTS_EMPLOYEEEMP201.getButtonId())) {
            return REPORTS_EMPLOYEEEMP201;
        }
        if (buttonID.equalsIgnoreCase(EXTRACTS_HEADCOUNTANDSTATS.getButtonId())) {
            return EXTRACTS_HEADCOUNTANDSTATS;
        }
        if (buttonID.equalsIgnoreCase(EXTRACTS_TAKEONANDDISCHARGES.getButtonId())) {
            return EXTRACTS_TAKEONANDDISCHARGES;
        }
        if (buttonID.equalsIgnoreCase(EXTRACTS_ALLEMPLOYEEATTRIBUTES.getButtonId())) {
            return EXTRACTS_ALLEMPLOYEEATTRIBUTES;
        }
        if (buttonID.equalsIgnoreCase(EXTRACTS_EMPLOYEEVARDETAIL.getButtonId())) {
            return EXTRACTS_EMPLOYEEVARDETAIL;
        }
        if (buttonID.equalsIgnoreCase(EXTRACTS_ALLEMPLOYEEEXCEPTIONS.getButtonId())) {
            return EXTRACTS_ALLEMPLOYEEEXCEPTIONS;
        }

        return EnumNavigation.LOGIN;
    }
}
