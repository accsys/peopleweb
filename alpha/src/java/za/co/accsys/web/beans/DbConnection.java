/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.beans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author lterblanche
 */
public class DbConnection {

    private Connection connection;

    /**
     * Creates a new instance of Connection
     * @param jdbcURL
     */
    public DbConnection(String jdbcURL) {
        connection = getSybaseConnection(jdbcURL);
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    private java.sql.Connection getSybaseConnection(String jdbcURL) {
        java.sql.Connection con = null;
        try {
            try {
                // Create database
                Class.forName("com.sybase.jdbc3.jdbc.SybDriver").newInstance();
                con = DriverManager.getConnection(jdbcURL, Constants.DB_USER, Constants.DB_PWD);

            } catch (ClassNotFoundException ex) {
                System.out.println(ex.getMessage());
            }

        } catch (InstantiationException | IllegalAccessException | SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return con;
    }

}
