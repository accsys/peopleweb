/*
 * Way to distinguish between different variable calculation types (INPUT, FORMULA)
 */
package za.co.accsys.web.beans;

/**
 *
 * @author Liam
 */
public enum EnumVarCalculationType {

    INPUT, FORMULA, DATAVALUE;

    public static EnumVarCalculationType stringToEnum(String calculationType) {
        if (calculationType.trim().equalsIgnoreCase("Input")) {
            return INPUT;
        }
        if (calculationType.trim().equalsIgnoreCase("Formula")) {
            return FORMULA;
        }

        // Default
        return DATAVALUE;
    }

}
