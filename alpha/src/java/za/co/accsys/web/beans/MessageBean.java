/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.beans;

import javax.annotation.ManagedBean;

/**
 *
 * @author fkleynhans
 */
@ManagedBean
public class MessageBean {
    private String message, emailAddress;
    private final static String DEFAULT_MESSAGE_1 = "(None: Message Not Sent)";
    private final static String DEFAULT_EMAIL_1 = "(No Reply Email GIven)";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    public String showMessage2() {
        if (message.isEmpty()) {
            message = DEFAULT_MESSAGE_1;
        }
        if (emailAddress.isEmpty()) {
            emailAddress = DEFAULT_EMAIL_1;
        }
        if (!message.isEmpty()) {
            sendMessageToCustomerSupport();
        }
        return (null); //So works if Ajax is disabled
    }
    
    private void sendMessageToCustomerSupport() {
        // Ignore message and tell customer
        // it has been filed for consideration :-)
    }
    
}
