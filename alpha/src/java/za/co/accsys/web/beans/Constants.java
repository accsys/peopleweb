/*
 * Global constants used in Web Server calls
 */
package za.co.accsys.web.beans;

public class Constants {
    /* ---- SEND PARAMETER constants ---- */

    public static final String sendParam_Login = "web_login";
    public static final String sendParam_Password = "web_pwd";
    public static final String sendParam_SessionId = "sessionid";
    public static final String sendParam_CompanyId = "companyid";
    public static final String sendParam_EmployeeId = "employeeid";
    public static final String sendParam_CostId = "costid";
    public static final String sendParam_PayrollId = "payrollid";
    public static final String sendParam_RunId = "runid";
    public static final String sendParam_EntityId = "entityid";
    public static final String sendParam_ReconMonth = "reconMonth";
    public static final String sendParam_ReconYear = "reconYear";
    public static final String sendParam_TradeCode = "tradeCode";

    /**
     * Used when passing through a table name that requires update/insert/select transactions
     */
    public static final String sendParam_TableName = "tablename";
    /**
     * Typically used in conjunction with sendParam_ValueList to create qualified column/value pairs for
     * insert/update statements
     */
    public static final String sendParam_ColumnList = "columnlist";
    /**
     * Typically used in conjunction with sendParam_ColumnName to create qualified column/value pairs for
     * insert/update statements
     */
    public static final String sendParam_ValueList = "valuelist";
    /**
     * Typically used in conjunction with sendParam_ValueList to select a particular column value from a given
     * table name
     */
    public static final String sendParam_ColumnName = "colname";
    /**
     * Typically used with serviceName_GetDBValue selectively filter result-sets
     */
    public static final String sendParam_Filter = "filter";
    /**
     * Used with a GetValue call to order the result
     */
    public static final String sendParam_Order = "order";

    /* ---- WEB SERVICE names ---- */
    public static final String serviceName_Login = "ws_HTTP_LOGIN";
    public static final String serviceName_Logout = "ws_HTTP_LOGOUT";
    public static final String serviceName_IsSessionLive = "ws_HTTP_ISSESSIONLIVE";
    public static final String serviceName_GetUserInfo = "ws_HTTP_GetUserInfo";
    public static final String serviceName_SetDBValue = "ws_HTTP_SETVALUE";
    public static final String serviceName_GetDBValue = "ws_HTTP_GETVALUE";
    public static final String serviceName_GetEmployeeList = "ws_HTTP_GetEmployeeList";
    public static final String serviceName_GetEmployeeData = "ws_HTTP_GetEmployeeData";
    public static final String serviceName_GetCompanyData = "ws_HTTP_GetCompanyData";
    public static final String serviceName_GetEmployeeAddressData = "ws_HTTP_GetEmployeeAddressData";
    public static final String serviceName_GetEmployeeAccountData = "ws_HTTP_GetEmployeeAccountData";
    public static final String serviceName_GetEmployeePackage = "ws_HTTP_GetEmployeePackage";
    public static final String serviceName_SetEmployeePackage = "ws_HTTP_SetEmployeePackage";
    public static final String serviceName_GetYTDEmployeePackage = "ws_HTTP_GetYTDEmployeePackage";
    public static final String serviceName_GetEmployeeRunStatus = "ws_HTTP_GetEmployeeRunStatus";
    public static final String serviceName_RequestRecalc = "ws_HTTP_RequestRecalc";
    public static final String serviceName_AdvancePayrollPeriod = "ws_HTTP_AdvancePayrollPeriod";
    public static final String serviceName_GetPayrollOverviewAndStatus = "ws_HTTP_GetPayrollOverviewAndStatus";
    public static final String serviceName_GetEmployeeCalcDebugInfo = "ws_HTTP_GetEmployeeCalcDebugInfo";
    public static final String serviceName_GetEmployeeTaxDiagnostics = "ws_HTTP_GetEmployeeTaxDiagnostics";
    public static final String serviceName_ResetAdminPassword = "ws_HTTP_RESET_ADMINPASSWORD";
    public static final String serviceName_InitiateNewDB = "ws_HTTP_PrepareDBForFirstUse";
     public static final String serviceName_GetCredits = "ws_HTTP_GetCredits";
    public static final String serviceName_AddCredits = "ws_HTTP_AddCredits";
    public static final String serviceName_GetCreditUsageEstimate = "ws_HTTP_GetCreditUsageEstimate";    
    public static final String serviceName_SaveCreditTransaction = "ws_HTTP_SaveCreditPurchase";

    public static final String serviceName_GetEmployeePayrollInfo = "ws_HTTP_GetEmployeePayrollInfo";
    public static final String serviceName_SetEmployeePayrollInfo = "ws_HTTP_SetEmployeePayrollInfo";
    public static final String serviceName_GetCompanyAccountData = "ws_HTTP_GetCompanyAccountData";
    public static final String serviceName_SetEmployeeData = "ws_HTTP_SetEmployeeData";
    public static final String serviceName_GetNewEmployeeID = "ws_HTTP_GetNewEmployeeID";
    public static final String serviceName_SetCompanyData = "ws_HTTP_SetCompanyData";
    public static final String serviceName_SetCostCentreData = "ws_HTTP_SetCostCentreData";
    public static final String serviceName_SetPayrollData = "ws_HTTP_SetPayrollData";
    public static final String serviceName_SetPayEntityData = "ws_HTTP_SetPayEntityData";
    public static final String serviceName_SetJobPositionData = "ws_HTTP_SetJobPositionData";
    public static final String serviceName_SetTitleData = "ws_HTTP_SetTitleData";
    public static final String serviceName_SetLanguageData = "ws_HTTP_SetLanguageData";
    public static final String serviceName_PrepareTaxCertificateData = "ws_HTTP_PrepareTaxCertificateData";
    public static final String serviceName_GetTaxCertificatePeriods = "ws_HTTP_GetTaxCertificatePeriods";

    public static final String serviceName_SetEmployeeAddressData = "ws_HTTP_SetEmployeeAddressData";
    public static final String serviceName_SetEmployeeAccountData = "ws_HTTP_SetEmployeeAccountData";
    public static final String serviceName_SetCompanyAccountData = "ws_HTTP_SetCompanyAccountData";
    public static final String serviceName_GetCostCentreList = "ws_HTTP_GetCostCentreList";
    public static final String serviceName_GetPayrollList = "ws_HTTP_GetPayrollList";
    public static final String serviceName_GetPayrollPeriodList = "ws_HTTP_GetPayrollPeriodList";
    public static final String serviceName_GetPayEntityList = "ws_HTTP_GetPayEntityList";
    public static final String serviceName_GetJobTitleList = "ws_HTTP_GetJobTitleList";
    public static final String serviceName_GetTitleList = "ws_HTTP_GetTitleList";
    public static final String serviceName_GetLanguageList = "ws_HTTP_GetLanguageList";

    public static final String serviceName_GetTaxCountryList = "ws_HTTP_GetTaxCountryList";
    public static final String serviceName_GetPayrollUifExtactList = "ws_HTTP_GetPayrollUifExtract";
    public static final String serviceName_GetPayrollTCExtactList = "ws_HTTP_GetPayrollTCExtract";
    public static final String serviceName_GetTaxCodeAndDescriptionList = "ws_HTTP_GetTaxCodeAndDescriptionList";
    public static final String serviceName_GetUIFCodeAndReasonList = "ws_HTTP_GetDischargeCodeAndReasonList";
    public static final String serviceName_GetTradeClassificationCodesList = "ws_HTTP_GetTradeClassificationCodes";
    public static final String serviceName_GetTradeClassificationSubCodesList = "ws_HTTP_GetTradeClassificationSubCodes";
    public static final String serviceName_GetSICCodesList = "ws_HTTP_GetSICCodes";
    public static final String serviceName_GetCountryList = "ws_HTTP_GetCountryList";
    public static final String serviceName_GetProvinceList = "ws_HTTP_GetProvinceList";
    public static final String serviceName_CanConnect = "ws_HTTP_CanConnect";

    /* ---- RECEIVE PARAMETER constants ---- */
    public static final String receiveParam_SessionID = "SESSION_ID";
    public static final String receiveParam_CompanyID = "COMPANY_ID";
    public static final String receiveParam_EmployeeID = "EMPLOYEE_ID";

    /* ----- TRAFFIC LIGHT IMAGES ----- */
    public static final String trafficLight_RED = "./images/icon-exclamation.png";

    /* ------ CREDIT THRESHOLDS ------ */
    public static final int creditWarningThreshold = 20;

    /* ------- REPORT NAMES --------- */
    public static final String REPORTNAME_PAYSLIP = "ESSP1102International.jasper";
    public static final String REPORTNAME_TAXRETURN = "P1411.jasper";
    public static final String REPORTNAME_EMP201 = "P1470.jasper";
    public static final String REPORTNAME_PAYROLLREGISTER = "P1404.jasper";
    public static final String REPORTNAME_IRP5 = "ESS_IRP5.jasper";
    public static final String REPORTNAME_UIFEXTRACT = "P_UIF.jasper";
    public static final String REPORTNAME_PAYROLLVARIANCE = "P1120.jasper";
    public static final String REPORTNAME_MASTERAUDIT = "P1081.jasper";
    public static final String REPORTNAME_CREDITHISTORY = "SalesInvoice.jasper";
    public static final String REPORTNAME_PAYROLLSDL = "P1415.jasper";
    public static final String REPORTNAME_PAYROLLREGISTERYTD = "P1302.jasper";

    /* ------- PASSWORD RESET EMAIL MESSAGE ------ */
    public static final String EMAIL_RPL_FIRSTNAME = "rplFirstName";
    public static final String EMAIL_RPL_LOGINNAME = "rplLoginName";
    public static final String EMAIL_RPL_PASSWORD = "rplPassword";
    public static final String EMAIL_RPL_ACCOUNTID = "rplAccID";
    public static final String EMAIL_RPL_LOGINURL = "rplLoginURL"; // "<a href=\"https://peopleweb.accsys.co.za\">https://peopleweb.accsys.co.za</a>"
    public static final String EMAIL_PASSWORD_RESET_HTML_MESSAGE_PWDHASCHANGED = "pwdResetNotice_PasswordHasChanged";
    public static final String EMAIL_PASSWORD_RESET_HTML_MESSAGE_YOURNEWPWD = "pwdResetNotice_YourNewPassword";
    public static final String EMAIL_PASSWORD_RESET_HTML_MESSAGE_YOURLOGINNAME = "pwdResetNotice_YourLoginName";
    public static final String EMAIL_PASSWORD_RESET_HTML_MESSAGE_PLEASECHANGE = "pwdResetNotice_PleaseLoginAndChange";
    public static final String EMAIL_PASSWORD_RESET_HTML_MESSAGE_REGARDS = "pwdResetNotice_FromPeopleWeb";
    public static final String EMAIL_PASSWORD_RESET_DISPLAY_CONFIRMATION = "pwdResetDisplayOnScreen";
    public static final String EMAIL_PASSWORD_RESET_HTML_MESSAGE = "Dear "
            + EMAIL_RPL_FIRSTNAME + "<br><br>" // Firstname here
            + EMAIL_PASSWORD_RESET_HTML_MESSAGE_PWDHASCHANGED + "<br><br>"
            + EMAIL_PASSWORD_RESET_HTML_MESSAGE_YOURLOGINNAME + ": " + EMAIL_RPL_LOGINNAME + "<br>" // Add Login Name here
            + EMAIL_PASSWORD_RESET_HTML_MESSAGE_YOURNEWPWD + ": " + EMAIL_RPL_PASSWORD + "<br>" // Add Password here
            + EMAIL_PASSWORD_RESET_HTML_MESSAGE_PLEASECHANGE + "<br><br>"
            + EMAIL_PASSWORD_RESET_HTML_MESSAGE_REGARDS;
    public static final String EMAIL_WELCOME_NEW_CLIENT_MESSAGE = "Dear "
            + EMAIL_RPL_FIRSTNAME + "<br><br>" // Firstname here
            + "Welcome to the Accsys Family.<br>You have created a new account on <b>PeopleWeb</b> with instant access to your new payroll."
            + "<br><br>As a reminder, herewith your login credentials:"
            + "<br>To access your payroll, simply follow this link: "
            + "<a href=\""+EMAIL_RPL_LOGINURL+"\">PeopleWeb</a>" 
            + "<br>Your Account ID: " + EMAIL_RPL_ACCOUNTID
            + "<br>Your Login Name: " + EMAIL_RPL_LOGINNAME
            + "<br><br>Regards<br>The Accsys PeopleWeb Team";
    public static final String EMAIL_ERROR = "We encountered a problem with our mail server.  Please contact our help-desk for assistance";
    public static final String EMAIL_CCLIST = "meason@accsys.co.za";
    public static final String PASSWORD_RESET_EMAILNOTEXISTS = "msgNoAdminWithThisEmail";
    public static final String SMTP_FROM_ADDRESS = "peopleweb@accsys.co.za";
    /* ---------- TAX COUNTRIES ----------- */
    public static final String TAXCOUNTRY_SOUTHAFRICA = "SOUTH AFRICA";

    /* ---------- INDICATOR CODES -------- */
    public static final String INDCODE_UIF_CO = "99010";
    public static final String INDCODE_UIF_EE = "99011";

    /* ---------- SUPPORTED MODULES ------- */
    public static final String MODULE_PAYROLL = "Payroll";
    public static final String MODULE_TIME = "Time";

    /* -------- ERROR CODES ---------- */
    public static final String ERR_UNABLE_TO_CONNECT = "X001";      // Unable to connect to particular database;
    public static final String ERR_FAILED_NEW_INSTANCE = "X010";    // Unable to create a new PeopleWeb instance;

    /* -------- DATABASE RELATED -------- */
    public static final String SQLITE_FILE = "settings.sqlite";
    public static final String DB_USER = "DBA";
    public static final String DB_PWD = "I,don'tKNOW!1t.";
    public static final boolean DEBUG = true;

    public static final int ADVANCE_ALLCALCULATED = 1;    //
    public static final int ADVANCE_SOMESKIPPED = 2;      // Used to describe the payroll in order for the UI to enable/disable the Advance To Next Period feature
    public static final int ADVANCE_NOCREDITS = 3;        //

    public static final String PAYGATE_ID = "37631012489";
    public static final String PAYGATE_ENCRYPTION_KEY = "OIg82lmAiKHV";
    public static final String PAYGATE_REQUEST_URL = "https://www.paygate.co.za/paywebv2/process.trans";
    public static final String PAYGATE_RETURNURL_JSP = "paygate_creditresponse.jsp";
    
    public int getADVANCE_ALLCALCULATED() {
        return ADVANCE_ALLCALCULATED;
    }

    public int getADVANCE_SOMESKIPPED() {
        return ADVANCE_SOMESKIPPED;
    }

    public int getADVANCE_NOCREDITS() {
        return ADVANCE_NOCREDITS;
    }

    public String getPAYGATE_ID() {
        return PAYGATE_ID;
    }

    public String getPAYGATE_ENCRYPTION_KEY() {
        return PAYGATE_ENCRYPTION_KEY;
    }

    public String getPAYGATE_RETURNURL_JSP() {
        return PAYGATE_RETURNURL_JSP;
    }

}
