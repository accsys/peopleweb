/*
 * Way to distinguish between different variable types (EARNINGS, DEDUCTIONS, ETC)
 */
package za.co.accsys.web.beans;

/**
 *
 * @author Liam
 */
public enum EnumVarPayEntityType {

    entityType_Earning, entityType_Deduction, entityType_Interim, entityType_PackageComponent, entityType_Loan, entityType_Saving, entityType_Tax;

    @Override
    public String toString() {
        switch (this) {
            case entityType_Deduction:
                return "entityType_Deduction";
            case entityType_Interim:
                return "entityType_Interim";
            case entityType_PackageComponent:
                return "entityType_PackageComponent";
            case entityType_Loan:
                return "entityType_Loan";
            case entityType_Saving:
                return "entityType_Saving";
            case entityType_Earning:
                return "entityType_Earning";
            case entityType_Tax:
                return "entityType_Tax";
        }
        return "Unknown Type";
    }

    public static EnumVarPayEntityType stringToEnum(String paymentType) {
        if (paymentType.trim().equalsIgnoreCase("entityType_Earning")) {
            return entityType_Earning;
        }
        if (paymentType.trim().equalsIgnoreCase("entityType_Deduction")) {
            return entityType_Deduction;
        }
        if (paymentType.trim().equalsIgnoreCase("entityType_Interim")) {
            return entityType_Interim;
        }
        if (paymentType.trim().equalsIgnoreCase("entityType_Loan")) {
            return entityType_Loan;
        }
        if (paymentType.trim().equalsIgnoreCase("entityType_Saving")) {
            return entityType_Saving;
        }
        if (paymentType.trim().equalsIgnoreCase("entityType_PackageComponent")) {
            return entityType_PackageComponent;
        }
        if (paymentType.trim().equalsIgnoreCase("entityType_Tax")) {
            return entityType_Tax;
        }

        // Default
        return entityType_Interim;
    }

    /**
     * Converts a String (translated into the local locale) to an enumerated
     * type
     *
     * @param paymentTypeInLocalLanguage
     * @return
     */
    public static EnumVarPayEntityType localeStringToEnum(String paymentTypeInLocalLanguage) {
        if (paymentTypeInLocalLanguage.trim().equalsIgnoreCase(PageManager.translate("entityType_Earning"))) {
            return entityType_Earning;
        }
        if (paymentTypeInLocalLanguage.trim().equalsIgnoreCase(PageManager.translate("entityType_Deduction"))) {
            return entityType_Deduction;
        }
        if (paymentTypeInLocalLanguage.trim().equalsIgnoreCase(PageManager.translate("entityType_Interim"))) {
            return entityType_Interim;
        }
        if (paymentTypeInLocalLanguage.trim().equalsIgnoreCase(PageManager.translate("entityType_Loan"))) {
            return entityType_Loan;
        }
        if (paymentTypeInLocalLanguage.trim().equalsIgnoreCase(PageManager.translate("entityType_Saving"))) {
            return entityType_Saving;
        }
        if (paymentTypeInLocalLanguage.trim().equalsIgnoreCase(PageManager.translate("entityType_PackageComponent"))) {
            return entityType_PackageComponent;
        }
        if (paymentTypeInLocalLanguage.trim().equalsIgnoreCase(PageManager.translate("entityType_Tax"))) {
            return entityType_Tax;
        }

        // Default
        return entityType_Interim;
    }

    private String enumToLocaleString;
    /**
     * Converts enumVarPayEntityType to string (translated into the local
     * locale)
     *
     * @return
     */
    public String getEnumToLocaleString() {
        switch (this) {
            case entityType_Deduction:
                return PageManager.translate("entityType_Deduction");
            case entityType_Interim:
                return PageManager.translate("entityType_Interim");
            case entityType_PackageComponent:
                return PageManager.translate("entityType_PackageComponent");
            case entityType_Loan:
                return PageManager.translate("entityType_Loan");
            case entityType_Saving:
                return PageManager.translate("entityType_Saving");
            case entityType_Earning:
                return PageManager.translate("entityType_Earning");
            case entityType_Tax:
                return PageManager.translate("entityType_Tax");
        }
        return "Unknown Type";

    }

}
