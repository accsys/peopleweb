/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

/**
 *
 * @author Liam
 */
public class LocaleDefinition {

    private String displayName;
    private String country;
    private String language;

    @Override
    public String toString(){
        return this.displayName;
    }
    public LocaleDefinition(String displayName, String country, String language) {
        this.displayName = displayName;
        this.country = country;
        this.language = language;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

}
