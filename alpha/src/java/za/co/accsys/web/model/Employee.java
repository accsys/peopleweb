/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.SelectEvent;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.helperclasses.DateUtils;
import za.co.accsys.web.beans.EnumVarPayEntityType;
import za.co.accsys.web.helperclasses.JSONRecord;
import za.co.accsys.web.helperclasses.ServerPrefs;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.beans.ReportPrinter;

/**
 *
 * @author Liam
 */
public final class Employee {

    private SessionUser sessionUser;
    private String employeeID;
    private String payrollID;
    private String firstname;
    private String secondname;
    private String surname;
    private String employeeNumber;
    private String preferredName;
    private String title;
    private String initials;
    private String gender;
    private String race;
    private String maritalStatus;
    private String homeLanguage;
    private String idNumber;
    private String taxNumber;
    private String passportNumber;
    private Date dateOfBirth;
    private Date dateOfEngagement;
    private Date groupJoinDate;
    private Date dateOfDischarge;
    private String passportHeld;
    private String jobTitle;
    private boolean stillEngaged;
    private boolean removeDischargeDate;
    private PhysicalAddress physicalAddress;
    private PostalAddress postalAddress;
    private BankAccount bankAccount;
    private String costCentreName;
    private Payroll payroll;
    private String payrollName;
    private boolean canTransfer;
    private String rateOfPay;
    private String hoursPerDay;
    private String hoursPerWeek;
    private String hoursPerMonth;
    private String daysPerWeek;
    private String daysPerMonth;
    private String nettPay;
    private String tax;
    private String taxOnBonus;
    private String payPer;
    private String essLogin;
    private String adminLogin;
    private String essPassword;
    private String adminPassword;
    private String adminPasswordVerified;
    private String payslipPrintForPeriod;
    private String taxCertificatePrintForPeriod;

    private String nbMedDependents;
    private boolean administrator;
    private int runStatus = 1;
    private boolean closedForRun;
    private final EmployeeManager employeeManager;
    private String dischargeReason;
    private boolean engagedInThisPeriod = false;
    private boolean dischargedInThisPeriod = false;
    private boolean notEngagedYet = false;
    private String email;
    private String telHome;
    private String telWork;
    private String telCell;
    private String ytdRFI;
    private String ytdNonRFI;

    // This list represents the employee's pay package (earnings, deductions, loans, savings, interims, and company contributions)
    private List<PayEntityInstance> payEntityInstances;
    private List<YTDPayEntityInstance> ytdPayEntityInstances;
    private PayEntityInstance selectedPayEntityInstance;
    private String[] calcExceptions;

    /**
     * Default constructor
     *
     * @param sessionUser
     * @param employeeManager
     */
    public Employee(SessionUser sessionUser, EmployeeManager employeeManager) {
        this.sessionUser = sessionUser;
        physicalAddress = new PhysicalAddress(sessionUser);
        postalAddress = new PostalAddress(sessionUser);
        bankAccount = new BankAccount(sessionUser);
        this.employeeManager = employeeManager;
        // Get the start date of the open period of the selected payroll as the dateOfEngagement
        Payroll selectedPayroll = sessionUser.getPageManager().getCompany().getPayrollManager().getSelectedPayroll();
        if (selectedPayroll != null) {
            dateOfEngagement = selectedPayroll.getOpenPeriod().getStartDate();
        } else {
            dateOfEngagement = new java.util.Date();
        }
        setDefaults();
    }

    private void setDefaults() {
        // Addresses
        this.physicalAddress.setCountry("South Africa");
        this.postalAddress.setCountry("South Africa");
        // Payroll Info
        this.daysPerMonth = "21.67";
        this.hoursPerDay = "9";
        this.daysPerWeek = "5";
        this.nbMedDependents = "0";
        this.payPer = "Month";
        this.removeDischargeDate = false;
        this.payroll = sessionUser.getPageManager().getCompany().getPayrollManager().getSelectedPayroll();
        if (this.payroll != null) {
            this.payrollName = this.payroll.getPayrollName();
        }

        // Employee Number
        employeeNumber = Integer.toString(getNextCompanyEmployeeNumber());
    }

    /**
     * Returns TRUE if this person is only to be active some time in the future
     *
     * @return
     */
    public boolean isNotEngagedYet() {
        notEngagedYet = dateOfEngagement.after(payroll.getOpenPeriod().getEndDate());
        return notEngagedYet;
    }

    public String getYtdRFI() {
        return ytdRFI;
    }

    public void setYtdRFI(String ytdRFI) {
        this.ytdRFI = ytdRFI;
    }

    public String getYtdNonRFI() {
        return ytdNonRFI;
    }

    public void setYtdNonRFI(String ytdNonRFI) {
        this.ytdNonRFI = ytdNonRFI;
    }

    public String getPayslipPrintForPeriod() {
        if (payslipPrintForPeriod == null) {
            payslipPrintForPeriod = this.getPayroll().getOpenPeriodDateAsString();
        }
        return payslipPrintForPeriod;
    }

    public void setPayslipPrintForPeriod(String payslipPrintForPeriod) {
        this.payslipPrintForPeriod = payslipPrintForPeriod;
    }

    public String getTaxCertificatePrintForPeriod() {
        if (taxCertificatePrintForPeriod == null) {
            taxCertificatePrintForPeriod = getTaxCertificateSubmissionPeriods().get(0);
        }
        return taxCertificatePrintForPeriod;
    }

    public void setTaxCertificatePrintForPeriod(String taxCertificatePrintForPeriod) {
        this.taxCertificatePrintForPeriod = taxCertificatePrintForPeriod;
    }

    /**
     * Called when the user opts to link a PayEntity to an employee
     *
     * @param event
     */
    public void onNewEntityRowSelect(SelectEvent event) {
        PayEntity chosenEntity = ((PayEntity) event.getObject());
        // Now link this entity to this employee
        PayEntityInstance chosenEntityInstance = new PayEntityInstance(this);
        chosenEntityInstance.setPayEntity(chosenEntity);
        // If this payEntity is of type INPUT, or its a loan or saving, get some default values in.
        // Loan, set premium and balance
        if (chosenEntityInstance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_Loan) {
            chosenEntityInstance.setTempValue("0");
            chosenEntityInstance.setBalance("0");
        } else {
            // Saving, set premium
            if (chosenEntityInstance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_Saving) {
                chosenEntityInstance.setTempValue("0");
            } else {
                // Variable, of type INPUT
                // Saving, set premium
                if (chosenEntityInstance.getPayEntity().getCalculationFormula().trim().equalsIgnoreCase("INPUT")) {
                    chosenEntityInstance.setTempValue("0");
                }
            }
        }
        chosenEntityInstance.setPayrollPeriod(payroll.getOpenPeriod());
        payEntityInstances.add(chosenEntityInstance);
        RequestContext.getCurrentInstance().execute("javascript:window.location.reload()");
        // Finally, initiate a calc to incorporate this new entity
        saveEmployeePayPackage(chosenEntityInstance);

        // Are we missing any compulsory earnings/deductions?
        linkDefaultVariablesToPackage();

        waitForCalcCompletion();
    }

    /**
     * Called when the user opts to link a PayEntity to an employee
     *
     * @param event
     */
    public void onExistingEntityRowSelect(SelectEvent event) {
        selectedPayEntityInstance = ((PayEntityInstance) event.getObject());
    }

    /**
     * Returns TRUE of the employees's CLOSED_YN is 'Y' in P_CALC_EMPLOYEELIST
     *
     * @return
     */
    public boolean isClosedForRun() {
        return closedForRun;
    }

    public void setClosedForRun(boolean closedForRun) {
        this.closedForRun = closedForRun;
    }

    public String getNbMedDependents() {
        return nbMedDependents;
    }

    public void setNbMedDependents(String nbMedDependents) {
        if (this.nbMedDependents == null ? nbMedDependents != null : !this.nbMedDependents.equals(nbMedDependents)) {
            this.nbMedDependents = nbMedDependents;
            // Immediately update the person's info in Sybase
            saveEmployeePayrollInfo();
            // Only wait if there is something to wait for
            if (this.getPayEntityInstances().size() > 0) {
                waitForCalcCompletion();
            }
        }
    }

    public String getCostCentreName() {
        return costCentreName;
    }

    public void setCostCentreName(String costCentreName) {
        if ((this.costCentreName == null) || (!this.costCentreName.equals(costCentreName))) {
            this.costCentreName = costCentreName;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getDischargeReason() {
        return dischargeReason;
    }

    public void setDischargeReason(String dischargeReason) {
        if ((this.dischargeReason == null) || (!this.dischargeReason.equals(dischargeReason))) {
            this.dischargeReason = dischargeReason;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPayrollName() {
        return payrollName;
    }

    public String getNettPay() {
        return nettPay;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (this.email == null ? email != null : !this.email.equals(email)) {
            this.email = email;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getTelHome() {
        return telHome;
    }

    public void setTelHome(String telHome) {
        if (this.telHome == null ? telHome != null : !this.telHome.equals(telHome)) {
            this.telHome = telHome;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getTelWork() {
        return telWork;
    }

    public void setTelWork(String telWork) {
        if (this.telWork == null ? telWork != null : !this.telWork.equals(telWork)) {
            this.telWork = telWork;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getTelCell() {
        return telCell;
    }

    public void setTelCell(String telCell) {
        if (this.telCell == null ? telCell != null : !this.telCell.equals(telCell)) {
            this.telCell = telCell;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public Payroll getPayroll() {
        return payroll;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTaxOnBonus() {
        return taxOnBonus;
    }

    public void setTaxOnBonus(String taxOnBonus) {
        this.taxOnBonus = taxOnBonus;
    }

    public String getEssLogin() {
        return essLogin;
    }

    public void setEssLogin(String essLogin) {
        if ((this.essLogin == null) || (!this.essLogin.equals(essLogin))) {
            this.essLogin = essLogin;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAdminLogin() {
        return adminLogin;
    }

    public void setAdminLogin(String adminLogin) {
        if ((this.adminLogin == null) || (!this.adminLogin.equals(adminLogin))) {
            this.adminLogin = adminLogin;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public boolean isEngagedInThisPeriod() {

        engagedInThisPeriod = (dateOfEngagement != null) && (DateUtils.betweenDates(dateOfEngagement, payroll.getOpenPeriod().getStartDate(), payroll.getOpenPeriod().getEndDate()));
        return engagedInThisPeriod;
    }

    public boolean isDischargedInThisPeriod() {
        dischargedInThisPeriod = (dateOfDischarge != null) && (DateUtils.betweenDates(dateOfDischarge, payroll.getOpenPeriod().getStartDate(), payroll.getOpenPeriod().getEndDate()));
        return dischargedInThisPeriod;
    }

    public String getEssPassword() {
        return essPassword;
    }

    public void setEssPassword(String essPassword) {
        if ((this.essPassword == null) || (!this.essPassword.equals(essPassword))) {
            this.essPassword = essPassword;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    /**
     * @param adminPassword
     */
    public void setAdminPassword(String adminPassword) {
        if (adminPassword != null && adminPassword.length() > 0) {
            if ((this.adminPassword == null) || (!this.adminPassword.equals(adminPassword))) {
                this.adminPassword = adminPassword;
                // Compare with Password
                sessionUser.getPageManager().setChangesOnPage(verifyPassword(adminPassword, adminPasswordVerified));
            }
        }
    }

    public String getAdminPasswordVerified() {
        return adminPasswordVerified;
    }

    public void setAdminPasswordVerified(String adminPasswordVerified) {
        if (adminPasswordVerified != null && adminPasswordVerified.length() > 0) {
            if ((this.adminPasswordVerified == null) || (!this.adminPasswordVerified.equals(adminPasswordVerified))) {
                this.adminPasswordVerified = adminPasswordVerified;
                // Compare with Password
                sessionUser.getPageManager().setChangesOnPage(verifyPassword(adminPassword, adminPasswordVerified));
            }
        }
    }

    private boolean verifyPassword(String password, String verifPassword) {
        if (password == null || verifPassword == null) {
            return false;
        }

        if (password.equals(verifPassword)) {
            return true;
        } else {
            sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_WARN, PageManager.translate("PasswordsNotMatching"), PageManager.translate("PasswordsNotMatching"));
            return false;
        }
    }

    /**
     * This user has full admin rights
     *
     * @return
     */
    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        if (this.administrator != administrator) {
            // We are not allowed to remove Admin rights from the person using PeopleWeb.
            // If that happens, there'd be no way to access their account
            if ((sessionUser.getEmployeeId().equals(employeeID)) && (!administrator)) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_WARN, PageManager.translate("warningCannotRemoveAdminAccountFromSelf"), PageManager.translate("warningCannotRemoveAdminAccountFromSelf"));
            } else {
                this.administrator = administrator;
                if (!this.administrator) {
                    setAdminLogin(null);
                    setAdminPassword(null);
                }
                sessionUser.getPageManager().setChangesOnPage(true);
            }
        }
    }

    public PayEntityInstance getSelectedPayEntityInstance() {
        return selectedPayEntityInstance;
    }

    public void setSelectedPayEntityInstance(PayEntityInstance selectedPayEntityInstance) {
        this.selectedPayEntityInstance = selectedPayEntityInstance;
    }

    public void deleteSelectedPayEntityInstance() {
        if (this.selectedPayEntityInstance != null) {
            this.payEntityInstances.remove(selectedPayEntityInstance);
            selectedPayEntityInstance = null;
            RequestContext.getCurrentInstance().execute("javascript:window.location.reload()");
        }
    }

    public void setPayroll(Payroll payroll) {
        if ((this.payroll == null) || (!this.payroll.equals(payroll))) {
            this.payroll = payroll;
        }
    }

    public String getPayPer() {
        return payPer;
    }

    public void setPayPer(String payPer) {
        if ((this.payPer == null) || (!this.payPer.equals(payPer))) {
            this.payPer = payPer;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public void setNettPay(String nettPay) {
        this.nettPay = nettPay;
    }

    // When the bean changes the payroll name, we need to change the underlying payroll too
    public void setPayrollName(String payrollName) {
        if ((this.payrollName == null) || (!this.payrollName.equals(payrollName))) {
            this.payrollName = payrollName;
            this.payroll = sessionUser.getPageManager().getCompany().getPayrollManager().getPayrollWithName(payrollName);

            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public List<YTDPayEntityInstance> getYtdPayEntityInstances() {
        return ytdPayEntityInstances;
    }

    public void setYtdPayEntityInstances(List<YTDPayEntityInstance> ytdPayEntityInstances) {
        this.ytdPayEntityInstances = ytdPayEntityInstances;
    }

    /**
     * Not yet implemented. For future PeopleWare compatibility
     *
     * @return
     */
    public boolean isCanTransfer() {
        return canTransfer;
    }

    public void setCanTransfer(boolean canTransfer) {
        this.canTransfer = canTransfer;
    }

    public SessionUser getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
    }

    /**
     * Returns TRUE if the employee is engaged in the active period
     *
     * @return
     */
    public boolean isStillEngaged() {
        return stillEngaged;
    }

    public void setStillEngaged(boolean stillEngaged) {
        if (this.stillEngaged != stillEngaged) {
            this.stillEngaged = stillEngaged;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        if ((this.bankAccount == null) || (!this.bankAccount.equals(bankAccount))) {
            this.bankAccount = bankAccount;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public PhysicalAddress getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(PhysicalAddress physicalAddress) {
        if ((this.physicalAddress == null) || (!this.physicalAddress.equals(physicalAddress))) {
            this.physicalAddress = physicalAddress;
            sessionUser.getPageManager().setChangesOnPage(true);
        }

    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        if ((this.postalAddress == null) || (!this.postalAddress.equals(postalAddress))) {
            this.postalAddress = postalAddress;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        if ((this.jobTitle == null) || (!this.jobTitle.equals(jobTitle))) {
            this.jobTitle = jobTitle;
            sessionUser.getPageManager().setChangesOnPage(true);
        }

    }

    public String getPassportHeld() {
        return passportHeld;
    }

    public void setPassportHeld(String passportHeld) {
        if ((this.passportHeld == null) || (!this.passportHeld.equals(passportHeld))) {
            this.passportHeld = passportHeld;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        if ((this.initials == null) || (!this.initials.equals(initials))) {
            this.initials = initials;
            sessionUser.getPageManager().setChangesOnPage(true);
        }

    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        if ((this.gender == null) || (!this.gender.equals(gender))) {
            this.gender = gender;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        if ((this.race == null) || (!this.race.equals(race))) {
            this.race = race;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        if ((this.maritalStatus == null) || (!this.maritalStatus.equals(maritalStatus))) {
            this.maritalStatus = maritalStatus;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getHomeLanguage() {
        return homeLanguage;
    }

    public void setHomeLanguage(String homeLanguage) {
        if ((this.homeLanguage == null) || (!this.homeLanguage.equals(homeLanguage))) {
            this.homeLanguage = homeLanguage;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        if (idNumber != null) {

            if ((this.idNumber == null) || (!this.idNumber.equals(idNumber))) {
                this.idNumber = idNumber;
                if (this.idNumber.length() >= 8 && (this.dateOfBirth == null)) {
                    try {
                        String yy = idNumber.substring(0, 2);
                        String mm = idNumber.substring(2, 4);
                        String dd = idNumber.substring(4, 6);
                        if (Integer.valueOf(yy) < 30) {
                            yy = "20" + yy;
                        } else {
                            yy = "19" + yy;
                        }

                        Date tempDOB = DateUtils.formatDate(yy + "/" + mm + "/" + dd);
                        // [Liam] Something causes the day to be one less than what it should.
                        // I'm convinced it has something to do with the regional settings and/or timezones.
                        //tempDOB = DateUtils.addNDays(tempDOB, 2);

                        if (DateUtils.betweenDates(tempDOB, DateUtils.formatDate("1930/01/01"), DateUtils.formatDate("2020/01/01"))) {
                            setDateOfBirth(tempDOB);
                        }
                    } catch (Exception e) {
                        // Unable to convert DOB
                    }
                }
                sessionUser.getPageManager().setChangesOnPage(true);
            }
        }
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        if ((this.taxNumber == null) || (!this.taxNumber.equals(taxNumber))) {
            this.taxNumber = taxNumber;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        if ((this.passportNumber == null) || (!this.passportNumber.equals(passportNumber))) {
            this.passportNumber = passportNumber;
            sessionUser.getPageManager().setChangesOnPage(true);
        }

    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public String getDateOfBirthString() {
        return DateUtils.formatDate(dateOfBirth);
    }

    public void setDateOfBirth(Date dateOfBirth) {
        if ((this.dateOfBirth == null) || (!this.dateOfBirth.equals(dateOfBirth))) {
            this.dateOfBirth = dateOfBirth;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public Date getDateOfEngagement() {
        return dateOfEngagement;
    }

    public void updateDateOfEngagement(SelectEvent event) {
        Date date = (Date) event.getObject();
        setDateOfEngagement(date);
        sessionUser.getPageManager().setChangesOnPage(true);
    }

    public void updateDateOfDischarge(SelectEvent event) {
        Date date = (Date) event.getObject();
        setDateOfDischarge(date);
        sessionUser.getPageManager().setChangesOnPage(true);
    }

    public void updateGroupJoinDate(SelectEvent event) {
        Date date = (Date) event.getObject();
        setGroupJoinDate(date);
        sessionUser.getPageManager().setChangesOnPage(true);
    }

    public void updateDateOfBirth(SelectEvent event) {
        Date date = (Date) event.getObject();
        setDateOfBirth(date);
        sessionUser.getPageManager().setChangesOnPage(true);
    }

    public String getDateOfEngagementString() {
        return DateUtils.formatDate(dateOfEngagement);
    }

    public void setDateOfEngagement(Date dateOfEngagement) {
        if ((this.dateOfEngagement == null) || (!this.dateOfEngagement.equals(dateOfEngagement))) {
            this.dateOfEngagement = dateOfEngagement;
            sessionUser.getPageManager().setChangesOnPage(true);
            // Update the group join date if it is null
            if (groupJoinDate == null) {
                setGroupJoinDate(dateOfEngagement);
            }
        }
    }

    public Date getGroupJoinDate() {
        return groupJoinDate;
    }

    public String getGroupJoinDateString() {
        return DateUtils.formatDate(groupJoinDate);
    }

    public void setGroupJoinDate(Date groupJoinDate) {
        if ((this.groupJoinDate == null) || (!this.groupJoinDate.equals(groupJoinDate))) {
            this.groupJoinDate = groupJoinDate;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public Date getDateOfDischarge() {
        return dateOfDischarge;
    }

    public String getDateOfDischargeString() {
        return DateUtils.formatDate(dateOfDischarge);
    }

    public void setDateOfDischarge(Date dateOfDischarge) {

        if ((this.dateOfDischarge == null) || (!this.dateOfDischarge.equals(dateOfDischarge))) {
            Date oldDOD = this.dateOfDischarge;

            this.dateOfDischarge = dateOfDischarge;
            sessionUser.getPageManager().setChangesOnPage(true);

            if (dateOfDischarge != null) {
                this.removeDischargeDate = false;

                // Discharge before Engagement?
                if (dateOfDischarge.before(dateOfEngagement)) {
                    this.dateOfDischarge = oldDOD;
                } else {
                    // Discharged before open period?
                    if (payroll != null) {
                        if (dateOfDischarge.before(payroll.getOpenPeriod().getStartDate())) {
                            setStillEngaged(false);
                        }
                    }
                }
            }
            // Discharged staff are no longe administrators
            if (this.dateOfDischarge != null && this.isAdministrator()) {
                setAdministrator(false);
            }
        }
    }

    private boolean canDischarge;

    /**
     * One can only discharge a person if that person is not the present
     * operator
     *
     * @return
     */
    public boolean isCanDischarge() {
        return (!sessionUser.getEmployeeId().equals(this.employeeID));
    }

    public String getTitle() {
        return title;
    }

    public void undoDischarge() {
        this.dateOfDischarge = null;
        this.setStillEngaged(true);
        this.setDischargeReason(null);
        this.removeDischargeDate = true;
        // Recalculate the number of available credits
        sessionUser.getPageManager().reloadPayrollCreditsFromDB();

    }

    public void setTitle(String title) {
        if ((this.title == null) || (!this.title.equals(title))) {
            this.title = title;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPreferredName() {
        if (preferredName == null || preferredName.trim().isEmpty()) {
            preferredName = getFirstname();
        }
        return preferredName;
    }

    public void setPreferredName(String preferredName) {
        if ((this.preferredName == null) || (!this.preferredName.equals(preferredName))) {
            this.preferredName = preferredName;
            sessionUser.getPageManager().setChangesOnPage(true);
        }

    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        if ((this.firstname == null) || (!this.firstname.equals(firstname))) {
            this.firstname = firstname;
            if (this.initials == null || this.initials.isEmpty()) {
                this.initials = firstname.substring(0, 1);
            }
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        if ((this.surname == null) || (!this.surname.equals(surname))) {
            this.surname = surname;
            // Update default bank account detail?
            if (this.getBankAccount().getAccountName() == null || this.getBankAccount().getAccountName().isEmpty()) {
                this.getBankAccount().setAccountName((initials + " " + surname).trim());
            }
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getPayrollID() {
        return payrollID;
    }

    public void setPayrollID(String payrollID) {
        this.payrollID = payrollID;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        if ((this.employeeNumber == null) || (!this.employeeNumber.equals(employeeNumber))) {
            this.employeeNumber = employeeNumber;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        if ((this.secondname == null) || (!this.secondname.equals(secondname))) {
            this.secondname = secondname;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    /**
     * Constructs the string representation for menus and drop down lists of an
     * employee
     *
     * @return
     */
    private String toString;

    public String getToString() {
        if (this.surname == null) {
            return "New Employee";
        } else {
            return surname.trim() + ", " + firstname.trim() + " [" + employeeNumber.trim() + "]";
        }
    }

    private String toOverviewString;

    public String getToOverviewString() {
        String rslt = null;
        if (firstname != null && surname != null) {
            rslt = firstname + " " + surname;
        }
        if (payrollName != null && payroll != null) {
            rslt = rslt + " [" + payrollName + " - " + DateUtils.formatDate(payroll.getOpenPeriod().getStartDate())
                    + " to " + DateUtils.formatDate(payroll.getOpenPeriod().getEndDate()) + "]";
        }
        return rslt;

    }

    /**
     * Runs a web service call to retrieve all the address details for this
     * employee
     *
     * @return
     */
    public boolean loadEmployeeAddressData() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));

        // Call the GetValue service
        ArrayList<NameValuePair> employeeAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        employeeAttributes = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_GetEmployeeAddressData, listToSend);

        // What did we get back?
        // Step through each record, and construct an EMPLOYEE instance
        for (NameValuePair nvp : employeeAttributes) {
            //
            // Physical Address
            //
            if (nvp.getName().equalsIgnoreCase("physCountry")) {
                physicalAddress.setCountry(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physProvince")) {
                physicalAddress.setProvince(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physPostalCode")) {
                physicalAddress.setPostalCode(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physUnitNo")) {
                physicalAddress.setUnitNo(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physComplex")) {
                physicalAddress.setComplex(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physStreetNo")) {
                physicalAddress.setStreetNo(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physStreetOrFarmName")) {
                physicalAddress.setStreetOrFarmName(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physSuburb")) {
                physicalAddress.setSuburb(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physCityOrTown")) {
                physicalAddress.setCityOrTown(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postalSameAsPhysical")) {
                physicalAddress.setPostalSameAsPhysical(nvp.getValue() != null && nvp.getValue().equals("1"));
            }
            //
            // Postal Address
            //
            if (nvp.getName().equalsIgnoreCase("postCountry")) {
                postalAddress.setCountry(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postProvince")) {
                postalAddress.setProvince(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postPostCode")) {
                postalAddress.setPostalCode(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postPostOffice")) {
                postalAddress.setPostOffice(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postPostalAgencyOrSubUnit")) {
                postalAddress.setPostalAgencyOrSubUnit(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postBoxOrBagNumber")) {
                postalAddress.setBoxOrBagNumber(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postIsPOBox")) {
                postalAddress.setIsPOBox(nvp.getValue() != null && nvp.getValue().equals("1"));
            }
            if (nvp.getName().equalsIgnoreCase("postIsPrivateBag")) {
                postalAddress.setIsPrivateBag(nvp.getValue() != null && nvp.getValue().equals("1"));
            }
            if (nvp.getName().equalsIgnoreCase("postPostalAddressIsCareOf")) {
                postalAddress.setPostalAddressIsCareOf(nvp.getValue() != null && nvp.getValue().equals("1"));
            }
            if (nvp.getName().equalsIgnoreCase("postCityOrTown")) {
                postalAddress.setCityOrTown(nvp.getValue());
            }
        }
        return true;
    }

    /**
     * Runs a web service call to retrieve the first bank account for this
     * employee
     *
     * @return
     */
    public boolean loadEmployeeAccountData() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));

        // Call the GetValue service
        ArrayList<NameValuePair> employeeAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        employeeAttributes = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_GetEmployeeAccountData, listToSend);

        // What did we get back?
        // Step through each record, and construct an EMPLOYEE instance
        for (NameValuePair nvp : employeeAttributes) {
            //
            // Bank Account details
            //
            if (nvp.getName().equalsIgnoreCase("accountID")) {
                bankAccount.setAccountID(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountType")) {
                bankAccount.setAccountType(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountName")) {
                bankAccount.setAccountName(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountDescription")) {
                bankAccount.setAccountDescription(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountBank")) {
                bankAccount.setAccountBank(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountBranch")) {
                bankAccount.setAccountBranch(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountBankCode")) {
                bankAccount.setAccountBankCode(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountBranchCode")) {
                bankAccount.setAccountBranchCode(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountNumber")) {
                bankAccount.setAccountNumber(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountHolderRelationship")) {
                bankAccount.setAccountHolderRelationship(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountLineNumber")) {
                bankAccount.setAccountLineNumber(nvp.getValue());
            }

        }
        return true;
    }

    /**
     * Runs a web service call to retrieve the employee's nett pay for this
     * period
     *
     * @return
     */
    public boolean loadEmployeePayrollInfo() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, payroll.getPayrollID()));

        // Call the GetValue service
        ArrayList<NameValuePair> employeeAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        employeeAttributes = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_GetEmployeePayrollInfo, listToSend);

        // What did we get back?
        // Step through each record, and construct an EMPLOYEE instance
        for (NameValuePair nvp : employeeAttributes) {
            if (nvp.getName().equalsIgnoreCase("nettPay")) {
                nettPay = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("tax")) {
                tax = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("taxOnBonus")) {
                taxOnBonus = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("rateOfPay")) {
                rateOfPay = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("hoursPerDay")) {
                hoursPerDay = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("hoursPerWeek")) {
                hoursPerWeek = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("hoursPerMonth")) {
                hoursPerMonth = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("daysPerWeek")) {
                daysPerWeek = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("daysPerMonth")) {
                daysPerMonth = nvp.getValue();
            }
            if (nvp.getName().equalsIgnoreCase("payPer")) {
                payPer = PageManager.translateFromEnglishToLocale(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("nbMedDependents")) {
                nbMedDependents = PageManager.translateFromEnglishToLocale(nvp.getValue());
            }

        }

        return true;
    }

    public String getRateOfPay() {
        return PageManager.formatStringAsCurrency(rateOfPay, getSessionUser().getUserLocale());
    }

    public double getDoubleValue(String aValue) {
        if (aValue == null || rateOfPay.trim().length() == 0) {
            aValue = "0";
        }
        double f = Double.parseDouble(aValue);
        return f;
    }

    /**
     * Constructs a display of the value + unit or currency + value
     *
     * @return
     */
    private String currencyAndPayRate;

    public String getCurrencyAndPayRate() {
        if (rateOfPay == null || rateOfPay.trim().length() == 0) {
            rateOfPay = "0";
        }
        currencyAndPayRate = payroll.getCurrencyString() + " " + PageManager.formatStringAsCurrency(rateOfPay, getSessionUser().getUserLocale());
        return currencyAndPayRate;
    }

    public void setRateOfPay(String rateOfPay) {
        if ((this.rateOfPay == null)
                || (!PageManager.formatStringAsCurrency(this.rateOfPay, getSessionUser().getUserLocale()).equals(PageManager.formatStringAsCurrency(rateOfPay, getSessionUser().getUserLocale())))) {
            this.rateOfPay = rateOfPay;
            // Immediately update the person's info in Sybase
            if (employeeID != null) {
                saveEmployeePayrollInfo();
            }
            // Only wait if there is something to wait for
            if (this.getPayEntityInstances() != null) {
                if (this.getPayEntityInstances().size() > 0) {
                    waitForCalcCompletion();
                }
            }
        }
    }

    public String getHoursPerDay() {
        return hoursPerDay;
    }

    /**
     * We want to automatically calculate the hours per month based on hours per
     * day and days per month
     *
     * @param sHrsPerDay
     * @param sDaysPerMonth
     * @return
     */
    private String calcHoursPerMonth(String sHrsPerDay, String sDaysPerMonth) {
        String rslt = "0";
        // Update HoursPerMonth
        if ((sHrsPerDay != null) && (sDaysPerMonth != null)) {
            try {
                float fHrsPerDay = Float.parseFloat(sHrsPerDay);
                float fDaysPerMonth = Float.parseFloat(sDaysPerMonth);
                rslt = String.valueOf(fHrsPerDay * fDaysPerMonth);
            } catch (NumberFormatException e) {
                return "";
            }
        }
        return rslt;
    }

    public void setHoursPerDay(String hoursPerDay) {
        if ((this.hoursPerDay == null)
                || (!this.hoursPerDay.equals(hoursPerDay))) {
            this.hoursPerDay = hoursPerDay;

            // Update HoursPerMonth
            this.hoursPerMonth = calcHoursPerMonth(this.hoursPerDay, this.daysPerMonth);

            // Immediately update the person's info in Sybase
            saveEmployeePayrollInfo();
            // Only wait if there is something to wait for
            if (this.getPayEntityInstances().size() > 0) {
                waitForCalcCompletion();
            }
        }

    }

    public String getHoursPerWeek() {
        return hoursPerWeek;
    }

    public void setHoursPerWeek(String HoursPerWeek) {
        this.hoursPerWeek = HoursPerWeek;
    }

    public String getHoursPerMonth() {
        return hoursPerMonth;
    }

    public void setHoursPerMonth(String HoursPerMonth) {
        this.hoursPerMonth = HoursPerMonth;
    }

    public String getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(String daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public String getDaysPerMonth() {
        return daysPerMonth;
    }

    public void setDaysPerMonth(String daysPerMonth) {
        if ((this.daysPerMonth == null)
                || (!this.daysPerMonth.equals(daysPerMonth))) {
            this.daysPerMonth = daysPerMonth;

            // Update HoursPerMonth
            this.hoursPerMonth = calcHoursPerMonth(this.hoursPerDay, this.daysPerMonth);

            // Immediately update the person's info in Sybase
            saveEmployeePayrollInfo();
            // Only wait if there is something to wait for
            if (this.getPayEntityInstances().size() > 0) {
                waitForCalcCompletion();
            }

        }
    }

    /**
     * This method is used to add particular Earnings/Deductions/Etc to employee
     * packages, depending on payroll country, payroll type, etc.
     */
    private boolean linkDefaultVariablesToPackage() {
        // South African Payroll?
        if (this.getPayroll().isSouthAfrica()) {
            // Add UIF
            boolean hasUIFee = false;
            boolean hasUIFco = false;
            for (PayEntityInstance pe : payEntityInstances) {
                // Company Contribution
                if (pe.getPayEntity().getIndicatorCodes().contains(Constants.INDCODE_UIF_CO)) {
                    hasUIFco = true;
                }
                // Employe Contribution
                if (pe.getPayEntity().getIndicatorCodes().contains(Constants.INDCODE_UIF_EE)) {
                    hasUIFee = true;
                }

            }
            // Are we missing UIF-CO?
            if (!hasUIFco) {
                if (sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntitiesWithIndCode(Constants.INDCODE_UIF_CO).size() > 0) {
                    PayEntity uifCO = sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntitiesWithIndCode(Constants.INDCODE_UIF_CO).get(0);
                    PayEntityInstance instCo = new PayEntityInstance(this);
                    instCo.setPayEntity(uifCO);
                    payEntityInstances.add(instCo);
                    saveEmployeePayPackage(instCo);
                }
            }
            // Are we missing UIF-EE?
            if (!hasUIFee) {
                if (sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntitiesWithIndCode(Constants.INDCODE_UIF_EE).size() > 0) {
                    PayEntity uifEE = sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntitiesWithIndCode(Constants.INDCODE_UIF_EE).get(0);
                    PayEntityInstance instEE = new PayEntityInstance(this);
                    instEE.setPayEntity(uifEE);
                    payEntityInstances.add(instEE);
                    saveEmployeePayPackage(instEE);
                }
            }
        }
        return true;
    }

    /**
     * Returns the list of loans, savings, and variables linked to this person's
     * package.
     *
     * @return
     */
    public List<PayEntityInstance> getPayEntityInstances() {
        return payEntityInstances;
    }

    /**
     * Returns a list of USED PayEntities of type EARNING
     *
     * @return
     */
    public List<PayEntityInstance> getPayEntityInstances_Earnings() {
        List<PayEntityInstance> rslt = new ArrayList<>();
        if (payEntityInstances != null) {
            for (PayEntityInstance instance : payEntityInstances) {
                if (!instance.isUnlinked()) {
                    if (instance.getPayEntity() != null) {
                        if (instance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_Earning) {
                            rslt.add(instance);
                        }
                    }
                }
            }
        }
        return rslt;
    }

    /**
     * Returns the total earnings (plus currency)
     *
     * @return
     */
    public String getCurrencyAndValue_Earnings_Total() {
        double total = 0;
        for (PayEntityInstance inst : getPayEntityInstances_Earnings()) {
            total += getDoubleValue(inst.getCalcValue());
        }

        return payroll.getCurrencyString() + " "
                + PageManager.formatStringAsCurrency(String.valueOf(total), getSessionUser().getUserLocale());
    }

    /**
     * Returns the total deductions (plus currency)
     *
     * @return
     */
    public String getCurrencyAndValue_Deductions_Total() {
        double total = 0;
        for (PayEntityInstance inst : getPayEntityInstances_Deductions()) {
            total += getDoubleValue(inst.getCalcValue());
        }

        return payroll.getCurrencyString() + " "
                + PageManager.formatStringAsCurrency(String.valueOf(total), getSessionUser().getUserLocale());
    }

    /**
     * Returns a list of UNUSED PayEntities by this employee - of type EARNING
     *
     * @return
     */
    public List<PayEntity> getPayEntities_UnusedEarnings() {
        List<PayEntity> rslt = new ArrayList<>();
        // Payroll Type
        String prType = getPayroll().getPayrollType();
        // PayPer Type
        String ppType = this.getPayPer();
        for (PayEntity payEntity : sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntities()) {
            if (payEntity != null) {
                // Earnings
                if (payEntity.getEntityType() == EnumVarPayEntityType.entityType_Earning) {
                    // Payroll Type
                    if ((payEntity.getPayrollType().equalsIgnoreCase(PageManager.translateFromEnglishToLocale("All")))
                            || (payEntity.getPayrollType().equalsIgnoreCase(prType))) {
                        // PayPer Type
                        if ((payEntity.getPayPerType().equalsIgnoreCase(PageManager.translateFromEnglishToLocale("All")))
                                || (payEntity.getPayPerType().equalsIgnoreCase(ppType))) {
                            // Tax Country
                            String entityCountry = payEntity.getTaxCountry();
                            String payrollCountry = this.getPayroll().getTaxCountry();
                            if (entityCountry.isEmpty() || entityCountry.equalsIgnoreCase(payrollCountry)) {

                                boolean inUse = false;
                                // Is the payEntity linked to this employee?
                                for (PayEntityInstance payEntityInstance : payEntityInstances) {
                                    if (payEntityInstance.getPayEntity() != null) {
                                        if (payEntityInstance.getPayEntity().equals(payEntity)) {
                                            inUse = true;
                                        }
                                    }
                                }
                                if (!inUse) {
                                    rslt.add(payEntity);
                                }
                            }
                        }
                    }
                }
            }
        }
        return rslt;
    }

    public List<PayEntityInstance> getPayEntityInstances_Deductions() {
        List<PayEntityInstance> rslt = new ArrayList<>();
        if (payEntityInstances != null) {
            for (PayEntityInstance instance : payEntityInstances) {
                if (!instance.isUnlinked()) {
                    if (instance.getPayEntity() != null) {
                        if (instance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_Deduction
                                || instance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_Loan
                                || instance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_Saving
                                || instance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_Tax) {
                            rslt.add(instance);
                        }
                    }
                }
            }
        }
        return rslt;
    }

    /**
     * Returns a list of UNUSED PayEntities by this employee - of type
     * DEDUCTIONS
     *
     * @return
     */
    public List<PayEntity> getPayEntities_UnusedDeductions() {
        List<PayEntity> rslt = new ArrayList<>();

        String prType = PageManager.translateFromEnglishToLocale(getPayroll().getPayrollType());
        for (PayEntity payEntity : sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntities()) {
            if (payEntity != null) {
                if (payEntity.getEntityType() == EnumVarPayEntityType.entityType_Deduction
                        || payEntity.getEntityType() == EnumVarPayEntityType.entityType_Loan
                        || payEntity.getEntityType() == EnumVarPayEntityType.entityType_Saving) {
                    if ((payEntity.getPayrollType().equalsIgnoreCase(PageManager.translateFromEnglishToLocale("All")))
                            || (PageManager.translateFromEnglishToLocale(payEntity.getPayrollType()).equalsIgnoreCase(prType))) {
                        // Tax Country
                        String entityCountry = payEntity.getTaxCountry();
                        String payrollCountry = this.getPayroll().getTaxCountry();
                        if (entityCountry.isEmpty() || entityCountry.equalsIgnoreCase(payrollCountry)) {
                            boolean inUse = false;
                            // Is the payEntity linked to this employee?
                            for (PayEntityInstance payEntityInstance : payEntityInstances) {
                                if (payEntityInstance.getPayEntity() != null) {
                                    if (payEntityInstance.getPayEntity().equals(payEntity)) {
                                        inUse = true;
                                    }
                                }
                            }
                            if (!inUse) {
                                rslt.add(payEntity);
                            }
                        }
                    }
                }
            }
        }
        return rslt;
    }

    public List<PayEntityInstance> getPayEntityInstances_NonPayable() {
        List<PayEntityInstance> rslt = new ArrayList<>();
        if (payEntityInstances != null) {
            for (PayEntityInstance instance : payEntityInstances) {
                if (!instance.isUnlinked()) {
                    if (instance.getPayEntity() != null) {
                        if (instance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_Interim
                                || instance.getPayEntity().getEntityType() == EnumVarPayEntityType.entityType_PackageComponent) {
                            rslt.add(instance);
                        }
                    }
                }
            }
        }
        return rslt;
    }

    /**
     * Returns a list of UNUSED PayEntities by this employee - of type
     * NON-PAYABLE
     *
     * @return
     */
    private ArrayList<PayEntity> payEntities_UnusedNonPayable;

    public ArrayList<PayEntity> getPayEntities_UnusedNonPayable() {
        payEntities_UnusedNonPayable = new ArrayList<>();
        String prType = getPayroll().getPayrollType();

        for (PayEntity payEntity : sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntities()) {
            if (payEntity != null) {
                if (payEntity.getEntityType() == EnumVarPayEntityType.entityType_Interim
                        || payEntity.getEntityType() == EnumVarPayEntityType.entityType_PackageComponent) {
                    if ((payEntity.getPayrollType().equalsIgnoreCase(PageManager.translateFromEnglishToLocale("All")))
                            || (payEntity.getPayrollType().equalsIgnoreCase(prType))) {
                        // Tax Country
                        String entityCountry = payEntity.getTaxCountry();
                        String payrollCountry = this.getPayroll().getTaxCountry();
                        if (entityCountry.isEmpty() || entityCountry.equalsIgnoreCase(payrollCountry)) {
                            boolean inUse = false;
                            // Is the payEntity linked to this employee?
                            for (PayEntityInstance payEntityInstance : payEntityInstances) {
                                if (payEntityInstance.getPayEntity() != null) {
                                    if (payEntityInstance.getPayEntity().equals(payEntity)) {
                                        inUse = true;
                                    }
                                }
                            }
                            if (!inUse) {
                                payEntities_UnusedNonPayable.add(payEntity);
                            }
                        }
                    }
                }
            }
        }
        return payEntities_UnusedNonPayable;
    }

    public void setPayEntityInstances(List<PayEntityInstance> payEntityInstances) {
        this.payEntityInstances = payEntityInstances;
    }

    /**
     * Runs a web service call to retrieve the list of earnings/deductions/etc
     * for this employee
     *
     * @return
     */
    public boolean loadEmployeePayPackage() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, payroll.getPayrollID()));

        // List that contains the employees pay entities
        payEntityInstances = new ArrayList<>();
        PayrollPeriod openPeriod = payroll.getOpenPeriod();

        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetEmployeePackage, listToSend);

        // What did we get back?
        // The result is a JSON Array with records per cost centre
        for (JSONRecord jsonRecord : listRetreived) {

            String entityID = null;
            String entityType = null;
            String defaultValue = null;
            String tempValue = null;
            String calcValue = null;
            String activeFrom = null;
            String activeTo = null;
            String balance = null;
            String interpretedFormula = null;
            boolean rfi = false;

            // Array, each record containing name/value pairs for the employees
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            // Step through each record, and construct a COST CENTRE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("entityID")) {
                    entityID = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("entityType")) {
                    entityType = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("defaultValue")) {
                    defaultValue = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("tempValue")) {
                    tempValue = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("calcValue")) {
                    calcValue = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("activeFrom")) {
                    activeFrom = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("activeTo")) {
                    activeTo = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("balance")) {
                    balance = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("rfi")) {
                    rfi = nvp.getValue().equalsIgnoreCase("Y");
                }
                if (nvp.getName().equalsIgnoreCase("interpretedFormula")) {
                    interpretedFormula = nvp.getValue();
                }

            }

            // If the value was T-Typed, the interpreted formula should be changed.
            if (tempValue != null && tempValue.trim().length() > 0) {
                interpretedFormula = PageManager.translate("payslip_ValueOverwritten");
            }
            // Now that we have all the info
            // 1. Fetch the appropriate payEntity from the Session Scoped list
            PayEntity payEntity = getGlobalPayEntity(entityID, entityType);
            if (payEntity != null) {
                // 2. Create a PayEntityInstance
                PayEntityInstance payEntityInstance = new PayEntityInstance(this);
                {
                    payEntityInstance.setPayEntity(payEntity);
                    payEntityInstance.setPayrollPeriod(openPeriod);
                    payEntityInstance.initiateTempValue(tempValue);
                    payEntityInstance.initiateBalance(balance);
                    payEntityInstance.setCalcValue(calcValue);
                    payEntityInstance.setInterpretedFormula(interpretedFormula);
                    payEntityInstance.setRfi(rfi);
                }
                payEntityInstances.add(payEntityInstance);
            }
        }

        // Now that we've loaded the official VARIABLES, LOANS and SAVINGS, we need to convert 
        // Tax and TaxOnBonus into PayEntityInstances
        if (tax != null && getDoubleValue(tax) > 0) {
            PayEntityInstance instTax = new PayEntityInstance(this);
            {
                PayEntity peTax = new PayEntity(sessionUser);
                peTax.setEntityType(EnumVarPayEntityType.entityType_Tax);
                peTax.setCaption("Tax");
                peTax.setName("Tax");
                instTax.setPayEntity(peTax);
                instTax.setCalcValue(tax);
                instTax.setPayrollPeriod(openPeriod);
                payEntityInstances.add(instTax);
            }
        }
        if (taxOnBonus != null && getDoubleValue(taxOnBonus) > 0) {
            PayEntityInstance instTax = new PayEntityInstance(this);
            {
                PayEntity peTax = new PayEntity(sessionUser);
                peTax.setEntityType(EnumVarPayEntityType.entityType_Tax);
                peTax.setCaption("Tax On Bonus");
                peTax.setName("Tax On Bonus");
                instTax.setPayEntity(peTax);
                instTax.setCalcValue(taxOnBonus);
                instTax.setPayrollPeriod(openPeriod);
                payEntityInstances.add(instTax);
            }
        }

        sessionUser.getPageManager().setChangesOnPage(false);
        return true;
    }

    /**
     * Runs a web service call to retrieve the list of ytd values for
     * earnings/deductions/etc for this employee
     *
     * @return
     */
    public boolean loadEmployeeYtdPayPackage() {
        ytdRFI = "0";
        ytdNonRFI = "0";
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, payroll.getPayrollID()));

        // List that contains the employees pay entities
        ytdPayEntityInstances = new ArrayList<>();
        PayrollPeriod openPeriod = payroll.getOpenPeriod();

        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetYTDEmployeePackage, listToSend);

        // What did we get back?
        // The result is a JSON Array with records per cost centre
        for (JSONRecord jsonRecord : listRetreived) {

            String entityID = null;
            String entityType = null;
            String ytdValue = null;
            String nbCalculations = null;

            // Array, each record containing name/value pairs for the employees
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            // Step through each record, and construct a COST CENTRE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("entityID")) {
                    entityID = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("entityType")) {
                    entityType = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("ytdValue")) {
                    ytdValue = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("nbCalculations")) {
                    nbCalculations = nvp.getValue();
                }

            }
            // Now that we have all the info
            // 1. Fetch the appropriate payEntity from the Session Scoped list
            PayEntity payEntity = getGlobalPayEntity(entityID, entityType);
            if (payEntity == null) {
                // This variable was used, has YTD values, but do not exist in the global variable list.  Why not?
            } else {
                // 2. Create a PayEntityInstance
                YTDPayEntityInstance ytdPayEntityInstance = new YTDPayEntityInstance(this);
                {
                    ytdPayEntityInstance.setPayEntity(payEntity);
                    ytdPayEntityInstance.setPayrollPeriod(openPeriod);
                    ytdPayEntityInstance.setYtdValue(ytdValue);
                    ytdPayEntityInstance.setYtdCount(nbCalculations);
                }
                ytdPayEntityInstances.add(ytdPayEntityInstance);
            }

            // YTD RFI (ID=-10)
            if ((Double.parseDouble(entityID) == -10) && (this.getPayroll().isSouthAfrica())) {
                if (Double.parseDouble(ytdValue) != 0) {
                    ytdRFI = ytdValue;
                }
            }
            // YTD Non-RFI (ID=-20)
            if ((Double.parseDouble(entityID) == -20) && (this.getPayroll().isSouthAfrica())) {
                if (Double.parseDouble(ytdValue) != 0) {
                    ytdNonRFI = ytdValue;
                }
            }
        }

        return true;
    }

    /**
     * Fetches a particular PayEntity from the global (session scoped) list of
     * PayEntities
     *
     * @param entityID
     * @param sEntityType
     * @return
     */
    private PayEntity getGlobalPayEntity(String entityID, String sEntityType) {
        PayEntity rslt = null;
        // GenericVariable can either be an Earning, Deduction, Interim, or PackageComponent
        if (sEntityType.equalsIgnoreCase("entityType_GenericVariable")) {
            // Earning?
            rslt = sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntity(entityID, EnumVarPayEntityType.entityType_Earning);
            // Deduction?
            if (rslt == null) {
                rslt = sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntity(entityID, EnumVarPayEntityType.entityType_Deduction);
            }
            // Interim?
            if (rslt == null) {
                rslt = sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntity(entityID, EnumVarPayEntityType.entityType_Interim);
            }
            // PackageComponent?
            if (rslt == null) {
                rslt = sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntity(entityID, EnumVarPayEntityType.entityType_PackageComponent);
            }
        }
        // Loans
        if (sEntityType.equalsIgnoreCase("entityType_Loan")) {
            rslt = sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntity(entityID, EnumVarPayEntityType.entityType_Loan);
        }
        // Savings
        if (sEntityType.equalsIgnoreCase("entityType_Saving")) {
            rslt = sessionUser.getPageManager().getPayEntityManager().getGlobalPayEntity(entityID, EnumVarPayEntityType.entityType_Saving);
        }
        String entityName = "";
        if (rslt != null) {
            entityName = rslt.getName();
        }
        return rslt;
    }

    private String boolToBit(boolean value) {
        if (value) {
            return "1";
        } else {
            return "0";
        }
    }

    /**
     * Runs a web service call to update address information for this employee
     *
     * @return
     */
    public boolean saveEmployeeAddressData() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));
        listToSend.add(new BasicNameValuePair("physCountry", physicalAddress.getCountry()));
        listToSend.add(new BasicNameValuePair("physProvince", physicalAddress.getProvince()));
        listToSend.add(new BasicNameValuePair("physPostalCode", physicalAddress.getPostalCode()));
        listToSend.add(new BasicNameValuePair("physUnitNo", physicalAddress.getUnitNo()));
        listToSend.add(new BasicNameValuePair("physComplex", physicalAddress.getComplex()));
        listToSend.add(new BasicNameValuePair("physStreetNo", physicalAddress.getStreetNo()));
        listToSend.add(new BasicNameValuePair("physStreetOrFarmName", physicalAddress.getStreetOrFarmName()));
        listToSend.add(new BasicNameValuePair("physSuburb", physicalAddress.getSuburb()));
        listToSend.add(new BasicNameValuePair("physCityOrTown", physicalAddress.getCityOrTown()));
        listToSend.add(new BasicNameValuePair("postalSameAsPhysical", boolToBit(physicalAddress.isPostalSameAsPhysical())));
        listToSend.add(new BasicNameValuePair("postCountry", postalAddress.getCountry()));
        listToSend.add(new BasicNameValuePair("postProvince", postalAddress.getProvince()));
        listToSend.add(new BasicNameValuePair("postPostCode", postalAddress.getPostalCode()));
        listToSend.add(new BasicNameValuePair("postPostOffice", postalAddress.getPostOffice()));
        listToSend.add(new BasicNameValuePair("postPostalAgencyOrSubUnit", postalAddress.getPostalAgencyOrSubUnit()));
        listToSend.add(new BasicNameValuePair("postBoxOrBagNumber", postalAddress.getBoxOrBagNumber()));
        listToSend.add(new BasicNameValuePair("postIsPOBox", boolToBit(postalAddress.isIsPOBox())));
        listToSend.add(new BasicNameValuePair("postIsPrivateBag", boolToBit(postalAddress.isIsPrivateBag())));
        listToSend.add(new BasicNameValuePair("postPostalAddressIsCareOf", boolToBit(postalAddress.isPostalAddressIsCareOf())));
        listToSend.add(new BasicNameValuePair("postCityOrTown", postalAddress.getCityOrTown()));

        // Call the GetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetEmployeeAddressData, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {

            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                return (nvp.getValue().equalsIgnoreCase("OK"));
            }
        }
        return false;
    }

    /**
     * Runs a web service call to update account information for this employee
     *
     * @return
     */
    public boolean saveEmployeeAccountData() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));
        listToSend.add(new BasicNameValuePair("accountType", bankAccount.getAccountType()));
        listToSend.add(new BasicNameValuePair("accountName", bankAccount.getAccountName()));
        listToSend.add(new BasicNameValuePair("accountDescription", bankAccount.getAccountDescription()));
        listToSend.add(new BasicNameValuePair("accountBank", bankAccount.getAccountBank()));
        listToSend.add(new BasicNameValuePair("accountBranch", bankAccount.getAccountBranch()));
        listToSend.add(new BasicNameValuePair("accountBankCode", bankAccount.getAccountBankCode()));
        listToSend.add(new BasicNameValuePair("accountBranchCode", bankAccount.getAccountBranchCode()));
        listToSend.add(new BasicNameValuePair("accountNumber", bankAccount.getAccountNumber()));
        listToSend.add(new BasicNameValuePair("accountHolderRelationship", bankAccount.getAccountHolderRelationship()));
        listToSend.add(new BasicNameValuePair("accountLineNumber", bankAccount.getAccountLineNumber()));

        // Call the GetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetEmployeeAccountData, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {

            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                return (nvp.getValue().equalsIgnoreCase("OK"));
            }
        }
        return false;
    }

    public boolean saveEmployeePayPackage() {
        for (PayEntityInstance payEntityInstance : payEntityInstances) {
            if (!saveEmployeePayPackage(payEntityInstance)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Runs a web service call to update employee's CONFIGURE PACKAGE
     *
     * @param payEntityInstance
     * @return
     */
    public boolean saveEmployeePayPackage(PayEntityInstance payEntityInstance) {

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, payrollID));
        // @entityID, @entityType, @value, @balance, @referenceNo
        listToSend.add(new BasicNameValuePair("entityID", payEntityInstance.getPayEntity().getEntityID()));
        listToSend.add(new BasicNameValuePair("entityType", payEntityInstance.getPayEntity().getEntityType().toString()));
        // We need to distinguish between TEMP values, and cleared TEMP values.
        listToSend.add(new BasicNameValuePair("value", payEntityInstance.getTempValue()));
        if (payEntityInstance.getPayEntity().getEntityType().equals(EnumVarPayEntityType.entityType_Loan) && payEntityInstance.getTempValue() != null) {
            listToSend.add(new BasicNameValuePair("balance", null));
        } else if (payEntityInstance.getPayEntity().getEntityType().equals(EnumVarPayEntityType.entityType_Loan) && payEntityInstance.getTempValue() == null) {
            listToSend.add(new BasicNameValuePair("balance", payEntityInstance.getBalance()));
        } else {
            listToSend.add(new BasicNameValuePair("balance", payEntityInstance.getBalance()));
        }
        if (payEntityInstance.isRfi()) {
            listToSend.add(new BasicNameValuePair("rfi", "Y"));
        } else {
            listToSend.add(new BasicNameValuePair("rfi", "N"));
        }
        listToSend.add(new BasicNameValuePair("referenceNo", payEntityInstance.getReferenceNo()));
        if (payEntityInstance.isUnlinked()) {
            listToSend.add(new BasicNameValuePair("unlink", "yes"));
        }
        if (payEntityInstance.isRemoveTempValue()) {
            listToSend.add(new BasicNameValuePair("removeTempVal", "yes"));
        }

        // Call the SetPackage service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetEmployeePackage, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {

            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                return (nvp.getValue().equalsIgnoreCase("OK"));
            }
        }

        return false;
    }

    /**
     * Runs a web service call to update information in P_E_BASIC
     *
     * @return
     */
    public boolean saveEmployeePayrollInfo() {

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));
        listToSend.add(new BasicNameValuePair("rateOfPay", getRateOfPay()));
        listToSend.add(new BasicNameValuePair("hoursPerDay", getHoursPerDay()));
        listToSend.add(new BasicNameValuePair("hoursPerWeek", getHoursPerWeek()));
        listToSend.add(new BasicNameValuePair("hoursPerMonth", getHoursPerMonth()));
        listToSend.add(new BasicNameValuePair("daysPerWeek", getDaysPerWeek()));
        listToSend.add(new BasicNameValuePair("daysPerMonth", getDaysPerMonth()));
        listToSend.add(new BasicNameValuePair("payPer", PageManager.translateFromLocaleToEnglish(getPayPer())));
        listToSend.add(new BasicNameValuePair("nbMedDependents", getNbMedDependents()));

        // Call the GetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetEmployeePayrollInfo, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {

            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                return (nvp.getValue().equalsIgnoreCase("OK"));
            }
        }

        return false;
    }

    public boolean loadEmployeePersonalData() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));

        // Call the GetValue service
        ArrayList<NameValuePair> employeeAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        employeeAttributes = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_GetEmployeeData, listToSend);

        // What did we get back?
        // Step through each record, and construct an EMPLOYEE instance
        for (NameValuePair nvp : employeeAttributes) {

            if (nvp.getName().equalsIgnoreCase("firstname")) {
                setFirstname(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("secondname")) {
                setSecondname(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("surname")) {
                setSurname(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("employeeNumber")) {
                setEmployeeNumber(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("preferredName")) {
                setPreferredName(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("title")) {
                setTitle(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("initials")) {
                setInitials(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("gender")) {
                setGender(PageManager.translateFromEnglishToLocale(nvp.getValue()));
            }
            if (nvp.getName().equalsIgnoreCase("maritalStatus")) {
                setMaritalStatus(PageManager.translateFromEnglishToLocale(nvp.getValue()));
            }
            if (nvp.getName().equalsIgnoreCase("homeLanguage")) {
                setHomeLanguage(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("groupJoinDate")) {
                Date date = DateUtils.formatDate(nvp.getValue());
                setGroupJoinDate(date);
            }
            if (nvp.getName().equalsIgnoreCase("dateOfBirth")) {
                Date date = DateUtils.formatDate(nvp.getValue());
                setDateOfBirth(date);
            }
            if (nvp.getName().equalsIgnoreCase("taxNumber")) {
                setTaxNumber(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("idNumber")) {
                setIdNumber(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("passportHeld")) {
                setPassportHeld(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("passportNumber")) {
                setPassportNumber(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("race")) {
                setRace(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("dateOfEngagement")) {
                Date date = DateUtils.formatDate(nvp.getValue());
                setDateOfEngagement(date);
            }
            if (nvp.getName().equalsIgnoreCase("dateOfDischarge")) {
                Date date = DateUtils.formatDate(nvp.getValue());
                setDateOfDischarge(date);
            }
            if (nvp.getName().equalsIgnoreCase("costCentreID")) {
                // With the ID, we can get the name
                if (nvp.getValue() != null) {
                    String costID = nvp.getValue();
                    String costName = sessionUser.getPageManager().getCompany().getCostCentreManager().getCostCentreWithID(costID).getName();
                    setCostCentreName(costName);
                }
            }
            if (nvp.getName().equalsIgnoreCase("jobTitle")) {
                setJobTitle(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("payroll")) {
                setPayrollName(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("canTransfer")) {
                setCanTransfer(nvp.getValue().equalsIgnoreCase("Y"));
            }
            if (nvp.getName().equalsIgnoreCase("isAdministrator")) {
                setAdministrator(nvp.getValue().equalsIgnoreCase("Y"));
            }
            if (nvp.getName().equalsIgnoreCase("essLogin")) {
                setEssLogin(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("adminLogin")) {
                setAdminLogin(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("adminPwd")) {
                setAdminPassword(nvp.getValue());
                setAdminPasswordVerified(nvp.getValue());
            }

            if (nvp.getName().equalsIgnoreCase("telHome")) {
                setTelHome(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("telWork")) {
                setTelWork(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("telCell")) {
                setTelCell(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("eMail")) {
                setEmail(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("dischargeReason")) {
                setDischargeReason(nvp.getValue());
            }

        }

        return true;
    }

    /**
     * Only load what needs to be updated when payroll information has been
     * changed.
     *
     * @return
     */
    public boolean quickLoadPayslipData() {
        boolean rslt = loadEmployeeYtdPayPackage();
        rslt = rslt && loadEmployeePayrollInfo();
        rslt = rslt && loadEmployeePayPackage();

        // Fetch the calcDebugReport
        calcDebugReport = retrieveCalcDebugReport();
        calcTaxDiagnostics = retrieveCalcTaxDiagnostics();
        retrieveRunStatus();

        return rslt;
    }

    public boolean quickSavePayslipData() {
        return (saveEmployeePayPackage() && saveEmployeePayrollInfo());
    }

    public boolean loadEmployee() {
        return (loadEmployeePersonalData())
                && (loadEmployeeAccountData())
                && (loadEmployeeAddressData())
                && quickLoadPayslipData();
    }

    public boolean saveEmployee() {
        boolean rslt = (saveEmployeePersonalData())
                && (saveEmployeeAccountData())
                && (saveEmployeeAddressData())
                && (saveEmployeePayPackage())
                && (saveEmployeePayrollInfo());

        return rslt;
    }

    /**
     * Calls a Web Service routine to fetch the next available Employee_ID for
     * the given company
     *
     * @return
     */
    private String getNextNewEmployeeID(String companyID) {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, companyID));

        // Call the GetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_GetNewEmployeeID, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {

            if (nvp.getName().equalsIgnoreCase("EmployeeID")) {
                return (nvp.getValue());
            }
        }
        return null;
    }

    private int getNextCompanyEmployeeNumber() {
        int minNumber = 1;
        if (employeeManager.getAllEmployees() != null) {
            for (Employee employee : employeeManager.getAllEmployees()) {
                try {
                    int empNo = Integer.parseInt(employee.getEmployeeNumber());
                    if (empNo > minNumber) {
                        minNumber = empNo;
                    }
                } catch (NumberFormatException e) {

                }
            }
        }

        return minNumber + 1;
    }

    /**
     * When saving an employee, we need to know if this is a new employee, or an
     * existing one. New employees (no EMPLOYEE_ID), needs to retrieve an
     * EMPLOYEE_ID from the data-store first. NOTE: I realise it's better to
     * create a new ID at time of persisting the employee - and may still change
     * the logic accordingly - but at this time, the additional round-trip of
     * saving and then fetching the employee_id afterwards is too cumbersome.
     *
     * @return
     */
    public boolean saveEmployeePersonalData() {
        // Is this a new employee?
        if (employeeID == null) {
            employeeID = getNextNewEmployeeID(sessionUser.getCompanyId());
        }

        // Still no employee id?
        if (employeeID == null) {
            System.out.println("!!!! UNABLE TO CREATE NEW EMPLOYEE.");
        } else {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
            listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
            listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, employeeID));
            listToSend.add(new BasicNameValuePair("firstname", firstname));
            listToSend.add(new BasicNameValuePair("secondname", secondname));
            listToSend.add(new BasicNameValuePair("surname", surname));
            listToSend.add(new BasicNameValuePair("employeeNumber", employeeNumber));
            listToSend.add(new BasicNameValuePair("preferredName", preferredName));
            listToSend.add(new BasicNameValuePair("title", title));
            listToSend.add(new BasicNameValuePair("initials", initials));
            listToSend.add(new BasicNameValuePair("gender", PageManager.translateFromLocaleToEnglish(gender)));
            listToSend.add(new BasicNameValuePair("maritalStatus", PageManager.translateFromLocaleToEnglish(maritalStatus)));
            listToSend.add(new BasicNameValuePair("homeLanguage", homeLanguage));
            listToSend.add(new BasicNameValuePair("groupJoinDate", DateUtils.formatDate(groupJoinDate)));
            listToSend.add(new BasicNameValuePair("dateOfBirth", DateUtils.formatDate(dateOfBirth)));
            listToSend.add(new BasicNameValuePair("taxNumber", taxNumber));
            listToSend.add(new BasicNameValuePair("idNumber", idNumber));
            listToSend.add(new BasicNameValuePair("passportHeld", passportHeld));
            listToSend.add(new BasicNameValuePair("passportNumber", passportNumber));
            listToSend.add(new BasicNameValuePair("race", race));
            listToSend.add(new BasicNameValuePair("dateOfEngagement", DateUtils.formatDate(dateOfEngagement)));
            listToSend.add(new BasicNameValuePair("dateOfDischarge", DateUtils.formatDate(dateOfDischarge)));
            listToSend.add(new BasicNameValuePair("dischargeReason", dischargeReason));

            if (sessionUser.getPageManager().getCompany().getCostCentreManager().getCostCentreWithName(costCentreName) != null) {
                listToSend.add(new BasicNameValuePair("costCentreID", sessionUser.getPageManager().getCompany().getCostCentreManager().getCostCentreWithName(costCentreName).getCostID()));
            }
            listToSend.add(new BasicNameValuePair("jobTitle", jobTitle));
            listToSend.add(new BasicNameValuePair("payroll", payrollName));
            if (isAdministrator()) {
                listToSend.add(new BasicNameValuePair("isAdministrator", "Y"));
                listToSend.add(new BasicNameValuePair("adminLogin", adminLogin));
                listToSend.add(new BasicNameValuePair("adminPassword", adminPassword));

            } else {
                listToSend.add(new BasicNameValuePair("isAdministrator", "N"));
            }
            listToSend.add(new BasicNameValuePair("essLogin", essLogin));
            listToSend.add(new BasicNameValuePair("essPassword", essPassword));
            listToSend.add(new BasicNameValuePair("telHome", telHome));
            listToSend.add(new BasicNameValuePair("telWork", telWork));
            listToSend.add(new BasicNameValuePair("telCell", telCell));
            listToSend.add(new BasicNameValuePair("eMail", email));
            if (removeDischargeDate) {
                listToSend.add(new BasicNameValuePair("undoDischarge", "Y"));
            }

            // Call the GetValue service
            ArrayList<NameValuePair> wsResult;
            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetEmployeeData, listToSend);

            // What did we get back?
            for (NameValuePair nvp : wsResult) {

                if (nvp.getName().equalsIgnoreCase("RESULT")) {
                    return (nvp.getValue().equalsIgnoreCase("OK"));
                }
            }
        }
        return false;
    }

    private boolean renderDebugReport;

    public boolean isRenderDebugReport() {

        return (calcDebugReport != null && calcDebugReport.length > 0);
    }

    public void setRenderDebugReport(boolean renderDebugReport) {
        this.renderDebugReport = renderDebugReport;
    }

    private String[] calcDebugReport;

    public String[] getCalcDebugReport() {
        return calcDebugReport;
    }

    private String[] calcTaxDiagnostics;

    public String[] getCalcTaxDiagnostics() {
        return calcTaxDiagnostics;
    }

    public void setCalcTaxDiagnostics(String[] calcTaxDiagnostics) {
        this.calcTaxDiagnostics = calcTaxDiagnostics;
    }

    /**
     * Returns the contents of P_CALCREPORT
     *
     * @return
     */
    private String[] retrieveCalcDebugReport() {
        // List of items in P_CALCREPORT
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, getPayrollID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, getEmployeeID()));

        List<String> debugReport = new ArrayList<>();

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetEmployeeCalcDebugInfo, listToSend);

        // What did we get back?
        // The result is a JSON Array listing all supported tax countries
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the cost centre
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            String description = null;
            // Step through each record, and construct an EMPLOYEE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("DESCRIPTION")) {
                    description = nvp.getValue();
                    if (description != null) {
                        if (description.trim().length() > 0) {
                            debugReport.add(description);
                        }
                    }
                }
            }
        }

        String[] rslt = debugReport.toArray(new String[debugReport.size()]);
        return rslt;
    }

    /**
     * Returns the run_status and CLOSED_YN from P_CALC_EMPLOYEELIST
     *
     * @return
     */
    private void retrieveRunStatus() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, getPayroll().getPayrollID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, getEmployeeID()));

        // Call the GetValue service
        ArrayList<NameValuePair> employeeAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        employeeAttributes = WebServiceClient.getInstance().getOneLineResponse(
                sessionUser, URL, Constants.serviceName_GetEmployeeRunStatus, listToSend);

        String rslt = "0";
        // What did we get back?
        for (NameValuePair nvp : employeeAttributes) {
            if (nvp.getName().equalsIgnoreCase("RUN_STATUS")) {
                runStatus = Integer.parseInt(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("CLOSED_YN")) {
                closedForRun = (nvp.getValue().equalsIgnoreCase("Y"));
            }
        }
    }

    private String readibleRunStatus;

    public String getReadibleRunStatus() {
        switch (runStatus) {
            case 1:
                return PageManager.translate("runStatus_Uninitiated");
            case 2:
                return PageManager.translate("runStatus_ToBeProcessed");
            case 3:
                return PageManager.translate("runStatus_Calculated");
            case 4:
                return PageManager.translate("runStatus_ToBeSkipped");
            case 5:
                return PageManager.translate("runStatus_Skipped");
            default:
                return PageManager.translate("runStatus_Uninitiated");
        }
    }

    /**
     * Returns the contents of P_DEBUGREPORT for this employee
     *
     * @return
     */
    private String[] retrieveCalcTaxDiagnostics() {
        // List of items in P_CALCREPORT
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, getPayrollID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, getEmployeeID()));

        List<String> debugReport = new ArrayList<>();

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetEmployeeTaxDiagnostics, listToSend);

        // What did we get back?
        // The result is a JSON Array listing all supported tax countries
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the cost centre
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            String description = null;
            // Step through each record, and construct an EMPLOYEE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("DESCRIPTION")) {
                    description = nvp.getValue();
                }
            }
            if (description != null) {
                if (description.trim().length() > 0) {
                    debugReport.add(description);
                }
            }
        }

        String[] rslt = debugReport.toArray(new String[debugReport.size()]);
        return rslt;
    }

    /**
     * Sends a web service call to request a recalculation
     *
     * @return
     */
    private void requestRecalc() {
        // List of items in P_CALCREPORT
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, getPayrollID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, getEmployeeID()));

        List<String> debugReport = new ArrayList<>();

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_RequestRecalc, listToSend);

    }

    /**
     * Triggers a re-calc
     *
     */
    public void redoPayCalc() {
        if (!closedForRun) {
            requestRecalc();
            waitForCalcCompletion();
            sessionUser.getPageManager().getCompany().getPayrollManager().reloadPayrollsStatus();
        }
    }

    /**
     * Marks this person as the selected employee
     */
    public void setAsSelectedEmployee() {
        employeeManager.setSelectedEmployee(this);
    }

    /**
     * Navigates to this employee's payslip screen
     */
    public void navigateToPayslipScreen() {
        employeeManager.setSelectedEmployee(this);
        sessionUser.getPageManager().navigateToPayslipScreen();
    }

    /**
     * When changes were made to the employee that may affect his/her payslip, a
     * calculation in PeopleWare will automatically initiate. This routine waits
     * for the calculation to complete.
     */
    public void waitForCalcCompletion() {

        boolean stillCalculating = true;
        // Now we're running the web service to see if this person is still being calculated (CALC_STATUS = DONE)

        int timeoutCounter = 0;
        while (stillCalculating) {
            // Current run status of employee
            retrieveRunStatus();

            if ((runStatus == 5) || (runStatus == 3)) {
                stillCalculating = false;
            } else {
                try {
                    Thread.sleep(400);                 //1000 milliseconds is one second.
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                // Escape plan
                timeoutCounter++;
                if (timeoutCounter >= 10) {
                    stillCalculating = false;
                }
            }
        }
        // Reload the employee's information
        quickLoadPayslipData();

        // Update PayrollCredits?
        sessionUser.getPageManager().reloadPayrollCreditsFromDB();

        // Force a screen-reload (selective)
        sessionUser.getPageManager().checkStatusForTips();
        RequestContext.getCurrentInstance().execute("javascript:window.location.reload()");
    }

    public String runStatusCaption = "Uninitiated";
    private String runStatusImageUrl;

    public int getRunStatus() {
        return runStatus;
    }

    public String getRunStatusCaption() {
        if (runStatus == 1 || runStatus == 2 || runStatus == 4) {
            return PageManager.translate("statusUnititiated");
        }
        if (runStatus == 3) {
            return PageManager.translate("statusCalculated");
        }
        return PageManager.translate("statusSkipped");
    }

    public void setRunStatus(int runStatus) {
        this.runStatus = runStatus;
    }

    /**
     * This URL is used in the payslip screen to display the appropriate icon
     * depending on the employee's run-status
     *
     * @return
     */
    public String getRunStatusImageUrl() {
        this.runStatusImageUrl = "";
        if (runStatus == 5) {
            this.runStatusImageUrl = Constants.trafficLight_RED;
        }
        return runStatusImageUrl;
    }

    public void setRunStatusImageUrl(String runStatusImageUrl) {
        this.runStatusImageUrl = runStatusImageUrl;
    }

    /**
     * Generates a payslip for this employee
     */
    public void printPayslip() {
        ReportPrinter printer = new ReportPrinter();
        printer.printPayslip(sessionUser, this);
    }

    /**
     * Generates a payslip for this employee and emails it to him/her
     */
    public void emailPayslip() {
        ReportPrinter printer = new ReportPrinter();
        printer.emailPayslip(sessionUser, this);
    }

    /**
     * Generates a TaxCertificate for this employee
     */
    public void printTaxCertificate() {
        // Now print the reports
        ReportPrinter printer = new ReportPrinter();
        printer.printTaxCertificate(sessionUser, this);
    }

    /**
     * Generates a Tax Certificate for this employee and emails it to him/her
     */
    public void emailTaxCertificate() {
        // Now print the reports
        ReportPrinter printer = new ReportPrinter();
        printer.emailTaxCertificate(sessionUser, this);
    }

    /**
     * Returns the tax certificate submission periods
     *
     * @return
     */
    public List<String> getTaxCertificateSubmissionPeriods() {
        // Parameters
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, getPayrollID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_EmployeeId, getEmployeeID()));

        List<String> submissionPeriods = new ArrayList<>();

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetTaxCertificatePeriods, listToSend);

        // What did we get back?
        // The result is a JSON Array listing all supported tax countries
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the cost centre
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            String submissionPeriod = null;
            // Step through each record, and get the date/period
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("SubmissionPeriod")) {
                    submissionPeriod = nvp.getValue();
                }
            }
            if (submissionPeriod != null) {
                if (submissionPeriod.trim().length() > 0) {
                    submissionPeriods.add(submissionPeriod);
                }
            }
        }

        //String[] rslt = submissionPeriods.toArray(new String[submissionPeriods.size()]);
        return submissionPeriods;
    }

    /**
     * Returns TRUE if the employee has been calculated and his/her payslip has
     * an actual NETT-PAY >= 0
     *
     * @return
     */
    private boolean validPayslip;

    public boolean isValidPayslip() {
        if (getNettPay() != null) {
            return (getRunStatus() == 3 && Float.parseFloat(getNettPay()) >= 0 && payEntityInstances.size() > 0);
        }
        return false;
    }

    /**
     * Returns TRUE if the employee has been calculated and his/her tax
     * certificate list has submission dates
     *
     * @return
     */
    private boolean validTaxCertificate;

    public boolean isValidTaxCertificate() {
        return (getTaxCertificateSubmissionPeriods().size() > 0);
    }
}
