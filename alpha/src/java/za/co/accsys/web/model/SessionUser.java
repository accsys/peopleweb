/**
 * Once logged in, each session has a Session User that represents the person that logged into this session.
 * As with the rest of PeopleWare, a SessionUser MUST also be an employee in PeopleWare
 */
package za.co.accsys.web.model;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.context.RequestContext;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.helperclasses.ListManager;
import za.co.accsys.web.helperclasses.ServerPrefs;

/**
 *
 * @author Liam
 */
public class SessionUser {

    private final String sessionID;
    private String preferredName;
    private String companyId;
    private String employeeId;
    private String accountID;
    /* User Preferences */
    private String theme;
    private String surname;
    private Timer tmrSessionVerification;
    private boolean sessionExpired = false;
    private final PageManager pageManager;

    public String getSessionID() {
        return sessionID;
    }

    public boolean isSessionExpired() {
        return sessionExpired;
    }

    @Override
    public String toString() {
        return "AccountID:" + this.accountID + ", CompanyID:" + this.companyId + ", EmployeeID:" + this.employeeId + ", SessionID:" + this.sessionID;
    }

    public SessionUser(String sessionID, String accountID, PageManager pageManager) {
        this.preferredName = "";
        this.surname = "";
        this.sessionID = sessionID;
        this.accountID = accountID;
        this.pageManager = pageManager;
        this.theme = ListManager.getDefaultTheme();
        // What is the current locale in use?
        this.localeDefinition = getContextInstanceLocale();
        loadSessionUser();

        SessionVerifier(20);
    }

    /**
     * Used to cleanup this instance, including terminating the event. This method must be called prior to
     * freeing the SessionUser instance.
     */
    public void terminateSession() {
        tmrSessionVerification.cancel();  // Terminates this timer, discarding any currently scheduled tasks.
        tmrSessionVerification.purge();
    }

    public boolean saveSessionUser() {
        //savePersonalData();
        return (savePreferredLocale(localeDefinition) && savePreferredTheme(theme));
    }

    public final boolean loadSessionUser() {
        return (loadPersonalData() && loadUserPreferences());
    }

    public PageManager getPageManager() {
        return pageManager;
    }
    
    public RequestContext getRequestContext(){
        return RequestContext.getCurrentInstance();
    }

    /**
     * Runs a web service call to retrieve the information for this operator
     *
     * @return - true if this user has an active session
     */
    private boolean loadPersonalData() {
        boolean allOK = false;
        // NameValuePair to send
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));

        // Call the User Info Service
        ArrayList<NameValuePair> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(accountID);
        listRetreived = WebServiceClient.getInstance().getOneLineResponse(this, URL, Constants.serviceName_GetUserInfo, listToSend);

        // What did we get back?
        for (NameValuePair nvp : listRetreived) {
            if (nvp.getName().compareToIgnoreCase("Preferred_Name") == 0) {
                preferredName = nvp.getValue();
                allOK = true;
            }
            if (nvp.getName().compareToIgnoreCase("Surname") == 0) {
                surname = nvp.getValue();
                allOK = true;
            }
            if (nvp.getName().compareToIgnoreCase("company_id") == 0) {
                companyId = nvp.getValue();
                allOK = true;
            }
            if (nvp.getName().compareToIgnoreCase("employee_id") == 0) {
                employeeId = nvp.getValue();
                allOK = true;
            }
        }
        return allOK;
    }

    private boolean loadUserPreferences() {
        // Preferred Theme
        setTheme(loadPreferredTheme());
        setLocaleDefinition(loadPreferredLocale());
        // Now that we've loaded this person's preferred locale, lets set it to the session
        return true;
    }

    /**
     * Runs a web service call to retrieve the preferences from WEB_PREFERENCES for this operator
     *
     * @return - true if this user has an active session
     */
    private String loadPreferredTheme() {
        String prefTheme = ListManager.getDefaultTheme();

        // NameValuePairs to send
        // fn_HTTP_GETVALUE ( :sessionid, :tablename, :columnname, :filter ) 
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_TableName, "WEB_PREFERENCES"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ColumnName, "pref_value"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_Filter, "company_id=" + companyId + " and employee_id=" + employeeId + " and pref_key='theme'"));

        // Call the User Info Service
        ArrayList<NameValuePair> listRetreived;
        listRetreived = WebServiceClient.getInstance().getOneLineResponse(this, ServerPrefs.getInstance().getDbURL(accountID), Constants.serviceName_GetDBValue, listToSend);

        // What did we get back?
        for (NameValuePair nvp : listRetreived) {
            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                prefTheme = nvp.getValue();
            }
        }
        return prefTheme;
    }

    /**
     * Runs a web service call to set the THEME preference in WEB_PREFERENCES for this operator
     *
     * return TRUE if update was successful
     */
    private boolean savePreferredTheme(String theme) {
        boolean success = false;
        // NameValuePairs to send
        // fn_HTTP_GETVALUE ( :sessionid, :tablename, :columnname, :filter ) 
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_TableName, "WEB_PREFERENCES"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ColumnList, "company_id, employee_id, pref_key, pref_value"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ValueList, companyId + "," + employeeId + ",'theme','" + theme + "'"));

        // Call the User Info Service
        ArrayList<NameValuePair> listRetreived;
        listRetreived = WebServiceClient.getInstance().getOneLineResponse(this, ServerPrefs.getInstance().getDbURL(accountID), Constants.serviceName_SetDBValue, listToSend);

        // Was the update successful?
        for (NameValuePair nvp : listRetreived) {
            if (nvp.getName().compareToIgnoreCase("RESULT") == 0) {
                success = (nvp.getValue().compareToIgnoreCase("OK") == 0);
            }
        }
        return success;
    }

    /* 
     * ************************************
     * GETTERS AND SETTERS
     * ************************************
     */
    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String newTheme) {
        if (!newTheme.equalsIgnoreCase(this.theme)) {
            this.theme = newTheme;
            boolean updateOK = savePreferredTheme(this.theme);
            if (!updateOK) {
                System.out.println("Failed to update preferences theme");
            }
        }
        getPageManager().setChangesOnPage(true);
    }

    public String getPreferredName() {
        return preferredName;
    }

    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSessionExpired(boolean sessionExpired) {
        this.sessionExpired = sessionExpired;
        if (sessionExpired) {
            this.tmrSessionVerification.cancel();
            this.tmrSessionVerification.purge();
        }
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    /**
     * Periodically checks the underlying database to see if this sessionID still exists
     *
     * @param seconds - seconds of the checking intervals
     */
    private void SessionVerifier(int seconds) {
        tmrSessionVerification = new Timer();  //At this line a new Thread will be created
        tmrSessionVerification.schedule(new RemindTask(), new java.util.Date(), seconds * 1000); //delay in milliseconds
    }

    private class RemindTask extends TimerTask {

        @Override
        public void run() {
            if (sessionID.trim().length() > 0) {
                // NameValuePair to send 
                ArrayList<NameValuePair> listToSend = new ArrayList<>();
                listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));

                // Call the Web Service to test session liveness
                ArrayList<NameValuePair> listRetreived;
                listRetreived = WebServiceClient.getInstance().getOneLineResponse(pageManager.getSessionUser(), ServerPrefs.getInstance().getDbURL(accountID), Constants.serviceName_IsSessionLive, listToSend);
                // Is all OK?
                if (listRetreived.size() > 0) {
                    NameValuePair nvp = listRetreived.get(0);
                    if (!nvp.getValue().equalsIgnoreCase("OK")) {
                        // Nope, the session no longer exists in Sybase
                        setSessionExpired(true);
                    }
                }
            }
            // I'd also like a few other intermittent checks
            //1. Refresh Payroll Status
            if (pageManager.getCompany() != null) {
                pageManager.getCompany().getPayrollManager().reloadPayrollsStatus();
            }
        }
    }

    /**
     * *****************************************************************************
     * LOCALE Methods ******************************************************************************
     */
    private LocaleDefinition localeDefinition;

    /**
     * Getter
     *
     * @return
     */
    public LocaleDefinition getLocaleDefinition() {
        return localeDefinition;
    }

    /**
     * Setter for LocaleDefinition. Updates the FacesContext.locale as well
     *
     * @param localeDefinition
     */
    public void setLocaleDefinition(LocaleDefinition localeDefinition) {
        if (!localeDefinition.equals(this.localeDefinition)) {
            this.localeDefinition = localeDefinition;
            boolean updateOK = savePreferredLocale(localeDefinition);
            if (!updateOK) {
                System.out.println("Failed to update preferences theme");
            }
        }
        this.localeDefinition = localeDefinition;
        // Update PF locale
        Locale locale = new Locale(localeDefinition.getLanguage(), localeDefinition.getCountry());
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
        getPageManager().setChangesOnPage(true);

    }

    public void themeChanged(ValueChangeEvent e) {

    }

    /**
     * Called when the user chooses another country/language
     *
     * @param e
     */
    public void countryLocaleNameChanged(ValueChangeEvent e) {

        String newLocaleName = e.getNewValue().toString();
        //loop country map to compare the locale code
        ListManager listManager = new ListManager(this);
        int i = 0;
        for (String lookupName : listManager.getListItems_LocaleDisplay()) {
            if (newLocaleName.equalsIgnoreCase(lookupName)) {
                String localCountry = listManager.getListItems_LocaleCountry()[i];
                String localLanguage = listManager.getListItems_LocaleLanguage()[i];

                setLocaleDefinition(new LocaleDefinition(getLocaleDisplayName(localLanguage, localCountry), localCountry, localLanguage));
                
                break;
            }
            i++;
        }
    }

    /**
     * Each locale has a display name, language, and country. This function returns the display name
     *
     * @param language
     * @param country
     * @return
     */
    private String getLocaleDisplayName(String language, String country) {
        ListManager listMan = new ListManager(this);
        for (LocaleDefinition def : listMan.getListItems_LocalDefinitions()) {
            if (def.getCountry().equalsIgnoreCase(country)
                    && def.getLanguage().equalsIgnoreCase(language)) {
                return def.getDisplayName();
            }
        }
        return "Default";
    }

    /**
     * Returns the current local in use by the FacesContext. In the form of a LocaleDefinition
     *
     * @return
     */
    private LocaleDefinition getContextInstanceLocale() {
        String country = FacesContext.getCurrentInstance().getViewRoot().getLocale().getCountry();
        String language = FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        String description = getLocaleDisplayName(language, country);
        return (new LocaleDefinition(description, country, language));
    }

    /**
     * Runs a web service call to retrieve the preferences from WEB_PREFERENCES for this operator
     *
     * @return - true if this user has an active session
     */
    private LocaleDefinition loadPreferredLocale() {
        Locale prefLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        String localeLang = prefLocale.getLanguage();
        String localeCountry = prefLocale.getCountry();

        // NameValuePairs to send
        // fn_HTTP_GETVALUE ( :sessionid, :tablename, :columnname, :filter ) 
        //  Locale Language
        //
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_TableName, "WEB_PREFERENCES"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ColumnName, "pref_value"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_Filter, "company_id=" + companyId + " and employee_id=" + employeeId + " and pref_key='locale_language'"));

        // Call the User Info Service
        ArrayList<NameValuePair> listRetreived;
        listRetreived = WebServiceClient.getInstance().getOneLineResponse(this, ServerPrefs.getInstance().getDbURL(accountID), Constants.serviceName_GetDBValue, listToSend);

        // What did we get back?
        for (NameValuePair nvp : listRetreived) {
            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                localeLang = nvp.getValue();
            }
        }

        //  Locale Language
        //
        listToSend.clear();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_TableName, "WEB_PREFERENCES"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ColumnName, "pref_value"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_Filter, "company_id=" + companyId + " and employee_id=" + employeeId + " and pref_key='locale_country'"));

        // Call the User Info Service
        listRetreived.clear();
        listRetreived = WebServiceClient.getInstance().getOneLineResponse(this, ServerPrefs.getInstance().getDbURL(accountID), Constants.serviceName_GetDBValue, listToSend);

        // What did we get back?
        for (NameValuePair nvp : listRetreived) {
            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                localeCountry = nvp.getValue();
            }
        }

        LocaleDefinition ldef = new LocaleDefinition(getLocaleDisplayName(localeLang, localeCountry), localeCountry, localeLang);

        return ldef;
    }

    /**
     * Runs a web service call to set the LOCALE preference in WEB_PREFERENCES for this operator
     *
     * return TRUE if update was successful
     */
    private boolean savePreferredLocale(LocaleDefinition localeDef) {
        boolean success = false;
        // NameValuePairs to send
        // fn_HTTP_GETVALUE ( :sessionid, :tablename, :columnname, :filter ) 

        // Locale Country
        //
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_TableName, "WEB_PREFERENCES"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ColumnList, "company_id, employee_id, pref_key, pref_value"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ValueList, companyId + "," + employeeId + ",'locale_country','" + localeDef.getCountry() + "'"));

        // Call the User Info Service
        ArrayList<NameValuePair> listRetreived;
        listRetreived = WebServiceClient.getInstance().getOneLineResponse(this, ServerPrefs.getInstance().getDbURL(accountID), Constants.serviceName_SetDBValue, listToSend);

        // Was the update successful?
        for (NameValuePair nvp : listRetreived) {
            if (nvp.getName().compareToIgnoreCase("RESULT") == 0) {
                success = (nvp.getValue().compareToIgnoreCase("OK") == 0);
            }
        }

        // Locale Language
        //
        listToSend.clear();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_TableName, "WEB_PREFERENCES"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ColumnList, "company_id, employee_id, pref_key, pref_value"));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ValueList, companyId + "," + employeeId + ",'locale_language','" + localeDef.getLanguage() + "'"));

        // Call the User Info Service
        listRetreived.clear();
        listRetreived = WebServiceClient.getInstance().getOneLineResponse(this, ServerPrefs.getInstance().getDbURL(accountID), Constants.serviceName_SetDBValue, listToSend);

        // Was the update successful?
        for (NameValuePair nvp : listRetreived) {
            if (nvp.getName().compareToIgnoreCase("RESULT") == 0) {
                success = success && (nvp.getValue().compareToIgnoreCase("OK") == 0);
            }
        }
        return success;
    }

    public Locale getUserLocale() {
        return (new Locale(localeDefinition.getLanguage(), localeDefinition.getCountry()));
    }
}
