/*
 * Encapsulates a bank account for an employee
 */
package za.co.accsys.web.model;

import za.co.accsys.web.beans.PageManager;

/**
 *
 * @author Liam
 */
public class BankAccount {

    private String accountType;
    private String accountName;
    private String accountDescription;
    private String accountBank;
    private String accountBranch;
    private String accountBankCode;
    private String accountBranchCode;
    private String accountNumber;
    private String accountHolderRelationship;
    private String accountLineNumber;
    private String accountID;
    private final SessionUser sessionUser;

    public BankAccount(SessionUser sessionUser){
        this.sessionUser = sessionUser;
    }
    
    public String getAccountType() {
        return PageManager.translateFromEnglishToLocale(accountType);
    }

    public void setAccountType(String accountType) {
        if ((this.accountType == null) || (!this.accountType.equals(accountType))) {
            this.accountType = accountType;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        if ((this.accountName == null) || (!this.accountName.equals(accountName))) {
            this.accountName = accountName;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        if ((this.accountDescription == null) || (!this.accountDescription.equals(accountDescription))) {
            this.accountDescription = accountDescription;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountBank() {
        return accountBank;
    }

    public void setAccountBank(String accountBank) {
        if ((this.accountBank == null) || (!this.accountBank.equals(accountBank))) {
            this.accountBank = accountBank;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountBranch() {
        return accountBranch;
    }

    public void setAccountBranch(String accountBranch) {
        if ((this.accountBranch == null) || (!this.accountBranch.equals(accountBranch))) {
            this.accountBranch = accountBranch;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountBankCode() {
        return accountBankCode;
    }

    public void setAccountBankCode(String accountBankCode) {
        if ((this.accountBankCode == null) || (!this.accountBankCode.equals(accountBankCode))) {
            this.accountBankCode = accountBankCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountBranchCode() {
        return accountBranchCode;
    }

    public void setAccountBranchCode(String accountBranchCode) {
        if ((this.accountBranchCode == null) || (!this.accountBranchCode.equals(accountBranchCode))) {
            this.accountBranchCode = accountBranchCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        if ((this.accountNumber == null) || (!this.accountNumber.equals(accountNumber))) {
            this.accountNumber = accountNumber;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountHolderRelationship() {
        return accountHolderRelationship;
    }

    public void setAccountHolderRelationship(String accountHolderRelationship) {
        if ((this.accountHolderRelationship == null) || (!this.accountHolderRelationship.equals(accountHolderRelationship))) {
            this.accountHolderRelationship = accountHolderRelationship;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountLineNumber() {
        return accountLineNumber;
    }

    public void setAccountLineNumber(String accountLineNumber) {
        if ((this.accountLineNumber == null) || (!this.accountLineNumber.equals(accountLineNumber))) {
            this.accountLineNumber = accountLineNumber;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

}
