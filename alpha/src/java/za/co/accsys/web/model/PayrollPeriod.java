/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import java.util.Date;
import za.co.accsys.web.helperclasses.DateUtils;

/**
 *
 * @author liam
 */
public class PayrollPeriod {

    private String payrollID;
    private String payrollDetailID;
    private Date startDate;
    private Date endDate;
    private Date monthEnd;
    private boolean openPeriod;

    public String getPayrollID() {
        return payrollID;
    }

    public void setPayrollID(String payrollID) {
        this.payrollID = payrollID;
    }

    public String getPayrollDetailID() {
        return payrollDetailID;
    }

    public void setPayrollDetailID(String payrollDetailID) {
        this.payrollDetailID = payrollDetailID;
    }

    @Override
    public String toString() {
        return DateUtils.formatDate(startDate) + " - " + DateUtils.formatDate(endDate);
    }

    public PayrollPeriod(String payrollID) {
        this.payrollID = payrollID;
    }

    
    public boolean isOpenPeriod() {
        return openPeriod;
    }

    public void setOpenPeriod(boolean openPeriod) {
        this.openPeriod = openPeriod;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getMonthEnd() {
        return monthEnd;
    }

    public void setMonthEnd(Date monthEnd) {
        this.monthEnd = monthEnd;
    }

}
