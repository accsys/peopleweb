/*
 * Represents a Job Title in J_JOBPOSITION
 */
package za.co.accsys.web.model;

/**
 *
 * @author liam
 */
public class JobPosition {

    private String jobPositionID;
    private String jobTitle;
    private String jobDescription;
    private String jobCode;
    private String jobGrade;
    private boolean deleted;
    private final SessionUser sessionUser;

    public JobPosition(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        deleted = false;
        jobPositionID = "-1";
        jobTitle = "new:" + (int) Math.ceil(Math.random() * 100);
    }

    
    public String getJobPositionID() {
        return jobPositionID;
    }

    public void setJobPositionID(String jobPositionID) {
        this.jobPositionID = jobPositionID;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        if ((this.jobTitle == null) || (!this.jobTitle.equals(jobTitle))) {
            //Is the name unique?
            if (sessionUser.getPageManager().getCompany().getJobPositionManager() != null) {
                if (sessionUser.getPageManager().getCompany().getJobPositionManager().titleExists(jobTitle)) {
                    jobTitle = jobTitle + "_";
                }
            }
            this.jobTitle = jobTitle;
            if (sessionUser.getPageManager().getCompany().getJobPositionManager() != null) {
                sessionUser.getPageManager().getCompany().getJobPositionManager().saveJobPosition(this);
            }
        }
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        if ((this.jobDescription == null) || (!this.jobDescription.equals(jobDescription))) {
            this.jobDescription = jobDescription;
        }
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void delete() {
        this.deleted = true;
    }

    public void setDeleted(boolean deleted) {
        if (this.deleted != deleted) {
            this.deleted = deleted;
        }
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        if ((this.jobCode == null) || (!this.jobCode.equals(jobCode))) {
            this.jobCode = jobCode;
        }
    }

    public String getJobGrade() {
        return jobGrade;
    }

    public void setJobGrade(String jobGrade) {
        if ((this.jobGrade == null) || (!this.jobGrade.equals(jobGrade))) {
            this.jobGrade = jobGrade;
        }
    }

}
