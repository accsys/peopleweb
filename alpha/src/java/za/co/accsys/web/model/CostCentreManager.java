/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.helperclasses.JSONRecord;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.helperclasses.ServerPrefs;
import za.co.accsys.web.controller.WebServiceClient;
/**
 *
 * @author lterblanche
 */
public class CostCentreManager {

    private CostCentre selectedCostCentre;
    private CostCentre newCostCentre;
    private final SessionUser sessionUser;
    private TreeNode costCentreRootNode;
    private ArrayList<CostCentre> costCentres;
    private TreeNode selectedCostCentreNode;
    private String selectedCostCentreName;
    private String[] validCostCentreParents;

    public CostCentreManager(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        loadCostCentres();

        newCostCentre = new CostCentre(sessionUser);
        newCostCentre.setParentCostCentreName(getHomeCostCentre().getName());
    }

    /**
     * Because we're working with name attributes and not object refereces for this referral to the parent, it
     * may be necessary to rebuild that information from the database when name changes occur.
     */
    public final void rebuildParentCostCentreNames() {
        // Now that we have loaded all the cost centres, add each one's 'parent'
        for (CostCentre costCentre : costCentres) {
            if (getParent(costCentre) != null) {
                costCentre.setParentCostCentreName(getParent(costCentre).getName());
            }
        }
    }

    private CostCentre getHomeCostCentre() {
        for (CostCentre costCentre : costCentres) {
            if (getParent(costCentre) == null) {
                return costCentre;
            }
        }
        return null;
    }

    public CostCentre getNewCostCentre() {
        return newCostCentre;
    }

    public void setNewCostCentre(CostCentre newCostCentre) {
        this.newCostCentre = newCostCentre;
    }

    public String getSelectedCostCentreName() {
        if (selectedCostCentre == null) {
            return "";
        } else {
            return selectedCostCentre.getName();
        }
    }

    public void setSelectedCostCentreName(String selectedCostCentreName) {
        this.selectedCostCentreName = selectedCostCentreName;
        this.selectedCostCentre = getCostCentreByName(selectedCostCentreName);
    }

    /**
     * Used in the constructor to rebuild the Cost Centre hierarchy
     */
    private void populateCostCentreHierarchyView() {
        // Fetch the root cost centre
        CostCentre rootCostCentre = getRootCostCentre();

        if (rootCostCentre != null) {
            costCentreRootNode = new DefaultTreeNode(rootCostCentre.getName(), null);
            costCentreRootNode.setExpanded(true);
            // Populate Cost Centre hierarchy
            populateCostCentreWithChildren(rootCostCentre, costCentreRootNode);
        } else {
            costCentreRootNode = null;
        }
    }

    /**
     * Traverses through the cost centre hierarchy and returns the level (0=top) where this cost centre is in
     * the layout
     *
     * @param costCentre
     * @return
     */
    private int getCostLevelInHierarchy(CostCentre costCentre) {
        int i = 0;
        while (costCentre.getParentCostID() != null) {
            i++;
            costCentre = getCostCentreWithID(costCentre.getParentCostID());
        }
        return i;
    }

    /**
     * Returns cost centre with given cost_id
     *
     * @param costID
     * @return
     */
    public CostCentre getCostCentreWithID(String costID) {
        if (costID != null) {
            for (CostCentre costCentre : costCentres) {
                if (costCentre.getCostID().equalsIgnoreCase(costID)) {
                    return costCentre;
                }
            }
        }
        return null;
    }

    /**
     * Returns cost centre with given name
     *
     * @param costName
     * @return
     */
    public CostCentre getCostCentreWithName(String costName) {
        for (CostCentre costCentre : costCentres) {
            if (costCentre.getName().equalsIgnoreCase(costName)) {
                return costCentre;
            }
        }
        return null;
    }

    /**
     * Builds hierarchical structure for cost centres
     *
     * @param parentCostCentre
     * @param parentCostCentreNode
     */
    private void populateCostCentreWithChildren(CostCentre parentCostCentre, TreeNode parentCostCentreNode) {
        for (CostCentre aCostCentre : costCentres) {
            if (!aCostCentre.isDeleted()) {
                String aCostCentreParentName = aCostCentre.getParentCostCentreName();
                if (aCostCentreParentName != null) {
                    CostCentre aCostCentreParent = getCostCentreByName(aCostCentreParentName);
                    if (aCostCentreParent != null) {
                        String evalCC = parentCostCentre.getName();
                        if (aCostCentreParentName.compareToIgnoreCase(evalCC) == 0) {
                            // New node, 
                            TreeNode node = new DefaultTreeNode(aCostCentre.getName(), parentCostCentreNode);
                            node.setExpanded(getCostLevelInHierarchy(aCostCentre) < 4);
                            populateCostCentreWithChildren(aCostCentre, node);
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns the parent of the current cost centre
     *
     * @param currentCostCentre
     * @return
     */
    private CostCentre getParent(CostCentre currentCostCentre) {
        for (CostCentre costCentre : costCentres) {
            if (currentCostCentre.getParentCostID() != null) {
                if (costCentre.getCostID().equalsIgnoreCase(currentCostCentre.getParentCostID())) {
                    return costCentre;
                }
            }
        }
        return null;
    }

    /**
     * Returns the first cost centre without a parent
     *
     * @return
     */
    private CostCentre getRootCostCentre() {
        for (CostCentre costC : costCentres) {
            if (!costC.isDeleted()) {
                if (costC.getParentCostID() == null) {
                    return costC;
                }
            }
        }
        return null;
    }

    public void onCostCentreNodeSelect(NodeSelectEvent event) {
        TreeNode selNode = event.getTreeNode();
        String selString = (String) selNode.getData();
        setSelectedCostCentre(getCostCentreWithName(selString));
    }

    /**
     * Runs a web service call to update all the information for this company
     *
     * @return
     */
    public boolean saveCostCentres() {
        boolean rslt = false;
        for (CostCentre costCentre : costCentres) {
            // Default to NO PARENT
            costCentre.setParentCostID(null);
            if (costCentre.getParentCostCentreName() != null) {
                CostCentre parentCC = getCostCentreByName(costCentre.getParentCostCentreName());
                if (parentCC != null) {
                    costCentre.setParentCostID(getCostCentreByName(costCentre.getParentCostCentreName()).getCostID());
                }
            }

            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
            listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
            listToSend.add(new BasicNameValuePair(Constants.sendParam_CostId, costCentre.getCostID()));
            listToSend.add(new BasicNameValuePair("name", costCentre.getName()));
            listToSend.add(new BasicNameValuePair("number", costCentre.getNumber()));
            listToSend.add(new BasicNameValuePair("description", costCentre.getDescription()));
            listToSend.add(new BasicNameValuePair("parentCostID", costCentre.getParentCostID()));
            if (costCentre.isDeleted()) {
                listToSend.add(new BasicNameValuePair("doDelete", "Y"));
            } else {
                listToSend.add(new BasicNameValuePair("doDelete", "N"));
            }

            // Call the GetValue service
            ArrayList<NameValuePair> wsResult;
            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetCostCentreData, listToSend);

            // What did we get back?
            for (NameValuePair nvp : wsResult) {
                if (nvp.getName().equalsIgnoreCase("RESULT")) {
                    rslt = (nvp.getValue().equalsIgnoreCase("OK"));
                }
            }
        }

        // After we've saved all the cost centres, reload it so that the newly created ones can have proper cost-ids
        loadCostCentres();
        rebuildParentCostCentreNames();
        populateCostCentreHierarchyView();

        return rslt;
    }

    /**
     * Runs a web service call to retrieve all the company cost centres
     *
     */
    public final void loadCostCentres() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));

        costCentres = new ArrayList<>();
        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetCostCentreList, listToSend);

        // What did we get back?
        // The result is a JSON Array with records per cost centre
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the employees
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            CostCentre costCentre = new CostCentre(sessionUser);
            // Step through each record, and construct a COST CENTRE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("costID")) {
                    costCentre.setCostID(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("parentCostID")) {
                    costCentre.setParentCostID(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("costName")) {
                    costCentre.setName(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("costDescription")) {
                    costCentre.setDescription(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("costNumber")) {
                    costCentre.setNumber(nvp.getValue());
                }

            }
            costCentres.add(costCentre);
        }

        if (costCentres.isEmpty()) {
            CostCentre baseCostCentre = new CostCentre(sessionUser);
            baseCostCentre.setName(PageManager.translate("HeadOffice"));

            costCentres.add(baseCostCentre);
            selectedCostCentre = baseCostCentre;
        }
        rebuildParentCostCentreNames();
        populateCostCentreHierarchyView();

    }

    public CostCentre getSelectedCostCentre() {
        return selectedCostCentre;
    }

    /**
     * All the attributes for this cost centre already exists.
     *
     * @param actionEvent
     */
    public void createNewCostCentre(ActionEvent actionEvent) {
        // Does this cost centre exist?
        boolean canCreate = true;
        for (CostCentre costcentre : costCentres) {
            if (costcentre.getName().trim().equalsIgnoreCase(newCostCentre.getName().trim())) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, newCostCentre.getName() + " " + PageManager.translate("IsNotUnique"), PageManager.translate("PleaseChooseAnotherName"));
                canCreate = false;
            }
        }

        if (canCreate) {
            // try to link Cost Centre as a child to the selected cost centre
            if (selectedCostCentre != null){
                if (canBeChildOf(newCostCentre, selectedCostCentre)){
                    newCostCentre.setParentCostID(selectedCostCentre.getCostID());
                }
            }
            
//            if (newCostCentre.getParentCostCentreName() != null) {
//                CostCentre parentCC = getCostCentreByName(newCostCentre.getParentCostCentreName());
//                if (parentCC != null) {
//                    newCostCentre.setParentCostID(getCostCentreByName(newCostCentre.getParentCostCentreName()).getCostID());
//                }
//            }
            costCentres.add(newCostCentre);
            selectedCostCentre = newCostCentre;

            rebuildParentCostCentreNames();
            populateCostCentreHierarchyView();

            // Now select this entity in the treeview
            TreeNode selectedNode = getNodeFor(selectedCostCentre, costCentreRootNode);
            if (selectedNode != null) {
                selectedNode.setSelected(true);
            }

            newCostCentre = new CostCentre(sessionUser);
            newCostCentre.setParentCostCentreName(getHomeCostCentre().getName());
            
        }
        sessionUser.getPageManager().reloadFullPage();
    }


    // Returns the TreeNode representing this costCentre
    private TreeNode getNodeFor(CostCentre costCentre, TreeNode parentNode) {

        for (TreeNode treeNode : parentNode.getChildren()) {
            if (treeNode.getChildCount() == 0) {
                if (((String) treeNode.getData()).equalsIgnoreCase(costCentre.getName())) {
                    return treeNode;
                }
            } else {
                return getNodeFor(costCentre, treeNode);
            }
        }
        return null;
    }

    public void deleteSelectedCostCentre(ActionEvent actionEvent) {
        // Before we remove a cost centre, 
        // 1. Assign all the child cost centres to its parent cost centre
        CostCentre parentCC = getParent(selectedCostCentre);
        for (CostCentre costCentre : costCentres) {
            if (!costCentre.equals(selectedCostCentre)) {
                if (costCentre.getParentCostCentreName() != null) {
                    if (costCentre.getParentCostCentreName().equals(selectedCostCentre.getName())) {
                        if (parentCC != null) {
                            costCentre.setParentCostCentreName(parentCC.getName());
                            costCentre.setParentCostID(parentCC.getCostID());
                        } else {
                            costCentre.setParentCostCentreName(null);
                            costCentre.setParentCostID(null);
                        }
                    }
                } else {

                }
            }
        }
        // 2. Assign employees linked to this cost centre to its parent CC
        for (Employee employee : sessionUser.getPageManager().getCompany().getEmployeeManager().getEmployees()) {
            if (employee.getCostCentreName() != null) {
                if (employee.getCostCentreName().equalsIgnoreCase(selectedCostCentre.getName())) {
                    employee.setCostCentreName(parentCC.getName());
                    // save changes for employee
                    employee.saveEmployee();
                }
            }
        }

        selectedCostCentre.setDeleted(true);
        selectedCostCentre = null;
        populateCostCentreHierarchyView();
        rebuildParentCostCentreNames();

        sessionUser.getPageManager().reloadFullPage();

    }

    public void setValidCostCentreParents(String[] validCostCentreParents) {
        this.validCostCentreParents = validCostCentreParents;
    }

    public String[] getValidCostCentreParents() {
        if (selectedCostCentre == null) {
            String[] empty = {""};
            return (empty);
        } else {
            List<String> validCostCentreNames = new ArrayList<>();
            for (CostCentre costCentre : costCentres) {
                if (canBeChildOf(selectedCostCentre, costCentre)) {
                    validCostCentreNames.add(costCentre.getName());
                }
            }
            String[] rslt = validCostCentreNames.toArray(new String[validCostCentreNames.size()]);
            return rslt;
        }
    }

    public void onCellEdit(CellEditEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        CostCentre editedCC;
        editedCC = context.getApplication().evaluateExpressionGet(context, "#{costCentre}", CostCentre.class);
        // Already assigned the new cost centre.  We need to change that!
        String oldStringValue = (String) event.getOldValue();
        String newStringValue = (String) event.getNewValue();

        // Did we modify the 'Parent' cost centre selection?
        String column_name;
        column_name = event.getColumn().getHeaderText();
        //        
        // Changing where this cost centre fits into the hierarchy
        //
        if (column_name.equals("Parent")) {

            // Already assigned the new cost centre.  We need to change that!
            CostCentre oldParent = getCostCentreByName(oldStringValue);
            CostCentre newParent = getCostCentreByName(newStringValue);

            if (newParent != null && !newParent.equals(oldParent)) {

                // Can we permit this move?
                // Note.  At this point, AJAX has already assigned the new cost centre.  We need to first
                //  change it back.  And only apply the change if its valid
                editedCC.setParentCostCentreName(oldStringValue);

                if (!canBeChildOf(editedCC, newParent)) {
                    sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, PageManager.translate("CostCentreCannotBeLinkedToOwnDescendent"), oldParent.getName() + " -> " + newParent.getName());

                } else {
                    editedCC.setParentCostCentreName(newStringValue);

                }
            }
            populateCostCentreHierarchyView();
        }
        //
        // Cost Centre Name changed
        //
        if (column_name.equals("Name")) {
            //1. Is this name unique?
            if (!isCostCentreNameUnique(editedCC, newStringValue)) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, newStringValue + " " + PageManager.translate("IsNotUnique"), PageManager.translate("PleaseChooseAnotherName"));
                editedCC.setName(oldStringValue);
            } else {

                // Now that it's changed, update the ParentCostName where applicable
                for (CostCentre costCentre : costCentres) {
                    if (costCentre.getParentCostCentreName() != null) {
                        if (costCentre.getParentCostCentreName().equalsIgnoreCase(oldStringValue)) {
                            costCentre.setParentCostCentreName(newStringValue);
                        }
                    }
                }

                editedCC.setName(newStringValue);

                rebuildParentCostCentreNames();
                populateCostCentreHierarchyView();

                //2. Change the cost centre of those employees who were linked to this CC
                for (Employee employee : sessionUser.getPageManager().getCompany().getEmployeeManager().getEmployees()) {
                    if (employee.getCostCentreName() != null) {
                        if (employee.getCostCentreName().equalsIgnoreCase(oldStringValue)) {
                            employee.setCostCentreName(newStringValue);
                        }
                    }
                }
            }
        }
    }

    private boolean isCostCentreNameUnique(CostCentre currentCostCentre, String newName) {
        for (CostCentre costCentre : costCentres) {
            if (!costCentre.getCostID().equals(currentCostCentre.getCostID())) {
                if (costCentre.getName().trim().equalsIgnoreCase(newName)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Returns the FIRST cost centre with this given name
     *
     * @param costCentreName
     * @return
     */
    private CostCentre getCostCentreByName(String costCentreName) {
        for (CostCentre costCentre : costCentres) {
            if (costCentre.getName().trim().equalsIgnoreCase(costCentreName)) {
                return costCentre;
            }
        }
        return null;
    }

    /**
     * Scans the cost centre hierarchy to see if 'thisCC' is an ancestor node of 'newParentCC' (A parent
     * cannot become a child of its own children)
     *
     * @param childCostCentre
     * @param otherCostCentre
     * @return
     */
    private boolean canBeChildOf(CostCentre thisCC, CostCentre newParentCC) {

        // I am family of myself
        if (thisCC.getName().equalsIgnoreCase(newParentCC.getName())) {
            return false;
        }

        while (newParentCC.getParentCostID() != null) {
            String parentCostID = newParentCC.getParentCostID();
            CostCentre parentCC = getCostCentreWithID(parentCostID);
            if (parentCC != null) {
                if (parentCC.getName().equalsIgnoreCase(thisCC.getName())) {
                    return false;
                }
                newParentCC = parentCC;
            }
        }
        return true;
    }

    public void setSelectedCostCentre(CostCentre selectedCostCentre) {
        this.selectedCostCentre = selectedCostCentre;
    }

    public TreeNode getCostCentreRootNode() {
        return costCentreRootNode;
    }

    public void setCostCentreRootNode(TreeNode costCentreRootNode) {
        this.costCentreRootNode = costCentreRootNode;
    }

    public ArrayList<CostCentre> getCostCentres() {
        return costCentres;
    }

    public ArrayList<CostCentre> getGlobalCostCentres() {
        return this.costCentres;
    }

    public void setCostCentres(ArrayList<CostCentre> costCentres) {
        this.costCentres = costCentres;
    }

    public TreeNode getSelectedCostCentreNode() {
        return selectedCostCentreNode;
    }

    public void setSelectedCostCentreNode(TreeNode selectedCostCentreNode) {
        this.selectedCostCentreNode = selectedCostCentreNode;
    }

    public void onNodeSelect(NodeSelectEvent event) {
        this.selectedCostCentre = getCostCentreByName((String) event.getTreeNode().getData());
        sessionUser.getPageManager().reloadOuterPanel();
    }

}
