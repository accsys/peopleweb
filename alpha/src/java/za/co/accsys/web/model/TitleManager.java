/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import java.util.ArrayList;
import javax.faces.event.ActionEvent;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.event.SelectEvent;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.helperclasses.JSONRecord;
import za.co.accsys.web.helperclasses.ServerPrefs;

/**
 *
 * @author liam
 */
public final class TitleManager {

    private Title selectedTitle;
    private Title newTitle;
    private String newStringTitle = "";
    private final SessionUser sessionUser;
    private ArrayList<Title> titles;

    public TitleManager(SessionUser sessionUser) {
        this.sessionUser = sessionUser;

        loadTitles();
        newTitle = new Title(sessionUser);
    }

    /**
     * Runs a web service call to retrieve the list of Job Positions
     *
     * @return
     */
    public boolean loadTitles() {
        titles = new ArrayList<>();
        boolean rslt = true;

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetTitleList, listToSend);

        // What did we get back?
        // The result is a JSON Array listing titles
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the cost centre
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            Title title = new Title(sessionUser);
            title.setDeleted(false);

            // Step through each record, and construct an EMPLOYEE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("title")) {
                    title.setTitle(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("titleID")) {
                    title.setTitleID(nvp.getValue());
                }
            }
            titles.add(title);
        }
        return rslt;
    }

    public SessionUser getSessionUser() {
        return sessionUser;
    }

    public String getNewStringTitle() {
        return newStringTitle;
    }

    public void setNewStringTitle(String newStringTitle) {
        this.newStringTitle = newStringTitle;
        this.newTitle.setTitle(newStringTitle);
    }
    
    public Title getSelectedTitle() {
        return selectedTitle;
    }

    public void setSelectedTitle(Title selectedTitle) {
        this.selectedTitle = selectedTitle;
    }

    /**
     * All the attributes for this payEntity already exists in 'newPayEntity'
     *
     * @param actionEvent
     */
    public void createNewTitle(ActionEvent actionEvent) {
        // Add current 'newEntity' into the ArrayList
        saveTitle(newTitle);
        titles.add(newTitle);
        // Mark it as the current selected entity
        selectedTitle = newTitle;

        // Create a new entity for future use
        newTitle = new Title(sessionUser);
    }

    public void onRowSelect(SelectEvent event) {
        this.selectedTitle = ((Title) event.getObject());
    }

    public Title getNewTitle() {
        return newTitle;
    }

    public void setNewTitle(Title newTitle) {
        this.newTitle = newTitle;
    }

    public ArrayList<Title> getTitles() {
        return titles;
    }

    public void setTitles(ArrayList<Title> titles) {
        this.titles = titles;
    }

    /**
     * Returns a String[] of Job Positions
     *
     * @return
     */
    public String[] getStringTitles() {
        String[] rslt = new String[titles.size()];
        int i = 0;
        for (Title title : titles) {
            rslt[i++] = title.getTitle();
        }
        return rslt;
    }

    public boolean nameExists(String currentTitle) {
        int nbInstances = 0;
        for (Title title : titles) {
            if (title.getTitle().trim().equalsIgnoreCase(currentTitle)) {
                return true;
            }
        }
        return false;
    }

    public void deleteSelectedTitle(ActionEvent actionEvent) {
        selectedTitle.setDeleted(true);
        saveTitle(selectedTitle);
        titles.remove(selectedTitle);
        selectedTitle = null;
    }

    

    /**
     * Runs a web service call to update all the information for all the
     * loans/savings/earnings/deductions/etc
     *
     * @param title
     * @return
     */
    public boolean saveTitle(Title title) {

        boolean rslt = false;
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair("title", title.getTitle()));
        listToSend.add(new BasicNameValuePair("titleID", title.getTitleID()));

        if (title.isDeleted()) {
            listToSend.add(new BasicNameValuePair("deleted", "Y"));
        } else {
            listToSend.add(new BasicNameValuePair("deleted", "N"));
        }

        // Call the SetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetTitleData, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {
            if (nvp.getName().equalsIgnoreCase("titleID")) {
                title.setTitleID(nvp.getValue());
            }
        }

        return true;
    }

}
