/*
 * This class is a collection class managing all instanes of <Employee>.
 */
package za.co.accsys.web.model;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.event.SelectEvent;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.helperclasses.JSONRecord;
import za.co.accsys.web.helperclasses.ServerPrefs;

/**
 *
 * @author Liam
 */
public final class EmployeeManager {

    private final SessionUser sessionUser;
    private ArrayList<Employee> employees;
    private String filteredPayrollName;
    private ArrayList<Employee> activeEmployees;
    private ArrayList<Employee> filteredEmployees;
    private Employee selectedEmployee;
    private Employee previouslySelectedEmployee;
    private String txtSelectedEmployee;
    private boolean showDischargedEmployees;

    /**
     * Constructor. As part of the construction, we load the list of staff this user has access to
     *
     * @param user
     */
    public EmployeeManager(SessionUser user) {
        this.sessionUser = user;
        reloadEmployees();

    }

    public boolean reloadEmployees() {
        employees = loadEmployees();

        // Populate Filtered Employees
        filteredEmployees = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.isStillEngaged()) {
                filteredEmployees.add(employee);
            }
        }

        if (employees.size() > 0) {
            // Get first ACTIVE employee
            for (Employee employee : employees) {
                if (employee.isStillEngaged()) {
                    setSelectedEmployee(employee);
                    break;
                }
            }
        }
        return true;
    }

    public SessionUser getSessionUser() {
        return sessionUser;
    }

    public boolean isShowDischargedEmployees() {
        // reload the list, filtered to only show active, or all
        return showDischargedEmployees;
    }

    public String getFilteredPayrollName() {
        return filteredPayrollName;
    }

    public void setFilteredPayrollName(String filteredPayrollName) {
        if (this.filteredPayrollName == null || !this.filteredPayrollName.equals(filteredPayrollName)) {
            this.filteredPayrollName = filteredPayrollName;
            sessionUser.getPageManager().getCompany().getPayrollManager().setSelectedPayroll(sessionUser.getPageManager().getCompany().getPayrollManager().getPayrollWithName(filteredPayrollName));
            List<String> filtered = completeEmployeeSelection("");
            if (filtered.size() > 0) {
                // Navigate to first person in this filtered list
                setSelectedEmployee(getEmployeeWithString(filtered.get(0)));
            }
        }
    }

    public void setShowDischargedEmployees(boolean showDischargedEmployees) {
        this.showDischargedEmployees = showDischargedEmployees;
    }

    public Employee getPreviouslySelectedEmployee() {
        return previouslySelectedEmployee;
    }

    public void setPreviouslySelectedEmployee(Employee previouslySelectedEmployee) {
        this.previouslySelectedEmployee = previouslySelectedEmployee;
    }

    private String updateWidgetForPollComponent = null;

    public String getUpdateWidgetForPollComponent() {
        return updateWidgetForPollComponent;
    }

    /**
     * Used for displaying the currently selected employee
     *
     * @return
     */
    public String getTxtSelectedEmployee() {
        return txtSelectedEmployee;
    }

    /**
     * Sets the selected employee to be the person with the 'txtEmployee' displayed Name, Surname, [Number]
     *
     * @param txtEmployee
     */
    public void setTxtSelectedEmployee(String txtEmployee) {
        this.txtSelectedEmployee = txtEmployee;
        // Change the underlying selected employee
        for (Employee emp : employees) {
            if (emp.getToString().equalsIgnoreCase(txtEmployee)) {
                selectedEmployee = emp;
            }
        }
    }

    /**
     * Returns a list of employees that has been skipped (not calculated)
     *
     * @return
     */
    public List<Employee> getSkippedEmployees() {
        List<Employee> skippedEmployees = new ArrayList<>();

        for (Employee employee : employees) {
            // Firstname
            if (employee.getRunStatus() == 5) {
                skippedEmployees.add(employee);
            }
        }

        return skippedEmployees;
    }

    private int nbSkippedEmployees;

    public int getNbSkippedEmployees() {
        nbSkippedEmployees = getSkippedEmployees().size();
        return nbSkippedEmployees;
    }

    public Employee getSelectedEmployee() {
        return selectedEmployee;
    }

    public void setSelectedEmployee(Employee selectedEmployee) {
        if (selectedEmployee != null) {
            if (this.selectedEmployee != null) {
                if (!this.selectedEmployee.equals(selectedEmployee)) {
                    // We need to remember who the last person was, so that we can restore everything if this creation 
                    // gets cancelled.
                    this.previouslySelectedEmployee = this.selectedEmployee;
                }
            }

            this.selectedEmployee = selectedEmployee;
            txtSelectedEmployee = selectedEmployee.getToString();
        }
    }

    public ArrayList<Employee> getEmployees() {
        return this.employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getAllEmployees() {
        return employees;
    }

    private List<Employee> employeesWithCalcExceptions;

    public List<Employee> getEmployeesWithCalcExceptions() {
        employeesWithCalcExceptions = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.isRenderDebugReport()) {
                employeesWithCalcExceptions.add(employee);
            }
        }
        return employeesWithCalcExceptions;
    }

    private List<PayEntityInstance> allPayEntityInstances;

    /**
     * Returns the list of earnings/deductions/loans/etc for all employees in their current open period
     *
     * @return
     */
    public List<PayEntityInstance> getAllPayEntityInstances() {
        allPayEntityInstances = new ArrayList<>();
        for (Employee employee : employees) {
            for (PayEntityInstance instance : employee.getPayEntityInstances()) {
                allPayEntityInstances.add(instance);
            }
        }

        return allPayEntityInstances;
    }

    /**
     * Runs a web service call to retrieve all the employees that this person has access to
     *
     * @return - ArrayList<Employee>
     */
    private ArrayList<Employee> loadEmployees() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

        // List of employees retrieved from web server
        ArrayList<Employee> result = new ArrayList<>();

        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetEmployeeList, listToSend);

        // What did we get back?
        // The result is a JSON Array with records per employee
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the employees
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            Employee employee = new Employee(sessionUser, this);
            // Step through each record, and construct an EMPLOYEE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase(Constants.receiveParam_EmployeeID)) {
                    employee.setEmployeeID(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("Preferred_name")) {
                    employee.setPreferredName(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("Firstname")) {
                    employee.setFirstname(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("Surname")) {
                    employee.setSurname(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("EmployeeNumber")) {
                    employee.setEmployeeNumber(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("stillEngaged")) {
                    employee.setStillEngaged(nvp.getValue().equalsIgnoreCase("Y"));
                }
                if (nvp.getName().equalsIgnoreCase("PAYROLL_ID")) {
                    employee.setPayrollID(nvp.getValue());
                }
            }
            employee.loadEmployee();
            result.add(employee);
        }

        return result;

    }

    public ArrayList<Employee> getFilteredEmployees() {
        return filteredEmployees;
    }

    public void setFilteredEmployees(ArrayList<Employee> filteredEmployees) {
        this.filteredEmployees = filteredEmployees;
    }

    /**
     * Returns the list of employees linked to this payroll
     *
     * @param payroll
     * @param activeOnly
     * @return
     */
    public ArrayList<Employee> getEmployeesForPayroll(Payroll payroll, boolean activeOnly) {
        ArrayList<Employee> rslt = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getPayroll().equals(payroll)) {
                if ((activeOnly && employee.isStillEngaged()) || (!activeOnly)) {
                    rslt.add(employee);
                }
            }
        }
        return rslt;
    }

    /**
     * Routine that creates the drop-down list of employees that the person wishes to select one from.
     *
     * @param searchString
     * @return
     */
    public List<String> completeEmployeeSelection(String searchString) {
        List<String> results = new ArrayList<>();
        ArrayList<Employee> _localList;
        if (searchString.trim().length() > 0) {
            _localList = getEmployeesStartingWith(searchString);
        } else {
            _localList = employees;
        }

        for (Employee filteredEmployee : _localList) {
            String empString = "";
            // Active
            if (filteredEmployee.isStillEngaged()) {
                empString = filteredEmployee.getToString();
            }
            // Discharged but we still want them
            if ((!filteredEmployee.isStillEngaged()) && showDischargedEmployees) {
                empString = "* " + filteredEmployee.getToString();
            }

            // Do we filter by Payroll Name?
            if (filteredPayrollName != null) {
                if (!filteredPayrollName.isEmpty()) {
                    if (!filteredEmployee.getPayroll().getPayrollName().equalsIgnoreCase(filteredPayrollName)) {
                        empString = "";
                    }
                }
            }

            // Add to resultset
            if (!empString.isEmpty()) {
                results.add(empString);
            }
        }
        return results;
    }

    /**
     * Returns the list of ACTIVE employees
     *
     * @return
     */
    public List<Employee> getActiveEmployees() {
        if (activeEmployees == null) {
            activeEmployees = new ArrayList<>();
        }
        activeEmployees.clear();
        for (Employee employee : employees) {
            if (employee.isStillEngaged()) {
                activeEmployees.add(employee);
            }
        }
        return activeEmployees;
    }

    /**
     * Returns a list of employees that has 'searchString' as part of the surname, firstname, or number
     *
     * @param searchString
     * @return
     */
    private ArrayList<Employee> getEmployeesStartingWith(String searchString) {
        ArrayList<Employee> subselectionEmployees = new ArrayList<>();

        for (Employee employee : employees) {
            // Firstname
            if ((employee.getFirstname() != null) && (employee.getFirstname().toUpperCase().startsWith(searchString.trim().toUpperCase()))) {
                subselectionEmployees.add(employee);
            } else // Surname
            if ((employee.getSurname() != null) && (employee.getSurname().toUpperCase().startsWith(searchString.trim().toUpperCase()))) {
                subselectionEmployees.add(employee);
            } else // Number
            if ((employee.getEmployeeNumber() != null) && (employee.getEmployeeNumber().toUpperCase().startsWith(searchString.trim().toUpperCase()))) {
                subselectionEmployees.add(employee);
            }
        }

        return subselectionEmployees;
    }

    // Person selected an employee
    public void handleEmployeeSelect(SelectEvent event) {
        selectedEmployee = getEmployeeWithString(event.getObject().toString());

    }

    /**
     * Returns the object that has the employee's name, surname, and number
     *
     * @param aNameSurnameAndNumber
     * @return
     */
    private Employee getEmployeeWithString(String aNameSurnameAndNumber) {
        for (Employee employee : employees) {
            if (aNameSurnameAndNumber.contains(employee.getToString())) {
                return employee;
            }
        }
        return null;
    }

    public void onRowSelect(SelectEvent event) {
        this.selectedEmployee = ((Employee) event.getObject());
    }
}
