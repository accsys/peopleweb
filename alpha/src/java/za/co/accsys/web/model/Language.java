/*
 * Represents a Language
 */
package za.co.accsys.web.model;

/**
 *
 * @author liam
 */
public class Language {

    private String language;
    private String languageID;
    private boolean deleted;
    private final SessionUser sessionUser;

    public Language(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        deleted = false;
        languageID = "-1";
        language = "new:" + (int) Math.ceil(Math.random() * 100);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
         if ((this.language == null) || (!this.language.equals(language))) {
            //Is the LANGUAGE unique?
            if (sessionUser.getPageManager().getCompany().getLanguageManager() != null) {
                if (sessionUser.getPageManager().getCompany().getLanguageManager().languageExists(language)) {
                    language = language + "_";
                }
            }
            this.language = language;
            if (sessionUser.getPageManager().getCompany().getLanguageManager() != null) {
                sessionUser.getPageManager().getCompany().getLanguageManager().saveLanguage(this);
            }
        }
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void delete() {
        this.deleted = true;
    }

    public String getLanguageID() {
        return languageID;
    }

    public void setLanguageID(String languageID) {
        this.languageID = languageID;
    }

    public void setDeleted(boolean deleted) {
        if (this.deleted != deleted) {
            this.deleted = deleted;
        }
    }

}
