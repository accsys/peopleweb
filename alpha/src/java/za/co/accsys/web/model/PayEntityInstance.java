/*
 * This object represents a single variabe/loan/saving per person, per period
 */
package za.co.accsys.web.model;

import za.co.accsys.web.beans.EnumVarPayEntityType;
import za.co.accsys.web.beans.PageManager;

/**
 *
 * @author liam
 */
public class PayEntityInstance {

    private PayrollPeriod payrollPeriod;
    private PayEntity payEntity;
    private String tempValue;
    private String calcValue;
    private String balance;
    private boolean unlinked;
    private boolean removeTempValue;
    private String interpretedFormula;
    private String formula;
    private String referenceNo;
    private final Employee owner;
    private boolean rfi;

    public PayEntityInstance(Employee owner) {
        this.unlinked = false;
        this.owner = owner;
        this.rfi = false;
    }

    public String getBalance() {
        return balance;
    }

    public boolean isRemoveTempValue() {
        return removeTempValue;
    }

    public Employee getOwner() {
        return owner;
    }

    public void setRemoveTempValue(boolean removeTempValue) {
        this.removeTempValue = removeTempValue;
    }

    public String getFormula() {
        return payEntity.getCalculationFormula();
    }

    public boolean isRfi() {
        return rfi;
    }

    public void setRfi(boolean rfi) {
        this.rfi = rfi;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public boolean isUnlinked() {
        return unlinked;
    }

    public String getInterpretedFormula() {
        return interpretedFormula;
    }

    public void setInterpretedFormula(String interpretedFormula) {
        this.interpretedFormula = interpretedFormula;
    }

    public void setUnlinked(boolean unlinked) {
        this.unlinked = unlinked;
        owner.saveEmployeePayPackage(this);
        owner.waitForCalcCompletion();
    }

    public void markAsUnlinked() {
        setUnlinked(true);
    }

    public void undoTempOverride() {
        this.tempValue = null;
        this.setRemoveTempValue(true);
        owner.saveEmployeePayPackage(this);
        owner.waitForCalcCompletion();
    }

    public void toggleRFI() {
        setRfi(!this.rfi);
        // Recalculate
        owner.saveEmployeePayPackage(this);
        owner.waitForCalcCompletion();
    }

    private boolean showRFI;

    public boolean isShowRFI() {
        showRFI = this.owner.getPayroll().getTaxCountry().equalsIgnoreCase("South Africa") && getPayEntity().isBeRFI();
        return showRFI;
    }

    public PayrollPeriod getPayrollPeriod() {
        return payrollPeriod;
    }

    public void setPayrollPeriod(PayrollPeriod payrollPeriod) {
        this.payrollPeriod = payrollPeriod;
    }

    public PayEntity getPayEntity() {
        return payEntity;
    }

    /**
     * A PayEntityInstance is an instance of an Earning/Deduction/Loan/..
     * associated with a person. This method assigns the
     * Earning/Deduction/Loan/... to the instance
     *
     * @param payEntity
     */
    public void setPayEntity(PayEntity payEntity) {
        this.payEntity = payEntity;
    }

    public String getTempValue() {
        return tempValue;
    }

    /**
     * Method for initiating value without firing an update to the underlying
     * data store
     *
     * @param tempValue
     */
    public void initiateTempValue(String tempValue) {
        this.tempValue = tempValue;
    }

    public void setTempValue(String tempValue) {
        if (tempValue.trim().length() == 0) {
            tempValue = null;
        }

        initiateTempValue(tempValue);
        owner.saveEmployeePayPackage(this);
        owner.waitForCalcCompletion();
    }

    public void initiateBalance(String balance) {
        this.balance = balance;
    }

    public void setBalance(String balance) {
        initiateBalance(balance);
        owner.saveEmployeePayPackage();
        owner.redoPayCalc();
//        owner.waitForCalcCompletion();
    }

    public String getCalcValue() {
        if (this.calcValue != null && this.calcValue.trim().length() > 0) {
            return this.calcValue;
        } else {
            return this.tempValue;
        }
    }

    /**
     * Constructs a display of the value + unit or currency + value
     *
     * @return
     */
    public String getCurrencyAndValue() {

        String sDecimalValue = PageManager.formatStringAsCurrency(getCalcValue(), owner.getSessionUser().getUserLocale());
        if (this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Earning
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Deduction
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Loan
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Saving
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Tax
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_PackageComponent) {
            return owner.getSessionUser().getPageManager().getCompany().getPayrollManager().getPayrollWithID(this.payrollPeriod.getPayrollID()).getCurrencyString() + " " + sDecimalValue;
        } else {
            return getCalcValue() + " (" + payEntity.getUnit() + ")";
        }
    }

    public String getCurrencyAndBalance() {
        String sDecimalValue = PageManager.formatStringAsCurrency(balance, owner.getSessionUser().getUserLocale());
        if (this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Earning
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Deduction
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Loan
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Saving
                || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Tax) {
            return owner.getSessionUser().getPageManager().getCompany().getPayrollManager().getPayrollWithID(this.payrollPeriod.getPayrollID()).getCurrencyString() + " " + sDecimalValue;
        } else {
            return calcValue + "(" + payEntity.getUnit() + ")";
        }
    }

    public void setCalcValue(String calcValue) {
        this.calcValue = calcValue;
    }

}
