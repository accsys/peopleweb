/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import za.co.accsys.web.beans.EnumVarPayEntityType;

/**
 *
 * @author liam
 */
public class TaxCode {

    private  final String defaultCountry = "SOUTH AFRICA";
    private  final String defaultCode = "3601";
    private  final String defaultDescription = "Income taxable";
    private String taxCountry;
    private String code;
    private String description;
    private EnumVarPayEntityType payEntityType;
    private String codeAndDescription;

    public TaxCode() {
        this.taxCountry = defaultCountry;
        this.code = defaultCode;
        this.description = defaultDescription;
    }

    public String getCodeAndDescription() {
        this.codeAndDescription = this.code + " - " + this.description;
        return codeAndDescription;
    }

    public String getTaxCountry() {
        return taxCountry;
    }

    public void setTaxCountry(String taxCountry) {
        this.taxCountry = taxCountry;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }
 
    public void setDescription(String description) {
        this.description = description;
    }

    public EnumVarPayEntityType getPayEntityType() {
        return payEntityType;
    }

    public void setPayEntityType(EnumVarPayEntityType payEntityType) {
        this.payEntityType = payEntityType;
    }

}
