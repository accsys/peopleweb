/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import java.io.FileNotFoundException;
import za.co.accsys.web.beans.Constants;
import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.model.StreamedContent;
import za.co.accsys.web.beans.ReportPrinter;
import za.co.accsys.web.helperclasses.ServerPrefs;
import za.co.accsys.web.controller.WebServiceClient;

/**
 *
 * @author lterblanche
 */
public class Company {

    private String name;
    private String registrationNo;
    private String taxNo;
    private String phone;
    private String fax;
    private String email;
    private boolean diplomaticIndemnity;
    private String pensionNo;
    private String postalCountry;
    private String postalProvince;
    private String postalAddress;
    private String postalSuburb;
    private String postalCity;
    private String postalCode;
    private String physUnitNo;
    private String physComplex;
    private String physStreetNo;
    private String physStreet;
    private String physSuburb;
    private String physCity;
    private String physCode;
    private String physProvince;
    private String physCountry;
    private boolean postalSameAsPhysical;
    private String mainBusinessActivity;
    private String tradeCode;
    private String tradeClassification;
    private String tradeSubCode;
    private String tradeSubClassification;
    private String setaCode;
    private String setaDescription;
    private String sicCode;
    private String sicName;
    private String selectedCostCentreName;
    private String selectedCostCentreNumber;
    private String selectedCostCentreNamePath;
    private String selectedCostCentreNumberPath;
    private CostCentreManager costCentreManager;
    private PayrollManager payrollManager;
    private PayEntityManager payEntityManager;
    private EmployeeManager employeeManager;
    private JobPositionManager jobPositionManager;
    private TitleManager titleManager;
    private LanguageManager languageManager;
    private BankAccount bankAccount;

    private SessionUser sessionUser;

    public Company(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        loadCompany();
        setDefaults();

    }

    /**
     * All these 'Managers' require a reference to the company itself. For this to work, one has to
     * instantiate the class first, and only then can one instantiate the underlying container classes. Note:
     * This method MUST be called immediately after the constructor.
     */
    public void initialiseContainerClasses() {
        costCentreManager = new CostCentreManager(sessionUser);
        payrollManager = new PayrollManager(sessionUser);
        payEntityManager = new PayEntityManager(sessionUser);
        employeeManager = new EmployeeManager(sessionUser);
        jobPositionManager = new JobPositionManager(sessionUser);
        titleManager = new TitleManager(sessionUser);
        languageManager = new LanguageManager(sessionUser);
    }

    private void setDefaults() {
        setPostalCountry("South Africa");
        setPhysCountry("South Africa");
    }

    public final boolean loadCompany() {
        return (loadCompanyInfo() && loadCompanyAccountData());
    }

    public boolean saveCompany() {
        boolean rslt = (saveCompanyInfo()
                && saveCompanyAccountData());
        return rslt;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        if ((this.bankAccount == null) || (!this.bankAccount.equals(bankAccount))) {
            this.bankAccount = bankAccount;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public JobPositionManager getJobPositionManager() {
        return jobPositionManager;
    }

    public TitleManager getTitleManager() {
        return titleManager;
    }

    public LanguageManager getLanguageManager() {
        return languageManager;
    }

    public PayEntityManager getPayEntityManager() {
        return payEntityManager;
    }

    public EmployeeManager getEmployeeManager() {
        return employeeManager;
    }

    public void createNewEmployee() {
        // Create a new employee
        Employee employee = new Employee(sessionUser, employeeManager);
        employee.setStillEngaged(true);
        employeeManager.getEmployees().add(employee);
        employeeManager.setSelectedEmployee(employee);
        if (employee.isStillEngaged()) {
            employeeManager.getFilteredEmployees().add(employee);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if ((this.name == null) || (!this.name.equals(name))) {
            this.name = name;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        if ((this.registrationNo == null) || (!this.registrationNo.equals(registrationNo))) {
            this.registrationNo = registrationNo;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        if ((this.taxNo == null) || (!this.taxNo.equals(taxNo))) {
            this.taxNo = taxNo;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if ((this.phone == null) || (!this.phone.equals(phone))) {
            this.phone = phone;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        if ((this.fax == null) || (!this.fax.equals(fax))) {
            this.fax = fax;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if ((this.email == null) || (!this.email.equals(email))) {
            this.email = email;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public boolean getDiplomaticIndemnity() {
        return diplomaticIndemnity;
    }

    public void setDiplomaticIndemnity(boolean diplomaticIndemnity) {
        if (this.diplomaticIndemnity != diplomaticIndemnity) {
            this.diplomaticIndemnity = diplomaticIndemnity;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPensionNo() {
        return pensionNo;
    }

    public void setPensionNo(String pensionNo) {
        if ((this.pensionNo == null) || (!this.pensionNo.equals(pensionNo))) {
            this.pensionNo = pensionNo;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostalCountry() {
        return postalCountry;
    }

    public void setPostalCountry(String postalCountry) {
        if ((this.postalCountry == null) || (!this.postalCountry.equals(postalCountry))) {
            this.postalCountry = postalCountry;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostalProvince() {
        return postalProvince;
    }

    public void setPostalProvince(String postalProvince) {
        if ((this.postalProvince == null) || (!this.postalProvince.equals(postalProvince))) {
            this.postalProvince = postalProvince;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        if ((this.postalAddress == null) || (!this.postalAddress.equals(postalAddress))) {
            this.postalAddress = postalAddress;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostalSuburb() {
        return postalSuburb;
    }

    public void setPostalSuburb(String postalSuburb) {
        if ((this.postalSuburb == null) || (!this.postalSuburb.equals(postalSuburb))) {
            this.postalSuburb = postalSuburb;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostalCity() {
        return postalCity;
    }

    public void setPostalCity(String postalCity) {
        if ((this.postalCity == null) || (!this.postalCity.equals(postalCity))) {
            this.postalCity = postalCity;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        if ((this.postalCode == null) || (!this.postalCode.equals(postalCode))) {
            this.postalCode = postalCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysUnitNo() {
        return physUnitNo;
    }

    public void setPhysUnitNo(String physUnitNo) {
        if ((this.physUnitNo == null) || (!this.physUnitNo.equals(physUnitNo))) {
            this.physUnitNo = physUnitNo;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysComplex() {
        return physComplex;
    }

    public void setPhysComplex(String physComplex) {
        if ((this.physComplex == null) || (!this.physComplex.equals(physComplex))) {
            this.physComplex = physComplex;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysStreetNo() {
        return physStreetNo;
    }

    public void setPhysStreetNo(String physStreetNo) {
        if ((this.physStreetNo == null) || (!this.physStreetNo.equals(physStreetNo))) {
            this.physStreetNo = physStreetNo;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysStreet() {
        return physStreet;
    }

    public void setPhysStreet(String physStreet) {
        if ((this.physStreet == null) || (!this.physStreet.equals(physStreet))) {
            this.physStreet = physStreet;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysSuburb() {
        return physSuburb;
    }

    public void setPhysSuburb(String physSuburb) {
        if ((this.physSuburb == null) || (!this.physSuburb.equals(physSuburb))) {
            this.physSuburb = physSuburb;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysCity() {
        return physCity;
    }

    public void setPhysCity(String physCity) {
        if ((this.physCity == null) || (!this.physCity.equals(physCity))) {
            this.physCity = physCity;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysCode() {
        return physCode;
    }

    public void setPhysCode(String physCode) {
        if ((this.physCode == null) || (!this.physCode.equals(physCode))) {
            this.physCode = physCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysProvince() {
        return physProvince;
    }

    public void setPhysProvince(String physProvince) {
        if ((this.physProvince == null) || (!this.physProvince.equals(physProvince))) {
            this.physProvince = physProvince;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPhysCountry() {
        return physCountry;
    }

    public void setPhysCountry(String physCountry) {
        if ((this.physCountry == null) || (!this.physCountry.equals(physCountry))) {
            this.physCountry = physCountry;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public boolean getPostalSameAsPhysical() {
        return postalSameAsPhysical;
    }

    public void setPostalSameAsPhysical(boolean postalSameAsPhysical) {
        if (this.postalSameAsPhysical != postalSameAsPhysical) {
            this.postalSameAsPhysical = postalSameAsPhysical;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getMainBusinessActivity() {
        return mainBusinessActivity;
    }

    public void setMainBusinessActivity(String mainBusinessActivity) {
        if ((this.mainBusinessActivity == null) || (!this.mainBusinessActivity.equals(mainBusinessActivity))) {
            this.mainBusinessActivity = mainBusinessActivity;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        if ((this.tradeCode == null) || (!this.tradeCode.equals(tradeCode))) {
            this.tradeCode = tradeCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getTradeClassification() {
        return tradeClassification;
    }

    public void setTradeClassification(String tradeClassification) {
        if ((this.tradeClassification == null) || (!this.tradeClassification.equals(tradeClassification))) {
            this.tradeClassification = tradeClassification;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getTradeSubCode() {
        return tradeSubCode;
    }

    public void setTradeSubCode(String tradeSubCode) {
        if ((this.tradeSubCode == null) || (!this.tradeSubCode.equals(tradeSubCode))) {
            this.tradeSubCode = tradeSubCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getTradeSubClassification() {
        return tradeSubClassification;
    }

    public void setTradeSubClassification(String tradeSubClassification) {
        if ((this.tradeSubClassification == null) || (!this.tradeSubClassification.equals(tradeSubClassification))) {
            this.tradeSubClassification = tradeSubClassification;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSetaCode() {
        return setaCode;
    }

    public void setSetaCode(String setaCode) {
        if ((this.setaCode == null) || (!this.setaCode.equals(setaCode))) {
            this.setaCode = setaCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSetaDescription() {
        return setaDescription;
    }

    public void setSetaDescription(String setaDescription) {
        if ((this.setaDescription == null) || (!this.setaDescription.equals(setaDescription))) {
            this.setaDescription = setaDescription;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSicCode() {
        return sicCode;
    }

    public void setSicCode(String sicCode) {
        if ((this.sicCode == null) || (!this.sicCode.equals(sicCode))) {
            this.sicCode = sicCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSicName() {
        return sicName;
    }

    public void setSicName(String sicName) {
        if ((this.sicName == null) || (!this.sicName.equals(sicName))) {
            this.sicName = sicName;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public SessionUser getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
    }

    public String getSelectedCostCentreName() {
        return selectedCostCentreName;
    }

    public void setSelectedCostCentreName(String selectedCostCentreName) {
        if ((this.selectedCostCentreName == null) || (!this.selectedCostCentreName.equals(selectedCostCentreName))) {
            this.selectedCostCentreName = selectedCostCentreName;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSelectedCostCentreNumber() {
        return selectedCostCentreNumber;
    }

    public void setSelectedCostCentreNumber(String selectedCostCentreNumber) {
        if ((this.selectedCostCentreNumber == null) || (!this.selectedCostCentreNumber.equals(selectedCostCentreNumber))) {
            this.selectedCostCentreNumber = selectedCostCentreNumber;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSelectedCostCentreNamePath() {
        return selectedCostCentreNamePath;
    }

    public void setSelectedCostCentreNamePath(String selectedCostCentreNamePath) {
        if ((this.selectedCostCentreNamePath == null) || (!this.selectedCostCentreNamePath.equals(selectedCostCentreNamePath))) {
            this.selectedCostCentreNamePath = selectedCostCentreNamePath;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getSelectedCostCentreNumberPath() {
        return selectedCostCentreNumberPath;
    }

    public void setSelectedCostCentreNumberPath(String selectedCostCentreNumberPath) {
        if ((this.selectedCostCentreNumberPath == null) || (!this.selectedCostCentreNumberPath.equals(selectedCostCentreNumberPath))) {
            this.selectedCostCentreNumberPath = selectedCostCentreNumberPath;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public boolean isDiplomaticIndemnity() {
        return diplomaticIndemnity;
    }

    public boolean isPostalSameAsPhysical() {
        return postalSameAsPhysical;
    }

    public CostCentreManager getCostCentreManager() {
        return costCentreManager;
    }

    public PayrollManager getPayrollManager() {
        return payrollManager;
    }

    public final boolean loadCompanyInfo() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));

        // Call the GetValue service
        ArrayList<NameValuePair> companyAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        companyAttributes = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_GetCompanyData, listToSend);

        // What did we get back?
        // Step through each record, and construct a COMPANY instance
        for (NameValuePair nvp : companyAttributes) {

            if (nvp.getName().equalsIgnoreCase("name")) {
                name = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("registrationNo")) {
                registrationNo = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("taxNo")) {
                taxNo = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("phone")) {
                phone = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("fax")) {
                fax = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("email")) {
                email = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("diplomaticIndemnity")) {
                diplomaticIndemnity = (nvp.getValue().equalsIgnoreCase("Y"));
            }
            if (nvp.getName().equalsIgnoreCase("pensionNo")) {
                pensionNo = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postalCountry")) {
                postalCountry = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postalProvince")) {
                postalProvince = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postalAddress")) {
                postalAddress = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postalSuburb")) {
                postalSuburb = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postalCity")) {
                postalCity = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postalCode")) {
                postalCode = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physUnitNo")) {
                physUnitNo = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physComplex")) {
                physComplex = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physStreetNo")) {
                physStreetNo = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physStreet")) {
                physStreet = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physSuburb")) {
                physSuburb = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physCity")) {
                physCity = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physCode")) {
                physCode = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physProvince")) {
                physProvince = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("physCountry")) {
                physCountry = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("postalSameAsPhysical")) {
                postalSameAsPhysical = (nvp.getValue().equalsIgnoreCase("1"));
            }
            if (nvp.getName().equalsIgnoreCase("mainBusinessActivity")) {
                mainBusinessActivity = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("tradeCode")) {
                tradeCode = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("tradeClassification")) {
                tradeClassification = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("tradeSubCode")) {
                tradeSubCode = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("tradeSubClassification")) {
                tradeSubClassification = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("setaCode")) {
                setaCode = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("setaDescription")) {
                setaDescription = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("sicCode")) {
                sicCode = (nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("sicName")) {
                sicName = (nvp.getValue());
            }

        }
        return true;
    }

    /**
     * Runs a web service call to retrieve the first bank account for this employee
     *
     * @return
     */
    public boolean loadCompanyAccountData() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));

        // Call the GetValue service
        ArrayList<NameValuePair> employeeAttributes;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        employeeAttributes = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_GetCompanyAccountData, listToSend);

        bankAccount = new BankAccount(sessionUser);
        // What did we get back?
        // Step through each record, and construct an EMPLOYEE instance
        for (NameValuePair nvp : employeeAttributes) {
            //
            // Bank Account details
            //
            if (nvp.getName().equalsIgnoreCase("accountID")) {
                bankAccount.setAccountID(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountType")) {
                bankAccount.setAccountType(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountName")) {
                bankAccount.setAccountName(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountDescription")) {
                bankAccount.setAccountDescription(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountBank")) {
                bankAccount.setAccountBank(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountBranch")) {
                bankAccount.setAccountBranch(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountBankCode")) {
                bankAccount.setAccountBankCode(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountBranchCode")) {
                bankAccount.setAccountBranchCode(nvp.getValue());
            }
            if (nvp.getName().equalsIgnoreCase("accountNumber")) {
                bankAccount.setAccountNumber(nvp.getValue());
            }

        }
        return true;
    }

    public final boolean saveCompanyInfo() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair("name", name));
        listToSend.add(new BasicNameValuePair("registrationNo", registrationNo));
        listToSend.add(new BasicNameValuePair("taxNo", taxNo));
        listToSend.add(new BasicNameValuePair("phone", phone));
        listToSend.add(new BasicNameValuePair("fax", fax));
        listToSend.add(new BasicNameValuePair("email", email));
        if (diplomaticIndemnity) {
            listToSend.add(new BasicNameValuePair("diplomaticIndemnity", "1"));
        } else {
            listToSend.add(new BasicNameValuePair("diplomaticIndemnity", "0"));
        }
        listToSend.add(new BasicNameValuePair("pensionNo", pensionNo));
        listToSend.add(new BasicNameValuePair("postalCountry", postalCountry));
        listToSend.add(new BasicNameValuePair("postalProvince", postalProvince));
        listToSend.add(new BasicNameValuePair("postalAddress", postalAddress));
        listToSend.add(new BasicNameValuePair("postalSuburb", postalSuburb));
        listToSend.add(new BasicNameValuePair("postalCity", postalCity));
        listToSend.add(new BasicNameValuePair("postalCode", postalCode));
        listToSend.add(new BasicNameValuePair("physUnitNo", physUnitNo));
        listToSend.add(new BasicNameValuePair("physComplex", physComplex));
        listToSend.add(new BasicNameValuePair("physStreetNo", physStreetNo));
        listToSend.add(new BasicNameValuePair("physSuburb", physSuburb));
        listToSend.add(new BasicNameValuePair("physCity", physCity));
        listToSend.add(new BasicNameValuePair("physStreet", physStreet));
        listToSend.add(new BasicNameValuePair("physCode", physCode));
        listToSend.add(new BasicNameValuePair("physProvince", physProvince));
        listToSend.add(new BasicNameValuePair("physCountry", physCountry));
        if (postalSameAsPhysical) {
            listToSend.add(new BasicNameValuePair("postalSameAsPhysical", "1"));
        } else {
            listToSend.add(new BasicNameValuePair("postalSameAsPhysical", "0"));
        }
        listToSend.add(new BasicNameValuePair("mainBusinessActivity", mainBusinessActivity));
        listToSend.add(new BasicNameValuePair("tradeCode", tradeCode));
        listToSend.add(new BasicNameValuePair("tradeSubCode", tradeSubCode));
        listToSend.add(new BasicNameValuePair("setaCode", setaCode));
        listToSend.add(new BasicNameValuePair("sicCode", sicCode));

        // Call the GetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetCompanyData, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {

            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                return (nvp.getValue().equalsIgnoreCase("OK"));
            }
        }

        return false;
    }

    /**
     * Runs a web service call to update account information for this employee
     *
     * @return
     */
    public boolean saveCompanyAccountData() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair("accountType", bankAccount.getAccountType()));
        listToSend.add(new BasicNameValuePair("accountName", bankAccount.getAccountName()));
        listToSend.add(new BasicNameValuePair("accountDescription", bankAccount.getAccountDescription()));
        listToSend.add(new BasicNameValuePair("accountBank", bankAccount.getAccountBank()));
        listToSend.add(new BasicNameValuePair("accountBranch", bankAccount.getAccountBranch()));
        listToSend.add(new BasicNameValuePair("accountBankCode", bankAccount.getAccountBankCode()));
        listToSend.add(new BasicNameValuePair("accountBranchCode", bankAccount.getAccountBranchCode()));
        listToSend.add(new BasicNameValuePair("accountNumber", bankAccount.getAccountNumber()));

        // Call the GetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetCompanyAccountData, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {

            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                return (nvp.getValue().equalsIgnoreCase("OK"));
            }
        }
        return false;
    }

    /**
     * Used when exporting data to Excel
     *
     * @param document
     */
    public void postProcessXLS(Object document) {
        // Set up HEADER style
        HSSFWorkbook wb = (HSSFWorkbook) document;
        // Bold
        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        HSSFSheet sheet = wb.getSheetAt(0);
        CellStyle style = wb.createCellStyle();
        style.setFont(font);

        int i = 0;
        for (Row row : sheet) {
            // Only pdate the header row
            if (i == 0) {
                for (Cell cell : row) {
                    cell.setCellValue(cell.getStringCellValue().toUpperCase());
                    cell.setCellStyle(style);
                }
            }
            i++;
        }
    }

    /**
     * Generates a payslip for each employee and emails it to him/her
     */
    public void emailAllPayslips() {
        for (Employee employee : employeeManager.getFilteredEmployees()) {
            if (employee.isValidPayslip()) {
                employee.emailPayslip();
            }
        }
    }

    /**
     * Generates a payslip for each employee and shows it in one file
     */
    private StreamedContent allPayslips;

    public StreamedContent getAllPayslips() throws FileNotFoundException {
        return allPayslips;
    }

    public void setAllPayslips(StreamedContent allPayslips) {
        this.allPayslips = allPayslips;
    }

    public void prepareAllPayslips() {
        ReportPrinter printer = new ReportPrinter();
        allPayslips = printer.getMultiplePayslips(sessionUser, employeeManager.getFilteredEmployees());
    }
    
     /**
     * Generates a TaxCertificate for each employee and shows it in one file
     */
    private StreamedContent allTaxCertificates;

    public StreamedContent getAllTaxCertificates() throws FileNotFoundException {
        return allTaxCertificates;
    }

    public void setAllTaxCertificates(StreamedContent allTaxCertificates) {
        this.allTaxCertificates = allTaxCertificates;
    }

    public void prepareAllTaxCertificates() {
        ReportPrinter printer = new ReportPrinter();
        allTaxCertificates = printer.getMultipleTaxCertificates(sessionUser, employeeManager.getFilteredEmployees());
    }
    
    /**
     * Generates a tax certificate for each employee and emails it to him/her
     */
    public void emailAllTaxCertificates() {
        for (Employee employee : employeeManager.getFilteredEmployees()) {
            if (employee.isValidTaxCertificate()) {
                employee.emailTaxCertificate();
            }
        }
    }
    
    private boolean filteredEmployeesHasTaxCertificates;

    public boolean isFilteredEmployeesHasTaxCertificates() {
        filteredEmployeesHasTaxCertificates = false;
        for (Employee emp : employeeManager.getFilteredEmployees()) {
            filteredEmployeesHasTaxCertificates = filteredEmployeesHasTaxCertificates || emp.isValidTaxCertificate();
        }
        return filteredEmployeesHasTaxCertificates;
    }
    
    private boolean filteredEmployeesHasPayslips;

    public boolean isFilteredEmployeesHasPayslips() {
        filteredEmployeesHasPayslips = false;
        for (Employee emp : employeeManager.getFilteredEmployees()) {
            filteredEmployeesHasPayslips = filteredEmployeesHasPayslips || emp.isValidPayslip();
        }
        return filteredEmployeesHasPayslips;
    }
}
