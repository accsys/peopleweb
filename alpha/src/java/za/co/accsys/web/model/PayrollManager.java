/*
 * Collection class for handling payrolls
 */
package za.co.accsys.web.model;

import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.helperclasses.JSONRecord;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.helperclasses.ServerPrefs;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.helperclasses.DateUtils;
import za.co.accsys.web.helperclasses.ListManager;

/**
 *
 * @author lterblanche
 */
public final class PayrollManager {

    private Payroll selectedPayroll;
    private Payroll newPayroll;
    private final SessionUser sessionUser;
    private ArrayList<Payroll> payrolls;
    private TreeNode payrollRootNode;
    private TreeNode selectedPayrollNode;

    public PayrollManager(SessionUser sessionUser) {
        this.sessionUser = sessionUser;

        loadPayrolls();
        loadPayrollsStatus();

        newPayroll = new Payroll(sessionUser);
    }

    /**
     * Used in the constructor to rebuild the (flat) payroll hierarchy
     */
    private void populatePayrollHierarchyView() {
        // Fetch the root cost centre
        Payroll rootPayroll = new Payroll(sessionUser);
        rootPayroll.setPayrollName(PageManager.translate("Company"));
        rootPayroll.setPayrollID("0");

        payrollRootNode = new DefaultTreeNode(rootPayroll.getPayrollName(), null);
        payrollRootNode.setExpanded(true);
        // Populate Cost Centre hierarchy
        populateBaseWithPayrolls(payrollRootNode);
    }

    /**
     * Reloads the calculation count, engagements, discharges, etc, of each
     * payroll
     */
    public void reloadPayrollsStatus() {
        loadPayrollsStatus();
        if (RequestContext.getCurrentInstance() != null) {
            RequestContext.getCurrentInstance().execute("javascript:window.location.reload()");
        }
    }

    /**
     * Reloads the calculation count, engagements, discharges, etc, of each
     * payroll
     */
    private void loadPayrollsStatus() {
        String payrollID = null;
        int nbActiveEmployees = 0;
        int nbInactiveEmployees = 0;
        int nbPreviousActiveEmployees = 0;
        int nbEngaged = 0;
        int nbDischarged = 0;
        int nbCalculated = 0;
        int nbSkipped = 0;
        int nbInQueue = 0;
        String openPayrollDetailID = "";

        // List of items in P_CALCREPORT
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetPayrollOverviewAndStatus, listToSend);

        // What did we get back?
        // The result is a JSON Array listing all the payrolls and their current calc status, nbEmployees, etc
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the cost centre
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();

            // Step through each record, and construct an EMPLOYEE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("payrollID")) {
                    payrollID = nvp.getValue();
                }
                if (nvp.getName().equalsIgnoreCase("nbActiveEmployees")) {
                    nbActiveEmployees = Integer.parseInt(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("nbInactiveEmployees")) {
                    nbInactiveEmployees = Integer.parseInt(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("nbPreviousActiveEmployees")) {
                    nbPreviousActiveEmployees = Integer.parseInt(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("nbEngaged")) {
                    nbEngaged = Integer.parseInt(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("nbDischarged")) {
                    nbDischarged = Integer.parseInt(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("nbCalculated")) {
                    nbCalculated = Integer.parseInt(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("nbSkipped")) {
                    nbSkipped = Integer.parseInt(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("nbInQueue")) {
                    nbInQueue = Integer.parseInt(nvp.getValue());
                }

                if (nvp.getName().equalsIgnoreCase("openPayrollDetailID")) {
                    openPayrollDetailID = (nvp.getValue());
                }

            }
            // Which payroll?
            Payroll payroll = getPayrollWithID(payrollID);
            payroll.setNbActiveEmployees(nbActiveEmployees);
            payroll.setNbPreviousActiveEmployees(nbPreviousActiveEmployees);
            payroll.setNbDischargedInThisPeriod(nbDischarged);
            payroll.setNbEngagedInThisPeriod(nbEngaged);
            payroll.setNbCalculatedInThisPeriod(nbCalculated);
            payroll.setNbSkippedInThisPeriod(nbSkipped);
            payroll.setNbInCalcQueueInThisPeriod(nbInQueue);
            payroll.setOpenPeriodID(openPayrollDetailID);
            payroll.setNbInactiveEmployees(nbInactiveEmployees);
        }
    }

    /**
     * Builds flat structure for all payrolls
     *
     * @param parentCostCentre
     * @param parentCostCentreNode
     */
    private void populateBaseWithPayrolls(TreeNode parentNode) {
        for (Payroll payroll : payrolls) {
            if (!payroll.isDeleted()) {
                TreeNode node = new DefaultTreeNode(payroll.getPayrollName(), parentNode);
                node.setExpanded(false);
                populatePayrollWithPeriods(payroll, node);
            }
        }
    }

    /**
     * Builds flat structure for all payroll periods
     *
     * @param parentCostCentre
     * @param parentCostCentreNode
     */
    private void populatePayrollWithPeriods(Payroll parentPayroll, TreeNode parentNode) {
        if (parentPayroll.getPeriods() != null) {
            for (PayrollPeriod payrollPeriod : parentPayroll.getPeriods()) {
                // Get the open period
                String sOpenPeriod = parentPayroll.getOpenPeriodID();
                TreeNode node;
                String periodDisplay = DateUtils.formatDate(payrollPeriod.getStartDate()) + " -> " + DateUtils.formatDate(payrollPeriod.getEndDate());
                // Closed Periods
                if (Integer.valueOf(payrollPeriod.getPayrollDetailID()) < Integer.valueOf(sOpenPeriod)) {
                    node = new DefaultTreeNode("closedPeriod", periodDisplay, parentNode);
                }
                // Open Period
                if (payrollPeriod.isOpenPeriod()) {
                    node = new DefaultTreeNode("openPeriod", periodDisplay, parentNode);
                }
                // Closed Periods
                if (Integer.valueOf(payrollPeriod.getPayrollDetailID()) > Integer.valueOf(sOpenPeriod)) {
                    node = new DefaultTreeNode("unusedPeriod", periodDisplay, parentNode);
                }
            }
        }
    }

    public Payroll getSelectedPayroll() {
        return selectedPayroll;
    }

    public void setSelectedPayroll(Payroll selectedPayroll) {
        this.selectedPayroll = selectedPayroll;
    }

    public TreeNode getPayrollRootNode() {
        return payrollRootNode;
    }

    public void setPayrollRootNode(TreeNode payrollRootNode) {
        this.payrollRootNode = payrollRootNode;
    }

    public TreeNode getSelectedPayrollNode() {
        return selectedPayrollNode;
    }

    public void setSelectedPayrollNode(TreeNode selectedPayrollNode) {
        this.selectedPayrollNode = selectedPayrollNode;
    }

    public Payroll getNewPayroll() {
        return newPayroll;
    }

    public void setNewPayroll(Payroll newPayroll) {
        this.newPayroll = newPayroll;
    }

    public SessionUser getSessionUser() {
        return sessionUser;
    }

    public ArrayList<Payroll> getPayrolls() {
        return payrolls;
    }

    public void setPayrolls(ArrayList<Payroll> payrolls) {
        this.payrolls = payrolls;
    }

    /**
     * Returns payroll with given payroll_id
     *
     * @param payrollID
     * @return
     */
    public Payroll getPayrollWithID(String payrollID) {
        if (payrollID != null) {
            for (Payroll payroll : payrolls) {
                if (payroll.getPayrollID().equalsIgnoreCase(payrollID)) {
                    return payroll;
                }
            }
        }
        return null;
    }

    /**
     * Returns payroll with given name
     *
     * @param payrollName
     * @return
     */
    public Payroll getPayrollWithName(String payrollName) {
        if (payrollName != null) {
            for (Payroll payroll : payrolls) {
                if (payroll.getPayrollName().equalsIgnoreCase(payrollName)) {
                    return payroll;
                }
            }
        }
        return null;
    }

    /**
     * Runs a web service call to update all the information for all the
     * payrolls
     *
     * @return
     */
    public boolean savePayrolls() {
        boolean rslt = false;
        for (Payroll payroll : payrolls) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
            listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, payroll.getPayrollID()));
            listToSend.add(new BasicNameValuePair("payrollName", payroll.getPayrollName()));
            // The payroll type needs to be translated to English before we save it.
            listToSend.add(new BasicNameValuePair("payrollType", PageManager.translateFromLocaleToEnglish(payroll.getPayrollType())));
            listToSend.add(new BasicNameValuePair("taxYearStart", DateUtils.formatDate(payroll.getTaxYearStart())));
            listToSend.add(new BasicNameValuePair("taxYearEnd", DateUtils.formatDate(payroll.getTaxYearEnd())));
            listToSend.add(new BasicNameValuePair("openPeriod", payroll.getRequestedOpenPeriod()));
            listToSend.add(new BasicNameValuePair("taxCountry", payroll.getTaxCountry()));
            if (payroll.isDeleted()) {
                listToSend.add(new BasicNameValuePair("doDelete", "Y"));
            } else {
                listToSend.add(new BasicNameValuePair("doDelete", "N"));
            }
            listToSend.add(new BasicNameValuePair("taxNumber", payroll.getTaxNumber()));

            if (payroll.getTradeClassification() != null) {
                if (payroll.getTradeClassification().equalsIgnoreCase("-- None --") || payroll.getTradeClassification().equalsIgnoreCase("")) {
                    listToSend.add(new BasicNameValuePair("tradeClassificationCode", ""));
                } else {
                    listToSend.add(new BasicNameValuePair("tradeClassificationCode", payroll.getTradeClassification()));
                }
            }

            if (payroll.getTradeSubClass() != null) {
                if (payroll.getTradeSubClass().equalsIgnoreCase("-- None --") || payroll.getTradeSubClass().equalsIgnoreCase("")) {
                    listToSend.add(new BasicNameValuePair("tradeClassificationSubCode", ""));
                } else {
                    listToSend.add(new BasicNameValuePair("tradeClassificationSubCode",
                            payroll.getTradeSubClass().subSequence(0, payroll.getTradeSubClass().indexOf("-") - 1).toString()));
                }
            }

            if (payroll.getSicCode() != null) {
                if (payroll.getSicCode().equalsIgnoreCase("-- None --") || payroll.getSicCode().equalsIgnoreCase("")) {
                    listToSend.add(new BasicNameValuePair("sicCode", ""));
                } else {
                    listToSend.add(new BasicNameValuePair("sicCode",
                            payroll.getSicCode().subSequence(0, payroll.getSicCode().indexOf("-") - 1).toString()));
                }
            }

            // Call the GetValue service
            ArrayList<NameValuePair> wsResult;
            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetPayrollData, listToSend);

            // What did we get back?
            for (NameValuePair nvp : wsResult) {
                if (nvp.getName().equalsIgnoreCase("RESULT")) {
                    rslt = (nvp.getValue().equalsIgnoreCase("OK"));
                }
            }
        }

        // After we've saved all the cost centres, reload it so that the newly created ones can have proper cost-ids
        loadPayrolls();
        populatePayrollHierarchyView();
        selectedPayroll = null;

        return rslt;
    }

    /**
     * Runs a web service call to retrieve all the company cost centres
     *
     */
    public final void loadPayrolls() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));

        payrolls = new ArrayList<>();
        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetPayrollList, listToSend);

        // What did we get back?
        // The result is a JSON Array with records per payroll
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the employees
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            Payroll payroll = new Payroll(sessionUser);
            // Step through each record, and construct a COST CENTRE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("payrollID")) {
                    payroll.setPayrollID(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("payrollName")) {
                    payroll.setPayrollName(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("payrollType")) {
                    // This payroll type is presently in English
                    payroll.setPayrollType(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("taxYearStart")) {
                    Date date = DateUtils.formatDate(nvp.getValue());
                    payroll.setTaxYearStart(date);
                }
                if (nvp.getName().equalsIgnoreCase("taxYearEnd")) {
                    Date date = DateUtils.formatDate(nvp.getValue());
                    payroll.setTaxYearEnd(date);
                }
                if (nvp.getName().equalsIgnoreCase("taxCountry")) {
                    payroll.setTaxCountry(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("canDelete")) {
                    payroll.setCanDelete(nvp.getValue().trim().equals("1"));
                }
                if (nvp.getName().equalsIgnoreCase("currency")) {
                    payroll.setCurrencyString(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("taxNumber")) {
                    payroll.setTaxNumber(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("tradeClassificationCode")) {
                    payroll.setTradeClassification(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("tradeClassificationSubCode")) {
                    payroll.setTradeSubClass(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("sicCode")) {
                    payroll.setSicCode(nvp.getValue());
                }
            }
            payrolls.add(payroll);
        }

        // Now that we've loaded all the payrolls, let's fetch the periods
        for (Payroll payroll : payrolls) {
            payroll.setPeriods(loadPayrollPeriods(payroll.getPayrollID()));
        }
        populatePayrollHierarchyView();

    }

    /**
     * For the given country, returns the default currency
     *
     * @param countryName
     * @return
     */
    public String getUnitForCountry(String countryName) {
        for (Payroll payroll : payrolls) {
            if (payroll.getTaxCountry().equalsIgnoreCase(countryName)) {
                return payroll.getCurrencyString();
            }
        }
        return null;
    }

    /**
     * Runs a web service call to retrieve all the payroll periods
     *
     */
    private ArrayList<PayrollPeriod> loadPayrollPeriods(String payrollID) {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, payrollID));

        ArrayList<PayrollPeriod> periods = new ArrayList<>();
        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetPayrollPeriodList, listToSend);

        // What did we get back?
        // The result is a JSON Array with records per payroll
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the employees
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            PayrollPeriod payrollPeriod = new PayrollPeriod(payrollID);
            // Step through each record, and construct a COST CENTRE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("payrollDetailID")) {
                    payrollPeriod.setPayrollDetailID(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("startDate")) {
                    Date date = DateUtils.formatDate(nvp.getValue());
                    payrollPeriod.setStartDate(date);
                }
                if (nvp.getName().equalsIgnoreCase("endDate")) {
                    Date date = DateUtils.formatDate(nvp.getValue());
                    payrollPeriod.setEndDate(date);
                }
                if (nvp.getName().equalsIgnoreCase("monthEnd")) {
                    Date date = DateUtils.formatDate(nvp.getValue());
                    payrollPeriod.setMonthEnd(date);
                }
                if (nvp.getName().equalsIgnoreCase("openPeriod")) {
                    payrollPeriod.setOpenPeriod(nvp.getValue().equalsIgnoreCase("Y"));
                }

            }
            periods.add(payrollPeriod);
        }
        return periods;

    }

    /**
     * All the attributes for this payroll already exists in 'newPayroll'
     *
     * @param actionEvent
     */
    public void createNewPayroll(ActionEvent actionEvent) {
        // Does this payroll already exist?
        boolean canCreate = true;
        for (Payroll payroll : payrolls) {
            if (payroll.getPayrollName().trim().equalsIgnoreCase(newPayroll.getPayrollName().trim())) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, newPayroll.getPayrollName() + " " + PageManager.translate("IsNotUnique"), PageManager.translate("PleaseChooseAnotherName"));
                canCreate = false;
            }
        }

        if (canCreate) {
            payrolls.add(newPayroll);
            selectedPayroll = newPayroll;
            populatePayrollHierarchyView();

            // Now select this entity in the treeview
            TreeNode selectedNode = getNodeFor(selectedPayroll, payrollRootNode);
            if (selectedNode != null) {
                selectedNode.setSelected(true);
            }
            sessionUser.getPageManager().reloadOuterPanel();

            // Create a new payroll for future use
            newPayroll = new Payroll(sessionUser);
        }

    }

    /**
     * User changed the taxNumber/uifNumber
     */
    public void taxOrUifChanged() {
        sessionUser.getPageManager().setChangesOnPage(true);
    }

    /**
     * User changed the entity name
     */
    public void nameChanged() {
        //Is the name unique?
        if (!isNameUnique(selectedPayroll)) {
            sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, selectedPayroll.getPayrollName() + " " + PageManager.translate("IsNotUnique"), PageManager.translate("PleaseChooseAnotherName"));
            sessionUser.getPageManager().setChangesOnPage(false);
        }
    }

    private boolean isNameUnique(Payroll currentPayroll) {
        int nbInstances = 0;
        for (Payroll payroll : payrolls) {
            if (payroll.getPayrollName().trim().equalsIgnoreCase(currentPayroll.getPayrollName())) {
                nbInstances++;
            }
        }
        return (nbInstances < 2);
    }

    // Returns the TreeNode representing this payroll
    private TreeNode getNodeFor(Payroll payroll, TreeNode parentNode) {

        for (TreeNode treeNode : parentNode.getChildren()) {
            if (treeNode.getChildCount() == 0) {
                if (((String) treeNode.getData()).equalsIgnoreCase(payroll.getPayrollName())) {
                    return treeNode;
                }
            } else {
                return getNodeFor(payroll, treeNode);
            }
        }
        return null;
    }

    public void deleteSelectedPayroll(ActionEvent actionEvent) {
        // Unlink employees assigned to this cost centre
        for (Employee employee : sessionUser.getPageManager().getCompany().getEmployeeManager().getEmployees()) {
            if (employee.getPayrollName() != null) {
                if (employee.getPayrollName().equalsIgnoreCase(selectedPayroll.getPayrollName())) {
                    employee.setPayrollName(null);
                    // save changes for employee
                    employee.saveEmployee();
                }
            }
        }

        selectedPayroll.setDeleted(true);
        selectedPayroll = null;
        populatePayrollHierarchyView();

        sessionUser.getPageManager().reloadFullPage();

    }

    public void onCellEdit(CellEditEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        Payroll editedPR = context.getApplication().evaluateExpressionGet(context, "#{payroll}", Payroll.class);
        // Already assigned the new Payroll.  We need to change that!
        String oldStringValue = (String) event.getOldValue();
        String newStringValue = (String) event.getNewValue();

        // Which column did we work on?
        String column_name;
        column_name = event.getColumn().getHeaderText();
        //
        // Payroll Name changed
        //
        if (column_name.equals("Name")) {
            //1. Is this name unique?
            if (!isPayrollNameUnique(editedPR, newStringValue)) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, newPayroll.getPayrollName() + " " + PageManager.translate("IsNotUnique"), PageManager.translate("PleaseChooseAnotherName"));
                editedPR.setPayrollName(oldStringValue);
            } else {
                editedPR.setPayrollName(newStringValue);

                //2. Change the payroll of those employees who were linked to this CC
                for (Employee employee : sessionUser.getPageManager().getCompany().getEmployeeManager().getEmployees()) {
                    if (employee.getPayrollName() != null) {
                        if (employee.getPayrollName().equalsIgnoreCase(oldStringValue)) {
                            employee.setPayrollName(newStringValue);
                        }
                    }
                }
            }
        }
    }

    private boolean isPayrollNameUnique(Payroll currentPayroll, String newName) {
        for (Payroll payroll : payrolls) {
            if (!payroll.getPayrollID().equals(currentPayroll.getPayrollID())) {
                if (payroll.getPayrollName().trim().equalsIgnoreCase(newName)) {
                    return false;
                }
            }
        }
        return true;
    }

    public ArrayList<Payroll> getGlobalPayrolls() {
        return this.payrolls;
    }

    public void onNodeSelect(NodeSelectEvent event) {
        // Type:
        String type = (String) event.getTreeNode().getType();
        if (type.equalsIgnoreCase("default")) {
            Payroll payroll = getPayrollWithName((String) event.getTreeNode().getData());
            if (payroll != null) {
                selectedPayroll = payroll;
                sessionUser.getPageManager().reloadOuterPanel();
            }
        }

    }
}
