/*
 * This object represents the Year-To-Date totals per variabe/loan/saving per person, per period
 */
package za.co.accsys.web.model;

import za.co.accsys.web.beans.EnumVarPayEntityType;
import za.co.accsys.web.beans.PageManager;

/**
 *
 * @author liam
 */
public class YTDPayEntityInstance {

    private PayrollPeriod payrollPeriod;
    private PayEntity payEntity;
    private String ytdValue;
    private String ytdCount;
    private final Employee owner;

    public YTDPayEntityInstance(Employee owner) {
        this.owner = owner;
    }

    
    public PayrollPeriod getPayrollPeriod() {
        return payrollPeriod;
    }

    public void setPayrollPeriod(PayrollPeriod payrollPeriod) {
        this.payrollPeriod = payrollPeriod;
    }

    public PayEntity getPayEntity() {
        return payEntity;
    }

    public void setPayEntity(PayEntity payEntity) {
        this.payEntity = payEntity;
    }

    /**
     * Constructs a display of the value + unit or currency + value
     *
     * @return
     */
    public String getCurrencyAndYTDValue() {
        if (getYtdValue() == null || getYtdValue().trim().length() == 0) {
            ytdValue = "0";
        }
        String sDecimalValue = PageManager.formatStringAsCurrency(getYtdValue(), owner.getSessionUser().getUserLocale());
        if (this.payEntity != null) {
            if (this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Earning
                    || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Deduction
                    || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Loan
                    || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Saving
                    || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_Tax
                    || this.payEntity.getEntityType() == EnumVarPayEntityType.entityType_PackageComponent) {
                return owner.getSessionUser().getPageManager().getCompany().getPayrollManager().getPayrollWithID(this.payrollPeriod.getPayrollID()).getCurrencyString() + " " + sDecimalValue;
            } else {
                return getYtdValue() + " (" + payEntity.getUnit() + ")";
            }
        }
        return "";  
    }

    public String getYtdValue() {
        return ytdValue;
    }

    public void setYtdValue(String ytdValue) {
        this.ytdValue = ytdValue;
    }

    public String getYtdCount() {
        return ytdCount;
    }

    public void setYtdCount(String ytdCount) {
        this.ytdCount = ytdCount;
    }

}
