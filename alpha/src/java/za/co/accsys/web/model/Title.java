/*
 * Represents a Title
 */
package za.co.accsys.web.model;

import javax.faces.application.FacesMessage;
import za.co.accsys.web.beans.PageManager;

/**
 *
 * @author liam
 */
public class Title {

    private String title;
    private String titleID;
    private boolean deleted;
    private final SessionUser sessionUser;

    public Title(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        deleted = false;
        titleID = "-1";
        title = "new:" + (int) Math.ceil(Math.random() * 100);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if ((this.title == null) || (!this.title.equals(title))) {
            //Is the name unique?
            if (sessionUser.getPageManager().getCompany().getTitleManager() != null) {
                if (sessionUser.getPageManager().getCompany().getTitleManager().nameExists(title)) {
                    title = title + "_";
                }
            }
            this.title = title;
            if (sessionUser.getPageManager().getCompany().getTitleManager() != null) {
                sessionUser.getPageManager().getCompany().getTitleManager().saveTitle(this);
            }
        }
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void delete() {
        this.deleted = true;
    }

    public String getTitleID() {
        return titleID;
    }

    public void setTitleID(String titleID) {
        this.titleID = titleID;
    }

    public void setDeleted(boolean deleted) {
        if (this.deleted != deleted) {
            this.deleted = deleted;
        }
    }

}
