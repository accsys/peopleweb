/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

//    private String country;
import za.co.accsys.web.beans.PageManager;

//    private String province;
//    private String postalCode;
//    private String cityOrTown;
/**
 *
 * @author Liam
 */
public class PhysicalAddress extends Address {

    private String unitNo;
    private String complex;
    private String streetNo;
    private String streetOrFarmName;
    private String suburb;
    private boolean postalSameAsPhysical;

    public PhysicalAddress(SessionUser sessionUser) {
        super(sessionUser);
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        if ((this.unitNo == null) || (!this.unitNo.equals(unitNo))) {
            this.unitNo = unitNo;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public String getComplex() {
        return complex;
    }

    public void setComplex(String complex) {
        if ((this.complex == null) || (!this.complex.equals(complex))) {
            this.complex = complex;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        if ((this.streetNo == null) || (!this.streetNo.equals(streetNo))) {
            this.streetNo = streetNo;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public String getStreetOrFarmName() {
        return streetOrFarmName;
    }

    public void setStreetOrFarmName(String streetOrFarmName) {
        if ((this.streetOrFarmName == null) || (!this.streetOrFarmName.equals(streetOrFarmName))) {
            this.streetOrFarmName = streetOrFarmName;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        if ((this.suburb == null) || (!this.suburb.equals(suburb))) {
            this.suburb = suburb;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public boolean isPostalSameAsPhysical() {
        return postalSameAsPhysical;
    }

    public void setPostalSameAsPhysical(boolean postalSameAsPhysical) {
        if (this.postalSameAsPhysical != postalSameAsPhysical) {
            this.postalSameAsPhysical = postalSameAsPhysical;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

}
