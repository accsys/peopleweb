/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import java.util.Objects;

/**
 *
 * @author lterblanche
 */
public class CostCentre {

    private String costID;
    private String name;
    private String number;
    private String description;
    private String parentCostID;
    private String parentCostCentreName;
    private boolean deleted;
    private final SessionUser sessionUser;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        if (this.deleted != deleted) {
            this.deleted = deleted;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getParentCostCentreName() {
        return parentCostCentreName;
    }

    public void setParentCostCentreName(String parentCostCentreName) {
        // Can this be right?
        if ((this.parentCostCentreName == null) || (!this.parentCostCentreName.equals(parentCostCentreName))) {
            this.parentCostCentreName = parentCostCentreName;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public CostCentre(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        deleted = false;
        costID = "-1";
        name = "new:" + (int) Math.ceil(Math.random() * 100);
    }

    public String getCostID() {
        return costID;
    }

    public void setCostID(String costID) {
        this.costID = costID;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CostCentre)) {
            return false;
        }

        CostCentre wl = (CostCentre) obj;
        return this.costID.equals(wl.getCostID());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.costID);
        return hash;
    }

    public void setName(String name) {
        if ((this.name == null) || (!this.name.equals(name))) {
            boolean duplicateCC = false;
            // Is this name unique?
            if (sessionUser.getPageManager().getCompany().getCostCentreManager() != null) {
                CostCentre anotherCC = sessionUser.getPageManager().getCompany().getCostCentreManager().getCostCentreWithName(name);
                if (anotherCC != null) {
                    duplicateCC = true;
                }
            }
            if (duplicateCC) {
                this.name = name + (int) Math.ceil(Math.random() * 100);
            } else {
                this.name = name;
            }
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        if ((this.number == null) || (!this.number.equals(number))) {
            this.number = number;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if ((this.description == null) || (!this.description.equals(description))) {
            this.description = description;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getParentCostID() {
        return parentCostID;
    }

    public void setParentCostID(String parentCostID) {
        if ((this.parentCostID == null) || (!this.parentCostID.equals(parentCostID))) {
            this.parentCostID = parentCostID;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

}
