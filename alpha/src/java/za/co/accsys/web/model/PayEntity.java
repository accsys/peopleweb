/*
 * This is the base class for variables, loans, savings, tax, sdl, etc.
 */
package za.co.accsys.web.model;

import java.util.Objects;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.beans.EnumVarPayEntityType;

/**
 *
 * @author Liam
 */
public final class PayEntity {

    private String entityID;
    private EnumVarPayEntityType entityType;
    private String name;
    private String caption;
    private String unit;
    private boolean showYtdOnPayslip;
    private String calculationFormula;
    private String payrollType;
    private String payPerType;
    private String interest;
    private boolean deleted;
    private boolean canDelete;
    private boolean isSystemVar;
    private TaxCode taxCode;
    private String taxCodeAndDescription;
    private String taxCountry;
    private String entityTypeString;
    private String indicatorCodes;
    private boolean beRFI;

    private boolean renderShowYtdOnPayslip;
    private boolean renderShowTaxCode;
    private boolean renderShowInterest;
    private boolean renderShowUnit;
    private boolean renderShowFormulaExplanation;
    private boolean renderShowRule;
    private boolean renderShowBalance;
    private boolean renderShowTaxDiagnostics;
    private boolean renderAllowBalanceChange;
    private final SessionUser sessionUser;

    /**
     * As default, we create a EARNING variable.
     *
     * @param sessionUser
     */
    public PayEntity(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
        setDefaults();
    }
    
    private void setDefaults(){
        entityID = "-1";
        name = "new:" + (int) Math.ceil(Math.random() * 100);
        deleted = false;
        setEntityType(EnumVarPayEntityType.entityType_Earning);
        canDelete = true;
        setTaxCode(new TaxCode());
        payrollType = "All";
        taxCountry = taxCode.getTaxCountry();
        unit = "R";
        indicatorCodes = "";
        calculationFormula="INPUT";
        payPerType = "All";
    }

    public boolean isRenderShowBalance() {
        return (entityType == EnumVarPayEntityType.entityType_Loan) || (entityType == EnumVarPayEntityType.entityType_Saving);
    }

    public boolean isRenderAllowBalanceChange() {
        return (entityType == EnumVarPayEntityType.entityType_Loan);
    }

    public boolean isRenderShowFormulaExplanation() {
        if (calculationFormula != null) {
            return !(calculationFormula.trim().equalsIgnoreCase("INPUT")
                    || calculationFormula.trim().equalsIgnoreCase("NO FORMULA")
                    || entityType == EnumVarPayEntityType.entityType_Tax);
        } else {
            return false;
        }
    }

    public String getIndicatorCodes() {
        return indicatorCodes;
    }

    public void setIndicatorCodes(String indicatorCodes) {
        this.indicatorCodes = indicatorCodes;
    }

    public String getPayPerType() {
        return payPerType;
    }

    public void setPayPerType(String payPerType) {
        this.payPerType = payPerType;
    }

    
    public boolean isRenderShowTaxDiagnostics() {
        return (entityType == EnumVarPayEntityType.entityType_Tax);
    }

    public String getPayrollType() {
        if (payrollType == null) {
            payrollType = "All";
        }
        return payrollType;
    }

    public void setPayrollType(String payrollType) {
        if (!this.payrollType.equalsIgnoreCase(payrollType)) {
            this.payrollType = payrollType;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    /**
     * Returns true if this variable can contribute to RFI
     * @return 
     */
    public boolean isBeRFI() {
        return beRFI;
    }

    
    public void setBeRFI(boolean beRFI) {
        this.beRFI = beRFI;
    }

    /**
     * Does this entity have a formula that should be displayed?
     * @return 
     */
    public boolean isRenderShowRule() {
        return (getCalculationFormula() != null);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.entityID);
        hash = 31 * hash + Objects.hashCode(this.entityType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PayEntity other = (PayEntity) obj;
        if (!Objects.equals(this.entityID, other.entityID)) {
            return false;
        }
        return this.entityType == other.entityType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public String getRowStyle() {
        switch (entityType) {
            case entityType_Deduction:
                return "payEntityDeductionRow";
            case entityType_Earning:
                return "payEntityEarningRow";
            case entityType_Interim:
                return "payEntityInterimRow";
            case entityType_Loan:
                return "payEntityLoanRow";
            case entityType_Saving:
                return "payEntitySavingRow";
            default:
                return "payEntityDefaultRow";
        }
    }

    public boolean isIsSystemVar() {
        return isSystemVar;
    }

    public void setIsSystemVar(boolean isSystemVar) {
        this.isSystemVar = isSystemVar;
    }

    public String getEntityID() {
        return entityID;
    }

    public String getEntityTypeString() {
        return entityTypeString;
    }

    public void setEntityTypeString(String entityTypeString) {

        if (!this.entityTypeString.equalsIgnoreCase(entityTypeString)) {
            this.entityTypeString = entityTypeString;
            if (entityTypeString != null) {
                this.entityType = EnumVarPayEntityType.localeStringToEnum(entityTypeString);
                // Now that we've changed the entity type, we need to select a new tax-code, if necessary
                if (this.entityType == EnumVarPayEntityType.entityType_Earning) {
                    if (sessionUser.getPageManager().getPayEntityManager().getCountryAndTypeSpecificTaxCodesFor(this).length > 0) {
                        this.taxCode = sessionUser.getPageManager().getPayEntityManager().getCountryAndTypeSpecificTaxCodesFor(this)[0];
                    }
                } else {
                    this.taxCode = null;
                }
            }
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public TaxCode getTaxCode() {
        return taxCode;
    }

    public String getTaxCodeAndDescription() {
        if (taxCode != null) {
            return taxCode.getCodeAndDescription();
        } else {
            return "";
        }
    }

    public void setTaxCodeAndDescription(String taxCodeAndDescription) {
        if ((this.taxCodeAndDescription == null) || (!this.taxCodeAndDescription.equals(taxCodeAndDescription))) {
            this.taxCodeAndDescription = taxCodeAndDescription;
            if (taxCodeAndDescription != null) {
                // Now that we have set the CodeAndDescription ( CODE - DESCRIPTION ), let's separate the two
                if (taxCodeAndDescription.contains("-")) {
                    int hiphen = taxCodeAndDescription.indexOf(" - ");
                    String aCode = taxCodeAndDescription.substring(0, hiphen);
                    this.taxCode = sessionUser.getPageManager().getPayEntityManager().getTaxCodeWith(aCode);
                }
            }
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    /**
     * Returns the tax country associated with this payEntity
     *
     * @return
     */
    public String getTaxCountry() {
        return taxCountry;
    }

    public void setTaxCountry(String taxCountry) {
        if (!this.taxCountry.equalsIgnoreCase(taxCountry)) {
            this.taxCountry = taxCountry;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public void setTaxCode(TaxCode taxCode) {
        if (taxCode != null) {
            this.taxCode = taxCode;
            sessionUser.getPageManager().setChangesOnPage(true);
            this.taxCountry = taxCode.getTaxCountry();
        } else {
            this.taxCode = null;
        }
    }

    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    public void setDeleted(boolean deleted) {
        if (this.deleted != deleted) {
            this.deleted = deleted;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public boolean isCanDelete() {
        return canDelete && !isSystemVar;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        if ((this.interest == null) || (!this.interest.equals(interest))) {
            this.interest = interest;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getCalculationFormula() {
        if (entityType == EnumVarPayEntityType.entityType_Tax) {
            calculationFormula = PageManager.translate("hint_CountrySpecificTaxRules");
        }

        return calculationFormula;
    }

    public void setCalculationFormula(String calculationFormula) {
        if ((this.calculationFormula == null) || (!this.calculationFormula.equals(calculationFormula))) {
            this.calculationFormula = calculationFormula;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public EnumVarPayEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EnumVarPayEntityType entityType) {
        if ((this.entityType == null) || (!this.entityType.equals(entityType))) {
            this.entityType = entityType;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
        this.entityTypeString = PageManager.translate(entityType.toString());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if ((this.name == null) || (!this.name.equals(name))) {
            this.name = name;
            // Update caption?
            if (caption == null || caption.isEmpty()) {
                this.caption = name;
            }
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        if ((this.caption == null) || (!this.caption.equals(caption))) {
            this.caption = caption;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getUnit() {
        if (unit == null || unit.trim().length() == 0) {
            unit = sessionUser.getPageManager().getCompany().getPayrollManager().getUnitForCountry(taxCountry);
        }
        if (unit == null || unit.trim().length() == 0) {
            unit = "_";
        }
        return unit;
    }

    public void setUnit(String unit) {
        if ((this.unit == null) || (!this.unit.equals(unit))) {
            this.unit = unit;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public boolean isShowYtdOnPayslip() {
        return showYtdOnPayslip;
    }

    public void setShowYtdOnPayslip(boolean showYtdOnPayslip) {
        if (this.showYtdOnPayslip != showYtdOnPayslip) {
            this.showYtdOnPayslip = showYtdOnPayslip;
            sessionUser.getPageManager().setChangesOnPage(true);
        }

    }

    /**
     * Don't show this option for loans and savings
     *
     * @return
     */
    public boolean isRenderShowYtdOnPayslip() {
        renderShowYtdOnPayslip = (entityType == EnumVarPayEntityType.entityType_Deduction
                || entityType == EnumVarPayEntityType.entityType_Earning
                || entityType == EnumVarPayEntityType.entityType_Interim
                || entityType == EnumVarPayEntityType.entityType_PackageComponent);
        return renderShowYtdOnPayslip;
    }

    /**
     * Don't show this option for loans and savings and interim variables
     *
     * @return
     */
    public boolean isRenderShowTaxCode() {
        renderShowTaxCode = (entityType == EnumVarPayEntityType.entityType_Deduction
                || entityType == EnumVarPayEntityType.entityType_Earning
                || entityType == EnumVarPayEntityType.entityType_PackageComponent);
        return renderShowTaxCode;
    }

    /**
     * Only show this option for loans and savings
     *
     * @return
     */
    public boolean isRenderShowInterest() {
        renderShowInterest = (entityType == EnumVarPayEntityType.entityType_Loan
                || entityType == EnumVarPayEntityType.entityType_Saving);
        return renderShowInterest;
    }

    /**
     * Units are being used in Variables only
     *
     * @return
     */
    public boolean isRenderShowUnit() {
        renderShowUnit = (entityType == EnumVarPayEntityType.entityType_Deduction
                || entityType == EnumVarPayEntityType.entityType_Earning
                || entityType == EnumVarPayEntityType.entityType_Interim
                || entityType == EnumVarPayEntityType.entityType_PackageComponent);
        return renderShowUnit;
    }

}
