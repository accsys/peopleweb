/*
 * Collection class for handling payrolls
 */
package za.co.accsys.web.model;

import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.helperclasses.JSONRecord;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.helperclasses.ServerPrefs;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.beans.EnumVarPayEntityType;

/**
 *
 * @author lterblanche
 */
public class PayEntityManager {

    private PayEntity selectedPayEntity;
    private PayEntity newPayEntity;
    private final SessionUser sessionUser;
    private ArrayList<PayEntity> payEntities;
    private ArrayList<PayEntity> filteredPayEntities;
    private ArrayList<TaxCode> taxCodes;
    private String[] countryAndTypeSpecificTaxCodes;
    private final int maxRowsToDisplay = 8; // Maximum number of rows we want to display
    private String maxRowsStyleScript;
    private ArrayList<PayEntity> payEntities_Earning;

    /**
     * Default constructor
     *
     * @param sessionUser
     */
    public PayEntityManager(SessionUser sessionUser) {
        this.sessionUser = sessionUser;

        loadTaxCodes();
        loadPayEntities();

        newPayEntity = new PayEntity(sessionUser);
    }

    public ArrayList<PayEntity> getFilteredPayEntities() {
        return filteredPayEntities;
    }

    public void setFilteredPayEntities(ArrayList<PayEntity> filteredPayEntities) {
        this.filteredPayEntities = filteredPayEntities;
    }

    /**
     * Returns an ArrayList of tax codes
     *
     * @return
     */
    public ArrayList<TaxCode> getTaxCodes() {
        return taxCodes;
    }

    public void setTaxCodes(ArrayList<TaxCode> taxCodes) {
        this.taxCodes = taxCodes;
    }

    public String getMaxRowsStyleScript() {
        maxRowsStyleScript = "height:" + Math.min(payEntities.size(), maxRowsToDisplay) * 47 + "px";
        return maxRowsStyleScript;
    }

    public ArrayList<PayEntity> getPayEntities_Earning() {
        payEntities_Earning = new ArrayList<>();
        for (PayEntity payEntity : payEntities) {
            if (payEntity.getEntityType() == EnumVarPayEntityType.entityType_Earning) {
                payEntities_Earning.add(payEntity);
            }
        }
        return this.payEntities_Earning;
    }

    public void setPayEntities_Earning(ArrayList<PayEntity> payEntities_Earning) {
        this.payEntities_Earning = payEntities_Earning;
    }

    /**
     * Returns the TaxCode object with the given code
     *
     * @param aCode
     * @return
     */
    public TaxCode getTaxCodeWith(String aCode) {
        for (TaxCode taxCode : taxCodes) {
            if (taxCode.getCode().equalsIgnoreCase(aCode)) {
                return (taxCode);
            }
        }
        return null;
    }

    public void setMaxRowsStyleScript(String maxRowsStyleScript) {
        this.maxRowsStyleScript = maxRowsStyleScript;
    }

    /**
     * Creates a String[] with all the tax codes associated with the TaxCountry
     * of the selectedPayEntity
     *
     * @return
     */
    public String[] getCountryAndTypeSpecificTaxCodes() {
        ArrayList<String> list = new ArrayList<>();
        for (TaxCode taxCode : taxCodes) {
            if (taxCode.getTaxCountry().equalsIgnoreCase(selectedPayEntity.getTaxCountry())) {
                if (taxCode.getPayEntityType().toString().equalsIgnoreCase(selectedPayEntity.getEntityType().toString())) {
                    list.add(taxCode.getCodeAndDescription());
                }
            }
        }
        if (selectedPayEntity.getEntityType() == EnumVarPayEntityType.entityType_Deduction) {
            list.add(0, "");
        }
        String[] rslt = list.toArray(new String[list.size()]);

        return rslt;
    }

    /**
     * Creates a String[] with all the tax codes associated with the TaxCountry
     * of the selected PayEntity
     *
     * @param payEntity
     * @return
     */
    public TaxCode[] getCountryAndTypeSpecificTaxCodesFor(PayEntity payEntity) {
        List<TaxCode> codes = new ArrayList<>();
        for (TaxCode taxCode : taxCodes) {
            if (taxCode.getTaxCountry().equalsIgnoreCase(payEntity.getTaxCountry())) {
                if (taxCode.getPayEntityType().toString().equalsIgnoreCase(payEntity.getEntityType().toString())) {
                    codes.add(taxCode);
                }
            }
        }
        TaxCode[] rslt = codes.toArray(new TaxCode[codes.size()]);
        return rslt;
    }

    public void setCountryAndTypeSpecificTaxCodes(String[] countryAndTypeSpecificTaxCodes) {
        this.countryAndTypeSpecificTaxCodes = countryAndTypeSpecificTaxCodes;
    }

    public PayEntity getSelectedPayEntity() {
        return selectedPayEntity;
    }

    public void setSelectedPayEntity(PayEntity selectedPayEntity) {
        if (selectedPayEntity != null) {
            this.selectedPayEntity = selectedPayEntity;
        }
    }

    public void resetSelectedPayEntity() {
        this.selectedPayEntity = null;
    }

    public PayEntity getNewPayEntity() {
        return newPayEntity;
    }

    public void setNewPayEntity(PayEntity newPayEntity) {
        this.newPayEntity = newPayEntity;
    }

    public ArrayList<PayEntity> getPayEntities() {
        return payEntities;
    }

    public void setPayEntities(ArrayList<PayEntity> payEntities) {
        this.payEntities = payEntities;
    }

    public boolean saveSelectedPayEntity() {
        return savePayEntity(selectedPayEntity);
    }

    private boolean savePayEntity(PayEntity payEntity) {
        boolean rslt = false;
        if (payEntity != null) {
            // List of attributes to pass to the web server
            ArrayList<NameValuePair> listToSend = new ArrayList<>();
            listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
            listToSend.add(new BasicNameValuePair(Constants.sendParam_EntityId, payEntity.getEntityID()));
            listToSend.add(new BasicNameValuePair("entityType", payEntity.getEntityType().toString()));
            listToSend.add(new BasicNameValuePair("name", payEntity.getName()));
            listToSend.add(new BasicNameValuePair("caption", payEntity.getCaption()));
            listToSend.add(new BasicNameValuePair("valueUnit", payEntity.getUnit()));
            listToSend.add(new BasicNameValuePair("payrollType", PageManager.translateFromLocaleToEnglish(payEntity.getPayrollType())));

            if (payEntity.isShowYtdOnPayslip()) {
                listToSend.add(new BasicNameValuePair("showYtdOnPayslip", "Y"));
            } else {
                listToSend.add(new BasicNameValuePair("showYtdOnPayslip", "N"));
            }
            listToSend.add(new BasicNameValuePair("interest", payEntity.getInterest()));
            if (payEntity.getTaxCode() != null) {
                listToSend.add(new BasicNameValuePair("taxCode", payEntity.getTaxCode().getCode()));
            }
            if (payEntity.isDeleted()) {
                listToSend.add(new BasicNameValuePair("doDelete", "Y"));
            } else {
                listToSend.add(new BasicNameValuePair("doDelete", "N"));
            }

            // Call the SetValue service
            ArrayList<NameValuePair> wsResult;
            String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
            wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetPayEntityData, listToSend);

            // What did we get back?
            for (NameValuePair nvp : wsResult) {
                if (nvp.getName().equalsIgnoreCase("entityID")) {
                    rslt = true;
                    payEntity.setEntityID(nvp.getValue());
                }
            }
        }

        // Refresh list
        sessionUser.getPageManager().getPayEntityManager().loadPayEntities();

        return rslt;
    }

    /**
     * Runs a web service call to update all the information for all the
     * loans/savings/earnings/deductions/etc
     *
     * @return
     */
    public boolean savePayEntities() {

        boolean rslt = false;
        for (PayEntity payEntity : payEntities) {
            rslt = savePayEntity(payEntity);
            if (!rslt) {
                System.out.println("\nProblem in saving PayEntity:" + payEntity.toString());
            }
        }

        // After we've saved all the payEntities, reload it all
        loadPayEntities();

        return rslt;
    }

    /**
     * Returns a list of all variables with the given Indicator Code
     *
     * @param indCode
     * @return
     */
    public List<PayEntity> getGlobalPayEntitiesWithIndCode(String indCode) {
        ArrayList<PayEntity> rslt = new ArrayList<>();
        for (PayEntity entity : payEntities) {
            if (entity.getIndicatorCodes().contains(indCode)) {
                rslt.add(entity);
            }
        }
        return rslt;
    }

    /**
     * Returns the TaxCode instance with the given code as a String
     *
     * @param aCode
     * @return
     */
    private TaxCode getTaxCode(String aCode) {
        for (TaxCode taxCode : taxCodes) {
            if (taxCode.getCode().equalsIgnoreCase(aCode)) {
                return taxCode;
            }
        }
        return null;
    }

    /**
     * Runs a web service call to retrieve all the pay entities (loans, savings,
     * and variables
     *
     */
    public final void loadPayEntities() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

        payEntities = new ArrayList<>();
        filteredPayEntities = new ArrayList<>();

        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetPayEntityList, listToSend);

        // What did we get back?
        // The result is a JSON Array with records per earning/deduction/loan/saving
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the employees
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            PayEntity payEntity = new PayEntity(sessionUser);
            // Step through each record, and construct a COST CENTRE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("entityID")) {
                    payEntity.setEntityID(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("entityType")) {
                    payEntity.setEntityType(EnumVarPayEntityType.stringToEnum(nvp.getValue()));
                }
                if (nvp.getName().equalsIgnoreCase("name")) {
                    payEntity.setName(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("caption")) {
                    payEntity.setCaption(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("unit")) {
                    payEntity.setUnit(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("showYtdOnPayslip")) {
                    payEntity.setShowYtdOnPayslip(nvp.getValue().equalsIgnoreCase("Y"));
                }
                if (nvp.getName().equalsIgnoreCase("calculationFormula")) {
                    payEntity.setCalculationFormula(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("interest")) {
                    payEntity.setInterest(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("taxCode")) {
                    payEntity.setTaxCode(this.getTaxCode(nvp.getValue()));
                }
                if (nvp.getName().equalsIgnoreCase("canDelete")) {
                    payEntity.setCanDelete(nvp.getValue().trim().equals("1"));
                }
                if (nvp.getName().equalsIgnoreCase("sysOption")) {
                    payEntity.setIsSystemVar(nvp.getValue().trim().equals("1"));
                }
                if (nvp.getName().equalsIgnoreCase("canBeRFI")) {
                    payEntity.setBeRFI(nvp.getValue().trim().isEmpty());
                }
                if (nvp.getName().equalsIgnoreCase("theFormula")) {
                    payEntity.setCalculationFormula(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("indicatorCodes")) {
                    payEntity.setIndicatorCodes(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("payrollType")) {
                    payEntity.setPayrollType(PageManager.translateFromEnglishToLocale(nvp.getValue()));
                }
                if (nvp.getName().equalsIgnoreCase("payPerType")) {
                    payEntity.setPayPerType(PageManager.translateFromEnglishToLocale(nvp.getValue()));
                }
            }
            // Fix missing Caption
            if (payEntity.getCaption() == null || payEntity.getCaption().trim().length() == 0) {
                payEntity.setCaption(payEntity.getName());
            }
            payEntities.add(payEntity);
            filteredPayEntities.add(payEntity);
        }
    }

    /**
     * Runs a web service call to retrieve all the tax codes
     *
     */
    public final void loadTaxCodes() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

        taxCodes = new ArrayList<>();
        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetTaxCodeAndDescriptionList, listToSend);

        // What did we get back?
        // The result is a JSON Array with records per tax country, per earning/deduction
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the tax codes
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            TaxCode taxCode = new TaxCode();
            // Step through each record, and construct a TaxCode instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("taxCountry")) {
                    taxCode.setTaxCountry(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("taxCode")) {
                    taxCode.setCode(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("description")) {
                    taxCode.setDescription(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("entityTypeString")) {
                    taxCode.setPayEntityType(EnumVarPayEntityType.stringToEnum(nvp.getValue()));
                }

            }
            taxCodes.add(taxCode);
        }
    }

    /**
     * All the attributes for this payEntity already exists in 'newPayEntity'
     *
     * @param actionEvent
     */
    public void createNewPayEntity(ActionEvent actionEvent) {
        // Does this cost centre exist?
        boolean canCreate = true;
        for (PayEntity payEntity : payEntities) {
            if (payEntity.getName().trim().equalsIgnoreCase(newPayEntity.getName().trim())) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, newPayEntity.getName() + " " + PageManager.translate("IsNotUnique"), PageManager.translate("PleaseChooseAnotherName"));
                canCreate = false;
            }
        }

        if (canCreate) {
            // Add current 'newEntity' into the ArrayList
            payEntities.add(newPayEntity);
            // Mark it as the current selected entity
            selectedPayEntity = newPayEntity;
            sessionUser.getPageManager().reloadOuterPanel();

            // Create a new entity for future use
            newPayEntity = new PayEntity(sessionUser);
        }

    }

    public void deleteSelectedPayEntity(ActionEvent actionEvent) {
        if (selectedPayEntity != null) {
            selectedPayEntity.setDeleted(true);
            savePayEntity(selectedPayEntity);

            filteredPayEntities.remove(selectedPayEntity);
            payEntities.remove(selectedPayEntity);

            selectedPayEntity = null;
            // If there are no entries left in the filtered list, reset it to show all remaining entities
            if (filteredPayEntities.isEmpty()) {
                filteredPayEntities.addAll(payEntities);
            }
        }
        sessionUser.getPageManager().reloadFullPage();
    }

    /**
     * User changed the entity name
     */
    public void nameChanged() {
        //Is the name unique?
        if (!isNameUnique(selectedPayEntity)) {
            sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, selectedPayEntity.getName() + " " + PageManager.translate("IsNotUnique"), PageManager.translate("PleaseChooseAnotherName"));
            sessionUser.getPageManager().setChangesOnPage(false);
        }
    }

    private boolean isNameUnique(PayEntity currentPayEntity) {
        int nbInstances = 0;
        for (PayEntity payEntity : payEntities) {
            if (payEntity.getName().trim().equalsIgnoreCase(currentPayEntity.getName())) {
                nbInstances++;
            }
        }
        return (nbInstances < 2);
    }

    /**
     * Returns the Session Scoped version of all pay entities
     *
     * @return
     */
    public ArrayList<PayEntity> getGlobalPayEntities() {
        // Let's sort it first
        Collections.sort(this.payEntities, new SortBasedOnName());
        return this.payEntities;
    }

    /*
     Private class to handle the default sorting of payentities
     */
    private class SortBasedOnName implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {

            PayEntity dd1 = (PayEntity) o1;// 
            PayEntity dd2 = (PayEntity) o2;
            if (dd1 != null && dd2 != null) {
                return dd1.getName().compareToIgnoreCase(dd2.getName());
            } else {
                return 1;

            }
        }

    }

    /**
     * Returns the Session Scoped version of all pay entities of a given
     * entityType
     *
     * @param payEntityType
     * @return
     */
    public ArrayList<PayEntity> getGlobalPayEntities(EnumVarPayEntityType payEntityType) {
        ArrayList<PayEntity> rslt = new ArrayList<>();
        for (PayEntity payEntity : payEntities) {
            if (payEntityType.equals(payEntity.getEntityType())) {
                rslt.add(payEntity);
            }
        }
        return rslt;
    }

    /**
     * Returns the Session Scoped instance of a payEntity entityType
     *
     * @param theEntityID
     * @param thePayEntityType
     * @return
     */
    public PayEntity getGlobalPayEntity(String theEntityID, EnumVarPayEntityType thePayEntityType) {
        for (PayEntity payEntity : payEntities) {
            if (thePayEntityType.equals(payEntity.getEntityType())) {
                if (theEntityID.equals(payEntity.getEntityID())) {
                    return payEntity;
                }
            }
        }
        return null;
    }

    private PayEntity getPayEntityByName(String payEntityName) {
        for (PayEntity payEntity : payEntities) {
            if (payEntity.getName().equalsIgnoreCase(payEntityName)) {
                return payEntity;
            }
        }
        return null;
    }

    public void onNodeSelect(NodeSelectEvent event) {
        this.selectedPayEntity = getPayEntityByName((String) event.getTreeNode().getData());
        sessionUser.getPageManager().reloadOuterPanel();
    }

    public void onRowSelect(SelectEvent event) {
        this.selectedPayEntity = ((PayEntity) event.getObject());
        sessionUser.getPageManager().reloadOuterPanel();
    }

}
