/*
 * Class representation of a person's payroll information
 */
package za.co.accsys.web.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.beans.PageManager;
import za.co.accsys.web.beans.ReportPrinter;
import za.co.accsys.web.helperclasses.DateUtils;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.helperclasses.JSONRecord;
import za.co.accsys.web.helperclasses.ListManager;
import za.co.accsys.web.helperclasses.ServerPrefs;

/**
 *
 * @author Liam
 */
public class Payroll {

    private String payrollID;
    private String payrollName;
    private String payrollType;
    private Date taxYearStart;
    private Date taxYearEnd;
    private String requestedOpenPeriod;
    private String taxCountry;
    private boolean deleted;
    private boolean canDelete;
    private String currencyString;
    private String printForPeriod;
    private String taxCertificateMonth;
    private String taxCertificateYear;
    private boolean taxCertificatePrepared;
    private boolean noTaxCertificateErrors;
    private String UIFNumber;
    private String SDLNumber;
    private String TaxNumber;
    private String tradeClassification;
    private String tradeSubClass;
    private String sicCode;
    private String taxCertificateWarningsAndErrors;
    private String taxCertificateRunID;

    private ArrayList<PayrollPeriod> periods;
    private int nbActiveEmployees;
    private int nbInactiveEmployees;
    private int nbPreviousActiveEmployees;
    private int nbEngagedInThisPeriod;
    private int nbDischargedInThisPeriod;
    private int nbCalculatedInThisPeriod;
    private int nbSkippedInThisPeriod;
    private int nbInCalcQueueInThisPeriod;
    private final SessionUser sessionUser;

    public Payroll(SessionUser sessionUser) {
        this.taxYearRollOverMessage_Part1 = PageManager.translate("taxYearRollOverMessage_Part1");
        this.taxYearRollOverMessage_Part2 = PageManager.translate("taxYearRollOverMessage_Part2");
        this.sessionUser = sessionUser;
        setDefaults();
    }

    public String getPrintForPeriod() {
        return printForPeriod;
    }

    public void setPrintForPeriod(String printForPeriod) {
        this.printForPeriod = printForPeriod;
    }

    public String getTaxCertificateMonth() {
        return taxCertificateMonth;
    }

    public void setTaxCertificateMonth(String taxCertificateMonth) {
        if (!this.taxCertificateMonth.equals(taxCertificateMonth)) {
            this.taxCertificateMonth = taxCertificateMonth;
            if (!this.taxCertificateMonth.equalsIgnoreCase("00")) {
                // If Feb then use TYE year as the year
                if (this.taxCertificateMonth.equalsIgnoreCase("02")) {
                    int lenght = this.taxYearEnd.toString().length();
                    this.taxCertificateYear = this.taxYearEnd.toString().substring(lenght - 4);
                }
                // If Agu Use Tax year start year as the year
                if (this.taxCertificateMonth.equalsIgnoreCase("08")) {
                    int lenght = this.taxYearStart.toString().length();
                    this.taxCertificateYear = this.taxYearStart.toString().substring(lenght - 4);
                }
                prepareTaxCertificateData();
            } else {
                taxCertificatePrepared = false;
            }
        }
    }

    public String getPayrollID() {
        return payrollID;
    }

    public void setPayrollID(String payrollID) {
        this.payrollID = payrollID;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public boolean isSouthAfrica() {
        if (taxCountry != null) {
            return taxCountry.equalsIgnoreCase(Constants.TAXCOUNTRY_SOUTHAFRICA);
        } else {
            return false;
        }
    }

    public int getNbCalculatedInThisPeriod() {
        return nbCalculatedInThisPeriod;
    }

    public void setNbCalculatedInThisPeriod(int nbCalculatedInThisPeriod) {
        this.nbCalculatedInThisPeriod = nbCalculatedInThisPeriod;
    }

    public String getUIFNumber() {
        return UIFNumber;
    }

    public void setUIFNumber(String UIFNumber) {
        this.UIFNumber = UIFNumber;
    }

    public String getSDLNumber() {
        return SDLNumber;
    }

    public void setSDLNumber(String SDLNumber) {
        this.SDLNumber = SDLNumber;
    }

    public String getTradeClassification() {
        return tradeClassification;
    }

    public void setTradeClassification(String tradeClassification) {
        if (tradeClassification == null) {
            this.tradeClassification = "-- None --";
        } else {
            this.tradeClassification = tradeClassification;
        }
    }

    public String getTradeSubClass() {
        return tradeSubClass;
    }

    public void setTradeSubClass(String tradeSubClass) {
        if (tradeSubClass == null) {
            this.tradeSubClass = "-- None --";
            this.tradeClassification = "-- None --";
        } else {
            this.tradeSubClass = tradeSubClass;
            this.tradeClassification = tradeSubClass.subSequence(0, 2).toString();
        }
    }

    public String getSicCode() {
        return sicCode;
    }

    public void setSicCode(String sicCode) {
        if (sicCode == null) {
            this.sicCode = "-- None --";
        } else {
            this.sicCode = sicCode;
        }
    }

    public String getTaxNumber() {
        return TaxNumber;
    }

    public void setTaxNumber(String TaxNumber) {
        this.TaxNumber = TaxNumber;
        if (TaxNumber != null) {
            if (!TaxNumber.equalsIgnoreCase("")) {
                this.UIFNumber = "U" + TaxNumber.substring(1);
                this.SDLNumber = "L" + TaxNumber.substring(1);
            } else {
                this.UIFNumber = "";
                this.SDLNumber = "";
            }
        }
    }

    private int periodAdvanceStatus;

    /**
     * We can only advance into the next period if we have sufficient payroll
     * credits to handle it.
     *
     * @return
     */
    public int getPeriodAdvanceStatus() {
        periodAdvanceStatus = Constants.ADVANCE_ALLCALCULATED;
        int availableCredits = sessionUser.getPageManager().getPayrollCredits();
        int nbEmployees = (this.sessionUser.getPageManager().getCompany().getEmployeeManager().getEmployeesForPayroll(this, true)).size();
        // Not enough credits
        if (availableCredits < nbEmployees) {
            periodAdvanceStatus = Constants.ADVANCE_NOCREDITS;
        }
        // Some employees were skipped
        if (getNbSkippedInThisPeriod() > 0) {
            periodAdvanceStatus = Constants.ADVANCE_SOMESKIPPED;
        }
        return periodAdvanceStatus;
    }

    public int getNbInactiveEmployees() {
        return nbInactiveEmployees;
    }

    public void setNbInactiveEmployees(int nbInactiveEmployees) {
        this.nbInactiveEmployees = nbInactiveEmployees;
    }

    public int getNbSkippedInThisPeriod() {
        return nbSkippedInThisPeriod;
    }

    public void setAsSelectedPayroll() {
        sessionUser.getPageManager().getCompany().getPayrollManager().setNewPayroll(this);
    }

    public int getNbInCalcQueueInThisPeriod() {
        return nbInCalcQueueInThisPeriod;
    }

    public void setNbInCalcQueueInThisPeriod(int nbInCalcQueueInThisPeriod) {
        this.nbInCalcQueueInThisPeriod = nbInCalcQueueInThisPeriod;
    }

    public void setNbSkippedInThisPeriod(int nbSkippedInThisPeriod) {
        this.nbSkippedInThisPeriod = nbSkippedInThisPeriod;
    }

    public int getNbActiveEmployees() {
        return nbActiveEmployees;
    }

    public void setNbActiveEmployees(int nbActiveEmployees) {
        this.nbActiveEmployees = nbActiveEmployees;
    }

    public int getNbPreviousActiveEmployees() {
        return nbPreviousActiveEmployees;
    }

    public void setOpenPeriodID(String payrollDetailID) {
        if (!getOpenPeriodID().equalsIgnoreCase(payrollDetailID)) {
            for (PayrollPeriod period : periods) {
                period.setOpenPeriod(period.getPayrollDetailID().equalsIgnoreCase(payrollDetailID));
            }
        }
    }

    public String getRequestedOpenPeriod() {
        return requestedOpenPeriod;
    }

    public void setRequestedOpenPeriod(String requestedOpenPeriod) {
        this.requestedOpenPeriod = requestedOpenPeriod;
    }

    /**
     * Returns a list of possible period start dates, based on the payroll type
     * (weekly/monthly), and tax year start date
     *
     * @return
     */
    private List<String> periodStartDates;

    public List<String> getPeriodStartDates() {
        List<String> rslt = new ArrayList<>();

        if ((taxYearStart == null) || (taxYearEnd == null) || (payrollType == null)) {
            return rslt;
        }

        Date firstDate = taxYearStart;
        // Monthly
        if (PageManager.translateFromLocaleToEnglish(payrollType).equalsIgnoreCase("Monthly")) {
            for (int i = 0; i <= 11; i++) {
                rslt.add(DateUtils.formatDate(DateUtils.addNMonths(firstDate, i)));
            }
        }
        // Weekly
        if (PageManager.translateFromLocaleToEnglish(payrollType).equalsIgnoreCase("Weekly")) {
            for (int i = 0; i <= 53; i++) {
                if (DateUtils.addNWeeks(firstDate, i).before(taxYearEnd)) {
                    rslt.add(DateUtils.formatDate(DateUtils.addNWeeks(firstDate, i)));
                }
            }
        }
        return rslt;
    }

    public List<String> getHistoricalPeriodStartDates() {
        List<String> rslt = new ArrayList<>();

        if ((taxYearStart == null) || (taxYearEnd == null) || (payrollType == null)) {
            return rslt;
        }

        Date firstDate = taxYearStart;
        // Monthly
        if (PageManager.translateFromLocaleToEnglish(payrollType).equalsIgnoreCase("Monthly")) {
            for (int i = 0; i <= 11; i++) {
                Date theDate = DateUtils.addNMonths(firstDate, i);
                if (theDate.before(taxYearEnd) && theDate.before(this.getOpenPeriod().getEndDate())) {
                    rslt.add(DateUtils.formatDate(theDate));
                }
            }
        }
        // Weekly
        if (PageManager.translateFromLocaleToEnglish(payrollType).equalsIgnoreCase("Weekly")) {
            for (int i = 0; i <= 53; i++) {
                Date theDate = DateUtils.addNWeeks(firstDate, i);
                if (DateUtils.addNWeeks(firstDate, i).before(taxYearEnd) && theDate.before(this.getOpenPeriod().getEndDate())) {
                    rslt.add(DateUtils.formatDate(theDate));
                }
            }
        }
        return rslt;
    }

    public void setNbPreviousActiveEmployees(int nbPreviousActiveEmployees) {
        this.nbPreviousActiveEmployees = nbPreviousActiveEmployees;
    }

    public int getNbEngagedInThisPeriod() {
        return nbEngagedInThisPeriod;
    }

    public void setNbEngagedInThisPeriod(int nbEngagedInThisPeriod) {
        this.nbEngagedInThisPeriod = nbEngagedInThisPeriod;
    }

    public int getNbDischargedInThisPeriod() {
        return nbDischargedInThisPeriod;
    }

    public void setNbDischargedInThisPeriod(int nbDischargedInThisPeriod) {
        this.nbDischargedInThisPeriod = nbDischargedInThisPeriod;
    }

    public ArrayList<PayrollPeriod> getPeriods() {
        return periods;
    }

    public void setDeleted(boolean deleted) {
        if (this.deleted != deleted) {
            this.deleted = deleted;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    /**
     * Returns the correct confirmation message, depending on the payroll state
     *
     * @return
     */
    private String confirmAdvancePeriodMessage;

    public String getConfirmAdvancePeriodMessage() {
        if (nbSkippedInThisPeriod == 0) {
            confirmAdvancePeriodMessage = PageManager.translate("ConfirmAdvancePayroll");
        } else {
            confirmAdvancePeriodMessage = PageManager.translate("ConfirmAdvancePayrollOutstandingCalcs");
        }
        return confirmAdvancePeriodMessage;
    }

    /**
     * Advances the current payroll into the new week/month NOTE: Some serious
     * coding still to be de done for tax-year-ends!
     */
    public void advancePeriod() {
        boolean rslt = false;
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, this.payrollID));

        // Call the GetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_AdvancePayrollPeriod, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {
            if (nvp.getName().equalsIgnoreCase("RESULT")) {
                rslt = (nvp.getValue().equalsIgnoreCase("OK"));
            }
        }

        // Wait 5 seconds before commencing, giving the payroll enough time to do what it needs to do.
        // The 'cleaner' route would be to ask the payroll to tell us when it's done, but I'm trying to 
        //   reduce the network traffice at all costs!
        try {
            Thread.sleep(5000);                 //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

        sessionUser.getPageManager().getCompany().getPayrollManager().reloadPayrollsStatus();
        //sessionUser.getPageManager().navigateToGeneralInfoScreen();
        sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_INFO, "Advanced to next period", "");
        sessionUser.getPageManager().reloadPayrollCreditsFromDB();
        // Now that we have advanced to the next period, navigate to logout
        sessionUser.getPageManager().navigateToLogoutScreen();
        // Log back in
        sessionUser.getPageManager().login();

        System.out.println("Payroll -" + this.getPayrollName() + "- advanced to next period.");
    }

    /**
     * A payroll may be deleted if none of the employees linked to it has ever
     * been calculated. EVER.
     *
     * @return
     */
    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    private void setDefaults() {
        taxYearStart = new Date();
        taxYearEnd = DateUtils.addNMonths(taxYearStart, 12);
        canDelete = true;
        deleted = false;
        payrollID = "-1";
        payrollType = "Monthly";
        payrollName = "new:" + (int) Math.ceil(Math.random() * 100);
        setTaxCountry("SOUTH AFRICA");
        UIFNumber = "";
        TaxNumber = "";
        tradeClassification = "";
        tradeSubClass = "";
        sicCode = "";
        taxCertificateMonth = "";
        taxCertificateYear = "";
        taxCertificatePrepared = false;
        noTaxCertificateErrors = false;
    }

    public String getPayrollName() {
        return payrollName;
    }

    public void setPeriods(ArrayList<PayrollPeriod> periods) {
        this.periods = periods;
    }

    /**
     * Returns the period that is presently 'Open'
     *
     * @return
     */
    public String getOpenPeriodID() {
        for (PayrollPeriod period : periods) {
            if (period.isOpenPeriod()) {
                return period.getPayrollDetailID();
            }
        }
        return "0";
    }

    private boolean lastPeriodInTaxYear;

    public boolean isLastPeriodInTaxYear() {
        lastPeriodInTaxYear = false;
        if (getOpenPeriod() != null) {
            lastPeriodInTaxYear = (DateUtils.formatDate(getOpenPeriod().getEndDate()).equalsIgnoreCase(DateUtils.formatDate(taxYearEnd)));
        }
        return lastPeriodInTaxYear;
    }

    private String taxYearRollOverMessage_Part1;
    private String taxYearRollOverMessage_Part2;

    public String getTaxYearRollOverMessage_Part1() {
        return taxYearRollOverMessage_Part1;
    }

    public String getTaxYearRollOverMessage_Part2() {
        return taxYearRollOverMessage_Part2;
    }

    public String getTaxCertificateWarningsAndErrors() {
        String warningsAndErrors = taxCertificateWarningsAndErrors; // Temp string so that we can add the header
        if (warningsAndErrors == null) {
            warningsAndErrors = "";
        }
        taxCertificateWarningsAndErrors = "Company:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + sessionUser.getPageManager().getCompany().getName() + "</br>"
                + "Payroll:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + this.payrollName + "</br>"
                + "Tax Year End:&nbsp;&nbsp;" + DateUtils.formatDate(this.taxYearEnd) + "</br>"
                + "Recon Year:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + this.taxCertificateYear + "</br>"
                + "Recon Month:&nbsp;&nbsp;" + this.taxCertificateMonth + "</br>"
                + "UIF Number:&nbsp;&nbsp;&nbsp;&nbsp;" + this.UIFNumber + "</br>"
                + "Tax Number:&nbsp;&nbsp;&nbsp;&nbsp;" + this.TaxNumber + "</br></br>"
                + warningsAndErrors;

        return taxCertificateWarningsAndErrors;
    }

    /**
     * Sends a web service call to prepare the Tax Certificate data
     *
     * @return
     */
    public void prepareTaxCertificateData() {
        // List of parameters
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, this.payrollID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ReconMonth, this.taxCertificateMonth));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_ReconYear, this.taxCertificateYear));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_TradeCode, this.tradeSubClass.substring(0, 4)));

        // Call the appropriate service
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        taxCertificateWarningsAndErrors = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_PrepareTaxCertificateData, listToSend).get(0).getValue();
        taxCertificateRunID = taxCertificateWarningsAndErrors.substring(0, taxCertificateWarningsAndErrors.indexOf("<"));
        taxCertificateWarningsAndErrors = taxCertificateWarningsAndErrors.substring(taxCertificateWarningsAndErrors.indexOf("<"));
        noTaxCertificateErrors = taxCertificateWarningsAndErrors.contains("Successful - No company errors found") && taxCertificateWarningsAndErrors.contains("Successful - No employee errors found");
        // Disable all other payrolls
        ArrayList<Payroll> payrollsList = sessionUser.getPageManager().getCompany().getPayrollManager().getPayrolls();
        for (Payroll payroll : payrollsList) {
            if (!payroll.equals(this)) {
                payroll.setTaxCertificatePrepared(false);
                payroll.setTaxCertificateMonth("00");
            }
        }
        // Set this payroll as enabled for printing tax certificates
        setTaxCertificatePrepared(true);
    }

    public boolean isTaxCertificatePrepared() {
        return taxCertificatePrepared;
    }

    public void setTaxCertificatePrepared(boolean taxCertificatePrepared) {
        if (this.taxCertificatePrepared != taxCertificatePrepared) {
            this.taxCertificatePrepared = taxCertificatePrepared;
        }
    }

    public boolean isNoTaxCertificateErrors() {
        return noTaxCertificateErrors;
    }

    public void setNoTaxCertificateErrors(boolean noTaxCertificateErrors) {
        if (this.noTaxCertificateErrors != noTaxCertificateErrors) {
            this.noTaxCertificateErrors = noTaxCertificateErrors;
        }
    }

    /**
     * Returns the period that is presently 'Open'
     *
     * @return
     */
    public PayrollPeriod getOpenPeriod() {
        for (PayrollPeriod period : periods) {
            if (period.isOpenPeriod()) {
                return period;
            }
        }
        return null;
    }

    /**
     * Returns the date of the period that is presently 'Open' in string format
     *
     * @return
     */
    public String getOpenPeriodDateAsString() {
        PayrollPeriod period = getOpenPeriod();
        String openPeriodDate = (DateUtils.formatDate(period.getStartDate()));
        return openPeriodDate;
    }

    public void setPayrollName(String payrollName) {
        if ((this.payrollName == null) || (!this.payrollName.equals(payrollName))) {
            this.payrollName = payrollName;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPayrollType() {
        return PageManager.translateFromEnglishToLocale(payrollType).trim();
    }

    public void setPayrollType(String payrollType) {
        if ((this.payrollType == null) || (!this.payrollType.equals(payrollType))) {
            this.payrollType = payrollType;
            this.taxYearEnd = getProjectedTaxYearEnd();
            sessionUser.getPageManager().setChangesOnPage(true);
            // Update the screen
            RequestContext.getCurrentInstance().update("frmCentrePage:outerPanel");
        }
    }

    public String getTaxCountry() {
        return taxCountry;
    }

    public void setTaxCountry(String taxCountry) {
        if ((this.taxCountry == null) || (!this.taxCountry.equals(taxCountry))) {
            this.taxCountry = taxCountry;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public Date getTaxYearStart() {
        return taxYearStart;
    }

    public String getTaxYearStartString() {
        return DateUtils.formatDate(taxYearStart);
    }

    public String getTaxYearEndString() {
        return DateUtils.formatDate(taxYearEnd);
    }

    public Date getProjectedTaxYearEnd() {
        Date rslt;
        if (payrollType.equalsIgnoreCase("Monthly")) {
            // Add 12 months
            rslt = DateUtils.addNMonths(taxYearStart, 12);
            // Minus one day
            rslt = DateUtils.addNDays(rslt, -1);
        } else {
            // Add 53 weeks
            rslt = DateUtils.addNWeeks(taxYearStart, 52);
            // Minus one day
            rslt = DateUtils.addNDays(rslt, -1);
        }
        return rslt;
    }

    public void setTaxYearStart(Date taxYearStart) {
        if (this.taxYearStart != taxYearStart) {
            this.taxYearStart = taxYearStart;
            // Let's set the default endDate
            this.taxYearEnd = getProjectedTaxYearEnd();
            sessionUser.getPageManager().setChangesOnPage(true);
            // Update the screen
            RequestContext.getCurrentInstance().update("frmCentrePage:outerPanel");
        }
    }

    public void updateTaxYearEnd(SelectEvent event) {
        // Let's set the default endDate
        this.taxYearEnd = getProjectedTaxYearEnd();
        // Update the screen
        RequestContext.getCurrentInstance().update("frmCentrePage:outerPanel");
    }

    public Date getTaxYearEnd() {
        return taxYearEnd;
    }

    public void setTaxYearEnd(Date taxYearEnd) {
        if (this.taxYearEnd != taxYearEnd) {
            this.taxYearEnd = taxYearEnd;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getCurrencyString() {
        if (currencyString == null) {
            currencyString = "";
        }
        return currencyString;
    }

    public void setCurrencyString(String currencyString) {
        this.currencyString = currencyString;
    }

    /**
     * Generates a Tax Return for this payroll
     */
    public void printTaxReturn() {
        ReportPrinter printer = new ReportPrinter();
        printer.printTaxReturn(sessionUser, this);
    }

    /**
     * Generates a Tax Return for this payroll
     */
    public void printPayrollRegister() {
        ReportPrinter printer = new ReportPrinter();
        printer.printPayrollRegister(sessionUser, this);
    }

    /**
     * Generates a UIF Extract for this payroll
     */
    public void printUifExtractReport() {
        ReportPrinter printer = new ReportPrinter();
        printer.printUifExtractReport(sessionUser, this);
    }

    /**
     * Generates a SDL report for this payroll
     */
    public void printPayrollSDL() {
        ReportPrinter printer = new ReportPrinter();
        printer.printPayrollSDL(sessionUser, this);
    }

    /**
     * Generates a Variance report for this payroll
     */
    public void printPayrollVariance() {
        ReportPrinter printer = new ReportPrinter();
        printer.printPayrollVariance(sessionUser, this);
    }

    /**
     * Generates a Year To Date Tax Return for this payroll
     */
    public void printPayrollRegisterYTD() {
        ReportPrinter printer = new ReportPrinter();
        printer.printPayrollRegisterYTD(sessionUser, this);
    }

    /**
     * Generates an EMP201 Return for this payroll
     */
    public void printEMP201() {
        ReportPrinter printer = new ReportPrinter();
        printer.printEMP201(sessionUser, this);
    }

    private StreamedContent uifExtractList;

    public StreamedContent getUifExtractList() {
        return getUifExtractListFile();
    }

    public void setUifExtractList(StreamedContent uifExtractList) {
        this.uifExtractList = uifExtractList;
    }

    /**
     * Retrieves the UIF Extract Data for the CSV file
     *
     * @return
     */
    private StreamedContent getUifExtractListFile() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, payrollID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_CompanyId, sessionUser.getCompanyId()));

        List<String> uifExtractData = new ArrayList<>();

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetPayrollUifExtactList, listToSend);

        // What did we get back?
        // The result is a JSON Array listing all UIF Extract rows
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the employee record
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            String extractEntry = null;
            // Step through each record, and construct a string
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("Employee_Record")) {
                    extractEntry = nvp.getValue();
                }
            }
            //System.out.println("extractEntry = " + extractEntry);
            if (extractEntry != null) {
                if (extractEntry.trim().length() > 0) {
                    uifExtractData.add(extractEntry.replace("~", "\"") + "\n");
                }
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        for (String line : uifExtractData) {
            try {
                baos.write(line.getBytes());
            } catch (IOException ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        byte[] bytes = baos.toByteArray();
        InputStream in = new ByteArrayInputStream(bytes);

        uifExtractList = new DefaultStreamedContent(in, "text/plain", "uif_extract.csv");

        return uifExtractList;
    }

    private StreamedContent taxCertificateExtractList;

    public StreamedContent getTaxCertificateExtractList() {
        return getTaxCertificateExtractListFile();
    }

    public void setTaxCertificateExtractList(StreamedContent taxCertificateExtractList) {
        this.taxCertificateExtractList = taxCertificateExtractList;
    }

    /**
     * Retrieves the Tax Certificate Extract Data for the SDF file
     *
     * @return
     */
    private StreamedContent getTaxCertificateExtractListFile() {
        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_PayrollId, payrollID));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_RunId, taxCertificateRunID));

        List<String> taxCertificateExtractData = new ArrayList<>();

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetPayrollTCExtactList, listToSend);

        // What did we get back?
        // The result is a JSON Array listing all UIF Extract rows
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the employee record
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            String extractEntry = null;
            // Step through each record, and construct a string
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("Employee_Record")) {
                    extractEntry = nvp.getValue();
                }
            }
            //System.out.println("extractEntry = " + extractEntry);
            if (extractEntry != null) {
                if (extractEntry.trim().length() > 0) {
                    taxCertificateExtractData.add(extractEntry.replace("~", "\"") + "\n");
                }
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        for (String line : taxCertificateExtractData) {
            try {
                baos.write(line.getBytes());
            } catch (IOException ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        byte[] bytes = baos.toByteArray();
        InputStream in = new ByteArrayInputStream(bytes);

        taxCertificateExtractList = new DefaultStreamedContent(in, "text/plain", this.getPayrollName() + "_Payroll_taxCertificate_extract.sdf");

        return taxCertificateExtractList;
    }
}
