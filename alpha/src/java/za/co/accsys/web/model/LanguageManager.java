/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

import java.util.ArrayList;
import javax.faces.event.ActionEvent;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.primefaces.event.SelectEvent;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.controller.WebServiceClient;
import za.co.accsys.web.helperclasses.JSONRecord;
import za.co.accsys.web.helperclasses.ServerPrefs;

/**
 *
 * @author liam
 */
public final class LanguageManager {

    private Language selectedLanguage;
    private Language newLanguage;
    private String newStringLanguage;
    private final SessionUser sessionUser;
    private ArrayList<Language> languages;

    public LanguageManager(SessionUser sessionUser) {
        this.sessionUser = sessionUser;

        loadLanguages();
        newLanguage = new Language(sessionUser);
    }

    /**
     * Runs a web service call to retrieve the list of Job Positions
     *
     * @return
     */
    public boolean loadLanguages() {
        languages = new ArrayList<>();
        boolean rslt = true;

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));

        // Call the appropriate service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(sessionUser.getSessionID(), URL, Constants.serviceName_GetLanguageList, listToSend);

        // What did we get back?
        // The result is a JSON Array listing languages
        for (JSONRecord jsonRecord : listRetreived) {
            // Array, each record containing name/value pairs for the cost centre
            ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
            Language language = new Language(sessionUser);
            language.setDeleted(false);

            // Step through each record, and construct an EMPLOYEE instance
            for (NameValuePair nvp : nvpList) {
                if (nvp.getName().equalsIgnoreCase("language")) {
                    language.setLanguage(nvp.getValue());
                }
                if (nvp.getName().equalsIgnoreCase("languageID")) {
                    language.setLanguageID(nvp.getValue());
                }
            }
            languages.add(language);
        }

        return rslt;
    }

    public SessionUser getSessionUser() {
        return sessionUser;
    }

    public Language getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(Language selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    /**
     * All the attributes for this payEntity already exists in 'newPayEntity'
     *
     * @param actionEvent
     */
    public void createNewLanguage(ActionEvent actionEvent) {

        // Add current 'newLanguage' into the ArrayList
        saveLanguage(newLanguage);
        languages.add(newLanguage);
        // Mark it as the current selected entity
        selectedLanguage = newLanguage;

        // Create a new entity for future use
        newLanguage = new Language(sessionUser);
    }

    public boolean languageExists(String currentLanguage) {
        for (Language language : languages) {
            if (language.getLanguage().trim().equalsIgnoreCase(currentLanguage)) {
                return true;
            }
        }
        return false;
    }

    public void onRowSelect(SelectEvent event) {
        this.selectedLanguage = ((Language) event.getObject());
    }

    public Language getNewLanguage() {
        return newLanguage;
    }

    public void setNewLanguage(Language newLanguage) {
        this.newLanguage = newLanguage;
    }
    
    public String getNewStringLanguage() {
        return newStringLanguage;
    }

    public void setNewStringLanguage(String newStringLanguage) {
        this.newStringLanguage = newStringLanguage;
        this.newLanguage.setLanguage(newStringLanguage);
    }

    public ArrayList<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(ArrayList<Language> languages) {
        this.languages = languages;
    }

    /**
     * Returns a String[] of Job Positions
     *
     * @return
     */
    public String[] getStringLanguages() {
        String[] rslt = new String[languages.size()];
        int i = 0;
        for (Language language : languages) {
            rslt[i++] = language.getLanguage();
        }
        return rslt;
    }


    public void deleteSelectedLanguage(ActionEvent actionEvent) {
        selectedLanguage.setDeleted(true);
        saveLanguage(selectedLanguage);
        languages.remove(selectedLanguage);
        selectedLanguage = null;
    }

    /**
     * Runs a web service call to update all the information for all the
     * loans/savings/earnings/deductions/etc
     *
     * @param language
     * @return
     */
    public boolean saveLanguage(Language language) {

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionUser.getSessionID()));
        listToSend.add(new BasicNameValuePair("language", language.getLanguage()));
        listToSend.add(new BasicNameValuePair("languageID", language.getLanguageID()));

        if (language.isDeleted()) {
            listToSend.add(new BasicNameValuePair("deleted", "Y"));
        } else {
            listToSend.add(new BasicNameValuePair("deleted", "N"));
        }

        // Call the SetValue service
        ArrayList<NameValuePair> wsResult;
        String URL = ServerPrefs.getInstance().getDbURL(sessionUser.getAccountID());
        wsResult = WebServiceClient.getInstance().getOneLineResponse(sessionUser, URL, Constants.serviceName_SetLanguageData, listToSend);

        // What did we get back?
        for (NameValuePair nvp : wsResult) {
            if (nvp.getName().equalsIgnoreCase("languageID")) {
                language.setLanguageID(nvp.getValue());
            }
        }
        return true;
    }

}
