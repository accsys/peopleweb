/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

/**
 *
 * @author Liam
 */
public class Address {

    private String country;
    private String province;
    private String postalCode;
    private String cityOrTown;
    private final SessionUser sessionUser;

    public Address(SessionUser sessionUser) {
        this.sessionUser = sessionUser;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if ((this.country == null) || (!this.country.equals(country))) {
            this.country = country;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    protected SessionUser getSessionUser(){
        return this.sessionUser;
    }
    
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        if ((this.province == null) || (!this.province.equals(province))) {
            this.province = province;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        if ((this.postalCode == null) || (!this.postalCode.equals(postalCode))) {
            this.postalCode = postalCode;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

    public String getCityOrTown() {
        return cityOrTown;
    }

    public void setCityOrTown(String cityOrTown) {
        if ((this.cityOrTown == null) || (!this.cityOrTown.equals(cityOrTown))) {
            this.cityOrTown = cityOrTown;
            sessionUser.getPageManager().setChangesOnPage(true);
        }
    }

}
