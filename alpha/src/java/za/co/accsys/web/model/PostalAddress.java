/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.web.model;

//    private String country;
import za.co.accsys.web.beans.PageManager;

//    private String province;
//    private String postalCode;
//    private String cityOrTown;
/**
 *
 * @author Liam
 */
public class PostalAddress extends Address {

    private String postOffice;
    private String postalAgencyOrSubUnit;
    private String boxOrBagNumber;
    private String boxType; // PO Box or Private Bag
    private boolean postalAddressIsCareOf;
    private boolean isPOBox;
    private boolean isPrivateBag;

    public PostalAddress(SessionUser sessionUser) {
        super(sessionUser);
    }

    public boolean isIsPOBox() {
        return isPOBox;
    }

    public void setIsPOBox(boolean isPOBox) {
        if (this.isPOBox != isPOBox) {
            this.isPOBox = isPOBox;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public boolean isIsPrivateBag() {
        return isPrivateBag;
    }

    public void setIsPrivateBag(boolean isPrivateBag) {
        if (this.isPrivateBag != isPrivateBag) {
            this.isPrivateBag = isPrivateBag;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        if ((this.postOffice == null) || (!this.postOffice.equals(postOffice))) {
            this.postOffice = postOffice;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public String getPostalAgencyOrSubUnit() {
        return postalAgencyOrSubUnit;
    }

    public void setPostalAgencyOrSubUnit(String postalAgencyOrSubUnit) {
        if ((this.postalAgencyOrSubUnit == null) || (!this.postalAgencyOrSubUnit.equals(postalAgencyOrSubUnit))) {
            this.postalAgencyOrSubUnit = postalAgencyOrSubUnit;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public String getBoxOrBagNumber() {
        return boxOrBagNumber;
    }

    public void setBoxOrBagNumber(String boxOrBagNumber) {
        if ((this.boxOrBagNumber == null) || (!this.boxOrBagNumber.equals(boxOrBagNumber))) {
            this.boxOrBagNumber = boxOrBagNumber;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public String getBoxType() {
        // PO BOX or PrivateBag?
        if (isPrivateBag) {
            return "Private Bag";
        } else {
            return "PO Box";
        }
    }

    public void setBoxType(String boxType) {
        if ((this.boxType == null) || (!this.boxType.equals(boxType))) {
            this.isPrivateBag = boxType.equalsIgnoreCase("Private Bag");
            this.isPOBox = boxType.equalsIgnoreCase("PO Box");
            this.boxType = boxType;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

    public boolean isPostalAddressIsCareOf() {
        return postalAddressIsCareOf;
    }

    public void setPostalAddressIsCareOf(boolean postalAddressIsCareOf) {
        if (this.postalAddressIsCareOf != postalAddressIsCareOf) {
            this.postalAddressIsCareOf = postalAddressIsCareOf;
            getSessionUser().getPageManager().setChangesOnPage(true);
        }
    }

}
