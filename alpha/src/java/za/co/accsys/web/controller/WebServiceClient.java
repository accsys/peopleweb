/*
 * This Singleton class is central to all the JSON Web service calls.
 * Use getOneLineResponse to send an HTTP request (with parameters) to a given server.
 * The result is parsed and returned in Key-Value pairs
 */
package za.co.accsys.web.controller;

import za.co.accsys.web.helperclasses.ServerPrefs;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import za.co.accsys.web.helperclasses.JSONRecord;
import za.co.accsys.web.beans.Constants;
import za.co.accsys.web.model.SessionUser;

/**
 *
 * @author lterblanche
 */
public class WebServiceClient {
    
    private static WebServiceClient webServiceClient;

    /**
     * Construct an instance of the WebServiceClient class
     *
     */
    private WebServiceClient() {
    }
    
    public static WebServiceClient getInstance() {
        if (WebServiceClient.webServiceClient == null) {
            WebServiceClient.webServiceClient = new WebServiceClient();
        }
        return webServiceClient;
    }

    /**
     * Quick lookup if this key exists in the NameValuePair list
     *
     * @param name
     * @param list - ArrayList(NameValuePair)
     * @return
     */
    public boolean listContainsName(String name, ArrayList<NameValuePair> list) {
        for (NameValuePair kvp : list) {
            if (kvp.getName().compareToIgnoreCase(name) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Easy to use method to retrieve specific values from ArrayList(NameValuePair)
     *
     * @param name
     * @param list - ArrayList(NameValuePair)
     * @return
     */
    public String getValueFrom(String name, ArrayList<NameValuePair> list) {
        for (NameValuePair kvp : list) {
            if (kvp.getName().compareToIgnoreCase(name) == 0) {
                return kvp.getValue();
            }
        }
        return "";
    }

    /**
     * Consumes a web service call and returns an array of Name/Value pairs for a single-line response
     *
     * @param sessionUser
     * @param baseURL
     * @param wsCall processed/returned
     * @param nameValuePairs - Parameters to be passed to web service call
     * @return List of parameters returned from call
     */
    public ArrayList<NameValuePair> getOneLineResponse(SessionUser sessionUser, String baseURL, String wsCall, ArrayList<NameValuePair> nameValuePairs) {
        
        HttpClient client = new DefaultHttpClient();
        String fullURL;
        if (sessionUser != null) {
            fullURL = buildWSURL(sessionUser.getSessionID(), baseURL, wsCall, nameValuePairs);
        } else {
            fullURL = buildWSURL(null, baseURL, wsCall, nameValuePairs);
        }
        //System.out.println("getOneLineResponse:"+fullURL);
        ArrayList<NameValuePair> rslt = new ArrayList<>();
        try {
            HttpPost post = new HttpPost(fullURL);
            // Reading the response
            StringBuilder httpResponse = new StringBuilder();

            // Executes the web service (with parameters)
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            
            String line;
            while ((line = rd.readLine()) != null) {
                httpResponse.append(line);
            }

            // now let's parse it properly
            HashMap<String, String> result = parseJSONLine(httpResponse.toString());
            // and print the result
            rslt.clear();
            if (result != null) {
                for (String key : result.keySet()) {
                    //System.out.println("\nKey: " + key + "\tValue:" + result.get(key));
                    rslt.add(new BasicNameValuePair(key, result.get(key)));
                }
            }
        } catch (java.net.ConnectException e) {
            if (sessionUser != null) {
                sessionUser.getPageManager().notifyUser(FacesMessage.SEVERITY_ERROR, "Unable to connect to Database Server.  Please check your Account ID and try again.", null);
            }
        } catch (IOException ce) {
            ce.printStackTrace();
        }
        return rslt;
    }

    /**
     * Consumes a web service call and returns the result set in rows of NVP values
     *
     * @param sessionID
     * @param baseURL
     * @param wsCall
     * @param nameValuePairs - Parameters to be passed to web service call
     * @return ArrayList of JSONRecord
     */
    public ArrayList<JSONRecord> getMultiLineResponse(String sessionID, String baseURL, String wsCall, ArrayList<NameValuePair> nameValuePairs) {
        
        HttpClient client = new DefaultHttpClient();
        String fullURL = buildWSURL(sessionID, baseURL, wsCall, nameValuePairs);
        System.out.println("getMuliLineResponse:" + fullURL);
        HttpPost post = new HttpPost(fullURL);

        // Reading the response
        StringBuilder httpResponse = new StringBuilder();
        try {

            // Executes the web service (with parameters)
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            
            String line;
            while ((line = rd.readLine()) != null) {
                httpResponse.append(line);
            }

            // now let's parse it properly
            ArrayList<JSONRecord> result = parseJSONRows(httpResponse.toString());
            System.out.println("\n");
            
            return result;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (java.net.ConnectException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Constructs a structured paramaterised URL
     *
     * @param sessionID - internal ID associated with the currently logged in user
     * @param baseURL - HOME address of the app
     * @param wsCall - Web Service name
     * @param nameValuePairs - paramaters to be sent to web service
     * @return
     */
    private String buildWSURL(String sessionID, String baseURL, String wsCall, ArrayList<NameValuePair> nameValuePairs) {
        
        ArrayList<NameValuePair> rslt = new ArrayList<>();

        // Does the parameters already include a sessionID?  If not, add it
        if (sessionID != null) {
            if (!listContainsName(Constants.sendParam_SessionId, nameValuePairs)) {
                nameValuePairs.add(new BasicNameValuePair(Constants.sendParam_SessionId, sessionID));
            }
        }
        
        StringBuilder callURL = new StringBuilder(baseURL);
        if (baseURL.trim().endsWith("/")) {
            callURL.append(wsCall);
        } else {
            callURL.append("/").append(wsCall);
        }
        // Add parameters
        if (nameValuePairs != null) {
            if (nameValuePairs.size() > 0) {
                callURL.append("?");
                for (NameValuePair nvp : nameValuePairs) {
                    try {
                        if (nvp.getValue() != null) {
                            callURL.append(URLEncoder.encode(nvp.getName(), "UTF-8"));
                            callURL.append("=");
                            callURL.append(URLEncoder.encode(nvp.getValue(), "UTF-8"));
                            callURL.append("&");
                        }
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(WebServiceClient.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        // Remove last '&'
        String url = callURL.toString();
        if (url.endsWith("&")) {
            url = url.trim().substring(0, url.length() - 1);
        }

        return url;
    }

    /**
     * Resets the local static session ID, and then calls the web service call to create a new session ID
     *
     * @param accountID
     * @param userName
     * @param password
     * @return - SessionID
     */
    public String logIn(String accountID, String userName, String password) {

        // NameValuePair to send
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair(Constants.sendParam_Login, userName));
        listToSend.add(new BasicNameValuePair(Constants.sendParam_Password, password));

        // Call the Login Web Service
        // The call returns a session ID or an empty value
        // e.g. "SESSION_ID": "758161b2-b3c6-435f-8aa2-67a6c6380ef7"
        //   or "SESSION_ID": ""
        ArrayList<NameValuePair> listRetreived;
        listRetreived = getOneLineResponse(null, ServerPrefs.getInstance().getDbURL(accountID), Constants.serviceName_Login, listToSend);

        // Let's get the session id generated by Sybase
        if (listRetreived.size() > 0) {
            NameValuePair nvp = listRetreived.get(0);
            String newSessionID = (nvp.getValue().trim());
            if (newSessionID.trim().length() == 0) {
                return null;
            } else {
                return newSessionID;
            }
        } else {
            return null;
        }
    }

    /**
     * parses a JSON String (flat structure) and returns the key-value pairs in a HashMap (intended for a
     * single line result-set)
     *
     * @param jsonString
     * @return
     */
    private HashMap<String, String> parseJSONLine(String jsonString) {
        /*
         * Some house-keeping
         * Remove first '[' and last ']'
         */
        jsonString = jsonString.trim();
        if (jsonString.startsWith("[")) {
            jsonString = jsonString.substring(1, jsonString.length() - 1);
        }
        if (jsonString.endsWith("]")) {
            jsonString = jsonString.substring(0, jsonString.length() - 2);
        }
        HashMap<String, String> rslt = new HashMap<>();
        
        if (jsonString.trim().length() > 0) {
            try {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
                return (HashMap<String, String>) jsonObject;
                
            } catch (ParseException pe) {
                System.out.println(pe);
            } catch (org.json.simple.parser.ParseException ex) {
                Logger.getLogger(WebServiceClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * parses a JSON String (flat structure) and returns the key-value pairs in a HashMap (intended for a
     * single record result-set)
     *
     * @param jsonString
     * @return
     */
    private ArrayList<NameValuePair> parseJSONLineToNVP(String jsonString) {
        /*
         * Some house-keeping
         * Remove first '[' and last ']'
         */
        jsonString = jsonString.trim();
        if (jsonString.startsWith("[")) {
            jsonString = jsonString.substring(1, jsonString.length() - 1);
        }
        if (jsonString.endsWith("]")) {
            jsonString = jsonString.substring(0, jsonString.length() - 2);
        }
        ArrayList<NameValuePair> rslt = new ArrayList<>();
        
        if (jsonString.trim().length() > 0) {
            try {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
                HashMap<String, String> obj = (HashMap<String, String>) jsonObject;
                for (String key : obj.keySet()) {
                    rslt.add(new BasicNameValuePair(key, obj.get(key)));
                }
                
                return rslt;
                
            } catch (ParseException pe) {
                System.out.println(pe);
            } catch (org.json.simple.parser.ParseException ex) {
                Logger.getLogger(WebServiceClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    private ArrayList<NameValuePair> parseJSONObject(JSONObject jsonObject) {
        
        ArrayList<NameValuePair> rslt = new ArrayList<>();
        
        try {
            JSONParser parser = new JSONParser();
            HashMap<String, String> obj = (HashMap<String, String>) jsonObject;
            for (String key : obj.keySet()) {
                rslt.add(new BasicNameValuePair(key, obj.get(key)));
            }
            
            return rslt;
            
        } catch (ParseException pe) {
            System.out.println(pe);
        }
        return null;
    }

    /**
     * parses a JSON String (arrays with attributes) and returns the key-value pairs in a HashMap (intended
     * for multi-row results)
     *
     * @param jsonString
     * @return
     */
    private ArrayList<JSONRecord> parseJSONRows(String jsonString) {
        ArrayList<JSONRecord> rslt = new ArrayList<>();
        jsonString = jsonString.trim();
        
        if (jsonString.trim().length() > 0) {
            try {
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(jsonString);
                int idx = 0;
                for (Object obj : array) {
                    JSONObject rowObject = (JSONObject) obj;
                    // Parse this row into a JSONRecord
                    // 1. Create an array list of NameValuePairs
                    ArrayList<NameValuePair> oneRowNVP = parseJSONObject(rowObject);
                    JSONRecord record = new JSONRecord(idx, oneRowNVP);
                    rslt.add(record);
                    idx++;
                }
                return rslt;
            } catch (ParseException pe) {
                System.out.println(pe);
            } catch (org.json.simple.parser.ParseException ex) {
                Logger.getLogger(WebServiceClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * Returns the number of available credits from the appropriate server
     *
     * @param module - Payroll / ...(future)
     * @param accountID
     * @return
     */
    public String GLOBAL_getCreditsAvailable(String accountID, String module) {
        String rslt = "0";

        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(accountID);
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(null, URL, Constants.serviceName_GetCredits, new ArrayList<NameValuePair>());

        // What did we get back?
        // The result is a JSON Array with credits per module
        if (listRetreived != null) {
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the employees
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                // Step through each record, and construct an EMPLOYEE instance
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase(module)) {
                        rslt = nvp.getValue();
                    }
                }
            }
            return rslt;
        } else {
            return Constants.ERR_UNABLE_TO_CONNECT;
        }
    }
    
    /**
     * Returns the projected monthly credit usage for this account
     *
     * @param module - Payroll / ...(future)
     * @param accountID
     * @return
     */
    public String GLOBAL_getMonthlyCreditUsageProjection(String accountID, String module) {
        String rslt = "0";

        // Call the GetValue service
        ArrayList<JSONRecord> listRetreived;
        String URL = ServerPrefs.getInstance().getDbURL(accountID);
        listRetreived = WebServiceClient.getInstance().getMultiLineResponse(null, URL, Constants.serviceName_GetCreditUsageEstimate, new ArrayList<NameValuePair>());

        // What did we get back?
        // The result is a JSON Array with credits per module
        if (listRetreived != null) {
            for (JSONRecord jsonRecord : listRetreived) {
                // Array, each record containing name/value pairs for the employees
                ArrayList<NameValuePair> nvpList = jsonRecord.getNameValuePairs();
                // Step through each record, and construct an EMPLOYEE instance
                for (NameValuePair nvp : nvpList) {
                    if (nvp.getName().equalsIgnoreCase(module)) {
                        rslt = nvp.getValue();
                    }
                }
            }
            return rslt;
        } else {
            return Constants.ERR_UNABLE_TO_CONNECT;
        }
    }

    /**
     * Returns to the appropriate server and prompts for the specific credits
     *
     * @param module - Payroll / ...(future)
     * @param accountID
     * @param nbCredits
     * @return
     */
    public int GLOBAL_addCredits(String accountID, String module, int nbCredits) {

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair("processName", module));
        listToSend.add(new BasicNameValuePair("nbCredits", Integer.toString(nbCredits)));

        // Call the GetValue service
        ArrayList<NameValuePair> response;
        String URL = ServerPrefs.getInstance().getDbURL(accountID);
        response = WebServiceClient.getInstance().getOneLineResponse(null, URL, Constants.serviceName_AddCredits, listToSend);
        String addedCredits = "0";

        // What did we get back?
        // Step through each record, and construct a COMPANY instance
        for (NameValuePair nvp : response) {
            if (nvp.getName().equalsIgnoreCase("AddedCredits")) {
                addedCredits = (nvp.getValue());
            }
        }
        nbCredits = Integer.parseInt(addedCredits);
        return nbCredits;
        
    }

    /**
     * Returns to the appropriate server and prompts for the specific credits
     *
     * @param module - Payroll / ...(future)
     * @param accountID
     * @param nbCredits
     * @param creditDate
     * @param transID
     * @param transRef
     * @param rsltCode
     * @param transStatus
     * @param rsltDescr
     * @param authCode
     * @param amtInCents
     * @param riskIndicator
     * @param chkSum
     * @return
     */
    public boolean GLOBAL_saveCreditTransaction(String accountID, String module, int nbCredits,
            String creditDate, String transID, String transRef, String transStatus, String rsltCode,
            String rsltDescr, String authCode, int amtInCents, String riskIndicator, String chkSum) {

        // List of attributes to pass to the web server
        ArrayList<NameValuePair> listToSend = new ArrayList<>();
        listToSend.add(new BasicNameValuePair("module", module));
        listToSend.add(new BasicNameValuePair("creditDate", creditDate));
        listToSend.add(new BasicNameValuePair("transID", transID));
        listToSend.add(new BasicNameValuePair("nbCredits", Integer.toString(nbCredits)));
        listToSend.add(new BasicNameValuePair("transRef", transRef));
        listToSend.add(new BasicNameValuePair("transStatus", transStatus));
        listToSend.add(new BasicNameValuePair("rsltCode", rsltCode));
        listToSend.add(new BasicNameValuePair("rsltDescr", rsltDescr));
        listToSend.add(new BasicNameValuePair("authCode", authCode));
        listToSend.add(new BasicNameValuePair("amtInCents", Integer.toString(amtInCents)));
        listToSend.add(new BasicNameValuePair("riskIndicator", riskIndicator));
        listToSend.add(new BasicNameValuePair("chkSum", chkSum));
        listToSend.add(new BasicNameValuePair("invToCC", ServerPrefs.getInstance().getCompanyName(accountID)));
        listToSend.add(new BasicNameValuePair("invToPerson", ServerPrefs.getInstance().getOperatorName(accountID)));

        // Call the GetValue service
        ArrayList<NameValuePair> response;
        String URL = ServerPrefs.getInstance().getDbURL(accountID);
        response = WebServiceClient.getInstance().getOneLineResponse(null, URL, Constants.serviceName_SaveCreditTransaction, listToSend);
        String recordCount = "0";

        // What did we get back?
        // Step through each record, and construct a COMPANY instance
        for (NameValuePair nvp : response) {
            if (nvp.getName().equalsIgnoreCase("DB_RSLT")) {
                recordCount = (nvp.getValue());
            }
        }
        
        return (recordCount.equals("1"));
        
    }

    /**
     * Creates a new PeopleWeb instance
     *
     * @param accountID The internal account number used by Parallels. We will use this same number (numeric
     * only) as the primary key for all entities stored in the client’s database.
     * @param companyName
     * @param userFirstName Name of the registered client (the person himself)
     * @param userSurname Surname of the registered client
     * @param userLogin Login name for registered client (will be used when creating the login/pwd credentials
     * on PeopleWeb)
     * @param userPwd Password for registered client (client will be able to change this afterwards)
     * @param userEmail Used to send password-reset instructions, low-credit-balance notifications and report
     * extracts to client.
     * @param payrollName PeopleWeb requires every employee, including the original registered client, to be
     * part of a payroll. It is therefore necessary to create at least one default payroll as part of the
     * registration process.
     * @param payrollType Monthly/Weekly
     * @param payrollCountry South Africa/Namibia/...
     * @param payrollTaxYearStart Date field that the user chose as the start date of his/her new payroll.
     * @return
     */
    public String GLOBAL_register_new(String accountID, String companyName, String userFirstName, String userSurname,
            String userLogin, String userPwd, String userEmail, String payrollName, String payrollType,
            String payrollCountry, String payrollTaxYearStart) {
        
        return "";
    }
    
}
