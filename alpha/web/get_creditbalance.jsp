<%-- 
    Document   : get_creditbalance
    Created on : 06 Jan 2015, 2:18:13 PM
    Author     : lterblanche
    Comments   : This routine does not send an updated value of what the credits should be.  
                 It only sends through the number of credits that the person has successfully purchased.  PeopleWeb will update the balance accordingly.
    Parameters : accountID - The internal account number used by Parallels. 
                 We will use this same number (numeric only) as the primary key for all entities stored in the client’s database.

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String paramVal = request.getParameter("accountID");
            if (paramVal == null) {
                out.write("Missing `accountID` parameter");
            } else {
                za.co.accsys.web.controller.WebServiceClient client = za.co.accsys.web.controller.WebServiceClient.getInstance();

                // Credit Balance
                String creditVal = client.GLOBAL_getCreditsAvailable(paramVal, za.co.accsys.web.beans.Constants.MODULE_PAYROLL);
                out.write(creditVal);
            }
        %>
    </body>
</html>
