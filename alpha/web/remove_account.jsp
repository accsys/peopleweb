<%-- 
    Document   : remove_account
    Created on : 2 Feb 2015
    Author     : lterblanche
    Comments   : Used to remove an account from PeopleWeb.  In doing so, the used database will be come free and available for the next user.
    Parameters : accountID - The internal account number used by Parallels. 
                 companyName - FailSafe to ensure we do not accidentally remove the wrong account
                 We will use this same number (numeric only) as the primary key for all entities stored in the client’s database.

--%>

<%@page import="java.util.ArrayList"%>
<%@page import="za.co.accsys.web.helperclasses.SvrUserAccount"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Remove PeopleWeb Account</title>
    </head>
    <body>
        <%
            String accountID = request.getParameter("accountID");
            String companyName = request.getParameter("companyName");
            String doDeleteThisAccountID = "";

            if (accountID == null || companyName == null) {
                out.write("Missing parameters");
            } else {

                za.co.accsys.web.helperclasses.ServerPrefs prefs = za.co.accsys.web.helperclasses.ServerPrefs.getInstance();
                prefs.reloadAccountPreferences();
                ArrayList<SvrUserAccount> accounts = prefs.getAccounts();

                // 
                // Step through accounts, deleting the required one
                //
                for (SvrUserAccount account : accounts) {
                    // Confirm that Account ID and Company Name matches
                    if (account.getCompanyName().trim().equalsIgnoreCase(companyName.trim())) {
                        if (account.getAccountID().trim().equalsIgnoreCase(accountID.trim())) {
                            // Remove the account
                            doDeleteThisAccountID = accountID;
                        }
                    }
                }
                if (!doDeleteThisAccountID.isEmpty()) {
                    prefs.removeAccount(doDeleteThisAccountID);
                    out.write("Account: " + companyName + " [" + accountID + "] has been removed.");
                } else {
                    out.write("The combination " + companyName + " [" + accountID + "] does not exist.  No account removed.");
                }
            }
        %>
    </body>
</html>
