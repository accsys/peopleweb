<%-- 
    Document   : add_credits
    Created on : 06 Jan 2015, 2:18:13 PM
    Author     : lterblanche
    Comments   : This routine does not send an updated value of what the credits should be.  
                 It only sends through the number of credits that the person has successfully purchased.  PeopleWeb will update the balance accordingly.
    Parameters : accountID - The internal account number used by Parallels. 
                 module - Payroll / Time Module
                 nbCredits - ... duh
                 We will use this same number (numeric only) as the primary key for all entities stored in the client’s database.

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String accountID = request.getParameter("accountID");
            String module = request.getParameter("module");
            String nbCredits = request.getParameter("nbCredits");
            if (accountID == null || nbCredits == null || module == null) {
                out.write("Missing parameters");
            } else {
                // Is the number of credits a valid integer?
                try {
                    int creditCnt = Integer.parseInt(nbCredits);

                    if (Integer.parseInt(nbCredits) > 0) {
                        za.co.accsys.web.controller.WebServiceClient client = za.co.accsys.web.controller.WebServiceClient.getInstance();

                        // Credit Balance
                        int creditVal = client.GLOBAL_addCredits(accountID, module, creditCnt);
                        out.write("Added " + creditVal + " " + module + " credit(s) to account " + accountID);
                    } else {
                        out.write("Credit amount must be a positive value");
                    }
                } catch (NumberFormatException e) {
                    out.write("Invalid credit amount");
                }
            }
        %>
    </body>
</html>
