-----------------------------------------------------------
-- Automatically make the Administrator the Contact person
-----------------------------------------------------------
if exists (select * from sysevent where upper(event_name) = upper('evt_http_autoCompanyContact')) then
      drop event "DBA"."evt_http_autoCompanyContact";
end if;

 CREATE EVENT "DBA"."evt_http_autoCompanyContact"
          SCHEDULE "sch_http_autoCompanyContact" BETWEEN '00:01' AND '23:59' EVERY 10 MINUTES
 HANDLER
 BEGIN 
   -- Check, on a semi-regular basis, if the C_CONTACT information is the same as that of the system administrator in PeopleWeb
   if exists (select * from a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
     if exists(select * from dba.e_master e join dba.al_emaster_agroup al join e_contact c
        where fn_E_EmployeeActiveOnDate_YN(e.company_id, e.employee_id, now(*))='Y') then    
        -- Make sure we're using the Admin as Contact person                    
        delete from c_contact;
        insert into c_contact (company_id, contact_id, name, tel_number, e_mail) (
             select first e.company_id, 1, e.FIRSTNAME||' '||e.SURNAME, c.tel_work, c.e_mail from dba.e_master e join dba.al_emaster_agroup al join e_contact c order by e.employee_id)

     end if;
    end if;
 END;
 COMMENT ON EVENT "DBA"."evt_http_autoCompanyContact" IS 'Makes the PeopleWeb administrator the contact person for the company.';

