------------------------------------------------------------------------------------
-- Title Lookup Table
------------------------------------------------------------------------------------
if not exists(select * from systable where table_name='WEB_LOOKUP_TITLE')
then
  create table dba.WEB_LOOKUP_TITLE (
    TITLE_ID numeric(10),
    Title char(50) not null,
    primary key (TITLE_ID));
  insert into WEB_LOOKUP_TITLE values (1, 'Mr');
  insert into WEB_LOOKUP_TITLE values (2, 'Ms');
  insert into WEB_LOOKUP_TITLE values (3, 'Mrs');
  insert into WEB_LOOKUP_TITLE values (4, 'Dr');
  insert into WEB_LOOKUP_TITLE values (5, 'Prof');
end if;
commit;