if exists (select * from sysprocedure where upper(proc_name) = upper('fn_HTTP_CanCloseTaxYear')) then
  drop procedure DBA.fn_HTTP_CanCloseTaxYear;
end if;

create function
"DBA"."fn_HTTP_CanCloseTaxYear" ( IN @sessionid long VARCHAR, IN @companyID long varchar, IN @payrollID long varchar )
returns varchar(250)
/*
<purpose>
fn_HTTP_CanCloseTaxYear is called by the Payroll Manager to check that all 
requirements have been met before performing a tax-year-end process, that is:
1. No other payroll may be busy closing the tax year
2. All companies linked to the payroll must be closed in the last period
3. No payroll calculation may be in progress
</purpose>
<history>
Date          CR#     Programmer      Changes
----          ---     ----------      -------
2015/03/09	 14693    Francois        Convert to PeopleWeb Script from fn_P_CanCloseTaxYear
</history>
*/
BEGIN
  declare @ResultMessage varchar(250);
  declare illegal_cursor_operation exception for sqlstate value '09W02';
  declare err_notfound exception for sqlstate value '02000'; // When to leave the cursor loop
  declare @Counter integer; 
  declare @Company_Id numeric(10); 
  declare @ClosingPayroll_Id numeric(10); 
  declare @LastPeriod_ID numeric(10);
  declare @CurrentPeriod_ID numeric(10);
  declare @Company_Name char(100);
  declare @Payroll_Name char(50);
  declare CompanyCursor no scroll cursor for 
    select distinct COMPANY_ID from PL_PAYDEF_EMASTER with (readuncommitted) where PAYROLL_ID = @payrollID;
  declare PayrollCursor no scroll cursor for 
    select distinct PAYROLL_ID from P_CLOSEYEAR_STATUS with (readuncommitted) where PAYROLL_ID <> @payrollID and (PAYROLL_ID <> 0);
  set @Counter = 0;
  
  -- If this session doesn't exist, return empty-handed
  if not exists(select * from WEB_SESSION where session_id=@sessionid) then
      return 'NO SESSION';
  end if;

  -- Touch this session
  call fn_HTTP_TouchSession(@sessionid);
	
  --
  -- 1. Payrolls in the process of closing the tax year.
  --
  set @ResultMessage = 'The following payrolls are currently busy closing their tax years:\x0a';
  open PayrollCursor; 
    -- Loop over the rows of the query
    Payroll_LOOP: loop
      --        
      fetch next PayrollCursor into @ClosingPayroll_Id;
      if sqlstate = err_notfound then
        leave Payroll_LOOP
      end if;
      set @Counter = @Counter + 1; 
      select Name into @Payroll_Name from P_PAYROLL with (readuncommitted) where Payroll_Id = @ClosingPayroll_Id;
      set @ResultMessage = @ResultMessage || '\x0a' ||@Payroll_Name;
    end loop Payroll_LOOP;
  -- Close the cursor
  close PayrollCursor;
  if @Counter > 0 then
    set @ResultMessage = @ResultMessage || '\x0a\x0aPlease wait for them to complete before closing this payroll.';
    return @ResultMessage
  end if;
  --
  -- 2. Outstanding Companies
  --
  select first Payrolldetail_ID into @LastPeriod_ID from P_Payrolldetail with (readuncommitted) where Payroll_Id = @payrollID
    order by EndDate desc; 
  set @ResultMessage = 'Outstanding Companies Warning: The following companies are not in the last period yet:';
  open CompanyCursor; 
    -- Loop over the rows of the query
    Company_LOOP: loop
      --        
      fetch next CompanyCursor into @Company_Id;
      if sqlstate = err_notfound then
        leave Company_LOOP
      end if;
      select max(Payrolldetail_Id) into @CurrentPeriod_ID from P_Calc_Master with (readuncommitted) where Company_Id = @Company_Id and 
        Payroll_Id = @payrollID;
      if @CurrentPeriod_ID < @LastPeriod_ID then
        -- Check that the company has employees who were active during the current tax year
        if (select count(*) from PL_Paydef_EMaster where company_id = @Company_Id and payroll_id = @payrollID and
          fn_P_EmployeeStillEngaged_YN(@Company_Id,PL_Paydef_EMaster.EMPLOYEE_ID,@payrollID,@CurrentPeriod_ID,
            fn_P_GetOpenRunType(@Company_Id,@payrollID,@CurrentPeriod_ID)) = 'Y') > 0 then
          set @Counter = @Counter + 1; 
          select Name into @Company_Name from C_Master with (readuncommitted) where Company_Id = @Company_Id;
          set @ResultMessage = @ResultMessage || '\x0a' ||@Company_Name;
        end if;
      end if;
    end loop Company_LOOP;
  -- Close the cursor
  close CompanyCursor;
  if @Counter > 0 then
    return @ResultMessage
  end if;
  --
  -- 3. Open Companies
  --
  set @ResultMessage = 'Open Companies Warning: The following companies are still open in the last period:';
  open CompanyCursor; 
    -- Loop over the rows of the query
    Company_LOOP: loop
      --        
      fetch next CompanyCursor into @Company_Id;
      if sqlstate = err_notfound then
        leave Company_LOOP
      end if;
      if (Select Run_Status from P_Calc_Master with (readuncommitted) where Company_Id = @Company_Id and 
        Payroll_Id = @payrollID and Payrolldetail_Id = @LastPeriod_ID) <> 3  then
        -- Check that the company has employees who were active during the current tax year
        if (select count(*) from PL_Paydef_EMaster where company_id = @Company_Id and payroll_id = @payrollID and
          fn_E_EmployeeActiveInRange_YN(@Company_Id,PL_Paydef_EMaster.EMPLOYEE_ID,
            (select Tax_Year_Start from P_Payroll where payroll_id = @payrollID),
            (select Tax_Year_End from P_Payroll where payroll_id = @payrollID)) = 'Y') > 0 then
          set @Counter = @Counter + 1; 
          select Name into @Company_Name from C_Master with (readuncommitted) where Company_Id = @Company_Id;
          set @ResultMessage = @ResultMessage || '\x0a' ||@Company_Name;
        end if;
      end if;
    end loop Company_LOOP;
  -- Close the cursor
  close CompanyCursor;
  
  if @Counter > 0 then
    return @ResultMessage
  end if;
  
  if exists (select * from p_calc_queue with (readuncommitted) ) then
    set @ResultMessage = 'Payrolls Calculating Warning: \x0a The tax year cannot be closed while a calculation is in progress';
    return @ResultMessage
  end if;
  
  -- CR 12388 - don't start closing in the can-close call... do it in fn_P_RollOverTaxYear call...
  -- insert into P_CLOSEYEAR_STATUS (Payroll_Id, Status) values (@payrollID,'CLOSING');
  
  set @ResultMessage = 'OK';
    
  return (@ResultMessage)  
END;

commit;
