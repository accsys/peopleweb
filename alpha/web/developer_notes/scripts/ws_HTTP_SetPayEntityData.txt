----------------------------------------------
-- WEB SERVICE TO UPDATE PAY ENTITIES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetPayEntityData') then
  drop service ws_HTTP_SetPayEntityData
end if;

create service ws_HTTP_SetPayEntityData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10), fn_HTTP_SetPayEntityData(:sessionID,:entityID,:entityType,:name,:caption,:valueUnit,:showOnPayslip,:showYtdOnPayslip,:calculationFormula,:payrollType, :interest,:taxCode,:doDelete)) as entityID;

comment on service ws_HTTP_SetPayEntityData is 'Updates pay entity (loans/savings/variables) information';

commit;