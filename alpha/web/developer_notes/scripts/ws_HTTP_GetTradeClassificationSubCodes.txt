----------------------------------------------
-- WEB SERVICE TO FETCH Trade Classification Sub Codes
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetTradeClassificationSubCodes') then
  drop service ws_HTTP_GetTradeClassificationSubCodes
end if;
create service ws_HTTP_GetTradeClassificationSubCodes
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select distinct "TRADE_SUB_CODE", "TRADE_SUB_CLASSIFICATION",
  "fn_HTTP_TouchSession"(:sessionid) as "SESSION_TOUCH"
  from "A_TRADE_SUB_CLASSIFICATION" order by "TRADE_SUB_CODE" asc;
    
comment on service ws_HTTP_GetTradeClassificationSubCodes is 'Returns Trade Classification Sub Codes';
commit;