----------------------------------------------------------------------
-- WEB SERVICE TO CALCULATE/ESTIMATE THE MONTHLY CREDIT UTILISATION --
----------------------------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetCreditUsageEstimate') then
  drop service ws_HTTP_GetCreditUsageEstimate
end if;
create service ws_HTTP_GetCreditUsageEstimate
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),count(*)) as Payroll, '0' as TimeModule
    from dba.pl_paydef_emaster pl join dba.p_payrolldetail pd on pl.payroll_id=pd.payroll_id 
    where dba.fn_P_EmployeeStillActive_YN(company_id, employee_id, pd.payroll_id, dba.fn_P_GetOpenPeriod(pl.company_id, pl.payroll_id))='Y'
    and pd.monthend = (select monthend from p_payrolldetail where payroll_id=pd.payroll_id and payrolldetail_id=dba.fn_P_GetOpenPeriod(pl.company_id, pl.payroll_id));

comment on service ws_HTTP_GetCreditUsageEstimate is 'Returns an estimate of the number of credits that will be used in this month';

commit;
