// requires: "sp_P_TC_SoutAfrica_WriteEeWarning.txt","sp_P_TC_SoutAfrica_WriteEeRecord.txt","sp_P_TC_SoutAfrica_WriteEeRejection.txt","fn_S_RemoveNonAlphaNumericFromString.txt"

create or replace function "DBA"."fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10),in @TYPE_ID numeric(10),in @TaxYearEnd date,in @NOP char(1),in @RECONCILIATION_YEAR numeric(4),in @RECONCILIATION_MONTH numeric(2),in @FILE_ID numeric(10))
returns bit
/*  
<purpose>  
fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues  is called by the South African
tax certificate functions to retrieve the File code values for the codes 
3144 to 3150 for the employee and save it in P_E_TAXCERTIFICATE_Employee_Record.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2009/09/07  6021    Christo Krause  Move IRP5 creation to stored procedure
2011/07/19  12568   Christo Krause  Remove special characters from the tax certificate
2013/08/21  13745   Christo Krause  Prevent right truncation errors
2015/07/08  14705   Francois        PeopleWeb Tax Certificates need to use company work address
</history>
*/
begin
  declare @AddressSaved bit;
  declare @Mandatory bit;
  declare @UnitNumber char(50);
  declare @Complex char(50);
  declare @StreetNumber char(50);
  declare @StreetOrFarm char(50);
  declare @SuburbOrDistrict char(50);
  declare @City char(50);
  declare @PostalCode char(10);
  declare @ValidString bit;
  -- Employee Address
if not exists (select * from DBA.a_setup where ITEM='PeopleWebDB' and DATA='Yes') then
  select isnull(trim(PHYS_WORK_UNITNO),''),isnull(trim(PHYS_WORK_COMPLEX),''),isnull(trim(PHYS_WORK_STREETNO),''),isnull(trim(PHYS_WORK_STREET),''),
    isnull(trim(PHYS_WORK_SUBURB),''),isnull(trim(PHYS_WORK_CITY),''),isnull(trim(PHYS_WORK_CODE),'') into @UnitNumber,
    @Complex,@StreetNumber,@StreetOrFarm,@SuburbOrDistrict,@City,@PostalCode from 
    E_ADDRESS where COMPANY_ID= @COMPANY_ID and EMPLOYEE_ID = @EMPLOYEE_ID;
else
  select isnull(trim(PHYSICAL_UNITNO),''),isnull(trim(PHYSICAL_COMPLEX),''),isnull(trim(PHYSICAL_STREETNO),''),isnull(trim(PHYSICAL_STREET),''),
    isnull(trim(PHYSICAL_SUBURB),''),isnull(trim(PHYSICAL_CITY),''),isnull(trim(PHYSICAL_CODE),'') into @UnitNumber,
    @Complex,@StreetNumber,@StreetOrFarm,@SuburbOrDistrict,@City,@PostalCode from 
    C_ADDRESS where COMPANY_ID= @COMPANY_ID;
end if;


  -- Mandatory 
  if length(@StreetOrFarm) = 0 or length(@PostalCode) = 0 or (length(@SuburbOrDistrict) = 0 and length(@City) = 0) then
    if @NOP <> 'N' then
      if  year(@TaxYearEnd) < '2011' then
        call sp_P_TC_SouthAfrica_WriteEeWarning(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3147,'Employee business address is not complete.')
      else
        if length(@StreetOrFarm) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3147,'Business address Street or Name of Farm is mandatory.')
        end if;
        if length(@SuburbOrDistrict) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3148,'Business address Suburb/District or City/Town is mandatory.')
        end if;
        if length(@City) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3149,'Business address Suburb/District or City/Town is mandatory.')
        end if;
        if length(@PostalCode) = 0 then
          call sp_P_TC_SouthAfrica_WriteEeRejection(@File_Id,@COMPANY_ID,@EMPLOYEE_ID,@PAYROLL_ID,3150,'Business address Postal Code is mandatory.')
        end if;
      end if;
    end if;
    set @AddressSaved = 0;
  else
    -- 3144 Unit Number (Optional)
    set @UnitNumber = fn_S_RemoveNonAlphaNumericFromString(@UnitNumber);
    if length(@UnitNumber) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3144,@UnitNumber,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3145 Complex (Optional)
    if length(@Complex) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3145,@Complex,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3146 Street Number (Optional)
    set @StreetNumber = fn_S_RemoveNonAlphaNumericFromString(@StreetNumber);
    if length(@StreetNumber) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3146,@StreetNumber,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3147 Street or Farm name (Mandatory)
    if length(@StreetOrFarm) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3147,@StreetOrFarm,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3148 Suburb or District (Conditional)
    if length(@SuburbOrDistrict) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3148,@SuburbOrDistrict,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3149 Suburb or District (Conditional)
    if length(@City) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3149,@City,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    -- 3150 Postal Code (Mandatory)
    set @PostalCode = fn_S_RemoveNonAlphaNumericFromString(@PostalCode);
    if length(@PostalCode) > 0 then
      call sp_P_TC_SouthAfrica_WriteEeRecord(@FILE_ID,3150,@PostalCode,@RECONCILIATION_YEAR,@RECONCILIATION_MONTH,1);
    end if;
    set @AddressSaved = 1;
  end if;
  return(@AddressSaved);
end;

commit;

comment on procedure fn_P_TC_SouthAfrica_ListEEWorkAddressFileCodeValues is "<h2>Purpose</h2><br/>Compiles the employee work address data for the South African Tax Certificates.";
