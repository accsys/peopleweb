----------------------------------------------
-- FUNCTION TO UPDATE COMPANY INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCompanyData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @name long varchar default null,IN @registrationNo long varchar default null,
        IN @taxNo long varchar default null,IN @phone long varchar default null,  IN @fax long varchar default null,IN @email long varchar default null,
        IN @diplomaticIndemnity char(1) default null,IN @pensionNo long varchar default null,IN @postalCountry long varchar default null,
        IN @postalProvince long varchar default null,IN @postalAddress long varchar default null,IN @postalSuburb long varchar default null,
        IN @postalCity long varchar default null, IN @postalCode long varchar default null, IN @physUnitNo long varchar default null,
        IN @physComplex long varchar default null,IN @physStreetNo long varchar default null, IN @physStreet long varchar default null,IN @physSuburb long varchar default null,
        IN @physCity long varchar default null,IN @physCode long varchar default null, IN @physProvince long varchar default null,IN @physCountry long varchar default null,
        IN @postalSameAsPhysical char(1) default null,IN @mainBusinessActivity long varchar default null,IN @tradeCode long varchar default null,
        IN @tradeSubCode long varchar default null, IN @setaCode long varchar default null,IN @sicCode long varchar default null)
returns varchar(10)
BEGIN

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);
    message('About to update for company '||@name);

    -- name
    if (@name is not null) then
        update c_master set name=@name  where company_id=@companyID;
    end if;
    -- registrationNo
    if (@registrationNo is not null) then
        update c_master set COMPANY_REGISTRATION=@registrationNo  where company_id=@companyID;
    end if;
    -- taxNo
    if (@taxNo is not null) then
        update c_master set TAX_NUMBER=@taxNo where company_id=@companyID;
    end if;
    -- phone
    if (@phone is not null) then
        update c_master set TEL=@phone where company_id=@companyID;
    end if;
    -- fax
    if (@fax is not null) then
        update c_master set FAX=@fax where company_id=@companyID;
    end if;
    -- email
    if (@email is not null) then
        update c_master set EMAIL=@email where company_id=@companyID;
    end if;
    -- diplomaticIndemnity
    if (@diplomaticIndemnity is not null) then
        if (@diplomaticIndemnity='1')then
          update c_master set DIPL_INDEMNITY='Y' where company_id=@companyID
        else
          update c_master set DIPL_INDEMNITY='N' where company_id=@companyID
        end if;
    end if;
    -- pensionNo
    if (@pensionNo is not null) then
        update c_master set PENSION_NUMBER=@pensionNo where company_id=@companyID;
    end if;
    -- postalCountry
    if (@postalCountry is not null) then
        insert into c_address (COMPANY_ID, POSTAL_COUNTRY) on existing update values(@companyID, @postalCountry);
    end if;
    -- postalProvince
    if (@postalProvince is not null) then
        insert into c_address (COMPANY_ID, POSTAL_PROVINCE) on existing update values(@companyID, @postalProvince);
    end if;
    -- postalAddress
    if (@postalAddress is not null) then
        insert into c_address (COMPANY_ID, POSTAL_ADDRESS) on existing update values(@companyID, @postalAddress);
    end if;
    -- postalSuburb
    if (@postalSuburb is not null) then
        insert into c_address (COMPANY_ID, POSTAL_SUBURB) on existing update values(@companyID, @postalSuburb);
    end if;
    -- postalCity
    if (@postalCity is not null) then
        insert into c_address (COMPANY_ID, POSTAL_CITY) on existing update values(@companyID, @postalCity);
    end if;
    -- postalCode
    if (@postalCode is not null) then
        insert into c_address (COMPANY_ID, POSTAL_CODE) on existing update values(@companyID, @postalCode);
    end if;
    -- physUnitNo
    if (@physUnitNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_UNITNO) on existing update values(@companyID, @physUnitNo);
    end if;
    -- physComplex
    if (@physComplex is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_COMPLEX) on existing update values(@companyID, @physComplex);
    end if;
    -- physStreetNo
    if (@physStreetNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_STREETNO) on existing update values(@companyID, @physStreetNo);
    end if;
    -- physStreet
    if (@physStreet is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_STREET) on existing update values(@companyID, @physStreet);
    end if;
    -- physSuburb
    if (@physSuburb is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_SUBURB) on existing update values(@companyID, @physSuburb);
    end if;
    -- physCity
    if (@physCity is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_CITY) on existing update values(@companyID, @physCity);
    end if;
    -- physCode
    if (@physCode is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_CODE) on existing update values(@companyID, @physCode);
    end if;
    -- physProvince
    if (@physProvince is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_PROVINCE) on existing update values(@companyID, @physProvince);
    end if;
    -- physCountry
    if (@registrationNo is not null) then
        insert into c_address (COMPANY_ID, PHYSICAL_COUNTRY) on existing update values(@companyID, @physCountry);
    end if;
    -- postalSameAsPhysical
    if (@postalSameAsPhysical is not null) then
       insert into c_address (COMPANY_ID, POSTAL_SAME_AS_PHYSICAL) on existing update values(@companyID, @postalSameAsPhysical);
    end if;
    -- mainBusinessActivity
    if (@mainBusinessActivity is not null) then
        update c_master set MAIN_BUSINESS_ACTIVITY=@mainBusinessActivity  where company_id=@companyID;
    end if;
    -- tradeCode
    if (@tradeCode is not null) then
        update c_master set TRADE_CLASS_ID=(select TRADE_CLASS_ID from A_TRADE_CLASSIFICATION where TRADE_CODE=@tradeCode )  where company_id=@companyID;
    end if;
    -- tradeSubCode
    if (@tradeSubCode is not null) then
        update c_master set TRADE_SUB_CODE=@tradeSubCode where company_id=@companyID;
    end if;
    -- setaCode
    if (@setaCode is not null) then
        update c_master set JOBSECTOR_ID=(select JOBSECTOR_ID from J_JOBSECTOR where SETA=@setaCode )  where company_id=@companyID;
    end if;
    -- sicCode
    if (@sicCode is not null) then
        update c_master set SIC_CODE_ID=(select SIC_CODE_ID from J_SIC_CODE where SIC_CODE=@sicCode )  where company_id=@companyID;
    end if;

    commit;
    return('OK'); 
END;

commit;
