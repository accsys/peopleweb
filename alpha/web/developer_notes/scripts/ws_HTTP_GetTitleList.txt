----------------------------------------------
-- WEB SERVICE TO FETCH TITLES
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetTitleList') then
  drop service ws_HTTP_GetTitleList
end if;

create service ws_HTTP_GetTitleList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),title_id) as titleID,  Title title,
    fn_HTTP_TouchSession(:sessionid) as SESSION_TOUCH 
    from 
        WEB_LOOKUP_TITLE 
    order by title;

comment on service ws_HTTP_GetTitleList is 'Returns a list of titles stored in WEB_LOOKUP_TITLE';

commit;