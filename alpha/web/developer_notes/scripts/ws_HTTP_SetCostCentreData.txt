----------------------------------------------
-- WEB SERVICE TO UPDATE COST CENTER INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetCostCentreData') then
  drop service ws_HTTP_SetCostCentreData
end if;

create service ws_HTTP_SetCostCentreData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetCostCentreData(:sessionID,:companyID,:costID,:name,:description,:number,:parentCostID,:doDelete) as RESULT;

comment on service ws_HTTP_SetCostCentreData is 'Updates cost centre information';

commit;