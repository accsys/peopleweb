----------------------------------------------
-- WEB SERVICE TO FETCH UIF EXTRACT FOR CSV
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayrollUifExtract') then
  drop service ws_HTTP_GetPayrollUifExtract
end if;

create service ws_HTTP_GetPayrollUifExtract
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
     select V.Employee_Record 
     from
       (select 
	      replace(Header_Record,'"','~') as Employee_Record
	      from vw_P_SouthAfrica_UIFextractHeaderAndFooter vw 
          where  vw.company_id=:companyid and vw.payroll_id=:payrollid
        union
        select 
	      replace(Employee_Record,'"','~') as Employee_Record
	      from vw_P_SouthAfrica_UIFextract vw 
          where vw.company_id=:companyid and vw.payroll_id=:payrollid and exists(select * from web_session where session_id=:sessionid)
        union
        select 
	      replace(Footer_Record,'"','~') as Employee_Record
	      from vw_P_SouthAfrica_UIFextractHeaderAndFooter vw
          where vw.company_id=:companyid and vw.payroll_id=:payrollid) 
    as V
    order by V.Employee_Record;

comment on service ws_HTTP_GetPayrollUifExtract is 'Returns the list of string values for the UIF extract for the given companyID and payrollID';

commit;

