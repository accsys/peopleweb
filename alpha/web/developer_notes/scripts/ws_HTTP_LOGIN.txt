-------------------------------------
-- WEB SERVICE USED TO LOG IN
-------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_LOGIN') then
  drop service ws_HTTP_LOGIN
end if;

create service ws_HTTP_LOGIN
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS select fn_HTTP_LOGIN ( :web_login, :web_pwd, :sessionid ) as SESSION_ID;
comment on service ws_HTTP_LOGIN is 'Creates a login and establishes a session ID';