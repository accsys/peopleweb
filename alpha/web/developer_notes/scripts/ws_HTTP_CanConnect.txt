---------------------------------------------------
-- WEB SERVICE TO CHECK IF WE CAN CONNECT
---------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_CanConnect') then
  drop service ws_HTTP_CanConnect
end if;
create service ws_HTTP_CanConnect
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select  property('starttime') StartTime, convert(char(10),datediff(hour, StartTime, now())) HrsRunning,
    (select first version_nr from s_db_ver order by date_of_version desc) PeopleWareVersion;

comment on service ws_HTTP_CanConnect is 'Can we connect to this database?';

commit;


