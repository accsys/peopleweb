----------------------------------------------
-- WEB SERVICE TO FETCH LOANS/SAVINGS/VARIABLES LIST 
-- Rules:
--   Variables: Active
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayEntityList') then
  drop service ws_HTTP_GetPayEntityList
end if;


create service ws_HTTP_GetPayEntityList
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    -- Earnings --
    select 
        convert(char(10),variable_id)  entityID, 'entityType_Earning' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code)  taxCode, s.description taxCodeCaption, c.name taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'') as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p join p_sars_code s on p.sars_code=s.sars_code join p_country c on s.country_id=c.country_id
    where 
        active_yn='Y' and p.result_type='EARNING' and COST_DETAIL='YY' and p.sars_code >1 
        and exists(select * from web_session where session_id=:sessionID)
    -- Package Component (Company Contribution - with tax codes) --
    union all
    select 
        convert(char(10),variable_id)  entityID, 'entityType_PackageComponent' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code)   taxCode, s.description taxCodeCaption, c.name taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p join p_sars_code s on p.sars_code=s.sars_code join p_country c on s.country_id=c.country_id
    where 
        active_yn='Y' and p.result_type='EARNING' and COST_DETAIL='NY' and p.sars_code >1  
        and exists(select * from web_session where session_id=:sessionID)
    -- Package Component (Company Contribution - without tax codes) --
    union all
    select 
        convert(char(10),variable_id)  entityID, 'entityType_PackageComponent' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code)   taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p 
    where 
        (sars_code is null or sars_code='') and
        active_yn='Y' and p.result_type='EARNING' and COST_DETAIL='NY' 
        and exists(select * from web_session where session_id=:sessionID)
    -- Deductions (with tax codes)--
    union all
    select 
        convert(char(10),variable_id)  entityID, 'entityType_Deduction' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code)   taxCode, s.description taxCodeCaption, c.name taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p join p_sars_code s on p.sars_code=s.sars_code join p_country c on s.country_id=c.country_id
    where 
        active_yn='Y' and p.result_type='DEDUCTION' and COST_DETAIL='YY' 
        and exists(select * from web_session where session_id=:sessionID)
    -- Deductions (without tax codes)--
    union all
    
    select 
        convert(char(10),variable_id)  entityID, 'entityType_Deduction' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),p.sars_code) taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
     from 
        p_variable p 
    where 
        (sars_code is null or sars_code='') and
        active_yn='Y' and p.result_type='DEDUCTION' and COST_DETAIL='YY' 
        and exists(select * from web_session where session_id=:sessionID)
    -- Interims --
    union all
    select 
        convert(char(10),variable_id)  entityID, 'entityType_Interim' entityType, p.NAME name, p.DESCRIPTION caption, 
        value_unit unit, DISPLAYONPAYSLIP_YN showOnPayslip, DISPLAYYTD_YN showYtdOnPayslip, 
        sp_P_CALCExtractFormulaFromVariable(theValue) calculationFormula, 
        '0' interest, convert(char(10),'')   taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from P_variables_for_employee where variable_id = p.variable_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        (case locate(coalesce(theOptions,''),'system') when 0 then '0' else '1' end) as sysOption,
        sp_P_CALCExtractFormulaFromVariable(theValue) as theFormula,
        PAYROLL_TYPE as payrollType, PAYPER_TYPE as payPerType, coalesce(IND_CODE,'')  as indicatorCodes,
        fn_P_CanContributeToRFI(entityID) as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variable p 
    where 
        active_yn='Y' and p.cost_detail='NN' 
        and exists(select * from web_session where session_id=:sessionID)
    -- Loans --
    union all
    select 
        convert(char(10),loan_id)  entityID, 'entityType_Loan' entityType, p.NAME name, p.DESCRIPTION caption, 
        '' unit, 'Y' showOnPayslip, 'N' showYtdOnPayslip, 
         coalesce(premium_formula,'INPUT') as calculationFormula, 
        convert(char(10),interest) interest, convert(char(10),'')   taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from Pl_emaster_loan where loan_id = p.loan_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        '0'  as sysOption,
        sp_P_CALCExtractFormulaFromVariable(premium_formula) as theFormula,
        'All' as payrollType, 'All' as payPerType, '' as indicatorCodes,
        'NO' as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_loan p 
    where exists(select * from web_session where session_id=:sessionID)
    -- Savings --
    union all
    select 
        convert(char(10),saving_id)  entityID, 'entityType_Saving' entityType, p.NAME name, p.DESCRIPTION caption, 
        '' unit, 'Y' showOnPayslip, 'N' showYtdOnPayslip, 
        coalesce(premium_formula,'INPUT') as calculationFormula, 
        convert(char(10),interest) interest, convert(char(10),'') taxCode, '' taxCodeCaption, '' taxCountry,
	    convert(char(10),(select count() from Pl_emaster_saving where saving_id = p.saving_id)) as nbUsedPeriods,
	    (case nbUsedPeriods when 0 then '1' else '0' end) as canDelete,
        '0'  as sysOption,
        sp_P_CALCExtractFormulaFromVariable(premium_formula) as theFormula,
        'All' as payrollType, 'All' as payPerType, '' as indicatorCodes,
        'NO' as canBeRFI,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_saving p
    where exists(select * from web_session where session_id=:sessionID)
    order by taxCode, caption;

comment on service ws_HTTP_GetPayEntityList is 'Returns a list of all the earnings, deductions, loans, savings, etc';
