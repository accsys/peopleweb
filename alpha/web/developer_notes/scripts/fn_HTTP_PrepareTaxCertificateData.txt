CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_PrepareTaxCertificateData" ( 
        IN @sessionID long VARCHAR, IN @payrollID long varchar, IN @companyID long varchar,
        IN @reconMonth long varchar, IN @reconYear long varchar, IN @tradeCode long varchar)
returns varchar(5000)
BEGIN
    declare @Run_ID numeric(10);
	declare @RSLT varchar(5000);
	
    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Prepare the Data?
	set @Run_ID = fn_P_TC_SouthAfrica_DoTaxCertificate(@companyID,@payrollID,'LIVE',1,@reconYear,@reconMonth,1,null,null,null,null,null,@tradeCode,null);
	set @RSLT = fn_HTTP_GetTaxCertificateWarningsAndErrors(@Run_ID); 
	
    return(@RSLT); 
END;


commit;

