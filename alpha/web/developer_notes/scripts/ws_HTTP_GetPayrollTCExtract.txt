--------------------------------------------------------------
-- WEB SERVICE TO FETCH TAX CERTIFICATE EXTRACT FOR SDF FILE
--------------------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetPayrollTCExtract') then
  drop service ws_HTTP_GetPayrollTCExtract
end if;

create service ws_HTTP_GetPayrollTCExtract
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
     select V.Employee_Record 
     from
       (select 
	      replace(fn_P_TC_SouthAfrica_HeaderOutputRecord(:payrollid,:runid),'"','~') as Employee_Record
        union
        select 
	      replace(fn_P_TC_SouthAfrica_EmployeeOutputRecord(FILE_ID),'"','~') as Employee_Record
	      FROM P_E_TAXCERTIFICATE_EMPLOYEELIST where RUN_ID = :runid
        union
        select 
	      replace(fn_P_TC_SouthAfrica_FooterOutputRecord(:runid),'"','~') as Employee_Record) 
    as V
    order by V.Employee_Record;

comment on service ws_HTTP_GetPayrollTCExtract is 'Returns the list of string values for the Tax Certificate extract for the given runID and payrollID';

commit;

