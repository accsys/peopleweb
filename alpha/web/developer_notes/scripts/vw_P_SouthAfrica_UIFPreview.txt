create or replace view dba.vw_P_SouthAfrica_UIFPreview
  /*  
  <purpose>  
  vw_P_SouthAfrica_UIFPreview supplies data in the format required for 
  South African electronic UIF submission.
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  2015/01/12    14233   Christo Krause  Move the UIF extract to a view
  </history>
  */
  as select vw.Company_Id,vw.Employee_Id,vw.Payroll_id,
  E.Surname ||'('||E.Company_Employee_Number||')' as "Surname (EmpNo)",
  (select sum(UIF_GROSSTAXABLE) from dba.vw_P_UIF 
    where company_id = vw.COMPANY_ID and employee_id = vw.EMPLOYEE_ID 
      and Payroll_Id = vw.PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
          where company_id = vw.COMPANY_ID and Payroll_id = vw.PAYROLL_ID)) as Taxable,
  (select lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)) from dba.vw_P_UIF 
    where company_id = vw.COMPANY_ID and employee_id = vw.EMPLOYEE_ID 
      and Payroll_Id = vw.PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
          where company_id = vw.COMPANY_ID and Payroll_id = vw.PAYROLL_ID)) as "UIF Remuneration",
  (select sum(UIF_ER) from dba.vw_P_UIF 
    where company_id = vw.COMPANY_ID and employee_id = vw.EMPLOYEE_ID 
      and Payroll_Id = vw.PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
          where company_id = vw.COMPANY_ID and Payroll_id = vw.PAYROLL_ID)) as Company,
  (select sum(UIF_EE) from dba.vw_P_UIF 
    where company_id = vw.COMPANY_ID and employee_id = vw.EMPLOYEE_ID 
      and Payroll_Id = vw.PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList 
          where company_id = vw.COMPANY_ID and Payroll_id = vw.PAYROLL_ID)) as Employee,
  Company + Employee as Total,

    dba.fn_P_UIF_SouthAfrica_EmployeeWarnings(vw.Company_Id,vw.Employee_Id,vw.Payroll_id) as Employee_Warnings
  from dba.vw_P_OpenMonthEmployeeList as vw join dba.E_Master as E
    on E.company_id = vw.COMPANY_ID and E.employee_id = vw.EMPLOYEE_ID;

comment on view dba.vw_P_SouthAfrica_UIFPreview is "<h2>Purpose</h2><br/>Supplies a preview with warnings for the UIF electronic file output data of all employees calculated or skipped in the current open month per company payroll combination.";

