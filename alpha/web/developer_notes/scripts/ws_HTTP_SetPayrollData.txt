----------------------------------------------
-- WEB SERVICE TO UPDATE COST CENTER INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_SetPayrollData') then
  drop service ws_HTTP_SetPayrollData
end if;

create service ws_HTTP_SetPayrollData
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select fn_HTTP_SetPayrollData(:sessionID,:payrollID,:payrollName,:payrollType,:taxYearStart,:taxYearEnd,:openPeriod, :taxCountry,
								  :doDelete,:taxNumber,:tradeClassificationCode,:tradeClassificationSubCode,:sicCode) as RESULT;

comment on service ws_HTTP_SetPayrollData is 'Updates payroll information';

commit;