----------------------------------------------
-- WEB SERVICE TO FETCH EMPLOYEE YTD PACKAGE INFORMATION
----------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetYTDEmployeePackage') then
  drop service ws_HTTP_GetYTDEmployeePackage
end if;

create service ws_HTTP_GetYTDEmployeePackage
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 

    -- Variables --
    select 
        convert(char(10),pl.variable_id)  entityID, 'entityType_GenericVariable' entityType, 
	    convert(char(10),sum(last_calc_value))  as ytdValue,
        convert(char(10),count(*)) as nbCalculations,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_variables_for_employee pl join p_variable p
    where 
        pl.company_id=:companyID and pl.employee_id=:employeeID and pl.payroll_id=:payrollID
        and p.DISPLAYYTD_YN='Y'
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType
    -- Loans --
    union all
    select 
        convert(char(10),pl.loan_id)  entityID, 'entityType_Loan' entityType, 
	    convert(char(10),sum(ammount_deducted)) as ytdValue, 
	    convert(char(10),count(*)) as nbCalculations,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_loan_log l join PL_EMASTER_LOAN pl
    where 
        pl.company_id=:companyID and pl.employee_id=:employeeID and pl.payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType
    -- Savings --
    union all
    select 
        convert(char(10),pl.saving_id)  entityID, 'entityType_Saving' entityType, 
	    convert(char(10),sum(ammount_saved)) as ytdValue, 
	    convert(char(10),count(*)) as nbCalculations,
        fn_HTTP_TouchSession(:sessionID) as SESSION_TOUCH 
    from 
        p_saving_log l join PL_EMASTER_SAVING pl
    where 
        pl.company_id=:companyID and pl.employee_id=:employeeID and pl.payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType
    -- RFI --
    union all
    select 
        '-10' as entityID, 'entityType_GenericVariable' entityType, 
	    convert(char(10),sum(RFI))  as ytdValue,
        convert(char(10),count(RFI)) as nbCalculations,
        fn_HTTP_TouchSession('5cbf4d98-ef3b-4ef4-8b34-205c13456351') as SESSION_TOUCH 
    from 
        vw_P_RFI_NonRFI
    where 
        company_id=:companyID and employee_id=:employeeID and payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType
    -- Non-RFI --
    union all
    select 
        '-20' as entityID, 'entityType_GenericVariable' entityType, 
	    convert(char(10),sum("non-RFI"))  as ytdValue,
        convert(char(10),count("non-RFI")) as nbCalculations,
        fn_HTTP_TouchSession('5cbf4d98-ef3b-4ef4-8b34-205c13456351') as SESSION_TOUCH 
    from 
        vw_P_RFI_NonRFI
    where 
        company_id=:companyID and employee_id=:employeeID and payroll_id=:payrollID
        and exists(select * from web_session where session_id=:sessionID)
    group by entityID, entityType

    order by entityType, entityID;

comment on service ws_HTTP_GetYTDEmployeePackage is 'Returns the YearToDate figures for all variables ';

commit;