CREATE OR REPLACE FUNCTION "DBA"."fn_P_UIF_SouthAfrica_EmployeeWarnings"(in @COMPANY_ID numeric(10),in @EMPLOYEE_ID numeric(10),in @PAYROLL_ID numeric(10)) 
returns char(5000)
/*  
<purpose>  
fn_P_UIF_SouthAfrica_EmployeeWarnings is called to create the electronic 
file output record for the employee according to the formatting requirements 
specified by the Department of Labour.
</purpose>  
<history>
Date        CR#     Programmer      Changes
----        ---     ----------      -------
2015/01/12  14233   Christo Krause  Move the UIF extract to a view
</history>
*/
begin
  declare @FileRecord char(5000); 
  declare @MonthEnd date;
  declare @FileCode numeric(5);
  declare @CodeValue char(500);
  declare @EeNumber char(25);
  declare @IDNumber char(20);
  declare @PassportNumber char(20);
  declare @AlternateNumber char(25);
  declare @Surname char(120);
  declare @FirstNames char(90);
  declare @SecondName char(90);
  declare @DateOfBirth char(8);
  declare @EmploymentStart date;
  declare @EmploymentEnd date;
  declare @UIF_EmpStatusCode char(02);
  declare @UIF_ReasonCode char(02);
  declare @GrossTaxable numeric(15,2);
  declare @UIF_Earning numeric(15,2);
  declare @UIF_EE numeric(15,2);
  declare @UIF_ER numeric(15,2);
  declare @Counter integer;
  declare @IsValid bit;
  declare @Debug_On bit;
  set @Debug_On = 0;
  if @Debug_On = 1 then
    message('fn_P_UIF_SouthAfrica_EmployeeWarnings ... ') to console
  end if;
  select max(MonthEnd) into @MonthEnd from dba.vw_P_OpenMonthPeriodList 
    where company_id = @COMPANY_ID and Payroll_id = @PAYROLL_ID;
  -- 8200 ID Number
  select trim(ID_Number) into @IDNumber from E_Number where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  select trim(Passport_Number) into @PassportNumber from E_Passport where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  if length(@IDNumber) > 0 then
    if fn_E_SouthAfrica_IsValidIdNo(@IDNumber) = 0 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; ID number ('|| @IDNumber ||') is not valid.'
      else
        set @FileRecord = 'Warning: ID number ('|| @IDNumber ||') is not valid.'
      end if;
      if length(@PassportNumber) = 0 then
        set @FileRecord = @FileRecord || ' - In the absence of a passport number it will be used in the Other Number field.'
      end if;
    end if;
  end if;
  -- 8210 Other Number
  if length(@PassportNumber) = 0 then
    select trim(Permit_Number) into @PassportNumber from E_Nationality where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  end if;
  -- Personal Information
  select left(trim(Company_Employee_Number),25),left(trim(Surname),120),left(trim(FirstName),90),left(trim(SecondName),90),
    convert(char(8),BirthDate,112) into @AlternateNumber,@Surname,@FirstNames,@SecondName,@DateOfBirth 
    from E_MASTER where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID;
  -- 8220 Alternate Number (Company Employee Number)
  if length(@IDNumber) + length(@PassportNumber) + length(@AlternateNumber) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; No ID number, Other number or Alternate number is present.'
    else
      set @FileRecord = 'Warning: No ID number, Other number or Alternate number is present.'
    end if
  end if;
  -- 8230 Surname
  if length(@Surname) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The surname is omitted.'
    else
      set @FileRecord = 'Warning: The surname is omitted.'
    end if
  end if;
  -- 8240 First Names
  if length(@FirstNames) + length(@SecondName) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The first names are omitted.'
    else
      set @FileRecord = 'Warning: The first names are omitted.'
    end if
  end if;
  -- 8250 Date of Birth
  if length(@DateOfBirth) = 0 then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; The date of birth is omitted.'
    else
      set @FileRecord = 'Warning: The date of birth is omitted.'
    end if
  else
    if datediff(year,@DateOfBirth,today()) < 15 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The employee born on ('||@DateOfBirth||') is younger than 15 years.'
      else
        set @FileRecord = 'Warning: The employee born on ('||@DateOfBirth||') is younger than 15 years.'
      end if
    end if;
  end if;
  -- Employment Dates and Status
  select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
    into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
    from E_ON_OFF E
    where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID and E.START_DATE <= @MonthEnd
    order by E.START_DATE desc;
  -- 8260 Date Employed From
  if length(@EmploymentStart) = 0 then
    select first  E.START_DATE, E.END_DATE,fn_P_GetEmployeeUifStatus(E.COMPANY_ID,E.EMPLOYEE_ID,E.END_DATE,E.REASON_ID) 
      into @EmploymentStart, @EmploymentEnd, @UIF_EmpStatusCode
      from E_ON_OFF E
      where E.COMPANY_ID = @COMPANY_ID and E.EMPLOYEE_ID = @EMPLOYEE_ID
      order by E.START_DATE desc;
    if length(@EmploymentStart) > 0 then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed from ('||@EmploymentStart||') is after the month end date ('|| @MonthEnd ||').'
      else
        set @FileRecord = 'Warning: The date employed from ('||@EmploymentStart||') is after the month end date ('|| @MonthEnd ||').'
      end if
    else
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed from is omitted.'
      else
        set @FileRecord = 'Warning: The date employed from is omitted.'
      end if
    end if;
  end if;
  -- 8270 Date Employed To
  -- 8280 Employment Status Code
  if @UIF_EmpStatusCode = '01' then
    if exists (select * from L_History where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID
      and Leave_Type_Id in (4,9) and Convert_YN = 'N' and Cancel_Date is null
      and Start_Date < @MonthEnd and End_Date >= @MonthEnd) then
      set @UIF_EmpStatusCode = '09';
    end if
  end if;
  if @EmploymentEnd between @EmploymentStart and dateadd(month,2,@MonthEnd) then
    if @UIF_EmpStatusCode in ('01','07','09') then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed to ('|| @EmploymentEnd ||') is valid but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      else
        set @FileRecord = 'Warning: The date employed to ('|| @EmploymentEnd ||') is valid but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      end if
    end if;
  else
    if @UIF_EmpStatusCode not in ('01','07','09') then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The date employed to is omitted but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      else
        set @FileRecord = 'Warning: The date employed to is omitted but the employment status code is ('|| @UIF_EmpStatusCode ||').'
      end if
    end if;
  end if;
  -- Earnings and Contributions
  select sum(UIF_GROSSTAXABLE), lesser(sum(UIF_EARNINGS),max(UIF_Ceiling)), sum(UIF_EE),  sum(UIF_ER) 
    into @GrossTaxable, @UIF_Earning, @UIF_EE, @UIF_ER
    from dba.vw_P_UIF where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID 
      and Payroll_Id = @PAYROLL_ID and Payrolldetail_id in
        (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
          and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd);
  -- 8290 Reason Code for Non-Contribution
  if (@UIF_EE + @UIF_ER) = 0 or @GrossTaxable = 0 then
    select coalesce((select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and UIF_Grosstaxable > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc), 
    (select first NonContribution_ReasonCode from dba.vw_P_UIF 
      where company_id = @COMPANY_ID and employee_id = @EMPLOYEE_ID and Payroll_Id = @PAYROLL_ID 
        and NonContribution_ReasonCode > 0 and Payrolldetail_id in
          (select Payrolldetail_id from dba.vw_P_OpenMonthPeriodList where company_id = @COMPANY_ID 
            and Payroll_id = @PAYROLL_ID and MonthEnd = @MonthEnd)
      order by Payrolldetail_id desc, Runtype_id asc)) into @UIF_ReasonCode;
    if @UIF_ReasonCode is null then
      if (@UIF_EE + @UIF_ER) = 0 then
        if length(@FileRecord) > 0 then
          set @FileRecord = @FileRecord || '; The UIF contribution is zero without a valid reason code.'
        else
          set @FileRecord = 'Warning: The UIF contribution is zero without a valid reason code.'
        end if
      else
        if length(@FileRecord) > 0 then
          set @FileRecord = @FileRecord || '; The gross taxable remuneration is zero without a valid reason code.'
        else
          set @FileRecord = 'Warning: The gross taxable remuneration is zero without a valid reason code.'
        end if
      end if;
    end if;
  end if;
  -- 8310 Remuneration subject to UIF
  if @UIF_ReasonCode is null then
    if convert(numeric(13,0),@UIF_Earning)*0.02 <> (@UIF_EE + @UIF_ER) then
      if length(@FileRecord) > 0 then
        set @FileRecord = @FileRecord || '; The UIF contribution is not 2% of the remuneration subject to UIF.'
      else
        set @FileRecord = 'Warning: The UIF contribution is not 2% of the remuneration subject to UIF.'
      end if
    end if;
  end if;
  -- 8320 UIF Contribution
  if @UIF_EE <> @UIF_ER then
    if length(@FileRecord) > 0 then
      set @FileRecord = @FileRecord || '; Company and Employee Contributions do not match.'
    else
      set @FileRecord = 'Warning: Company and Employee Contributions do not match.'
    end if
  end if;
  if @Debug_On = 1 then
    message(' FileRecord = '||@FileRecord) to console
  end if;
  return(@FileRecord)
end;

commit;

comment on procedure dba.fn_P_UIF_SouthAfrica_EmployeeWarnings is "<h2>Purpose</h2><br/>Prepares the Employee Warning Messages for the preview of the UIF electronic file creation.<br/><h2>Usage</h2>Payroll UIF Extract, South African electronic file.";
