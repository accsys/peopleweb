---------------------------------------------- 
-- FUNCTION TO UPDATE PAY ENTITY INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetPayEntityData" ( 
        IN @sessionID long VARCHAR, IN @entityID long varchar, IN @entityType long varchar default null,
        IN @name long varchar default null,IN @caption long varchar default null, IN @valueUnit long varchar default null, IN @showOnPayslip char(1) default 'Y',
        IN @showYtdOnPayslip char(1) default 'N',
        IN @calculationFormula long varchar default null, 
        IN @payrollType char(20) default null,
        IN @interest long varchar default null, 
        IN @taxCode long varchar default null, 
        IN @doDelete char(1) default 'N')
returns numeric(10)
BEGIN
    declare @newEntityID numeric(10);
    set @newEntityID = -1;


    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- If the user tries to change from one entity to another 
    --  we need to delete the old entity and create a new one
	-- LOAN (already in P_VARIABLE)
	if ((@entityID <> '-1') and (@entityType='entityType_Loan') and (@name is not null) and 
		exists(select * from p_variable where variable_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_variable where variable_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- LOAN (already in P_SAVING)
	if ((@entityID <> '-1') and (@entityType='entityType_Loan') and (@name is not null) and 
		exists(select * from p_saving where saving_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of saving
		delete from p_saving where saving_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- SAVING (already in P_VARIABLE)
	if ((@entityID <> '-1') and (@entityType='entityType_Saving') and (@name is not null) and 
		exists(select * from p_variable where variable_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_variable where variable_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- SAVING (already in P_LOAN)
	if ((@entityID <> '-1') and (@entityType='entityType_Saving') and (@name is not null) and 
		exists(select * from p_loan where loan_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of loan
		delete from p_loan where loan_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- VARIABLE (already in P_SAVING)
	if ((@entityID <> '-1') and ((@entityType='entityType_Earning') or (@entityType='entityType_Deduction') or (@entityType='entityType_PackageComponent') or (@entityType='entityType_Interim')) and (@name is not null) and 
		exists(select * from p_saving where saving_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of variable
		delete from p_saving where saving_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;
	-- VARIABLE (already in P_LOAN)
	if ((@entityID <> '-1') and ((@entityType='entityType_Earning') or (@entityType='entityType_Deduction') or (@entityType='entityType_PackageComponent') or (@entityType='entityType_Interim')) and (@name is not null) and 
		exists(select * from p_loan where loan_id=@entityID and trim(name)=trim(@name))) then
		-- Get rid of saving
		delete from p_loan where loan_id=@entityID;
		-- Mark entity as new
		set @entityID = '-1';
	end if;

    -- Should we create this entity?
    if (@entityID='-1') then
        //
        // Create Loan
        //
        if (@entityType='entityType_Loan') then
            select  coalesce(max(loan_id)+1,1) into @newEntityID from p_loan;
            message('New Loan ID:'||@newEntityID);
            set @entityID = @newEntityID;
            insert into p_loan (loan_id, name, interest, COMPOUND_YN, BEFORECALC_YN) values (@entityID, @name, 0, 'Y','N');
        else
            //
            // Create Saving
            //
            if (@entityType='entityType_Saving') then
                select  coalesce(max(saving_id)+1,1) into @newEntityID from p_saving;
                message('New Saving ID:'||@newEntityID);
                set @entityID = @newEntityID;
                insert into p_saving (saving_id, name, interest, COMPOUND_YN, BEFORECALC_YN) values (@entityID, @name, 0, 'Y','N');
            else
                //
                // Create Variable
                //
                begin
                    select  coalesce(max(variable_id)+1,1) into @newEntityID from p_variable;
                    message('New Variable ID:'||@newEntityID);
                    set @entityID = @newEntityID;
                    insert into p_variable (variable_id, NAME, RESULT_TYPE, VARIABLE_TYPE, THEVALUE, ACTIVE_YN) 
                        values (@entityID, @name, 'EARNING', 'INPUT','0','Y');
                    -- Make sure all variables are available in all payrolls
                    insert into PL_PVARIABLE_ppayroll (payroll_id, variable_id) on existing SKIP 
                        select payroll_id, variable_id
                        from p_variable, p_payroll

                end;
            end if;
        end if;
    end if;

        
    
    -- Should we delete this entity?
    if (@doDelete='Y') then
        //
        // Delete Loan
        //
        if (@entityType='entityType_Loan') then
            delete from P_LOAN where loan_id=@entityID
        else
            //
            // Delete Saving
            //
            if (@entityType='entityType_Saving') then
                delete from P_SAVING where saving_id=@entityID
            else
                //
                // Delete Variable
                //
                delete from P_VARIABLE where variable_id=@entityID
            end if;
        end if;
    else
        begin
        //
        // Change LOANS
        //
        if (@entityType='entityType_Loan') then
            if (@name is not null) then
              update p_loan set name=@name  where loan_id=@entityID;
            end if;
            if (@caption is not null) then
              update p_loan set description=@caption  where loan_id=@entityID;
            end if;
            if (@calculationFormula is not null) then
              update p_loan set PREMIUM_FORMULA=@calculationFormula  where loan_id=@entityID;
            end if;
            if (@interest is not null) then
              update p_loan set INTEREST=convert(numeric(16,2),@interest)  where loan_id=@entityID;
            end if;
        else 
            //
            // Change SAVINGS
            //
            if (@entityType='entityType_Saving') then
                if (@name is not null) then
                  update p_saving set name=@name  where saving_id=@entityID;
                end if;
                if (@caption is not null) then
                  update p_saving set description=@caption  where saving_id=@entityID;
                end if;
                if (@calculationFormula is not null) then
                  update p_saving set PREMIUM_FORMULA=@calculationFormula  where saving_id=@entityID;
                end if;
                if (@interest is not null) then
                  update p_saving set INTEREST=convert(numeric(16,2),@interest) where saving_id=@entityID;
                end if;           
            else
                //
                // Change VARIABLES
                //
                begin
                    if (@name is not null) then
                      update p_variable set name=@name  where variable_id=@entityID;
                    end if;
                    if (@caption is not null) then
                      update p_variable set description=@caption  where variable_id=@entityID;
                    end if;
                    -- If this is a SYSTEM variable, don't touch it further!
                    if not exists(select * from p_variable where variable_id=@entityID and theOptions like '%SYSTEM%') then
                        -- Variable type-specific
                        if (@entityType is not null) then
                            // EARNING
                            if (@entityType='entityType_Earning') then
                                update p_variable set RESULT_TYPE='EARNING', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='YY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;     
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                            // DEDUCTION
                            if (@entityType='entityType_Deduction') then
                                update p_variable set RESULT_TYPE='DEDUCTION', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='YY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;         
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                            // INTERIM
                            if (@entityType='entityType_Interim') then
                                update p_variable set RESULT_TYPE='INTERIM', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='NN', SARS_CODE='' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;         
                                --update p_variable set SARS_CODE=null where variable_id=@entityID;
                            end if;
                            // PACKAGE_COMPONENT
                            if (@entityType='entityType_PackageComponent') then
                                update p_variable set RESULT_TYPE='EARNING', DISPLAYONPAYSLIP_YN='Y', COST_DETAIL='NY' where variable_id=@entityID;
                                if (@valueUnit is not null) then
                                  update p_variable set VALUE_UNIT=@valueUnit where variable_id=@entityID;
                                end if;   
                                if (@taxCode is not null) then
                                  update p_variable set SARS_CODE=@taxCode where variable_id=@entityID;
                                end if;
                            end if;
                        end if;
                        if (@showOnPayslip is not null) then
                          update p_variable set DISPLAYONPAYSLIP_YN=@showOnPayslip  where variable_id=@entityID;
                        end if;
                        if (@showYtdOnPayslip is not null) then
                          update p_variable set DISPLAYYTD_YN=@showYtdOnPayslip  where variable_id=@entityID;
                        end if;
                        if (@calculationFormula is not null) then
                          update p_variable set THEVALUE=@calculationFormula, variable_type='FORMULA'  where variable_id=@entityID;
                        end if;
                        if (@payrollType is not null) then
                          update p_variable set PAYROLL_TYPE=@payrollType  where variable_id=@entityID;
                        end if;
                    end if;
           

                   
                end;
            end if;
        end if ;
        end;
    end if;

    return(@entityID); 
END;

commit;