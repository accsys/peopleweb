---------------------------------------------------
-- WEB SERVICE TO RETRIEVE THE AVAILABLE CREDITS
---------------------------------------------------
if exists (select * from SYSWEBSERVICE where service_name = 'ws_HTTP_GetCredits') then
  drop service ws_HTTP_GetCredits
end if;
create service ws_HTTP_GetCredits
   TYPE 'JSON' AUTHORIZATION OFF USER DBA
   AS 
    select convert(char(10),fn_WEB_GetCreditsAvailable('Payroll')) as Payroll,
    '0' as TimeModule;

comment on service ws_HTTP_GetCredits is 'Returns the number of credits available';

commit;