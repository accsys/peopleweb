----------------------------------------------
-- FUNCTION TO UPDATE COST CENTRE INFORMATION
----------------------------------------------
CREATE or REPLACE FUNCTION "DBA"."fn_HTTP_SetCostCentreData" ( 
        IN @sessionID long VARCHAR, IN @companyID long varchar, IN @costID long varchar,IN @name long varchar default null,
        IN @description long varchar default null,IN @number long varchar default null,  IN @parentCostID long varchar default null,
        IN @doDelete char(1) default 'N')
returns varchar(10)
BEGIN
    declare @newCostID numeric(10);

    -- If this session doesn't exist, don't do anything
    if not exists(select * from WEB_SESSION where session_id=@sessionID) then
        return 'NO SESSION';
    end if;

    -- Touch this session
    call fn_HTTP_TouchSession(@sessionid);

    -- Should we create this cost centre?
    if (@costID='-1') then
        select  coalesce(max(cost_id)+1,1) into @newCostID from c_costmain where company_id=@companyID;
        message('New Cost ID:'||@newCostID);
        set @costID = @newCostID;
        insert into C_COSTMAIN (company_id, cost_id, name) values (@companyID, @costID, 'New');
    end if;

    -- Should we delete this cost centre?
    if (@doDelete='Y') then
        -- Don't cascade delete if the parent gets deleted
        update c_costmain set PARENT_COST_ID=null where company_id=@companyID and PARENT_COST_ID=@costID;
        -- Now delete the cost centre
        delete from c_costmain where  company_id=@companyID and cost_id=@costID
    else
        -- name
        if (@name is not null) then
            update c_costmain set name=@name  where company_id=@companyID and cost_id=@costID;
        end if;
        -- description
        if (@description is not null) then
            update c_costmain set DESCRIPTION=@description  where company_id=@companyID and cost_id=@costID;
        end if;
        -- number
        if (@number is not null) then
            update c_costmain set COST_NUMBER=@number where company_id=@companyID and cost_id=@costID;
        end if;
        -- parentCostID
        if (@parentCostID is not null) then
            update c_costmain set PARENT_COST_ID=@parentCostID where company_id=@companyID and cost_id=@costID;
        end if;
    end if;   

    commit;
    return('OK'); 
END;

commit;