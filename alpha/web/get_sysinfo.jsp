
<!--
    Discription:
        This routine is the Swiss-Army-Knife of the PeopleWeb Administrator.
    Parameters:
        pwd: Simple password (to be improved later)
        [daysSinceActivation=0/1/2/...]: Optional.  If [listAccountHolder=true], lists the accounts who qualify for this criterion
        [daysOfInactivity=0/1/2/...]: Optional.  If [listAccountHolder=true], lists the accounts who qualify for this criterion

-->


<%@page import="java.io.File"%>
<%@page import="za.co.accsys.web.helperclasses.SvrUserDatabase"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.http.impl.cookie.DateUtils"%>
<%@page import="za.co.accsys.web.helperclasses.SvrUserAccount"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>
            PeopleWeb System Information
        </title>
        <meta http-equiv="refresh" content="120">
    </head>
    <body>
        <%
            // Parameters
            String parPWD = request.getParameter("pwd");
            String parDaysSinceActivation = request.getParameter("daysSinceActivation");
            String parDaysOfInactivity = request.getParameter("daysOfInactivity");

            // DAYS SINCE ACTIVATION
            int iDaysSinceActivation = -1;
            if (parDaysSinceActivation != null) {
                try {
                    iDaysSinceActivation = Integer.parseInt(parDaysSinceActivation);
                } catch (NumberFormatException ne) {
                    iDaysSinceActivation = -1;
                }
            }

            // DAYS OF INACTIVITY
            int iDaysOfInactivity = -1;
            if (parDaysOfInactivity != null) {
                try {
                    iDaysOfInactivity = Integer.parseInt(parDaysOfInactivity);
                } catch (NumberFormatException ne) {
                    iDaysOfInactivity = -1;
                }
            }

            // Parse the parameters
            boolean doListAccountHolders = (iDaysOfInactivity >= 0 || iDaysSinceActivation >= 0);

            if (parPWD != null) {
                if (parPWD.equals("zapper123")) {

                    za.co.accsys.web.helperclasses.ServerPrefs prefs = za.co.accsys.web.helperclasses.ServerPrefs.getInstance();
                    prefs.reloadAccountPreferences();
                    ArrayList<SvrUserAccount> accounts = prefs.getAccounts();

                    if (doListAccountHolders) {
                        // 
                        // Detailed filter on DaysOfInactivity or DaysSinceActivation
                        //
                        for (SvrUserAccount account : accounts) {

                            // DaysOfInactivity
                            if (iDaysOfInactivity >= 0) {
                                if (account.getIdleDays() == iDaysOfInactivity) {
                                    out.write("*" + account.getAccountID() + "|" + account.getCompanyName() + "|" + account.getOwnerFirstName() + "|" + account.getOwnerSurname() + "|" + account.getOwnerEmail());
                                }
                            }
                            // DaysSinceActivation
                            if (iDaysSinceActivation >= 0) {
                                if ((za.co.accsys.web.helperclasses.DateUtils.getNumberOfDaysBetween(account.getCreationDate(), (new java.util.Date())) - 1) == iDaysSinceActivation) {
                                    out.write("*" + account.getAccountID() + "|" + account.getCompanyName() + "|" + account.getOwnerFirstName() + "|" + account.getOwnerSurname() + "|" + account.getOwnerEmail());
                                }
                            }

                        }
                    } else {
                        //
                        // Standard report, listing active databases, accounts, etc.
                        //

                        // Status flags
                        boolean noDBs = false;
                        boolean lowAvailableDBs = false;
                        boolean noAvailableDBs = false;
                        boolean unreachableDBs = false;
                        boolean lowDiskSpace = false;
                        ArrayList<SvrUserDatabase> databases = prefs.getClientDatabases();
                        int cntUnreachable = 0;

                        for (SvrUserDatabase database : databases) {
                            if (!database.canConnect()) {
                                cntUnreachable++;
                            }
                        }

                        // 
                        // DATABASES
                        //
                        out.write("<br>----------------------------------------------");
                        out.write("<br>Sybase Database Configuration");
                        out.write("<br>----------------------------------------------");
                        out.write("<br>Total number of databases:\t" + databases.size());
                        if (databases.size() == 0) {
                            noDBs = true;
                        }

                        out.write("<br>Databases already in use:\t" + prefs.getNumberOfUsedDatabases());
                        int nbAvailable = prefs.getNumberOfUnusedDatabases();
                        // Highlight low available DB
                        if (nbAvailable > 10) {
                            out.write("<br>Remaining databases:\t" + nbAvailable);
                        } else {
                            out.write("<br><font size=\"4\" color=red><b>Unused databases available:\t" + nbAvailable + "</b></font>");
                        }

                        // Any databases not reachable?
                        if (cntUnreachable > 0) {
                            out.write("<br><font size=\"5\" color=red><b>Unreachable databases:\t" + cntUnreachable + "</b></font>");
                        }

                        if (nbAvailable < 10 && nbAvailable > 0) {
                            lowAvailableDBs = true;
                        }
                        if (nbAvailable == 0) {
                            noAvailableDBs = true;
                        }

                        // 
                        // ACCOUNTS
                        //
                        out.write("<br><br>-----------------------------");
                        out.write("<br>Accounts");
                        out.write("<br>-----------------------------");
                        out.write("<br>Total number of accounts:\t" + accounts.size());
                        out.write("<br><Table cellpadding=5 border=1>");
                        out.write("<tr><th>Account<br>ID</th><th>Creation<br>Date</th><th>Company</th><th>Owner</th><th>E-mail</th><th>Phone</th><th>Last<br>Accessed</th><th>Idle Time<br>(days)</th><th>Available<br>Payroll<br>Credits</th><th>Projected<br/>Monthy<br>Usage</th><th>Database URL</th><tr>");
                        for (SvrUserAccount account : accounts) {
                            try {
                                out.write("<tr><td>" + account.getAccountID() + "</td>"
                                        + "<td>" + za.co.accsys.web.helperclasses.DateUtils.formatDate(account.getCreationDate()) + "</td>"
                                        + "<td>" + account.getCompanyName() + "</td>"
                                        + "<td>" + account.getOwnerFirstName() + " " + account.getOwnerSurname() + "</td>"
                                        + "<td>" + account.getOwnerEmail() + "</td>"
                                        + "<td>" + account.getOwnerPhone()+ "</td>"
                                        + "<td>" + za.co.accsys.web.helperclasses.DateUtils.formatDate(account.getLastAccessDate()) + "</td>"
                                        + "<td>" + account.getIdleDays() + "</td>"
                                        + "<td>" + account.getPayrollCredits() + "</td>"
                                        + "<td>" + account.getPayrollMonthlyCreditUsageProjection() + "</td>"
                                        + "<td>" + account.getDbURL()+ "</td></tr>"); 
                            } catch (Exception e) {
                                out.write("<tr><td>" + account.getAccountNumber() + "</td><td colspan=\"2\" >Error with account information</td><td colspan=\"7\" >" + e.getMessage() + "</td></tr>");
                            }
                        }
                        out.write("</Table>");

                        if (cntUnreachable > 0) {
                            out.write("<br><br>Unreachable DBs:");
                            out.write("<br><Table cellpadding=5>");
                            out.write("<tr><th>ID</th><th>Database URL</th><th>Account ID</th><th>Uptime (hours)</th><th>PW Version</th></tr>");

                            for (SvrUserDatabase database : databases) {

                                String dbAccount;
                                if (database.isInUse()) {
                                    dbAccount = database.getAccountID();
                                } else {
                                    dbAccount = "";
                                };
                                if (!database.canConnect()) {
                                    cntUnreachable++;
                                    // This database is linked to an account
                                    out.write("<tr><td>" + database.getDbID() + "</td>"
                                            + "<td>" + database.getDbURL() + "</td>"
                                            + "<td>" + dbAccount + "</td>"
                                            + "<td><font size=\"3\" color=\"red\">UNABLE TO CONNECT</font></td>"
                                            + "<td>" + database.getPwVersion() + "</td>"
                                            + "<td></td>" + "</tr>");
                                }
                            }
                            out.write("</Table>");
                        }
                        out.write("<br><br>Full DB List:");
                        out.write("<br><Table cellpadding=5>");
                        out.write("<tr><th>ID</th><th>Database URL</th><th>Account ID</th><th>Uptime (hours)</th><th>PW Version</th></tr>");
                        String previousVersion = null;

                        for (SvrUserDatabase database : databases) {

                            String dbAccount;
                            if (database.isInUse()) {
                                dbAccount = database.getAccountID();
                            } else {
                                dbAccount = "";
                            };
                            if (!database.canConnect()) {
                                cntUnreachable++;
                                // This database is linked to an account
                                out.write("<tr><td>" + database.getDbID() + "</td>"
                                        + "<td>" + database.getDbURL() + "</td>"
                                        + "<td>" + dbAccount + "</td>"
                                        + "<td><font size=\"3\" color=\"red\">UNABLE TO CONNECT</font></td>"
                                        + "<td>" + database.getPwVersion() + "</td>"
                                        + "<td></td>" + "</tr>");

                            } else {
                                // Highlight differences in software version
                                if (previousVersion == null) {
                                    previousVersion = database.getPwVersion();
                                };
                                String dbVersionHTML = database.getPwVersion();
                                if (!previousVersion.equalsIgnoreCase(database.getPwVersion())) {
                                    dbVersionHTML = "<font size=\"3\" color=\"red\">" + dbVersionHTML + "</font>";
                                }
                                previousVersion = database.getPwVersion();

                                out.write("<tr><td>" + database.getDbID() + "</td>"
                                        + "<td>" + database.getDbURL() + "</td>"
                                        + "<td>" + dbAccount + "</td>"
                                        + "<td>" + database.getHrsRunning() + "</td>"
                                        + "<td>" + dbVersionHTML + "</td>"
                                        + "<td>" + "</td>" + "</tr>");
                            }

                        }
                        out.write("</Table>");

                        if (cntUnreachable > 0) {
                            unreachableDBs = true;
                        }

                        out.write("<br><br>-----------------------------");
                        out.write("<br>Disk space");
                        out.write("<br>-----------------------------");
                        out.write("<br>Disk total space:" + new File("/").getTotalSpace() / 1024 / 1024 / 1024 + " Gb");
                        out.write("<br>Disk free space:" + new File("/").getFreeSpace() / 1024 / 1024 / 1024 + " Gb");

                        // Low disk space (less than 30 Gb)
                        if (new File("/").getFreeSpace() / 1024 / 1024 / 1024 < 30) {
                            lowDiskSpace = true;
                        }
                        // Now let's output some status messages for monitoring purposes
                        if (noDBs) {
                            out.write("<br>** NO DATABASES **");
                        }
                        if (lowAvailableDBs) {
                            out.write("<br>** AVAILABLE DATABASES RUNNING LOW **");
                        }
                        if (noAvailableDBs) {
                            out.write("<br>** NO FREE DATABASES **");
                        }
                        if (unreachableDBs) {
                            out.write("<br>** DATABASES UNREACHABLE **");
                        }
                        if (lowDiskSpace) {
                            out.write("<br>** LOW DISK SPACE **");
                        }
                    }
                }
            }
        %>
    </body>
</html>
