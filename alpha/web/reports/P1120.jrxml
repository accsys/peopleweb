<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="P1120" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="555" leftMargin="28" rightMargin="12" topMargin="20" bottomMargin="20" whenResourceMissingType="Empty" uuid="5ba7f0ca-c2f7-4ce5-8ae8-4805026c7b94">
	<property name="ireport.zoom" value="1.650000000000002"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="REPORT_LOGO" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["../images/CompanyLogo.png"]]></defaultValueExpression>
	</parameter>
	<parameter name="COMPANY_ID" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["1000"]]></defaultValueExpression>
	</parameter>
	<parameter name="PAYROLL_ID" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["2"]]></defaultValueExpression>
	</parameter>
	<parameter name="PERIOD_START_DATE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["2014/04/05"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT "E_MASTER"."SURNAME",
"E_MASTER"."COMPANY_EMPLOYEE_NUMBER",
"C_MASTER"."NAME" as COMPANY_NAME,
"P_PAYROLLDETAIL"."STARTDATE",
"P_PAYROLLDETAIL"."ENDDATE",
"P_PAYROLL"."NAME" AS PAYROLL_NAME,
"E_MASTER"."FIRSTNAME",
"E_MASTER"."EMPLOYEE_ID",
"vw_P_NettPayVariance"."NettPay_Current",
"vw_P_NettPayVariance"."NettPay_Previous",
("vw_P_NettPayVariance"."NettPay_Current" - "vw_P_NettPayVariance"."NettPay_Previous") as DIFFERANCE,
"vw_P_NettPayVariance"."RUNTYPE_ID",
"E_MASTER"."COMPANY_ID",
"P_PAYROLL"."PAYROLL_ID",
"P_PAYROLLDETAIL"."PAYROLLDETAIL_ID"
FROM (("DBA"."P_E_MASTER" "P_E_MASTER"
INNER JOIN ("DBA"."C_MASTER" "C_MASTER"
INNER JOIN "DBA"."E_MASTER" "E_MASTER"
ON "C_MASTER"."COMPANY_ID"="E_MASTER"."COMPANY_ID")
ON ("P_E_MASTER"."COMPANY_ID"="E_MASTER"."COMPANY_ID"
AND "P_E_MASTER"."EMPLOYEE_ID"="E_MASTER"."EMPLOYEE_ID")
INNER JOIN "DBA"."vw_P_NettPayVariance" "vw_P_NettPayVariance"
ON ("E_MASTER"."COMPANY_ID"="vw_P_NettPayVariance"."COMPANY_ID")
AND ("E_MASTER"."EMPLOYEE_ID"="vw_P_NettPayVariance"."EMPLOYEE_ID"))
INNER JOIN "DBA"."P_PAYROLLDETAIL" "P_PAYROLLDETAIL"
ON ("vw_P_NettPayVariance"."PAYROLL_ID"="P_PAYROLLDETAIL"."PAYROLL_ID")
AND ("vw_P_NettPayVariance"."PAYROLLDETAIL_ID"="P_PAYROLLDETAIL"."PAYROLLDETAIL_ID"))
INNER JOIN "DBA"."P_PAYROLL" "P_PAYROLL"
ON "P_PAYROLLDETAIL"."PAYROLL_ID"="P_PAYROLL"."PAYROLL_ID"
WHERE  "P_PAYROLL"."PAYROLL_ID"=$P{PAYROLL_ID}
AND "E_MASTER"."COMPANY_ID"=$P{COMPANY_ID}
AND "P_PAYROLLDETAIL"."STARTDATE"=date($P{PERIOD_START_DATE})
AND ("vw_P_NettPayVariance"."NettPay_Current"<>0
OR "vw_P_NettPayVariance"."NettPay_Previous"<>0)
ORDER BY "C_MASTER"."NAME", "P_PAYROLL"."NAME","E_MASTER"."COMPANY_EMPLOYEE_NUMBER"]]>
	</queryString>
	<field name="SURNAME" class="java.lang.String"/>
	<field name="COMPANY_EMPLOYEE_NUMBER" class="java.lang.String"/>
	<field name="COMPANY_NAME" class="java.lang.String"/>
	<field name="STARTDATE" class="java.sql.Date"/>
	<field name="ENDDATE" class="java.sql.Date"/>
	<field name="PAYROLL_NAME" class="java.lang.String"/>
	<field name="FIRSTNAME" class="java.lang.String"/>
	<field name="EMPLOYEE_ID" class="java.math.BigDecimal"/>
	<field name="NettPay_Current" class="java.math.BigDecimal"/>
	<field name="NettPay_Previous" class="java.math.BigDecimal"/>
	<field name="DIFFERANCE" class="java.math.BigDecimal"/>
	<field name="RUNTYPE_ID" class="java.math.BigDecimal"/>
	<field name="COMPANY_ID" class="java.math.BigDecimal"/>
	<field name="PAYROLL_ID" class="java.math.BigDecimal"/>
	<field name="PAYROLLDETAIL_ID" class="java.math.BigDecimal"/>
	<variable name="NoOfEmps" class="java.lang.Integer" resetType="Group" resetGroup="Payroll" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{EMPLOYEE_ID}]]></variableExpression>
	</variable>
	<variable name="SumOfPrevious" class="java.lang.Number" resetType="Group" resetGroup="Payroll" calculation="Sum">
		<variableExpression><![CDATA[$F{NettPay_Previous}]]></variableExpression>
	</variable>
	<variable name="SumOfCurrent" class="java.lang.Number" resetType="Group" resetGroup="Payroll" calculation="Sum">
		<variableExpression><![CDATA[$F{NettPay_Current}]]></variableExpression>
	</variable>
	<variable name="SumOfDiff" class="java.lang.Number" resetType="Group" resetGroup="Payroll" calculation="Sum">
		<variableExpression><![CDATA[$F{DIFFERANCE}]]></variableExpression>
	</variable>
	<group name="Company">
		<groupExpression><![CDATA[$F{COMPANY_ID}]]></groupExpression>
		<groupHeader>
			<band/>
		</groupHeader>
	</group>
	<group name="Payroll" keepTogether="true">
		<groupExpression><![CDATA[$F{PAYROLL_ID}]]></groupExpression>
		<groupHeader>
			<band height="25">
				<textField isBlankWhenNull="false">
					<reportElement x="0" y="0" width="555" height="25" forecolor="#333333" uuid="f1b405cd-0246-4e42-8a42-0e93553ab8ec"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="14" isBold="false"/>
						<paragraph leftIndent="1"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{PAYROLL_NAME} == null? $R{emp201.payroll} + ": " : $R{emp201.payroll} + ": " + $F{PAYROLL_NAME}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="14">
				<textField>
					<reportElement mode="Opaque" x="0" y="0" width="222" height="14" backcolor="#E6E6E6" uuid="b88284d0-f015-4b8e-99e2-5823aaf0268c"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true" isUnderline="false"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$R{emp201.totalPayroll} + ": " + $V{NoOfEmps}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Opaque" x="222" y="0" width="111" height="14" backcolor="#E6E6E6" uuid="f74d44d4-8d83-4299-a2ec-5c5dda489b62"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true" isUnderline="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{SumOfPrevious}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Opaque" x="444" y="0" width="111" height="14" backcolor="#E6E6E6" uuid="3ec41e16-8dd3-4e26-8c18-8b9817822a12"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true" isUnderline="false"/>
						<paragraph rightIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{SumOfDiff}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Opaque" x="333" y="0" width="111" height="14" backcolor="#E6E6E6" uuid="0d65f7bc-dcfb-454a-a746-19a641b4320e"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true" isUnderline="false"/>
						<paragraph rightIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{SumOfCurrent}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="Employee" keepTogether="true">
		<groupExpression><![CDATA[$F{COMPANY_EMPLOYEE_NUMBER}]]></groupExpression>
		<groupHeader>
			<band height="14">
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="0" width="111" height="14" uuid="3214d896-cd88-4b24-86a7-9bf9dcfea0fa"/>
					<textElement verticalAlignment="Bottom">
						<font size="8" isBold="false"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{COMPANY_EMPLOYEE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="111" y="0" width="111" height="14" uuid="9f13efb4-a462-4c4b-80b2-74096f4d0f6a"/>
					<textElement verticalAlignment="Bottom">
						<font size="8" isBold="false"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{SURNAME} + ", " + $F{FIRSTNAME}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement x="222" y="0" width="111" height="14" uuid="efc40aa5-0bc7-433a-9b65-aa16bf838575"/>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font size="8" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{NettPay_Previous}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement x="444" y="0" width="111" height="14" uuid="ec003633-46af-4888-9719-3e2de2557760"/>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font size="8" isBold="false"/>
						<paragraph rightIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{DIFFERANCE}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement x="333" y="0" width="111" height="14" uuid="2a9be51e-97be-4bdc-a4cc-3f9a1f353088"/>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font size="8" isBold="false"/>
						<paragraph rightIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{NettPay_Current}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="72">
			<frame>
				<reportElement mode="Opaque" x="-20" y="-20" width="400" height="92" backcolor="#FFFFFF" uuid="c9b6bbf3-1eec-42a9-b078-c37b60acba0a"/>
				<textField>
					<reportElement x="20" y="58" width="380" height="20" forecolor="#000000" uuid="6cad4336-c06d-4fce-8c21-99eb161d4e58"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="14"/>
					</textElement>
					<textFieldExpression><![CDATA[$R{payrollVariance.period} + ": " + $F{STARTDATE}.toString() + " " + $R{payslip.to} + " " + $F{ENDDATE}.toString()]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="20" y="20" width="380" height="38" uuid="27457523-47c1-41bd-94c0-61bdcfecd304"/>
					<textElement>
						<font size="28"/>
					</textElement>
					<textFieldExpression><![CDATA[$R{payrollVariance.nettPayVariancelist}]]></textFieldExpression>
				</textField>
			</frame>
			<image scaleImage="RetainShape" hAlign="Center" vAlign="Middle" onErrorType="Blank">
				<reportElement x="380" y="-7" width="164" height="75" uuid="a40d6541-ad09-4393-b30a-33b7629d117e"/>
				<imageExpression><![CDATA[$P{REPORT_LOGO}]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band/>
	</pageHeader>
	<columnHeader>
		<band height="40">
			<textField>
				<reportElement mode="Opaque" x="0" y="0" width="111" height="40" forecolor="#404040" backcolor="#E6E6E6" uuid="020f128f-4295-4a48-a4df-89c384ebef12"/>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{payrollVariance.employeeNumber}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="444" y="0" width="111" height="40" forecolor="#404040" backcolor="#E6E6E6" uuid="d70d32d3-b7ec-4ce6-83b4-cafd2a393a6f"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{payrollVariance.difference}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="222" y="0" width="111" height="40" forecolor="#404040" backcolor="#E6E6E6" uuid="f92d56de-e867-44ce-bde4-e485913d9ba9"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{payrollVariance.previousPeriod}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="333" y="0" width="111" height="40" forecolor="#404040" backcolor="#E6E6E6" uuid="309eaa43-5ae1-4219-8816-a6f46af35ae7"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{payrollVariance.currentPeriod}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="111" y="0" width="111" height="40" forecolor="#404040" backcolor="#E6E6E6" uuid="6a0429dd-92e8-4265-9916-1dfebbe78e0b"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<paragraph rightIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{payrollVariance.emplyeeName}]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band/>
	</detail>
	<pageFooter>
		<band height="15">
			<textField pattern="yyyy/MM/dd h:mm a">
				<reportElement mode="Opaque" x="394" y="1" width="161" height="13" backcolor="#E6E6E6" uuid="d93abed3-766a-4bec-804f-bd89e03bb5c6"/>
				<textElement textAlignment="Right">
					<font size="9"/>
					<paragraph rightIndent="1"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="128" y="1" width="160" height="13" backcolor="#E6E6E6" uuid="0f7c36ee-0c6d-4339-812b-d3320972ee92"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{payslip.page} + " " + $V{PAGE_NUMBER} + " " + $R{payslip.of}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="0" y="1" width="128" height="13" backcolor="#E6E6E6" uuid="86f94e73-f2ae-4e36-bafc-0d4e189700f7"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Accsys PeopleWeb P1120"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement mode="Opaque" x="288" y="1" width="118" height="13" backcolor="#E6E6E6" uuid="b7e8f272-f0eb-4f1c-947d-3c75a9ce985c"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band/>
	</summary>
</jasperReport>
